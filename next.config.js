const ESLintPlugin = require("eslint-webpack-plugin");
const withPlugins = require("next-compose-plugins");
const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});

//  "http://150.95.110.110:2443"
const APOLLO_API_ENDPOINT =
  process.env.NODE_ENV === "production"
    ? "https://hanagold.unibiz.io"
    : process.env.NEXT_PUBLIC_APOLLO_API_ENDPOINT;

/**
 * @type {import('next/dist/next-server/server/config').NextConfig}
 **/
const nextConfig = {
  reactStrictmode: true,
  poweredByHeader: false,
  webpack: (config) => {
    // eslint-webpack-plugins
    config.plugins.push(
      new ESLintPlugin({
        extensions: ["tsx", "jsx", "ts"],
        failOnError: true,
      })
    );

    // graphql-gql loader
    config.module.rules = [
      {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: "graphql-tag/loader",
      },
      ...config.module.rules,
    ];
    return config;
  },

  // proxy for graphql API ENDPOINT due to cors
  async rewrites() {
    return [
      {
        source: "/graphql/:path*",
        destination: APOLLO_API_ENDPOINT + "/graphql/:path*",
      },
    ];
  },
  images: {
    domains: ["res.cloudinary.com", "hanagold.unibiz.io"],
  },
};

module.exports = withPlugins([withBundleAnalyzer], nextConfig);
