/* eslint-disable */
import { REQUEST_URL } from "@Constants";
import { PaginationQueryModel } from "@Models";
import { RequestUtil } from "@Utils";

export const ProductService = {
  filterProducts,
  getProductBySlug,
  getAllCategories,
  getAllOrderProducts,
};

/**
 * dùng trong trang /order
 * @returns OrderProductOutputModel[]
 */
function getAllOrderProducts() {
  const url = REQUEST_URL.GET_ALL_ORDER_PRODUCT;

  return RequestUtil.get({ url });
}

function filterProducts(
  query: PaginationQueryModel & {
    searchValue?: string;
    category?: string;
  } = {}
) {
  const url = REQUEST_URL.FILTER_PRODUCT;
  const queryData: any = { ...query };
  if (query.searchValue && query.searchValue.trim()) {
    queryData.searchValue = query.searchValue
      .trim()
      .split(" ")
      .map((str) => str.trim());
  }

  return RequestUtil.get({ url, query: queryData });
}

function getProductBySlug(slug: string) {
  const url = REQUEST_URL.GET_PRODUCT_BY_SLUG.replace("{SLUG}", slug);

  return RequestUtil.get({ url });
}

function getAllCategories() {
  const url = REQUEST_URL.GET_ALL_CATEGORIES;

  return RequestUtil.get({ url });
}
