/* eslint-disable */
import { REQUEST_URL } from "@Constants";
import { RequestUtil } from "@Utils";

export const AuthService = {
  signin,
  signup,
  forgetPassword,
  loadMe,
  changePassword,
};

function signin(data: { email: string; password: string }) {
  const url = REQUEST_URL.SIGN_IN;

  return RequestUtil.post({ url, data });
}

function signup(data: { email: string; password: string; fullName: string; phoneNumber: string }) {
  const url = REQUEST_URL.SIGN_UP;

  return RequestUtil.post({ url, data });
}

function forgetPassword(email: string) {
  const url = REQUEST_URL.FORGET_PASSWORD.replace("{EMAIL}", email);

  return RequestUtil.put({ url, data: {} });
}

function loadMe() {
  const url = REQUEST_URL.LOAD_ME;

  return RequestUtil.get({ url });
}

function changePassword(data: { currentPassword: string; newPassword: string }) {
  const url = REQUEST_URL.CHANGE_PASSWORD;

  return RequestUtil.put({ url, data });
}
