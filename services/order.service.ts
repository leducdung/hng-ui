/* eslint-disable */
import { ProductSugarOpt, REQUEST_URL } from "@Constants";
import { OrderPaymentType, OrderSource } from "@Models";
import { RequestUtil } from "@Utils";

export const OrderService = {
  order,
  getAllOrders,
  getOrderDetailByOrderCode,
};

function order(data: {
  customerId: string;
  shippingId: string;
  shippingAmount: number;
  shippingTime: null;
  paymentType: OrderPaymentType;
  source: OrderSource;
  note: string;
  orderQuantity: number;
  totalDiscount: number;
  orderAmount: number;
  orderDetails: {
    /**
     * id của variant
     */
    id: string;
    sugar: ProductSugarOpt | null;
    discount: number;
    quantity: number;
    unitPrice: number;
    totalPrice: number;
    toppings: {
      /**
       * id của topping
       */
      id: string;
      price: number;
      quantity: number;
    }[];
  }[];
}) {
  const url = REQUEST_URL.ORDER;

  return RequestUtil.post({ url, data });
}

function getAllOrders() {
  const url = REQUEST_URL.GET_ALL_ORDER;

  return RequestUtil.get({ url });
}

function getOrderDetailByOrderCode(orderCode: string) {
  const url = REQUEST_URL.GET_ORDER_DETAIL_BY_CODE.replace("{ORDER_CODE}", orderCode);

  return RequestUtil.get({ url });
}
