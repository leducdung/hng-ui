/* eslint-disable */
import { REQUEST_URL } from "@Constants";
import { MOCK_NEWS } from "../mock";
import { PaginationQueryModel } from "@Models";
import { RequestUtil } from "@Utils";

export const NewsService = {
  getNews,
  getNewsBySlug,
  getCategories,
  getServices,
  getServiceBySlug,
  getProjects,
  getProjectBySlug,
};

function getNews(
  query: PaginationQueryModel & {
    categoryBlogs?: string[];
    searchValue?: string;
    parentId?: string;
  } = {}
) {
  // const url = REQUEST_URL.FILTER_BLOG;

  // return RequestUtil.get({ url, query });
  return Promise.resolve({
    status: 200,
    data: {
      data: MOCK_NEWS,
    },
  });
}

function getNewsBySlug(slug: string) {
  const url = REQUEST_URL.GET_BLOG_BY_SLUG.replace("{SLUG}", slug);

  return RequestUtil.get({ url });
}

function getCategories(query: PaginationQueryModel = {}) {
  const url = REQUEST_URL.GET_BLOG_CATEGORIES;

  return RequestUtil.get({ url, query });
}

function getServices(query: PaginationQueryModel & { searchValue?: string } = {}) {
  const url = REQUEST_URL.FILTER_SERVICE;

  return RequestUtil.get({ url, query });
}

function getServiceBySlug(slug: string) {
  const url = REQUEST_URL.GET_SERVICE_BY_SLUG.replace("{SLUG}", slug);

  return RequestUtil.get({ url });
}

function getProjects(query: PaginationQueryModel & { searchValue?: string } = {}) {
  const url = REQUEST_URL.FILTER_PROJECT;

  return RequestUtil.get({ url, query });
}

function getProjectBySlug(slug: string) {
  const url = REQUEST_URL.GET_PROJECT_BY_SLUG.replace("{SLUG}", slug);

  return RequestUtil.get({ url });
}
