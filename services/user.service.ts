/* eslint-disable */
import { REQUEST_URL } from "@Constants";
import { ShippingAddressOutputModel, UserGender } from "@Models";
import { RequestUtil } from "@Utils";

export const UserService = {
  getAllShippingAddresses,
  createShippingAddress,
  updateShippingAddress,
  deleteShippingAddress,
  updateProfile,
  changeAvatar,
};

function getAllShippingAddresses() {
  const url = REQUEST_URL.GET_ALL_SHIPPING_ADDRESS;

  return RequestUtil.get({ url });
}

function createShippingAddress(data: Omit<ShippingAddressOutputModel, "id">) {
  const url = REQUEST_URL.SHIPPING_ADDRESS;

  return RequestUtil.post({ url, data });
}

function updateShippingAddress(id: string, data: Omit<ShippingAddressOutputModel, "id">) {
  const url = REQUEST_URL.SHIPPING_ADDRESS_DETAIL.replace("{ID}", id);

  return RequestUtil.put({ url, data });
}

function deleteShippingAddress(id: string) {
  const url = REQUEST_URL.SHIPPING_ADDRESS_DETAIL.replace("{ID}", id);

  return RequestUtil.delete({ url });
}

function updateProfile(data: {
  phoneNumber: string;
  gender: UserGender;
  birthDay: string;
  fullName: string;
}) {
  const url = REQUEST_URL.UPDATE_PROFILE;

  return RequestUtil.put({ url, data });
}

function changeAvatar(avatarFile: any) {
  const url = REQUEST_URL.UPDATE_AVATAR;
  const formData = new FormData();
  formData.append("avatar", avatarFile);

  return RequestUtil.put({
    url,
    data: formData,
  });
}
