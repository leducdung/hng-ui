# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org).

# unreleased

## Hang

- fix component advisory

# v1.0.0 - 15/09/2021

## Hang

- ui specifications admin
- update style order listing admin

## Thai

- init required icons + components for products admin page
- add products admin page
- fix products detail

# v1.0.0 - 04/09/2021

## Hang

- gold coins page + responsive
- gold price hng admin ui + api
- ui news + newdetail web + responsive
- ui location + api listing
- update admin page
- ui customer + detail + api listing

# v1.0.0 - 19/07/2021

## Hang

- fix ui + change banner + add links

## Khoa

- update product price
- update gold price
- update specification
- update admin authentication

## Hang

- update banner page products

## Hang

- Refactor img => Image

# v0.6.0 - 19/07/2021

## Khoa

- update routing for product and category
- add more graphql Query

# v0.5.1 - 09/07/2021

## Hang

- layout size guide page, update statistics page + banner guide, fix bug

# v0.5.0 - 09/07/2021

## Thai

- Update Cartpage

## Hang

- layout shopping, payment, policy guide web
- layout warranty web
- update layout web
- chagne scss => scss module
- layout userinfo, jewelry guide web, add link footer

## Khoa

- use next-seo package for SEO

# v0.4.1 - 02/07/2021

## Khoa

- update signin page
- adopting cloudinary CDN for images
- update loading fonts

## Hang

- layout agency page app
- update images, content ui home, agency, service page web
- fix bug layout
- update header page agency
- layout prices app

# v0.3.0 - 18/06/2021

## Khoa

- Add AppLayout component
- Add AppHeader component
- Add AppSidebar component
- Update routing logic in App pages
- add ComingSoon page for WIP App pages
- add login/logout logic, validate token server-side
- update dropdown style signup page
- custom validate phoneNumber, validate after submit
- toggle background in app homepage

## Hang

- code layout homepage app
- layout newspage app
- layout download app mobile
- layout newdetail app
- layout private
- layout settings

# v0.2.0 - 14/06/2021

## Khoa

- add custom document page
- add analyze build script
- Update store config
- update entry layout

## Hang

- fix ui
- update header

# v0.1.0 - 09/06/2021

- Config webpack, babel, eslint, prettier
- Add pre-commit hook with Husky
