declare module "*.graphql" {
  import { DocumentNode } from "graphql";
  const Schema: DocumentNode & {
    [key?: string]: DocumentNode;
  };
  export default Schema;
}
