export type PriceOutputModel = {
  id: number;
  buyPrice: number;
  sellPrice: number;
  createdAt: string;
  updatedAt: string;
  status: string;
};
