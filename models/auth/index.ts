export enum AuthTabPosition {
  SIGN_IN,
  SIGN_UP,
  FORGET,
}
