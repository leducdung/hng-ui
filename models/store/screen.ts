export type THtmlHeader = {
  pageTitle: string;
  metaDescription: string;
  canonicalLink: string;
};

export type ScreenState = {
  htmlHeader: THtmlHeader;
};
