import { UserGender } from "./../user/index";
import { AppNotiModel, ProductCartOutputModel } from "..";
import { RootReducer } from "../../redux/modules/root";

export type RootState = ReturnType<typeof RootReducer>;

// eslint-disable-next-line
export declare namespace AppState {
  export type Authentication = {
    loading: boolean;
    data: {
      token: string;
      refreshToken: string;
    } | null;
  };

  export type Me = {
    loading: boolean;
    data: {
      id: string;
      email: string;
      fullName: string;
      phoneNumber: string;
      code: string | null;
      // googleId: null;
      // facebookId: null;
      // shippingDefaultId: null;
      gender: UserGender | null;
      birthDay: string | null;
      avatar: string | null;
      address: null;
      // acceptEmailMkt: false;
      isActive: boolean;
    } | null;
  };

  export type ProductCartState = {
    data: ProductCartOutputModel[];
    quantity: number; // tổng số lượng
    isVAT: boolean;
    provisionalTotal: number;
    totalDiscount: number;
    total: number; // tổng tiền
    loading: boolean;
  };

  export type AppNotifications = {
    list: AppNotiModel[];
  };
}
