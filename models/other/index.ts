/* eslint-disable */
export type TCb = {
  success?: (data?: any) => void;
  fail?: (data?: any) => void;
  error?: (data?: any) => void;
};

export type IconModel = {
  color: string;
  size: number[]; // mang kich thuoc x,y (width, height)
  viewBox: number[]; // mang kich thuoc x,y (width, height)
  className?: string;
  textColor?: string;
  style?: React.CSSProperties;
  options?: {
    color: string;
    backgroundColor: string;
  };
  onClick?: () => void;
};

export type PaginationQueryModel = {
  page?: number;
  limit?: number;
};

export type FetchingPaginationDataModel<T> = {
  page: number;
  totalPages: number;
  limit: number;
  totalRecords: number;
  data: T[];
  loading: boolean;
};

export type FetchingDataModel<T> = {
  data: T;
  loading: boolean;
};

export type TBreadcrumb = {
  path?: string;
  title: string;
};

export type ActionState = {
  type: string;
  payload: {
    data: any;
    complete?: () => void;
    error?: (err: any) => void;
  };
};

export enum AppNotifications {
  SUCCESS = "success",
  ERROR = "error",
  WARNING = "warning",
  INFO = "info",
}

export type NotiModel = {
  type: AppNotifications;
  message: string;
};

export type AppNotiModel = {
  id: string;
} & NotiModel;

export enum AppLang {
  VN = "vn",
  EN = "en",
  AS = "as",
}
