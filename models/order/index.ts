import { ShippingAddressOutputModel } from "..";
import { ProductSugarOpt } from "@Constants";
import { ProductToppingOutputModel, ProductVariantOutputModel } from "../product";

export enum OrderSource {
  WEBSITE = "website",
}

export enum OrderPaymentType {
  COD = "COD",
}

export enum OrderStatus {
  CREATED = "CREATED",
  CONFIRMED = "CONFIRMED",
  DELIVERING = "DELIVERING",
  DELIVERED = "DELIVERED",
  COMPLETED = "COMPLETED",
  CANCELLED = "CANCELLED",
}

export enum PaymentStatus {
  CREATED,
  PROGRESSING,
  COMPLETED,
}

type Customer = {
  fullName: string;
};

export type OrderOutputModel = {
  id: string;
  code: string;
  customerId: string;
  orderFrom: string;
  shippingAmount: number;
  status: OrderStatus;
  note: string | null;
  totalDiscount: number;
  paymentType: OrderPaymentType;
  paymentStatus: PaymentStatus;
  customer: Customer;
  isPaid: true;
  //   shippingTime: null;
  cancelReason: string | null;
  createdAt: string;
  // eslint-disable-next-line
  details: any[];
};

export type OrderDetailOutputModel = {
  id: string;
  orderCode: string;
  customerId: string;
  source: OrderSource;
  shippingId: string;
  orderAmount: number;
  orderQuantity: number;
  shippingAmount: number;
  status: OrderStatus;
  note: string | null;
  totalDiscount: number;
  paymentType: OrderPaymentType;
  isPaid: boolean;
  // shippingTime: null;
  cancelReason: string | null;
  // employeeId: null;
  createdAt: string;
  deletedAt: string | null;
  shipping: ShippingAddressOutputModel;
  orderDetail: {
    id: string;
    productVariantId: string;
    quantity: number;
    unitPrice: number;
    // orderId: string;
    discount: number;
    sugar: ProductSugarOpt | null;
    deletedAt: string | null;
    orderDetailTopping: {
      id: string;
      // orderDetailId: string;
      toppingId: string;
      quantity: number;
      unitPrice: number;
      discount: number;
      deletedAt: string | null;
      topping: ProductToppingOutputModel;
    }[];
    productVariant: ProductVariantOutputModel;
  }[];
};
