import { ImageOutputModel } from "..";
export * from "./category";
export * from "./cart";

export type ProductOutputModel = {
  id: number;
  price: number;
  status: boolean;
  name: string;
  slug: string;
  code: string;
  description: string;
  sellOutOfStock?: boolean;
  inStock?: number;
  images?: ImageOutputModel | null;
};

export type ProductDetailOutputModel = {
  id: number;
  price: number;
  status: boolean;
  name: string;
  slug: string;
  code: string;
  description: string;
  specificationId?: number;
  specification?: Specification;
  categories?: ProductCategory2[] | null;
  images: OutputImageModel[];
};

export type ProductCategory2 = {
  // Trả ra từ getProductBySlug
  category: {
    id: number;
    name: string;
    slug: string;
    status: boolean;
    __typename: string;
  };
};

export type OutputImageModel = {
  path: string;
  __typename: string;
};

export type Specification = {
  id: number;
  name: string;
  status: boolean;
  specificationGroups: {
    id: number;
    name: string;
    specificationId: number;
    status: boolean;
    specificationGroupValues: {
      id: number;
      name: string;
      value: string;
      sortOrder: number;
      status: boolean;
      specificationGroupId: number;
    }[];
  }[];
};

export type ProductCategory = {
  // Trong doc của graphql của api getProductBySlug
  id: number;
  productId: number;
  categoryId: number;
  category: Category;
  product: ProductOutputModel;
};

export type Category = {
  id: number;
  code: string;
  slug: string;
  name: string;
  sortOrder: number;
  image: string;
  status: boolean;
  categoryChilds: [CategoryChildParent];
  categoryParents: [CategoryChildParent];
  categoryProducts: ProductCategory;
  _count: {
    categoryProducts: number;
  };
};

export type CategoryChildParent = {
  id: number;
  childId: number;
  categoryId: number;
  childCategory: Category;
  parentCategory: Category;
};

export type ProductVariantOutputModel = {
  id: string;
  productId: string;
  itemCode: string;
  name: string;
  price: number;
  volume: number;
  flavor: string;
  inStock: number;
  avatar: string | null;
  alt: string | null;
};

export type ProductToppingOutputModel = {
  id: string;
  // categoryId: string;
  name: string;
  price: number;
  unit: string | null;
  trackQuantity: boolean;
  inStock: number;
  picture: string | null;
};
