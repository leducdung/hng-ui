import { ProductToppingOutputModel, ProductVariantOutputModel } from ".";
import { ProductSugarOpt } from "@Constants";
import { ImageOutputModel } from "../image";

export type ProductCartOutputModel = {
  id: string;
  productVariantId: string;
  quantity: number;
  sugar: ProductSugarOpt | null;
  productVariant: ProductVariantOutputModel;
  toppings: {
    // id: string;
    // cartId: string;
    toppingId: string;
    quantity: number;
    // deletedAt: null;
    topping: ProductToppingOutputModel;
  }[];
};

export type OrderProductOutputModel = {
  id: string;
  itemCode: string;
  name: string;
  slugs: string[];
  sellOutOfStock: boolean;
  price: number;
  inStock: number;
  position: number;
  image: ImageOutputModel | null;
  categories: {
    id: string;
    productId: string;
    categoryId: string;
  }[];
};
