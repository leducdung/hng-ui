export type ProductCategoryOutputModel = {
  /**
   * id là id của bảng map giữa product và category
   */
  id: string;
  productId: string;
  categoryId: string;
  category: CategoryOutputModel;
};

export type CategoryOutputModel = {
  id: string;
  name: string;
  picture: string | null;
  description: string | null;
  isProduct: boolean;
  slug: string;
  // title: string | null;
  pageTitle: string | null;
  metaDescription: string | null;
  timePublication: string | null;
  status: boolean;
};
