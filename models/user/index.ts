export enum ShippingAddressLocation {
  HOME = "home",
  OFFICE = "office",
}

export type ShippingAddressOutputModel = {
  id: string;
  name: string;
  phoneNumber: string;
  provinceId?: number;
  districtId?: number;
  wardId?: number;
  address: string;
  place?: ShippingAddressLocation;
  // isDefault?: boolean;
  shippingDefault?: Record<string, unknown> | null;
  image?: string;
};

export enum UserGender {
  MAN,
  WOMAN,
}
