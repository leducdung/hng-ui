export type ImageOutputModel = {
  id: string;
  productId: string;
  picture: string;
  alt: string | null;
  isAvatar: boolean;
  //   position: 0;
};
