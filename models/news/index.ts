export type NewsOutputModel = {
  id: string;
  title: string | null;

  shortDescription: string | null;
  // blogCategory: NewsCategoryOutputModel;
  timePublication: string | null;
  tags: string;

  slugs: string[];

  /**
   * không có chỉ để ở FE
   */
  imageAltText?: string | null;
  imageFeatured: string | null;
  author: {
    id: string;
    fullName: string;
  };
};

export type NewsDetailOutputModel = {
  id: string;
  titleVn: string | null;
  titleEn: string | null;
  titleAs: string | null;
  pageTitle: string | null;
  metaDescription: string | null;
  shortDescription: string | null;
  contentVn: string | null;
  contentEn: string | null;
  contentAs: string | null;

  slugs: string[];

  imageTitle: string | null;
  imageAltText: string | null;
  imageDescription: string | null;
  imageCaption: string | null;
  imageFeatured: string | null;

  tagsVn: string | null;
  tagsEn: string | null;
  tagsAs: string | null;

  blogCategory: NewsCategoryOutputModel;

  createdBy: {
    id: string;
    fullName: string;
  };
  author: {
    id: string;
    fullName: string;
  };
  createAt: string;
  timePublication: string | null;
};

export type NewsCategoryOutputModel = {
  id: string;
  picture: string | null;
  nameVn: string | null;
  nameEn: string | null;
  nameAs: string | null;
  parentId: string | null;
  description: string | null;
  slug: string;
  status: true;
};
