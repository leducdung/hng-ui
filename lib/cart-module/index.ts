export const CART_CONSTANT = "__cart";
export const CART_METADATA_CONSTANT = "__metadata__cart";

const DEFAULT_CART_METADATA: CartMetadata = {
  orderFrom: "website",
  shippingFee: 0,
  taxFee: 0,
  paymentType: "CASH",
  shippingType: "TAKEAWAY",
  totalAmount: 0,
  userNote: "",
  shippingAddressId: null,
  address: null,
};
export type CartProduct = {
  id: number;
  quantity: number;
  product: {
    name: string;
    code: string;
    description: string;
    images: Array<{ path: string }>;
    price: number;
    slug: string;
    status: boolean;
  };
};
export const addToCart = (id: number, product: CartProduct["product"], quantity: number) => {
  let cart = getCart();
  const productIndex = cart.findIndex((p) => p.id === id);
  if (productIndex !== -1) {
    cart[productIndex] = {
      ...cart[productIndex],
      quantity: cart[productIndex].quantity + quantity,
    };
  } else {
    cart = [...cart, { id, product, quantity }];
  }
  saveCart(cart);
};

export const removeFromCart = (id: number, quantity: number) => {
  let cart = getCart();
  const productIndex = cart.findIndex((p) => p.id === id);
  if (productIndex !== -1) {
    cart[productIndex] = {
      ...cart[productIndex],
      quantity: cart[productIndex].quantity - quantity,
    };
    if (cart[productIndex].quantity === 0) cart = cart.filter((item) => item.id !== id);
  }
  saveCart(cart);
};

export const getCart: <T extends CartProduct>() => T[] = () => {
  try {
    const cart = localStorage.getItem(CART_CONSTANT);
    if (cart === null) return [];
    return JSON.parse(cart);
  } catch (error) {
    return [];
  }
};

export type PaymentType = "WALLET" | "CASH";
export type ShippingType = "TAKEAWAY" | "DELIVERY";
export type ShippingAddress = {
  name: string;
  email: string;
  phoneNumber: string;
  address: string;
  city: string;
  district: string;
  ward: string;
};

export type CartMetadata = {
  orderFrom: string;
  shippingFee: number;
  taxFee: number;
  paymentType: PaymentType;
  shippingType: ShippingType;
  totalAmount: number;
  userNote: string;
  shippingAddressId: number | null;
  address: ShippingAddress | null;
};

export const saveCart = (cart: CartProduct[]) => {
  try {
    const serializedCart = JSON.stringify(cart);
    localStorage.setItem(CART_CONSTANT, serializedCart);
  } catch (error) {
    console.error(error);
  }
};

export const getCartMetadata: () => CartMetadata = () => {
  try {
    const cart = localStorage.getItem(CART_METADATA_CONSTANT);
    if (cart === null) return DEFAULT_CART_METADATA;
    return JSON.parse(cart);
  } catch (error) {
    console.error(error);
    return DEFAULT_CART_METADATA;
  }
};

export const saveCartMetadata = (data: CartMetadata) => {
  try {
    const serializedCartMetadata = JSON.stringify(data);
    localStorage.setItem(CART_METADATA_CONSTANT, serializedCartMetadata);
  } catch (error) {
    console.error(error);
  }
};

export const getInitialcartMetadata = () => {
  return typeof window === "undefined" ? DEFAULT_CART_METADATA : getCartMetadata();
};
export const getInitialcart = () => {
  return typeof window === "undefined" ? [] : getCart;
};
