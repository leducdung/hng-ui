import validator from "validator";

export const validateMobilePhone =
  (message: string, phoneCode: string) => (mobilePhone: string | undefined) => {
    if (!mobilePhone) return message;
    let valid = false;
    switch (phoneCode) {
      case "+84":
        if (mobilePhone.startsWith("0")) {
          valid = validator.isMobilePhone(mobilePhone, ["vi-VN"]);
          break;
        }
        valid = validator.isMobilePhone(`+84${mobilePhone}`, ["vi-VN"]);
        break;
      default:
        valid = validator.isMobilePhone(mobilePhone);
    }
    return valid ? undefined : message;
  };
