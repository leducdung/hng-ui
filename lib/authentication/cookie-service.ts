import cookie from "cookie";
import jwt from "jsonwebtoken";
import { IncomingMessage } from "http";

export const parseCookie = (req: IncomingMessage | null | undefined) => {
  return cookie.parse(req ? req.headers.cookie || "" : document.cookie);
};

const SECRET = process.env.JWT_SECRET || "";

export const parseJwtToken = (compositeToken: string | undefined) => {
  if (!compositeToken) return ["", ""];
  const [, tokens] = compositeToken.split(":");
  const [accessToken, refreshToken] = tokens
    .replace("[", "")
    .replace("]", "")
    .replace(/\"/gi, "")
    .split(",");
  return [accessToken, refreshToken] as const;
};

export const isValidToken = (token: string) => {
  try {
    const decoded = jwt.verify(token, SECRET);
    if (decoded) return [true, decoded] as const;
    return [false, null] as const;
  } catch (error) {
    return [false, null] as const;
  }
};
