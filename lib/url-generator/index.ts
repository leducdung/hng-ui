import { CDN_IMG_URL, HNG_DOMAIN } from "@Constants/config";
// https://statically.io/
export const useImgCDN: (
  imgSrc: string,
  config?: Partial<{
    width: number;
    height: number;
    percentage: number;
    webP: boolean;
  }>
) => string = (imgSrc, { width, height, percentage, webP } = {}) => {
  const baseURL = `${CDN_IMG_URL}/${HNG_DOMAIN}`;
  let configPath = "/";
  if (width) configPath = `${configPath}w=${width}`;
  if (height) configPath = `${configPath},h=${width}`;
  if (webP) configPath = `${configPath},f=auto`;
  if (percentage) configPath = `${configPath},q=${percentage}`;
  return configPath !== "/" ? baseURL + configPath + imgSrc : baseURL + imgSrc;
};
