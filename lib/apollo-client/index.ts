/* eslint-disable */
import { useMemo } from "react";
import { ApolloClient, InMemoryCache, HttpLink, NormalizedCacheObject } from "@apollo/client";
import { NextApiRequest, NextApiResponse } from "next";
import { createUploadLink } from "apollo-upload-client";

const APOLLO_API_ENDPOINT =
  process.env.NODE_ENV === "production"
    ? "https://hanagold.unibiz.io"
    : process.env.NEXT_PUBLIC_APOLLO_API_ENDPOINT;

export interface GraphQlContext {
  req: NextApiRequest;
  res: NextApiResponse;
}

let apolloClient: ApolloClient<NormalizedCacheObject> | undefined;

function createIsomorphicLink(context: GraphQlContext | undefined) {
  /**
   * SSG and SSR
   */
  if (typeof window === "undefined") {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    // const { SchemaLink } = require("@apollo/client/link/schema");
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    // const { graphQlSchema } = require("./schema.graphql");
    // return new SchemaLink({ schema: graphQlSchema, context });
    return new HttpLink({
      uri: APOLLO_API_ENDPOINT + "/graphql",
      credentials: "include",
    });
  }

  /**
   * Client-side
   */
  return createUploadLink({
    uri: "/graphql",
    credentials: "include",
  });
}
export function initializeApollo(
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  initialState: any = null,
  // Pages with Next.js data fetching methods, like `getStaticProps`, can send
  // a custom context which will be used by `SchemaLink` to server render pages
  context?: GraphQlContext
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): ApolloClient<any> {
  const _apolloClient = apolloClient ?? createApolloClient(context);

  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // get hydrated here
  if (initialState) {
    _apolloClient.cache.restore(initialState);
  }

  /**
   * SSG and SSR
   * Always create a new Apollo Client
   */
  if (typeof window === "undefined") {
    return _apolloClient;
  }

  // Create the Apollo Client once in the client
  apolloClient = apolloClient ?? _apolloClient;

  return apolloClient;
}
const createApolloClient: (context?: GraphQlContext) => ApolloClient<any> = (context) => {
  return new ApolloClient({
    ssrMode: typeof window === "undefined",
    cache: new InMemoryCache(),
    link: createIsomorphicLink(context),
  });
};
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function useApollo(initialState: any) {
  const apolloStore = useMemo(() => initializeApollo(initialState), [initialState]);
  return apolloStore;
}
