import React from "react";
import { NextPage } from "next";
import GoldCoins from "@Widgets/GoldCoins";

const GoldCoinsPage: NextPage = () => {
  return <GoldCoins />;
};

export default GoldCoinsPage;
