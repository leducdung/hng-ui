import React from "react";
import { NextPage } from "next";
import ProductListing from "@Widgets/ProductListing";

const ProductListingPage: NextPage = () => {
  return <ProductListing />;
};

export default ProductListingPage;
