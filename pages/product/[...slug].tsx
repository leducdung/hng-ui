import React, { useState } from "react";
import { APP_ROUTES, DEFAULT_IMG_ALT } from "@Constants";
import { useBreakpoints } from "../../hooks";
import { LinearProgress } from "@material-ui/core";
// import { Search as SearchIcon } from "@material-ui/icons";
import { BtnGroup, ProductCard1 } from "@Components";
// import { Zoom, Fade } from "react-reveal";
import { useQuery } from "@apollo/client";
import GetProduct from "@Lib/queries/Product.graphql";
import { currencyFormat } from "@Utils";
import Image from "next/image";
import { GetStaticProps, GetStaticPaths } from "next";
import { initializeApollo } from "@Lib/apollo-client";
import PageHeader from "@Widgets/PageHeader";
import PageFooter from "@Widgets/PageFooter";
import Banner3 from "../../public/images/banner/banner-painting.png";
import Banner2 from "../../public/images/banner/banner-statue.png";
import Banner1 from "../../public/images/banner/banner-jewelry.png";
// import { Data } from "@react-google-maps/api";
// import ProductListing from "@Widgets/ProductListing";
// import { useRouter } from "next/router";

export type CategoryItem = {
  id: string;
  data: {
    name: string;
    slug: string;
    status: boolean;
  };
};

export type ProductItem = {
  id: string;
  name: string;
  price: number;
  status: boolean;
  slug: string;
  images: {
    path: string;
  };
  discount: string | null;
};

// const Product = ({ params, category }) => {
const Product = ({ params }) => {
  const breakpoints = useBreakpoints();
  // const [selectCategory, setSelectCategory] = useState<string>("");
  const [, setSelectCategory] = useState<string>("");
  const [prodList, setProdList] = useState<{
    list: Array<ProductItem>;
    allProds: Array<ProductItem>;
    curLength: number;
  }>({
    list: [],
    allProds: [],
    curLength: 25,
  });
  const activeCategory = "";

  // useEffect(() => {
  //   if (category.categoryChilds && category.categoryChilds.length) {
  //     refetch({ slug: category.categoryChilds[0].data.slug, ...pagingParam });
  //     setSelectCategory(category.categoryChilds[0].data.slug);
  //   }
  // }, [category.categoryChilds, refetch]);
  // const { data: productsData, refetch } = useQuery(GetProduct.GetProductByCategorySlug, {
  const { data: productsData } = useQuery(GetProduct.GetProductByCategorySlug, {
    variables: { slug: params.slug.join("/"), pageNum: 1, pageSize: 100 },
    onCompleted: (data) => {
      const arr = data.result.products.filter((e) => e.status === true);
      setProdList({ ...prodList, list: arr.slice(0, prodList.curLength), allProds: arr });
    },
  });

  const renderBanner = (slug) => {
    switch (slug) {
      case "tranh-ma-vang":
        return (
          <Image
            src={Banner3}
            alt={DEFAULT_IMG_ALT}
            placeholder="blur"
            layout="responsive"
            width={1440}
            height={245}
          />
        );
      case "tuong-phong-thuy":
        return (
          <Image
            src={Banner2}
            alt={DEFAULT_IMG_ALT}
            placeholder="blur"
            layout="responsive"
            width={1440}
            height={245}
          />
        );
      default:
        return (
          <Image
            src={Banner1}
            alt={DEFAULT_IMG_ALT}
            placeholder="blur"
            layout="responsive"
            width={1440}
            height={245}
          />
        );
    }
  };

  return (
    <>
      <PageHeader />
      <div className="product-listing">
        <div className="product-listing__banner">
          {/* {breakpoints.lg && (
            <Image
              src="/images/banner/banner-product.png"
              width={1454}
              height={737}
              alt={DEFAULT_IMG_ALT}
              layout="responsive"
            />
          )} */}
          {renderBanner(params.slug.join("/"))}
          {/* <div className="listBanner">
            <Zoom>
              <Image
                width={550}
                height={426}
                src={BannerProduct1}
                alt={DEFAULT_IMG_ALT}
                objectFit="cover"
              />
            </Zoom>

            {breakpoints.lg && (
              <>
                <div className="bannerColumn">
                  <Fade top>
                    <Image src={BannerProduct2} alt={DEFAULT_IMG_ALT} width={264} height={202} />
                  </Fade>
                  <Fade bottom>
                    <div className="imgBottom">
                      <Image src={BannerProduct3} alt={DEFAULT_IMG_ALT} width={264} height={202} />
                    </div>
                  </Fade>
                </div>
                <div className="bannerColumn">
                  <Fade top>
                    <Image src={BannerProduct4} alt={DEFAULT_IMG_ALT} width={264} height={202} />
                  </Fade>
                  <Fade bottom>
                    <div className="imgBottom">
                      <Image src={BannerProduct5} alt={DEFAULT_IMG_ALT} width={264} height={202} />
                    </div>
                  </Fade>
                </div>
              </>
            )}
          </div> */}
        </div>
        <div className="product-listing__filter">
          {breakpoints.lg && (
            <div className="product-listing__filter__count">
              {prodList.list?.length || 0} Sản phẩm
            </div>
          )}
          {/* <div className="product-listing__filter-right">
            <TextField
              fullWidth
              placeholder="Bạn đang tìm sản phẩm nào?"
              variant="outlined"
              className={`product-listing__filter__search-input`}
              inputProps={{ className: "input" }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon style={{ fontSize: "2.2rem", color: "#9E9E9E" }} />
                  </InputAdornment>
                ),
              }}
            />

            {breakpoints.lg && (
              <>
                {!!category && category.categoryChilds && !!category.categoryChilds.length && (
                  <Select
                    variant="outlined"
                    className="product-listing__filter__select"
                    value={selectCategory}
                    onChange={(e) => setSelectCategory(e.target.value as string)}
                    MenuProps={{
                      className: "product-listing__filter__select__menu",
                      anchorOrigin: {
                        vertical: 60,
                        horizontal: "left",
                      },
                      transformOrigin: {
                        vertical: "top",
                        horizontal: "left",
                      },
                      getContentAnchorEl: null,
                    }}>
                    {category.categoryChilds.map((cate) => {
                      return (
                        <MenuItem
                          value={cate.data.slug}
                          onClick={() => {
                            setSelectCategory(cate.data.slug);
                            refetch({
                              slug: cate.data.slug,
                              pageNum: 1,
                              pageSize: 100,
                            });
                          }}
                          key={cate.data.slug}
                          style={{ fontSize: "1.4rem" }}>
                          {cate.data.name}
                        </MenuItem>
                      );
                    })}
                  </Select>
                )}

                <Select
                  variant="outlined"
                  className="product-listing__filter__select"
                  renderValue={() => <span>Giá</span>}
                  displayEmpty
                  MenuProps={{
                    className: "product-listing__filter__select__menu",
                    anchorOrigin: {
                      vertical: 60,
                      horizontal: "left",
                    },
                    transformOrigin: {
                      vertical: "top",
                      horizontal: "left",
                    },
                    getContentAnchorEl: null,
                  }}>
                  {Price.map((p) => {
                    return (
                      <MenuItem key={p.price} style={{ fontSize: "1.4rem" }}>
                        {p.price}
                      </MenuItem>
                    );
                  })}
                </Select>

                <Select
                  variant="outlined"
                  className="product-listing__filter__select"
                  renderValue={() => <span>Chất liệu</span>}
                  displayEmpty
                  MenuProps={{
                    className: "product-listing__filter__select__menu",
                    anchorOrigin: {
                      vertical: 60,
                      horizontal: "left",
                    },
                    transformOrigin: {
                      vertical: "top",
                      horizontal: "left",
                    },
                    getContentAnchorEl: null,
                  }}></Select>
              </>
            )}
          </div> */}
          {/* {!breakpoints.lg && (
            <div className="product-listing__filter__sort">
              <img src="/images/icon/icon-sort.png" alt={DEFAULT_IMG_ALT} />
            </div>
          )} */}
        </div>
        {!breakpoints.lg && (
          <div className="product-listing__list__category">
            <BtnGroup<CategoryItem>
              list={[]}
              onClick={(cate) => {
                setSelectCategory(cate.data.slug);
              }}
              renderBtnLabel={(cate) => {
                if (!cate.data.slug) {
                  return (
                    <div
                      className={
                        !activeCategory
                          ? "product-listing__list__category__item active"
                          : "product-listing__list__category__item"
                      }
                      onClick={() => {
                        setSelectCategory(cate.data.slug);
                      }}>
                      Tất cả
                    </div>
                  );
                }
                return (
                  <div
                    className={
                      cate.data.slug === ""
                        ? "product-listing__list__category__item active"
                        : "product-listing__list__category__item"
                    }>
                    {cate.data.name}
                  </div>
                );
              }}
            />
          </div>
        )}

        {productsData && prodList.list.length > 0 && (
          <div className="product-listing__list">
            {prodList.list.map((product) => {
              const productSlug = APP_ROUTES.SAN_PHAM + "/" + product.slug;
              return (
                <div key={product.id}>
                  <ProductCard1
                    id={product.id}
                    slug={productSlug}
                    title={product.name}
                    img={product.images[0].path}
                    price={product.price > 0 ? `${currencyFormat(product.price)} đ` : "Liên hệ"}
                    discount={product.discount ?? "0"}
                  />
                </div>
              );
            })}
          </div>
        )}

        {productsData && (
          <div className="product-listing__list__btn">
            {breakpoints.lg && (
              <>
                <div className="product-listing__list__text">
                  {prodList.list.length} kết quả của {prodList.allProds.length} sản phẩm
                </div>

                <LinearProgress
                  variant="determinate"
                  value={(prodList.list.length * 100) / prodList.allProds.length}
                  style={{ width: "24rem", marginTop: "2.2rem" }}
                />
              </>
            )}
            {prodList.curLength < prodList.allProds.length && (
              <button
                onClick={() => {
                  setProdList({
                    ...prodList,
                    list: prodList.allProds.slice(0, prodList.curLength + 25),
                    curLength: prodList.curLength + 25,
                  });
                }}
                className="product-listing__list__btn-submit">
                Xem thêm
              </button>
            )}
          </div>
        )}
      </div>
      <PageFooter withoutDivider />
    </>
  );
};

const client = initializeApollo();

export const getStaticPaths: GetStaticPaths = async () => {
  const { data } = await client.query({
    query: GetProduct.GetCategories,
  });
  const paths = data.categories
    .filter((c) => c.status)
    .map((c) => {
      const slugs = c.slug.split("/");
      return {
        params: {
          slug: slugs.length > 1 ? [slugs[0], slugs[1]] : [slugs[0]],
        },
      };
    });
  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  try {
    const { data } = await client.query({
      query: GetProduct.GetCategories,
    });
    // NOTE: we don't do more than 2 levels
    const category = data.categories.find((cate) => {
      const slugs = cate.slug.split("/");
      if (slugs.length > 1) return slugs[0] === params!.slug![0] && slugs[1] === params!.slug![0];
      return cate.slug === params!.slug![0];
    });
    if (category)
      return {
        props: {
          params,
          category: category,
        },
      };
    return { notFound: true };
  } catch (error) {
    console.error(error);
    return { notFound: true };
  }
};

export default Product;
