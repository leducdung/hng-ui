import React from "react";
import DancingStonePage from "@Widgets/DancingStonePage";
import { NextPage } from "next";
import { NextSeo } from "next-seo";
import Head from "next/head";

const DancingStone: NextPage = () => {
  return (
    <React.Fragment>
      <NextSeo
        title="Trang sức Happy Stone"
        description="HanaGold là nền tảng kết nối tiệm kim hoàn và người đi mua vàng, tại đó người dùng có thể chọn lựa mẫu mã, mua vàng tích lũy online."
        canonical="https://hanagold.vn/trang-suc-happy-stone"
        openGraph={{
          title: "Trang sức Happy Stone",
          description:
            "HanaGold là nền tảng kết nối tiệm kim hoàn và người đi mua vàng, tại đó người dùng có thể chọn lựa mẫu mã, mua vàng tích lũy online.",
          url: "https://hanagold.vn/trang-suc-happy-stone",
          type: "website",
          images: [
            {
              url: `https://res.cloudinary.com/unibiz/image/upload/v1632548821/HNG/Screen_Shot_2021-09-25_at_12.46.24_PM_yjgixb.png`,
              alt: "Happy Stone Kiêu sa vẻ đẹp vĩnh cửu",
              width: 701.5,
            },
          ],
        }}
      />
      <Head>
        <meta
          property="og:image:secure_url"
          content={`https://res.cloudinary.com/unibiz/image/upload/v1632548821/HNG/Screen_Shot_2021-09-25_at_12.46.24_PM_yjgixb.png`}
        />

        <meta property="og:image:type" content="image/png" />
      </Head>
      <DancingStonePage />
    </React.Fragment>
  );
};

export default DancingStone;
