import { AddressCard1, CheckoutStepper } from "@Components";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Checkbox,
  Dialog,
  FormControlLabel,
  Grid,
  InputAdornment,
  Radio,
  RadioGroup,
  TextField,
} from "@material-ui/core";
import React, { useState } from "react";
import styles from "./ConfirmShipping.module.scss";
import CirlcleCheckedIcon from "@material-ui/icons/RadioButtonChecked";
import CircleUnchecked from "@material-ui/icons/RadioButtonUnchecked";
import { APP_ROUTES } from "@Constants";
import { useRouter } from "next/router";
import AppLayout from "@Components/AppLayout";
import { useCartMetadata } from "hooks/useCartMetadata";
import { useQuery } from "@apollo/client";
import CartQuery from "@Lib/queries/Cart.graphql";
import { useCart } from "hooks/useCart";
import { currencyFormat } from "@Utils/other";

const ConfirmShipping = () => {
  const router = useRouter();
  const [cartMetada, setCartMetadata] = useCartMetadata();
  const [, , totalProductPrices] = useCart();
  const [storePick, setStorePick] = useState<boolean>(true);
  const [delivery, setDelivery] = useState<boolean>(false);
  const [selectedStoreId, setSelectedStoreId] = useState<number>(-1);
  const [visibleDeliAddressList, setVisibleDeliAddressList] = useState<boolean>(false);
  const { data } = useQuery(CartQuery.GetLocations);
  const toggleStorePick = (bool: boolean) => {
    setStorePick(bool);
    delivery && bool && setDelivery(false);
  };

  // const toggleDelivery = (bool: boolean) => {
  //   setDelivery(bool);
  //   storePick && bool && setStorePick(false);
  // };

  const toggleDeliDialog = () => {
    setVisibleDeliAddressList(!visibleDeliAddressList);
  };

  return (
    <AppLayout title="Giỏ hàng của bạn">
      <div className={styles.confirmShippingPage}>
        <CheckoutStepper activeStep={1} />
        <Grid container spacing={2} className={styles.body}>
          <Grid item xs={9} className={styles.left}>
            <h1 className={styles.title}>Thông tin địa chỉ nhận hàng</h1>
            <Accordion
              className={styles.storePick}
              expanded={storePick}
              onChange={(e, expanded: boolean) => toggleStorePick(expanded)}>
              <AccordionSummary
                aria-label="storePick_ex"
                aria-controls="storePick"
                id="storePick_ex">
                <FormControlLabel
                  className={styles.subTitle}
                  aria-label="storePick_ex"
                  onClick={(e) => e.stopPropagation()}
                  control={
                    <Checkbox
                      checked={storePick}
                      onChange={(e) => toggleStorePick(e.target.checked)}
                      icon={<CircleUnchecked className={styles.circle} />}
                      checkedIcon={<CirlcleCheckedIcon className={styles.circleChecked} />}
                      className={styles.circleCheckedStyle}
                    />
                  }
                  label="Nhận hàng tại cửa hàng"
                  classes={{ label: styles.label }}
                />
              </AccordionSummary>
              <AccordionDetails>
                <div className={styles.storePick__addressList}>
                  <Grid container spacing={4}>
                    {!!data &&
                      data.locations.map((addr) => {
                        return (
                          <Grid
                            onClick={() => {
                              setSelectedStoreId(addr.id);
                              setCartMetadata({
                                ...cartMetada,
                                address: {
                                  name: addr.name,
                                  email: addr.email,
                                  phoneNumber: addr.phoneNumber,
                                  address: addr.address,
                                  city: addr.address,
                                  ward: addr.address,
                                  district: addr.address,
                                },
                              });
                            }}
                            item
                            xs={6}
                            key={addr.id}>
                            <AddressCard1
                              selected={addr.id === selectedStoreId}
                              key={addr.id}
                              data={addr}
                              onClickUpdate={() => {
                                // props.onOpenAddressForm(addr);
                              }}
                              chooseToOrder={() => {
                                // props.onSelectAddress(addr);
                              }}
                              className={styles.storePick__addressCard}
                            />
                          </Grid>
                        );
                      })}
                  </Grid>
                </div>
              </AccordionDetails>
            </Accordion>

            {/* <Accordion */}
            {/*   className={styles.delivery} */}
            {/*   expanded={delivery} */}
            {/*   onChange={(e, expanded: boolean) => toggleDelivery(expanded)}> */}
            {/*   <AccordionSummary aria-label="delivery_ex" aria-controls="delivery" id="delivery_ex"> */}
            {/*     <FormControlLabel */}
            {/*       className={styles.subTitle} */}
            {/*       aria-label="delivery_ex" */}
            {/*       onClick={(e) => e.stopPropagation()} */}
            {/*       control={ */}
            {/*         <Checkbox */}
            {/*           checked={delivery} */}
            {/*           onChange={(e) => toggleDelivery(e.target.checked)} */}
            {/*           icon={<CircleUnchecked className={styles.circle} />} */}
            {/*           checkedIcon={<CirlcleCheckedIcon className={styles.circleChecked} />} */}
            {/*           className={styles.circleCheckedStyle} */}
            {/*         /> */}
            {/*       } */}
            {/*       label="Giao hàng đến địa chỉ của bạn" */}
            {/*       classes={{ label: styles.label }} */}
            {/*     /> */}
            {/*   </AccordionSummary> */}
            {/*   <AccordionDetails> */}
            {/*     <div className={styles.deliveryAddress}> */}
            {/*       <div className={styles.deliveryAddress_title}> */}
            {/*         <p className={styles.name}>Terry</p> */}
            {/*         <p className={styles.isDefault}>Mặc định</p> */}
            {/*       </div> */}
            {/*       <div className={styles.deliveryAddress_info}> */}
            {/*         <p>Địa chỉ: 494B Le Van Sy, Ward 11, Phu Nhuan, Ho Chi Minh City</p> */}
            {/*         <p>Số điện thoại: 0901 34 69 03</p> */}
            {/*       </div> */}
            {/*       <div className={styles.deliveryAddress_edit}>Chỉnh sửa</div> */}
            {/*       <div className={styles.deliveryAddress_footer}> */}
            {/*         <p> */}
            {/*           Bạn muốn giao hàng đến địa chỉ khác?{" "} */}
            {/*           <span */}
            {/*             className={styles.deliveryAddress_footer_change} */}
            {/*             onClick={toggleDeliDialog}> */}
            {/*             Thay đổi địa chỉ */}
            {/*           </span> */}
            {/*         </p> */}
            {/*       </div> */}
            {/*     </div> */}
            {/*   </AccordionDetails> */}
            {/* </Accordion> */}
          </Grid>
          <Grid item xs={3} className={styles.right}>
            <div className={styles.rightAction}>
              <div className={styles.payment}>
                <div className={styles.summary}>Thành tiền</div>
                <Grid container justify="space-between" className={styles.provisional}>
                  <Grid item lg={4}>
                    <div>Tạm tính</div>
                  </Grid>
                  <Grid item lg={6} container justify="flex-end">
                    <div>
                      {currencyFormat(totalProductPrices)}
                      <span>đ</span>
                    </div>
                  </Grid>
                </Grid>
                <Grid container justify="space-between" className={styles.coupon}>
                  <Grid item lg={4}>
                    <div>Mã giảm giá</div>
                  </Grid>
                  <Grid item lg={6} container justify="flex-end" className={styles.coupon_value}>
                    <div>
                      {" "}
                      0<sup>đ</sup>{" "}
                    </div>
                  </Grid>
                </Grid>
                <Grid container justify="space-between" className={styles.total}>
                  <Grid item lg={4}>
                    <div>Tổng cộng</div>
                  </Grid>
                  <Grid item lg={6} container justify="flex-end">
                    <div>
                      {currencyFormat(totalProductPrices)}
                      <sup>đ</sup>
                    </div>
                  </Grid>
                </Grid>
              </div>
              <div className={styles.coupon_group}>
                <TextField
                  placeholder="Mã giảm giá"
                  className={styles.form_text_field}
                  inputProps={{ className: styles.input }}
                  variant="outlined"
                  fullWidth
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <Button className={styles.applyBtn}>Áp dụng</Button>
                      </InputAdornment>
                    ),
                    classes: {
                      notchedOutline: styles.noBorder,
                    },
                  }}
                />
              </div>

              <div className={styles.nextStep}>
                <button
                  style={{
                    cursor: selectedStoreId !== -1 ? "pointer" : "not-allowed",
                    pointerEvents: selectedStoreId !== -1 ? "auto" : "none",
                  }}
                  className={styles.nextBtn}
                  onClick={() => {
                    router.push(APP_ROUTES.CONFIRM_ORDER);
                  }}>
                  Bước tiếp theo
                </button>
              </div>
            </div>
            <div className={styles.rightNote}>
              <div className={styles.rightNote_title}>Ghi chú giao hàng</div>
              <TextField
                placeholder="Placeholder"
                value={cartMetada.userNote}
                onChange={(e) => {
                  setCartMetadata({ ...cartMetada, userNote: e.target.value as string });
                }}
                className={styles.rightNote_text}
                variant="outlined"
                fullWidth
                rows={6}
                multiline
                inputProps={{ className: styles.input }}
              />
            </div>
          </Grid>
        </Grid>
      </div>

      <Dialog
        open={visibleDeliAddressList}
        fullWidth
        maxWidth="lg"
        onClose={() => {
          toggleDeliDialog();
        }}
        className={styles.DeliAddressDialog}>
        <div className={styles.DeliAddressDialog_body}>
          <div className={styles.DeliAddressDialog_title}>Danh sách địa chỉ giao hàng</div>
          <div className={styles.DeliAddressDialog_list}>
            <RadioGroup defaultValue="a" aria-label="gender" name="customized-radios">
              <FormControlLabel
                value="a"
                control={<Radio color="primary" />}
                className={styles.DeliAddressDialog_list_info}
                classes={{ label: styles.DeliAddressDialog_list_label }}
                label={
                  <div>
                    <div className={styles.DeliAddressDialog_list_info_title}>
                      <p className={styles.name}>Hoàng Lê Nhật</p>
                      <p className={styles.isDefault}>Mặc định</p>
                    </div>
                    <div className={styles.DeliAddressDialog_list_info_address}>
                      <p>Địa chỉ: 494B Lê Văn Sỹ, Phường 11, Quận Phú Nhuận, TP Hồ Chí Minh</p>
                      <p>Số điện thoại: 0901 34 69 03</p>
                    </div>
                    <div className={styles.DeliAddressDialog_list_info_edit}>Chỉnh sửa</div>
                  </div>
                }
              />
              <FormControlLabel
                value="b"
                control={<Radio color="primary" />}
                className={styles.DeliAddressDialog_list_info}
                classes={{ label: styles.DeliAddressDialog_list_label }}
                label={
                  <div>
                    <div className={styles.DeliAddressDialog_list_info_title}>
                      <p className={styles.name}>Hoàng Lê</p>
                    </div>
                    <div className={styles.DeliAddressDialog_list_info_address}>
                      <p>Địa chỉ: 494B Lê Văn Sỹ, Phường 11, Quận Phú Nhuận, TP Hồ Chí Minh</p>
                      <p>Số điện thoại: 0901 34 69 03</p>
                    </div>
                    <div className={styles.DeliAddressDialog_list_info_edit}>Chỉnh sửa</div>
                  </div>
                }
              />
            </RadioGroup>
          </div>
          <div className={styles.DeliAddressDialog_add}>+ Thêm mới địa chỉ giao hàng</div>

          <div className={styles.DeliAddressDialog_actions}>
            <button className={styles.DeliAddressDialog_actions_cancel} onClick={toggleDeliDialog}>
              Hủy
            </button>
            <button className={styles.DeliAddressDialog_actions_update}>Cập nhật</button>
          </div>
        </div>
      </Dialog>
    </AppLayout>
  );
};

export default ConfirmShipping;
