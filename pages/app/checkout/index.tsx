import React from "react";
import CartPage from "@Widgets/CartPage";
import { NextPage, GetServerSideProps } from "next";
import { isValidToken, parseCookie, parseJwtToken } from "lib/authentication/cookie-service";

const Cart: NextPage = () => {
  return <CartPage />;
};
export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const cookie = parseCookie(req);
  const [token] = parseJwtToken(cookie.token);
  const [authenticated, data] = isValidToken(token);
  if (authenticated) {
    return {
      props: {
        user: data,
      },
    };
  }
  return {
    props: {},
    redirect: {
      statusCode: 302,
      destination: "/customer/signin",
    },
  };
};
export default Cart;
