import CheckoutStepper from "@Components/CheckoutStepper";
import {
  Button,
  FormControlLabel,
  Grid,
  InputAdornment,
  Radio,
  RadioGroup,
  TextField,
} from "@material-ui/core";
import React from "react";
import styles from "./ConfirmOrder.module.scss";
import { ShoppingCartOutlined as ShoppingCartOutlinedIcon } from "@material-ui/icons";
import { useRouter } from "next/router";
import { APP_ROUTES } from "@Constants";
import CirlcleCheckedIcon from "@material-ui/icons/RadioButtonChecked";
import CircleUnchecked from "@material-ui/icons/RadioButtonUnchecked";
import AppLayout from "@Components/AppLayout";
import { useCartMetadata } from "hooks/useCartMetadata";
import { useCart } from "hooks/useCart";
import { OrderProductCart1 } from "@Components";
import CustomerQuery from "@Lib/queries/Customer.graphql";
import CartQuery from "@Lib/queries/Cart.graphql";
import { useQuery, useMutation } from "@apollo/client";
import { currencyFormat } from "@Utils/other";
import NotificationModal from "@Components/NotificationModal";

const radioCircle = (
  <Radio
    color="primary"
    icon={<CircleUnchecked className={styles.circle} />}
    checkedIcon={<CirlcleCheckedIcon className={styles.circleChecked} />}
  />
);
const CartConfirm = () => {
  const router = useRouter();
  const [popup, setPopup] = React.useState({
    title: "Rất tiếc có vấn đề trong quá trình đặt hàng!",
    content: "Vui lòng thực hiện lại hoặc liên hệ hotline 0889 028 009 để được hỗ trợ.",
  });
  const [createOrder, { loading }] = useMutation(CartQuery.CreateOrder);
  const [showNotificationModal, setShownotificationModal] = React.useState<boolean>(false);
  const { data } = useQuery(CustomerQuery.GetData);
  const [cartMetada, setCartMetadata] = useCartMetadata();
  const [cart, , totalProductPrices, resetCart] = useCart();

  const submit = async () => {
    const param = {
      orderFrom: "website",
      shippingFee: cartMetada.shippingFee,
      taxFee: cartMetada.taxFee,
      totalAmount: cartMetada.shippingFee + cartMetada.taxFee + totalProductPrices,
      userNote: cartMetada.userNote,
      address: cartMetada.address,
      paymentType: "CASH",
      shippingType: "TAKEAWAY",
      products: cart.map((item) => ({
        productId: item.id,
        amount: item.quantity,
        price: item.product.price,
      })),
    };
    try {
      const { data: result } = await createOrder({ variables: { createOrderInput: param } });
      setPopup({
        title: `Đơn hàng #${result.order?.code || "#"} đã được đặt thành công!`,
        content: `Cảm ơn bạn đã mua hàng tại HanaGold!  Nếu có bất kì thắc mắc nào, vui lòng liên hệ hotline: 0889 028 009`,
      });
      setShownotificationModal(true);
      resetCart();
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <AppLayout title="Giỏ hàng của bạn">
      <div className={styles.confirmOrder}>
        <CheckoutStepper activeStep={2} />
        <Grid container spacing={2} className={styles.body}>
          <Grid item xs={9} className={styles.left}>
            <div className={styles.cartList}>
              <div className={styles.cartIcon}>
                <ShoppingCartOutlinedIcon
                  style={{ fontSize: "2.2rem", color: "#CACACA", marginRight: "2rem" }}
                />
                Giỏ hàng của tôi ({cart.length} sản phẩm)
                {/* {t("CART_MY_SHOPPING_CART")} ({props.Cart.quantity} {t("CART_ITEMS")}) */}
              </div>
              <div>
                {Boolean(cart.length) &&
                  cart.map((prodCart) => {
                    return (
                      <OrderProductCart1
                        changeItemQuantity={() => {
                          return;
                        }}
                        key={prodCart.id}
                        cartItem={prodCart}
                      />
                    );
                  })}
              </div>
            </div>
            {!!data && (
              <div className={styles.delivery}>
                <h1 className={styles.delivery_title}>Thông tin địa chỉ nhận hàng</h1>
                <div className={styles.delivery_Address}>
                  <div className={styles.delivery_Address_title}>
                    <p className={styles.name}>{cartMetada.address?.name}</p>
                    <p className={styles.isDefault}>Mặc định</p>
                  </div>
                  <div className={styles.delivery_Address_info}>
                    <p>Địa chỉ: {cartMetada.address?.address || "--/--"}</p>
                    <p>Số điện thoại: {data.user.phoneNumber}</p>
                  </div>
                  {/* <div className={styles.delivery_Address_edit}>Chỉnh sửa</div> */}
                </div>
              </div>
            )}

            <div className={styles.method}>
              <h1 className={styles.method_title}>Hình thức thanh toán</h1>
              <RadioGroup
                className={styles.method_list}
                defaultValue="a"
                aria-label="gender"
                name="customized-radios">
                <FormControlLabel
                  value="a"
                  control={radioCircle}
                  className={styles.method_list_info}
                  label="COD - Thanh toán khi nhận hàng"
                  classes={{ label: styles.method_list_label }}
                />
                {/* <FormControlLabel */}
                {/*   value="b" */}
                {/*   control={radioCircle} */}
                {/*   className={styles.method_list_info} */}
                {/*   label="Thanh toán bằng thẻ quốc tế VISA, MASTER, JCB" */}
                {/*   classes={{ label: styles.method_list_label }} */}
                {/* /> */}
                {/* <FormControlLabel */}
                {/*   value="b" */}
                {/*   control={radioCircle} */}
                {/*   className={styles.method_list_info} */}
                {/*   label="Thanh toán bằng thẻ ATM nội địa/ Internet Banking" */}
                {/*   classes={{ label: styles.method_list_label }} */}
                {/* /> */}
                {/* <FormControlLabel */}
                {/*   value="b" */}
                {/*   control={radioCircle} */}
                {/*   className={styles.method_list_info} */}
                {/*   label="Khác" */}
                {/*   classes={{ label: styles.method_list_label }} */}
                {/* /> */}
              </RadioGroup>
            </div>
          </Grid>
          <Grid item xs={3} className={styles.right}>
            <div className={styles.rightAction}>
              <div className={styles.payment}>
                <div className={styles.summary}>Thành tiền</div>
                <Grid container justify="space-between" className={styles.provisional}>
                  <Grid item lg={4}>
                    <div>Tạm tính</div>
                  </Grid>
                  <Grid item lg={6} container justify="flex-end">
                    <div>
                      {currencyFormat(totalProductPrices)}
                      <span>đ</span>
                    </div>
                  </Grid>
                </Grid>
                <Grid container justify="space-between" className={styles.coupon}>
                  <Grid item lg={4}>
                    <div>Mã giảm giá</div>
                  </Grid>
                  <Grid item lg={6} container justify="flex-end" className={styles.coupon_value}>
                    <div>
                      -{currencyFormat(0)}
                      <sup>đ</sup>
                    </div>
                  </Grid>
                </Grid>
                <Grid container justify="space-between" className={styles.deliFee}>
                  <Grid item lg={4}>
                    <div>Phí giao hàng</div>
                  </Grid>
                  <Grid item lg={6} container justify="flex-end">
                    <div>
                      -{currencyFormat(0)}
                      <sup>đ</sup>
                    </div>
                  </Grid>
                </Grid>
                <Grid container justify="space-between" className={styles.total}>
                  <Grid item lg={4}>
                    <div>Tổng cộng</div>
                  </Grid>
                  <Grid item lg={6} container justify="flex-end">
                    <div>
                      {currencyFormat(
                        totalProductPrices + cartMetada.taxFee + cartMetada.shippingFee
                      )}
                      <sup>đ</sup>
                    </div>
                  </Grid>
                </Grid>
              </div>
              <div className={styles.coupon_group}>
                <TextField
                  placeholder="Mã giảm giá"
                  className={styles.form_text_field}
                  inputProps={{ className: styles.input }}
                  variant="outlined"
                  fullWidth
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <Button className={styles.applyBtn}>Áp dụng</Button>
                      </InputAdornment>
                    ),
                    classes: {
                      notchedOutline: styles.noBorder,
                    },
                  }}
                />
              </div>

              <div className={styles.nextStep}>
                <button
                  style={{ pointerEvents: loading ? "none" : "auto" }}
                  className={styles.nextBtn}
                  onClick={() => submit()}>
                  Xác nhận
                </button>
              </div>
            </div>
            <div className={styles.rightNote}>
              <div className={styles.rightNote_title}>Ghi chú giao hàng</div>
              <TextField
                placeholder="Placeholder"
                value={cartMetada.userNote}
                onChange={(e) => {
                  setCartMetadata({ ...cartMetada, userNote: e.target.value as string });
                }}
                className={styles.rightNote_text}
                variant="outlined"
                fullWidth
                rows={6}
                multiline
                inputProps={{ className: styles.input }}
              />
            </div>
          </Grid>
        </Grid>
      </div>
      <NotificationModal show={showNotificationModal} title={popup.title} content={popup.content}>
        <button onClick={() => router.push(APP_ROUTES.HOME)} className="confirm">
          Về trang chủ
        </button>
      </NotificationModal>
    </AppLayout>
  );
};

export default CartConfirm;
