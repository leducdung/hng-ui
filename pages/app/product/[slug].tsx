import React, { useState } from "react";
import { NextPage } from "next";
// import { useRouter } from "next/router";
import AppLayout from "@Components/AppLayout";
import styles from "./ProductDetail.module.scss";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableRow,
} from "@material-ui/core";
import { ArrowDropDown as ArrowDropDownIcon } from "@material-ui/icons";
import { useBreakpoints } from "hooks";
import { CartPlusIcon } from "@Components/Icons";
import { QuantityBtnGroup } from "@Components";
import { Swiper, SwiperSlide } from "swiper/react";
import Image from "next/image";
import { DEFAULT_IMG_ALT } from "@Constants";

const detailList = [
  {
    img: "/images/product/app-detail-1.png",
    type: "Cao cấp",
    name: "Bông tai vàng đính đá CHKD CZE",
    price: "2,290,000 đ",
  },
  {
    img: "/images/product/app-detail-2.png",
    type: "Cao cấp",
    name: "Bông tai vàng đính đá CHKD CZE",
    price: "2,290,000 đ",
  },
  {
    img: "/images/product/app-detail-3.png",
    type: "Cao cấp",
    name: "Bông tai vàng đính đá CHKD CZE",
    price: "2,290,000 đ",
  },
  {
    img: "/images/product/app-detail-4.png",
    type: "Cao cấp",
    name: "Bông tai vàng đính đá CHKD CZE",
    price: "2,290,000 đ",
  },
  {
    img: "/images/product/app-detail-5.png",
    type: "Cao cấp",
    name: "Bông tai vàng đính đá CHKD CZE",
    price: "2,290,000 đ",
  },
];

const Product: NextPage = () => {
  // const router = useRouter();
  const breakpoints = useBreakpoints();
  const [activeSlide, setActiveSlide] = useState<number>(0);

  const onSlideChange = (e) => {
    setActiveSlide(e.realIndex);
  };

  return (
    <main className="hng-app">
      <AppLayout>
        <Swiper
          slidesPerView={5}
          spaceBetween={1}
          loop
          centeredSlides
          onSlideChange={onSlideChange}
          className={styles.detailPage__swiper}>
          {detailList.map((cate, idx) => {
            return (
              <SwiperSlide
                key={idx}
                className={`${styles.detailPage__swiperSlide} ${
                  activeSlide === idx ? styles.swiper_active : ""
                }`}>
                <Image
                  src={cate.img}
                  alt={DEFAULT_IMG_ALT}
                  layout="fill"
                  className={styles.detailPage__swiperSlide_img}
                />
              </SwiperSlide>
            );
          })}
          <div className={styles.product}>
            <div className={styles.product_type}>CAO CẤP</div>
            <div className={styles.product_name}>Bông tai vàng đính đá CHKD CZE</div>
            <div className={styles.product_product}>2.290.000đ</div>
          </div>
        </Swiper>
        <div className={styles.detailPage}>
          <Grid container className={styles.detailPage__info}>
            <Grid item sm={12} lg={6} className={styles.detailPage__info__left}>
              <div className={styles.detailPage__info__generals}>
                <div className={styles.detailPage__info__generals__title}>Thông tin chung</div>
                <div className={styles.detailPage__info__generals__table}>
                  <Table>
                    <TableBody>
                      <TableRow className={styles.row}>
                        <TableCell
                          className={`${styles.noBorder} ${styles.cell}`}
                          component="th"
                          scope="row">
                          Mã
                        </TableCell>
                        <TableCell className={`${styles.noBorder} ${styles.cell}`}>
                          1234567890
                        </TableCell>
                      </TableRow>
                      <TableRow className={styles.row}>
                        <TableCell
                          className={`${styles.noBorder} ${styles.cell}`}
                          component="th"
                          scope="row">
                          Kích cỡ
                        </TableCell>
                        <TableCell className={`${styles.noBorder} ${styles.cell}`}>
                          <div className={styles.sizesList}>
                            {["45mm", "50mm", "55mm"].map((e, i) => {
                              return (
                                <div
                                  key={i}
                                  className={`${styles.sizesList_item} ${
                                    i === 1 ? styles.active : ""
                                  }`}>
                                  {e}
                                </div>
                              );
                            })}
                          </div>
                        </TableCell>
                      </TableRow>
                      <TableRow className={styles.row}>
                        <TableCell
                          className={`${styles.noBorder} ${styles.cell}`}
                          component="th"
                          scope="row">
                          Màu sắc
                        </TableCell>
                        <TableCell className={`${styles.noBorder} ${styles.cell}`}>
                          <div className={styles.colorList}>
                            {["Vàng hồng", "Vàng trắng", "Vàng chanh"].map((e, i) => {
                              return (
                                <div
                                  key={i}
                                  className={`${styles.sizesList_item} ${
                                    i === 1 ? styles.active : ""
                                  }`}>
                                  {e}
                                </div>
                              );
                            })}
                          </div>
                        </TableCell>
                      </TableRow>
                      <TableRow className={styles.row}>
                        <TableCell
                          className={`${styles.noBorder} ${styles.cell}`}
                          component="th"
                          scope="row">
                          Chất liệu
                        </TableCell>
                        <TableCell className={`${styles.noBorder} ${styles.cell}`}>
                          <Grid container className={styles.materialList}>
                            {[
                              "Vàng 14K",
                              "Vàng 18K",
                              "Vàng 24K",
                              "Vàng 28K",
                              "Vàng 30K",
                              "Vàng 32K",
                            ].map((e, i) => {
                              return (
                                <Grid
                                  item
                                  xs={3}
                                  key={i}
                                  className={`${styles.sizesList_item} ${
                                    i === 1 ? styles.active : ""
                                  }`}>
                                  {e}
                                </Grid>
                              );
                            })}
                          </Grid>
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </div>

                <div className={styles.detailPage__info__cartWrap}>
                  <Grid alignItems="center" container>
                    <Grid item xs={2}>
                      Giá
                    </Grid>
                    <Grid item xs={4} className={styles.detailPage__info__cartWrap_price}>
                      10,680,000 đ
                    </Grid>
                    <Grid item xs={4} className={styles.detailPage__info__cartWrap_cost}>
                      10,780,000 đ
                    </Grid>
                  </Grid>
                  <Grid container className={styles.detailPage__info__cartWrap__actions}>
                    <Grid item xs={4}>
                      <div>
                        <QuantityBtnGroup
                          quantity={1}
                          onChangeQuantity={() => {
                            // onChangeSelectedVariantQuantity(newValue);
                          }}
                          className={styles.detailPage__info__cartWrap__actions__btnGroup}
                          iconColor="#a6a6a6"
                        />
                      </div>
                    </Grid>
                    <div className={styles.detailPage__info__cartWrap__actions_right}>
                      <button
                        className={styles.detailPage__info__cartWrap__actions__btn}
                        onClick={() => {
                          // onAddToCart(true);
                        }}>
                        MUA NGAY
                      </button>

                      <button
                        className={styles.detailPage__info__cartWrap__actions__addToCart}
                        onClick={() => {
                          // onAddToCart();
                        }}>
                        <CartPlusIcon size={[24, 24]} viewBox={[24, 24]} color="#A6A6A6" />
                      </button>
                    </div>
                  </Grid>
                </div>
              </div>
              <Accordion defaultExpanded classes={{ root: styles.detailPage__info__accordion }}>
                <AccordionSummary
                  expandIcon={
                    <ArrowDropDownIcon className={styles.detailPage__info__accordion__icon} />
                  }
                  className={styles.detailPage__info__accordion__title}>
                  MÔ TẢ
                </AccordionSummary>
                <AccordionDetails className={styles.detailPage__info__accordion__description}>
                  Nếu trang phục tôn lên vóc dáng và định hình phong cách thì trang sức sẽ đóng vai
                  trò thể hiện tính cách và đẳng cấp. Tùy vào sở thích cũng như loại nữ trang mà bạn
                  ưa chuộng mà chúng sẽ sở hữu vẻ đẹp khác nhau. Nhằm đem đến cho các cô gái những
                  mẫu trang sức đính đá lấp lánh, PNJ ra mắt sản phẩm nhẫn được chế tác trên chất
                  liệu vàng 18K cùng điểm nhấn đính đá CZ sang trọng, giúp cho quý cô trông thời
                  thượng và hiện đại. PNJ luôn trân quý và tôn vinh vẻ đẹp đặc trưng và duyên dáng
                  của phụ nữ Việt trong từng thiết kế trang sức. Ngoài ra, xu hướng thời trang ngày
                  càng phát triển, nữ trang vàng PNJ cũng vì vậy mà trở nên độc đáo và tinh tế hơn,
                  tô điểm thêm vẻ hiện đại cho chủ sở hữu, giúp nàng thể hiện sự tự tin và phong
                  cách của mình.
                </AccordionDetails>
              </Accordion>
              {!breakpoints.lg && (
                <Accordion defaultExpanded classes={{ root: styles.detailPage__info__accordion }}>
                  <AccordionSummary
                    expandIcon={
                      <ArrowDropDownIcon className={styles.detailPage__info__accordion__icon} />
                    }
                    className={styles.detailPage__info__accordion__title}>
                    THÔNG SỐ KỸ THUẬT
                  </AccordionSummary>
                  <AccordionDetails className={styles.detailPage__info__accordion__description}>
                    <Grid container className="product-detail__describe__table">
                      <Grid item xs={6}>
                        <div className="product-detail__describe__table__name">Bộ Sưu Tập</div>
                        <div className="product-detail__describe__table__name">Màu chất liệu</div>
                        <div className="product-detail__describe__table__name">Loại đá chính</div>
                        <div className="product-detail__describe__table__name">
                          Hình dạng đá chính
                        </div>
                        <div className="product-detail__describe__table__name">Loại đá phụ</div>
                        <div className="product-detail__describe__table__name">Tuổi vàng</div>
                        <div className="product-detail__describe__table__name">Độ tinh khiết</div>
                      </Grid>
                      <Grid item xs={6}>
                        <div className="product-detail__describe__table__description">
                          Young Generation
                        </div>
                        <div className="product-detail__describe__table__description">
                          Vàng trắng
                        </div>
                        <div className="product-detail__describe__table__description">CZ</div>
                        <div className="product-detail__describe__table__description">Tròn</div>
                        <div className="product-detail__describe__table__description">
                          Đá Crystal - Vàng hồng
                        </div>
                        <div className="product-detail__describe__table__description">Vàng 14k</div>
                        <div className="product-detail__describe__table__description">90%</div>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
              )}
            </Grid>
            {breakpoints.lg && (
              <Grid item sm={12} lg={6} className={styles.detailPage__info__right}>
                <div className={styles.detailPage__info__specifications}>
                  <div className={styles.detailPage__info__specifications__title}>
                    Thông số kỹ thuật
                  </div>
                  <Grid
                    alignItems="center"
                    container
                    className={styles.detailPage__info__specifications__container}>
                    {/* collections */}
                    <Grid xs={6}>Bộ sưu tập</Grid>
                    <Grid xs={6} className={styles.description}>
                      Young Generation
                    </Grid>
                    {/* weight */}
                    <Grid xs={6}>Trọng lượng vàng</Grid>
                    <Grid xs={6} className={styles.description}>
                      20g
                    </Grid>
                    {/* main stone */}
                    <Grid xs={6}>Loại đá chính</Grid>
                    <Grid xs={6} className={styles.description}>
                      CZ
                    </Grid>
                    {/* material */}
                    <Grid xs={6}>Chất liệu</Grid>
                    <Grid xs={6} className={styles.description}>
                      Vàng
                    </Grid>
                    {/* material color */}
                    <Grid xs={6}>Màu chất liệu</Grid>
                    <Grid xs={6} className={styles.description}>
                      Vàng trắng
                    </Grid>
                    {/* secondary stone type */}
                    <Grid xs={6}>Loại đá phụ - Màu đá phụ</Grid>
                    <Grid xs={6} className={styles.description}>
                      Đá Crystall - Vàng hồng
                    </Grid>
                    {/* gold age */}
                    <Grid xs={6}>Tuổi vàng</Grid>
                    <Grid xs={6} className={styles.description}>
                      Vàng 14K
                    </Grid>
                    {/* purity */}
                    <Grid xs={6}>Độ tinh khiết</Grid>
                    <Grid xs={6} className={styles.description}>
                      90%
                    </Grid>
                    {/* sex */}
                    <Grid xs={6}>Giới tính</Grid>
                    <Grid xs={6} className={styles.description}>
                      Nữ
                    </Grid>
                    {/* prod type */}
                    <Grid xs={6}>Loại sản phẩm</Grid>
                    <Grid xs={6} className={styles.description}>
                      Dây chuyền
                    </Grid>
                    {/* main stone shape  */}
                    <Grid xs={6}>Hình dạng đá chính</Grid>
                    <Grid xs={6} className={styles.description}>
                      Tròn
                    </Grid>
                  </Grid>
                </div>
                <Accordion classes={{ root: styles.detailPage__info__accordion }}>
                  <AccordionSummary
                    expandIcon={
                      <ArrowDropDownIcon className={styles.detailPage__info__accordion__icon} />
                    }
                    className={styles.detailPage__info__accordion__title}>
                    CHÍNH SÁCH BẢO HÀNH
                  </AccordionSummary>
                  <AccordionDetails
                    className={styles.detailPage__info__accordion__description}></AccordionDetails>
                </Accordion>
                <Accordion classes={{ root: styles.detailPage__info__accordion }}>
                  <AccordionSummary
                    expandIcon={
                      <ArrowDropDownIcon className={styles.detailPage__info__accordion__icon} />
                    }
                    className={styles.detailPage__info__accordion__title}>
                    THANH TOÁN VÀ GIAO HÀNG
                  </AccordionSummary>
                  <AccordionDetails
                    className={styles.detailPage__info__accordion__description}></AccordionDetails>
                </Accordion>
              </Grid>
            )}
          </Grid>
        </div>
      </AppLayout>
    </main>
  );
};

export default Product;
