import AppLayout from "@Components/AppLayout";
import { DEFAULT_IMG_ALT } from "@Constants";
import { Grid, LinearProgress } from "@material-ui/core";
import { useBreakpoints } from "hooks/useBreakpoints";
import { NextPage } from "next";
import React, { useState } from "react";
import styles from "./Products.module.scss";
import { Swiper, SwiperSlide } from "swiper/react";
import ProductCard1 from "@Components/ProductCard1";
import { useLazyQuery, useQuery } from "@apollo/client";
import GetProduct from "../../../lib/queries/Product.graphql";
import { currencyFormat } from "@Utils";
// import { Search as SearchIcon } from "@material-ui/icons";
import Image from "next/image";
import Banner1 from "../../../public/images/product/app-pro-banner1.png";
import Banner2 from "../../../public/images/product/app-pro-banner2.png";
import Banner3 from "../../../public/images/product/app-pro-banner3.png";
import Banner4 from "../../../public/images/product/app-pro-banner4.png";
import Banner5 from "../../../public/images/product/app-pro-banner5.png";
// import Icon from "../../../public/images/icon/icon-sort.png";

export type ProductItem = {
  id: string;
  name: string;
  price: number;
  status: boolean;
  slug: string;
  images: {
    path: string;
  };
  discount: string | null;
};

const HanaGoldApp: NextPage = () => {
  const breakpoints = useBreakpoints();
  const [prodList, setProdList] = useState<{
    list: Array<ProductItem>;
    allProds: Array<ProductItem>;
    curLength: number;
  }>({
    list: [],
    allProds: [],
    curLength: 25,
  });
  const [activeCate, setActiveCate] = useState<number>(0);

  const { data: catesData } = useQuery(GetProduct.GetCategories, {
    onCompleted: () => {
      const arr = catesData.categories.filter((e) => e.status === true);
      onSelectCategory(arr[0], 0);
    },
  });
  const [getProdByCate, { data: productsData }] = useLazyQuery(GetProduct.GetProductByCategory, {
    onCompleted: () => {
      const arr = productsData.getProductByCategory.products.filter((e) => e.status === true);
      setProdList({ ...prodList, list: arr.slice(0, prodList.curLength), allProds: arr });
    },
  });

  const onSelectCategory = (cate, index) => {
    getProdByCate({ variables: { categoryId: cate.id, pageNum: 1, pageSize: 100 } });
    setActiveCate(index);
  };

  return (
    <main className="hng-app">
      <AppLayout>
        <div className={styles.productsPage}>
          <div className={styles.productsPage__banner}>
            <Grid
              spacing={3}
              container
              direction="row"
              justify="center"
              alignItems="stretch"
              className={styles.productsPage__banner__list}>
              <Grid item xs={6} className={styles.productsPage__banner__imgLeft}>
                <Image
                  src={Banner1}
                  alt={DEFAULT_IMG_ALT}
                  layout="responsive"
                  width={550}
                  height={300}
                />
              </Grid>
              {breakpoints.lg && (
                <Grid
                  item
                  container
                  spacing={3}
                  xs={6}
                  className={styles.productsPage__banner__imgRight}>
                  <Grid item container xs={6}>
                    <Grid item>
                      <Image src={Banner2} alt={DEFAULT_IMG_ALT} />
                    </Grid>
                    <Grid item>
                      <Image src={Banner4} alt={DEFAULT_IMG_ALT} />
                    </Grid>
                  </Grid>
                  <Grid item container xs={6}>
                    <Grid item>
                      <Image src={Banner3} alt={DEFAULT_IMG_ALT} />
                    </Grid>
                    <Grid item>
                      <Image src={Banner5} alt={DEFAULT_IMG_ALT} />
                    </Grid>
                  </Grid>
                </Grid>
              )}
            </Grid>
          </div>

          {catesData && (
            <div className={styles.productsPage__swiper}>
              <Swiper
                slidesPerView={7}
                spaceBetween={30}
                allowSlideNext
                allowSlidePrev
                allowTouchMove
                className="mySwiper">
                {catesData.categories
                  .filter((e) => e.status === true)
                  .map((cate, idx) => {
                    return (
                      <SwiperSlide
                        key={cate.id}
                        className={`${styles.productsPage__swiperSlide} ${
                          activeCate === idx && styles.productsPage__swiperSlide_active
                        }`}
                        onClick={() => onSelectCategory(cate, idx)}>
                        <div className={styles.imgCate}>
                          <Image
                            src={cate.image || "/app-icon.png"}
                            alt={cate.slug}
                            layout="fill"
                            // objectFit="contain"
                          />
                        </div>
                        <div className={styles.productsPage__swiperSlide_name}>{cate.name}</div>
                      </SwiperSlide>
                    );
                  })}
              </Swiper>
            </div>
          )}

          {prodList && (
            <div className={styles.productsPage__filter}>
              {breakpoints.lg && (
                <div className={styles.productsPage__filter__count}>
                  {prodList.list?.length || 0} Sản phẩm
                </div>
              )}
              {/* <div className={styles.productsPage__filter_right}>
                <TextField
                  fullWidth
                  placeholder="Bạn đang tìm sản phẩm nào?"
                  variant="outlined"
                  className={`product-listing__filter__search-input`}
                  inputProps={{ className: "input" }}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <SearchIcon style={{ fontSize: "2.2rem", color: "#9E9E9E" }} />
                      </InputAdornment>
                    ),
                  }}
                />

                {breakpoints.lg && (
                  <>
                    <Select
                      variant="outlined"
                      className={styles.productsPage__filter__select}
                      renderValue={() => <span>Danh mục</span>}
                      displayEmpty
                      MenuProps={{
                        className: styles.productsPage__filter__select__menu,
                        anchorOrigin: {
                          vertical: 60,
                          horizontal: "left",
                        },
                        transformOrigin: {
                          vertical: "top",
                          horizontal: "left",
                        },
                        getContentAnchorEl: null,
                      }}>
                      {Category.map((cate) => {
                        return (
                          <MenuItem key={cate.id} style={{ fontSize: "1.4rem" }}>
                            {cate.name}
                          </MenuItem>
                        );
                      })}
                    </Select>

                    <Select
                      variant="outlined"
                      className={styles.productsPage__filter__select}
                      renderValue={() => <span>Giá</span>}
                      displayEmpty
                      MenuProps={{
                        className: styles.productsPage__filter__select__menu,
                        anchorOrigin: {
                          vertical: 60,
                          horizontal: "left",
                        },
                        transformOrigin: {
                          vertical: "top",
                          horizontal: "left",
                        },
                        getContentAnchorEl: null,
                      }}>
                      {Price.map((p) => {
                        return (
                          <MenuItem key={p.price} style={{ fontSize: "1.4rem" }}>
                            {p.price}
                          </MenuItem>
                        );
                      })}
                    </Select>

                    <Select
                      variant="outlined"
                      className={styles.productsPage__filter__select}
                      renderValue={() => <span>Chất liệu</span>}
                      displayEmpty
                      MenuProps={{
                        className: styles.productsPage__filter__select__menu,
                        anchorOrigin: {
                          vertical: 60,
                          horizontal: "left",
                        },
                        transformOrigin: {
                          vertical: "top",
                          horizontal: "left",
                        },
                        getContentAnchorEl: null,
                      }}>
                      {/* {Price.map((p) => {
                        return (
                          <MenuItem key={p.price} style={{ fontSize: "1.4rem" }}>
                            {p.price}
                          </MenuItem>
                        );
                      })} */}
              {/* </Select>
                  </>
                )}
              </div>
              {!breakpoints.lg && (
                <div className={styles.productsPage__filter__sort}>
                  <Image src={Icon} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                </div>
              )} */}
            </div>
          )}

          {prodList && (
            <Grid container spacing={4} className={styles.productsPage__list}>
              {prodList.list.map((pro) => {
                return (
                  <Grid item xs={3} key={pro.id}>
                    <ProductCard1
                      id={pro.id}
                      title={pro.name}
                      img={pro.images[0].path}
                      price={`${currencyFormat(pro.price)} đ`}
                      discount={pro.discount ?? "0"}
                      slug={`/app/product/${pro.slug}`}
                    />
                  </Grid>
                );
              })}
            </Grid>
          )}

          {productsData && (
            <div className={styles.productsPage__list__btn}>
              {breakpoints.lg && (
                <>
                  <div className={styles.productsPage__list__text}>
                    {prodList.list.length} kết quả của {prodList.allProds.length} sản phẩm
                  </div>

                  <LinearProgress
                    variant="determinate"
                    value={(prodList.list.length * 100) / prodList.allProds.length}
                    style={{ width: "24rem", marginTop: "2.2rem" }}
                    classes={{ bar: styles.productsPage__list__bar }}
                  />
                </>
              )}
              {prodList.curLength < prodList.allProds.length && (
                <button
                  onClick={() => {
                    setProdList({
                      ...prodList,
                      list: prodList.allProds.slice(0, prodList.curLength + 25),
                      curLength: prodList.curLength + 25,
                    });
                  }}
                  className={styles.productsPage__list__btn_submit}>
                  Xem thêm
                </button>
              )}
            </div>
          )}
        </div>
      </AppLayout>
    </main>
  );
};

export default HanaGoldApp;
