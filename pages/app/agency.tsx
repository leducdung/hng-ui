import AppLayout from "@Components/AppLayout";
import { NextPage } from "next";
import React from "react";
import { ShowRoomPackages, SpecialOffer, Opportunity } from "@Components";

const AgencyApp: NextPage = () => {
  return (
    <main className="hng-app">
      <AppLayout title="Đại lý nhượng quyền">
        <div className="agency-app">
          <Opportunity />
          <SpecialOffer />
          <ShowRoomPackages />
        </div>
      </AppLayout>
    </main>
  );
};

export default AgencyApp;
