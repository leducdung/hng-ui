import AppLayout from "@Components/AppLayout";
import { NextPage } from "next";
import React, { useState } from "react";
import { DEFAULT_IMG_ALT } from "@Constants";
import { HotNews, News, NewsCategory } from "mock";
import Link from "next/link";
import styles from "./News.module.scss";
import { BtnGroup } from "@Components";
import Image from "next/image";
import Icon from "../../../../public/images/mock/news/icon.png";

export type CategoryItem = {
  id: string;
  name: string;
};

const NewsPage: NextPage = () => {
  const [selectCategory, setSelectCategory] = useState<CategoryItem | null>(
    NewsCategory[""] ?? null
  );
  const activeCategory = NewsCategory.find((cate) => cate.id === selectCategory?.id);
  return (
    <main className="hng-app">
      <AppLayout>
        <div className={styles.news}>
          <div className={styles.top}>
            <div className={styles.text}> TIN NỔI BẬT</div>
            <div className={styles.body}>
              <div className={styles.left}>
                {HotNews.map((h) => {
                  if (h.id === "1")
                    return (
                      <div className={styles.item}>
                        <Link key={h.name} href="/app/statistics/news/slug" passHref>
                          <a>
                            <div className={styles.icon}>
                              <Image
                                src={Icon}
                                alt={DEFAULT_IMG_ALT}
                                objectFit="contain"
                                width={13}
                                height={3}
                              />
                            </div>
                            <div className={styles.image}>
                              <Image
                                src={h.img}
                                alt={DEFAULT_IMG_ALT}
                                objectFit="cover"
                                layout="fill"
                              />
                            </div>
                            <div className={styles.content}>
                              <div className={styles.cate}> {h.category}</div>
                              <div className={styles.name}> {h.name}</div>
                              <div className={styles.view}>
                                <span>{h.view} lượt xem • </span>
                                {h.time} tiếng trước
                              </div>
                            </div>
                          </a>
                        </Link>
                      </div>
                    );
                })}
              </div>
              <div className={styles.right}>
                {HotNews.slice(1, HotNews.length).map((h) => {
                  return (
                    <Link key={h.name} href="/app/statistics/news/slug" passHref>
                      <a>
                        <div className={styles.item}>
                          <div className={styles.image}>
                            <Image src={h.img} alt={DEFAULT_IMG_ALT} objectFit="cover" />
                          </div>
                          <div>
                            <div className={styles.cate}> {h.category}</div>
                            <div className={styles.name}> {h.name}</div>
                            <div className={styles.view}>
                              <span>
                                {h.view} lượt xem • {h.time} tiếng trước
                              </span>
                              <Image
                                src={Icon}
                                alt={DEFAULT_IMG_ALT}
                                objectFit="contain"
                                width={13}
                                height={3}
                              />
                            </div>
                          </div>
                        </div>
                      </a>
                    </Link>
                  );
                })}
              </div>
            </div>
          </div>

          <BtnGroup<CategoryItem>
            list={[{ id: "", name: "TẤT CẢ DANH MỤC" }, ...NewsCategory]}
            onClick={(cate) => {
              setSelectCategory(cate);
            }}
            renderBtnLabel={(cate) => {
              // return <div className="product-listing__list__category__item">{cate.name}</div>;

              if (!cate.id) {
                return (
                  <div
                    className={
                      !activeCategory ? `${styles.itemcate} ${styles.active}` : styles.itemcate
                    }
                    onClick={() => {
                      // props.onChangeCategory(cate);
                    }}>
                    TẤT CẢ DANH MỤC
                  </div>
                );
              }
              return (
                <div
                  className={
                    cate.id === activeCategory?.id
                      ? `${styles.itemcate} ${styles.active}`
                      : styles.itemcate
                  }>
                  {cate.name}
                </div>
              );
            }}
          />

          <div className={styles.list}>
            {News.map((n) => {
              return (
                <div key={n.name} className={styles.item}>
                  <div className={styles.icon}>
                    <Link href="/app/statistics/news/slug" passHref>
                      <a>
                        <Image
                          src={Icon}
                          alt={DEFAULT_IMG_ALT}
                          objectFit="contain"
                          width={13}
                          height={3}
                        />
                      </a>
                    </Link>
                  </div>
                  <div className={styles.image}>
                    <Image
                      src={n.img}
                      alt={DEFAULT_IMG_ALT}
                      objectFit="cover"
                      width={380}
                      height={182}
                    />
                  </div>
                  <div className={styles.cate}> {n.category}</div>
                  <div className={styles.name}> {n.name}</div>
                  <div className={styles.view}>
                    <span>{n.view} lượt xem • </span>
                    {n.time} tiếng trước
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </AppLayout>
    </main>
  );
};

export default NewsPage;
