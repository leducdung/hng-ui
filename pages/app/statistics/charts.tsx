import AppLayout from "@Components/AppLayout";
import ComingSoon from "@Components/ComingSoon";
import { NextPage } from "next";
import React from "react";

const ChartPage: NextPage = () => {
  return (
    <main className="hng-app">
      <AppLayout>
        <ComingSoon />
      </AppLayout>
    </main>
  );
};

export default ChartPage;
