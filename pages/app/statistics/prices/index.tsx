import AppLayout from "@Components/AppLayout";
import styles from "./Prices.module.scss";
import { Grid } from "@material-ui/core";
import { DEFAULT_IMG_ALT } from "@Constants";
import { NextPage } from "next";
import React from "react";
import { PriceData } from "mock";
import Image from "next/image";
import IconDecre from "../../../../public/images/icon/ic_decre.png";
import IconIncre from "../../../../public/images/icon/ic_incre.png";

const PricingPage: NextPage = () => {
  return (
    <main className="hng-app">
      <AppLayout>
        <div className={styles.pricesApp}>
          <div className={styles.title}>GIÁ VÀNG TRONG NƯỚC</div>
          {PriceData.map((p) => {
            const isDecre = p.name === "PNJ";
            return (
              <Grid
                container
                key={p.name}
                justify="center"
                alignItems="center"
                className={styles.item}>
                <Grid item lg={3}>
                  <div className={styles.name}>{p.name}</div>
                </Grid>
                <Grid item lg={3} className={styles.datetime}>
                  {p.date} , {p.time}
                </Grid>
                <Grid item lg={3} className={styles.price}>
                  {p.buy.price}
                  <span className={isDecre ? styles.decre : styles.incre}>
                    &emsp;
                    {isDecre ? (
                      <Image src={IconDecre} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                    ) : (
                      <Image src={IconIncre} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                    )}
                    {p.buy.num}
                  </span>
                </Grid>
                <Grid item lg={3} className={styles.price}>
                  {p.sell.price}
                  <span className={isDecre ? styles.decre : styles.incre}>
                    &emsp;
                    {isDecre ? (
                      <Image src={IconDecre} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                    ) : (
                      <Image src={IconIncre} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                    )}
                    {p.sell.num}
                  </span>
                </Grid>
              </Grid>
            );
          })}
        </div>
      </AppLayout>
    </main>
  );
};

export default PricingPage;
