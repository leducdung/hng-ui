import AppLayout from "@Components/AppLayout";
import { NextPage } from "next";
import React from "react";
import { DEFAULT_IMG_ALT } from "@Constants";
import { NewsOverview, GoldPrice, Times } from "mock";
import { BtnGroup } from "@Components";
import styles from "./Statistics.module.scss";
import Image from "next/image";
import IconDecre from "../../../public/images/icon/ic_decre.png";
import IconIncre from "../../../public/images/icon/ic_incre.png";
import Icon from "../../../public/images/mock/news/icon.png";
import ImgChart from "../../../public/images/mock/news/chart.png";

export type NewItem = {
  id: string;
  img: StaticImageData;
  name: string;
  category: string;
  time: string;
  view: string;
};

const StatisticsPage: NextPage = () => {
  return (
    <main className="hng-app">
      <AppLayout>
        <div className={styles.statistics}>
          <div className={styles.top}>
            <div className={styles.left}>
              <div className={styles.group}>
                <div className={styles.text}>GIÁ VÀNG TRONG NƯỚC</div>
                <div className={styles.textcolor}>Xem chi tiết &gt;</div>
              </div>
              <div className={styles.table}>
                <div className={styles.row}>
                  <div>LV • x1000/LƯỢNG</div>
                  <div className={styles.groupprice}>
                    <div>&nbsp;&nbsp;&nbsp;&nbsp;MUA</div>
                    <div>BÁN&ensp;</div>
                  </div>
                </div>
                {GoldPrice.map((g) => {
                  const isDecre = g.name === "SJC";
                  return (
                    <div key={g.name} className={styles.row}>
                      <div className={styles.name}>{g.name}</div>
                      <div className={styles.groupprice}>
                        <div className={styles.price}>
                          {g.buy.price}
                          <br />
                          <span className={isDecre ? styles.decre : styles.incre}>
                            {isDecre ? (
                              <Image src={IconDecre} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                            ) : (
                              <Image src={IconIncre} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                            )}
                            {g.buy.num}
                          </span>
                        </div>
                        <div className={styles.price}>
                          {g.sell.price}
                          <br />
                          <span className={isDecre ? styles.decre : styles.incre}>
                            {isDecre ? (
                              <Image src={IconDecre} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                            ) : (
                              <Image src={IconIncre} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                            )}
                            {g.sell.num}
                          </span>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
            <div className={styles.right}>
              <div className={styles.text}>BIỂU ĐỒ VÀNG</div>
              <div className={styles.imagechart}>
                <Image src={ImgChart} alt={DEFAULT_IMG_ALT} objectFit="contain" />
              </div>
            </div>
          </div>
          <div className={styles.bottom}>
            <div className={styles.text}>TIN NỔI BẬT</div>
            <BtnGroup<NewItem>
              list={NewsOverview}
              renderBtnLabel={(n) => {
                return (
                  <div key={n.id} className={styles.item}>
                    <div className={styles.icon}>
                      <Image
                        src={Icon}
                        alt={DEFAULT_IMG_ALT}
                        objectFit="contain"
                        width={13}
                        height={3}
                      />
                    </div>
                    <div className={styles.image}>
                      <Image
                        src={n.img}
                        alt={DEFAULT_IMG_ALT}
                        objectFit="cover"
                        height={200}
                        width={306}
                      />
                    </div>
                    <div className={styles.cate}>{n.category}</div>
                    <div className={styles.name}>{n.name}</div>
                    <div className={styles.view}>
                      <span>{n.view} lượt xem • </span>
                      {n.time} tiếng trước
                    </div>
                  </div>
                );
              }}
            />
          </div>
          <div className={styles.sort}>
            {Times.map((t) => {
              return (
                <div key={t.time} className={styles.textTime}>
                  {t.time}
                </div>
              );
            })}
          </div>
        </div>
      </AppLayout>
    </main>
  );
};

export default StatisticsPage;
