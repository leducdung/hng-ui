import AppLayout from "@Components/AppLayout";
import { FormUIUtil } from "@Utils";
import { NextPage } from "next";
import React from "react";
import { Grid, InputAdornment } from "@material-ui/core";
import { Field, Form } from "react-final-form";
import { TextField } from "@material-ui/core";
import { LeftNavSettings } from "@Components";
import { MuiPickersUtilsProvider, DatePicker } from "@material-ui/pickers";
import { ArrowDropDown as ArrowDropDownIcon } from "@material-ui/icons";
import DateFnsUtils from "@date-io/date-fns";
import styles from "./Settings.module.scss";

const SettingPage: NextPage = () => {
  return (
    <main className="hng-app">
      <AppLayout>
        <div className={styles.setting}>
          <Grid container>
            <Grid item lg={4}>
              <LeftNavSettings />
            </Grid>
            <Grid item lg={8} className={styles.right}>
              <div className={styles.title}>Hồ sơ cá nhân</div>
              <Form
                initialValuesEqual={() => true}
                onSubmit={async (values) => {
                  console.log(values);
                }}
                render={({ handleSubmit, form }) => {
                  return (
                    <form onSubmit={handleSubmit}>
                      <Grid container spacing={2}>
                        <Grid item lg={6}>
                          <Field name="name">
                            {({ input, meta, ...rest }) => {
                              return FormUIUtil.renderFormItem(
                                "Họ tên",
                                <TextField
                                  {...input}
                                  {...rest}
                                  fullWidth
                                  placeholder=""
                                  className={styles.formtextfield}
                                  inputProps={{ className: "input" }}
                                  variant="outlined"
                                  onChange={(e) => input.onChange(e.target.value)}
                                  helperText={meta.touched ? meta.error : ""}
                                  error={meta.error && meta.touched}
                                />
                              );
                            }}
                          </Field>

                          <Field name="phoneNumber">
                            {({ input, meta, ...rest }) => {
                              return FormUIUtil.renderFormItem(
                                "Số điện thoại",
                                <TextField
                                  {...input}
                                  {...rest}
                                  fullWidth
                                  placeholder=""
                                  className={styles.formtextfield}
                                  inputProps={{ className: "input" }}
                                  variant="outlined"
                                  onChange={(e) => input.onChange(e.target.value)}
                                  helperText={meta.touched ? meta.error : ""}
                                  error={meta.error && meta.touched}
                                />
                              );
                            }}
                          </Field>
                        </Grid>
                        <Grid item lg={6}>
                          <Field name="email">
                            {({ input, meta, ...rest }) => {
                              return FormUIUtil.renderFormItem(
                                "Email",
                                <TextField
                                  {...input}
                                  {...rest}
                                  fullWidth
                                  placeholder=""
                                  className={styles.formtextfield}
                                  inputProps={{ className: "input" }}
                                  variant="outlined"
                                  onChange={(e) => input.onChange(e.target.value)}
                                  helperText={meta.touched ? meta.error : ""}
                                  error={meta.error && meta.touched}
                                />
                              );
                            }}
                          </Field>

                          <Field name="birthDate">
                            {({ input, meta }) => {
                              return FormUIUtil.renderFormItem(
                                "Ngày sinh",
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                  <DatePicker
                                    inputVariant="outlined"
                                    value={input.value}
                                    onChange={(value) => {
                                      if (value) {
                                        input.onChange(value.toISOString());
                                      }
                                    }}
                                    fullWidth
                                    format="dd-MM-yyyy"
                                    className={styles.formtextfield}
                                    inputProps={{ className: "input" }}
                                    InputProps={{
                                      endAdornment: (
                                        <InputAdornment position="end">
                                          <ArrowDropDownIcon
                                            style={{ fontSize: "2.2rem", color: "#D8D8D8" }}
                                          />
                                        </InputAdornment>
                                      ),
                                    }}
                                    helperText={meta.touched ? meta.error : ""}
                                    error={meta.error && meta.touched}
                                  />
                                </MuiPickersUtilsProvider>
                              );
                            }}
                          </Field>
                        </Grid>
                      </Grid>
                      <Field name="address">
                        {({ input, meta, ...rest }) => {
                          return FormUIUtil.renderFormItem(
                            "Địa chỉ",
                            <TextField
                              {...input}
                              {...rest}
                              fullWidth
                              placeholder=""
                              className={styles.formtextfield}
                              inputProps={{ className: "input" }}
                              variant="outlined"
                              onChange={(e) => input.onChange(e.target.value)}
                              helperText={meta.touched ? meta.error : ""}
                              error={meta.error && meta.touched}
                            />
                          );
                        }}
                      </Field>
                      <button className={styles.btn}>lưu thay đổi</button>
                    </form>
                  );
                }}
              />
            </Grid>
          </Grid>
        </div>
      </AppLayout>
    </main>
  );
};

export default SettingPage;
