import AppLayout from "@Components/AppLayout";
import { NextPage } from "next";
import React from "react";
import { Grid } from "@material-ui/core";
import { LeftNavSettings } from "@Components";
import styles from "./Settings.module.scss";

const PaymentPage: NextPage = () => {
  return (
    <main className="hng-app">
      <AppLayout>
        <div className={styles.setting}>
          <Grid container>
            <Grid item lg={4}>
              <LeftNavSettings />
            </Grid>
            <Grid item lg={8} className={styles.right}>
              <div className={styles.title}>Thanh toán</div>
              <div className={styles.bank}>
                <div className={styles.title}>LIÊN KẾT NGÂN HÀNG</div>
                <div className={styles.item}>+ Thêm tài khoản</div>
                <div className={styles.title}>LIÊN KẾT VÍ ĐIỆN TỬ</div>
                <div className={styles.item}>+ Thêm ví</div>
              </div>
            </Grid>
          </Grid>
        </div>
      </AppLayout>
    </main>
  );
};

export default PaymentPage;
