import AppLayout from "@Components/AppLayout";
import { NextPage } from "next";
import React, { useState } from "react";
import { IconButton, InputAdornment, TextField, Grid } from "@material-ui/core";
import { Field, Form } from "react-final-form";
import { LeftNavSettings } from "@Components";
import {
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from "@material-ui/icons";
import { FormUIUtil } from "@Utils";
import styles from "./Settings.module.scss";

const PasswordPage: NextPage = () => {
  const [visiblePassword, setVisiblePassword] = useState<boolean>(false);
  const [visibleConfirmPassword, setVisibleConfirmPassword] = useState<boolean>(false);
  const [visibleNewPassword, setVisibleNewPassword] = useState<boolean>(false);

  const toggleVisiblePassword = () => {
    setVisiblePassword(!visiblePassword);
  };

  const toggleVisibleConfirmPassword = () => {
    setVisibleConfirmPassword(!visibleConfirmPassword);
  };

  const toggleVisibleNewPassword = () => {
    setVisibleNewPassword(!visibleNewPassword);
  };

  return (
    <main className="hng-app">
      <AppLayout>
        <div className={styles.setting}>
          <Grid container>
            <Grid item lg={4}>
              <LeftNavSettings />
            </Grid>
            <Grid item lg={8} className={styles.right}>
              <div className={styles.title}>đổi Mật Khẩu</div>
              <Form
                initialValuesEqual={() => true}
                onSubmit={async (values) => {
                  console.log(values);
                }}
                render={({ handleSubmit, form }) => {
                  return (
                    <form onSubmit={handleSubmit}>
                      <Grid container spacing={2}>
                        <Grid item lg={6}>
                          <Field name="password">
                            {({ input, meta, ...rest }) => {
                              return FormUIUtil.renderFormItem(
                                "Mật khẩu hiện tại",
                                <TextField
                                  {...input}
                                  {...rest}
                                  fullWidth
                                  autoComplete="password"
                                  placeholder={"Mật khẩu"}
                                  className={styles.formtextfield}
                                  inputProps={{ className: "input" }}
                                  variant="outlined"
                                  onChange={(e) => input.onChange(e.target.value)}
                                  helperText={meta.touched ? meta.error : ""}
                                  error={meta.error && meta.touched}
                                  InputProps={{
                                    type: visiblePassword ? "text" : "password",
                                    endAdornment: (
                                      <InputAdornment position="end">
                                        <IconButton onClick={toggleVisiblePassword}>
                                          {visiblePassword ? (
                                            <VisibilityIcon
                                              style={{ fontSize: "2rem", color: "#959595" }}
                                            />
                                          ) : (
                                            <VisibilityOffIcon
                                              style={{ fontSize: "2rem", color: "#959595" }}
                                            />
                                          )}
                                        </IconButton>
                                      </InputAdornment>
                                    ),
                                  }}
                                />
                              );
                            }}
                          </Field>
                          <Field name="confirmPassword">
                            {({ input, meta, ...rest }) => {
                              return FormUIUtil.renderFormItem(
                                "Xác thực mật khẩu",
                                <TextField
                                  {...input}
                                  {...rest}
                                  fullWidth
                                  autoComplete="confirm-password"
                                  placeholder={"Xác thực mật khẩu"}
                                  className={styles.formtextfield}
                                  inputProps={{ className: "input" }}
                                  variant="outlined"
                                  onChange={(e) => input.onChange(e.target.value)}
                                  helperText={meta.touched ? meta.error : ""}
                                  error={meta.error && meta.touched}
                                  InputProps={{
                                    type: visibleConfirmPassword ? "text" : "password",
                                    endAdornment: (
                                      <InputAdornment position="end">
                                        <IconButton onClick={toggleVisibleConfirmPassword}>
                                          {visibleConfirmPassword ? (
                                            <VisibilityIcon
                                              style={{ fontSize: "2rem", color: "#959595" }}
                                            />
                                          ) : (
                                            <VisibilityOffIcon
                                              style={{ fontSize: "2rem", color: "#959595" }}
                                            />
                                          )}
                                        </IconButton>
                                      </InputAdornment>
                                    ),
                                  }}
                                />
                              );
                            }}
                          </Field>
                        </Grid>
                        <Grid item lg={6}>
                          <Field name="newPassword">
                            {({ input, meta, ...rest }) => {
                              return FormUIUtil.renderFormItem(
                                "Mật khẩu mới",
                                <TextField
                                  {...input}
                                  {...rest}
                                  fullWidth
                                  autoComplete="new-password"
                                  placeholder={"Mật khẩu mới"}
                                  className={styles.formtextfield}
                                  inputProps={{ className: "input" }}
                                  variant="outlined"
                                  onChange={(e) => input.onChange(e.target.value)}
                                  helperText={meta.touched ? meta.error : ""}
                                  error={meta.error && meta.touched}
                                  InputProps={{
                                    type: visibleConfirmPassword ? "text" : "password",
                                    endAdornment: (
                                      <InputAdornment position="end">
                                        <IconButton onClick={toggleVisibleNewPassword}>
                                          {visibleConfirmPassword ? (
                                            <VisibilityIcon
                                              style={{ fontSize: "2rem", color: "#959595" }}
                                            />
                                          ) : (
                                            <VisibilityOffIcon
                                              style={{ fontSize: "2rem", color: "#959595" }}
                                            />
                                          )}
                                        </IconButton>
                                      </InputAdornment>
                                    ),
                                  }}
                                />
                              );
                            }}
                          </Field>
                        </Grid>
                      </Grid>

                      <button className={styles.btn}>ĐỔI MẬT KHẨU</button>
                    </form>
                  );
                }}
              />
            </Grid>
          </Grid>
        </div>
      </AppLayout>
    </main>
  );
};

export default PasswordPage;
