import AppLayout from "@Components/AppLayout";
import { NextPage } from "next";
import React from "react";
import { Grid } from "@material-ui/core";
import { LeftNavSettings, ComingSoon } from "@Components";

const BrokerPage: NextPage = () => {
  return (
    <main className="hng-app">
      <AppLayout>
        <div className="setting">
          <Grid container>
            <Grid item lg={4}>
              <LeftNavSettings />
            </Grid>
            <Grid item lg={8} className="right">
              <ComingSoon />
            </Grid>
          </Grid>
        </div>
      </AppLayout>
    </main>
  );
};

export default BrokerPage;
