import AppLayout from "@Components/AppLayout";
import { NextPage } from "next";
import React from "react";
import { Grid } from "@material-ui/core";
import { AccuracyStepper } from "@Components";
import { DEFAULT_IMG_ALT } from "@Constants";
import Link from "next/link";
import styles from "./Private.module.scss";
import Image from "next/image";
import Img from "../../../../public/images/icon/imgid.png";
import IconCame from "../../../../public/images/icon/ic_came.png";
import IconFile from "../../../../public/images/icon/ic_file.png";

const VerifyPage: NextPage = () => {
  return (
    <main className="hng-app">
      <AppLayout>
        <div className={styles.verify}>
          <Grid container>
            <Grid item lg={4} className={styles.left}>
              <div className={styles.title}>XÁC THỰC TÀI KHOẢN</div>
              <div className={styles.des}>
                Để bắt đầu quá trình xác thực, vui lòng chuẩn bị giấy tờ tùy thân của bạn.
                <br />
                <br />( Thẻ căn cước, Hộ chiếu quốc tế )
              </div>
              <AccuracyStepper activeStep={1} />
            </Grid>
            <Grid item lg={8} className={styles.right}>
              <div className={styles.title}>Xác thực thông tin CMND / Căn cước / Hộ chiếu</div>
              <div className={styles.des}>
                Vui lòng chụp hai mặt trước,sau của thẻ để xác thực thông tin tài khoản
              </div>
              <Grid container spacing={2}>
                <Grid item lg={6}>
                  <div className={styles.text}>MẶT TRƯỚC</div>
                  <div className={styles.item}>
                    <Image src={Img} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                    <div className={styles.icon}>
                      <Image
                        src={IconCame}
                        alt={DEFAULT_IMG_ALT}
                        objectFit="contain"
                        width={54}
                        height={54}
                      />
                      <Image
                        src={IconFile}
                        alt={DEFAULT_IMG_ALT}
                        objectFit="contain"
                        width={54}
                        height={54}
                      />
                    </div>
                  </div>
                </Grid>
                <Grid item lg={6}>
                  <div className={styles.text}>MẶT SAU</div>
                  <div className={styles.item}>
                    <Image src={Img} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                    <div className={styles.icon}>
                      <Image
                        src={IconCame}
                        alt={DEFAULT_IMG_ALT}
                        objectFit="contain"
                        width={54}
                        height={54}
                      />
                      <Image
                        src={IconFile}
                        alt={DEFAULT_IMG_ALT}
                        objectFit="contain"
                        width={54}
                        height={54}
                      />
                    </div>
                  </div>
                </Grid>
              </Grid>
              <Link href={"/app/settings/private/avata"} passHref>
                <a>
                  <button className={styles.btn}>tiếp tục</button>
                </a>
              </Link>
            </Grid>
          </Grid>
        </div>
      </AppLayout>
    </main>
  );
};

export default VerifyPage;
