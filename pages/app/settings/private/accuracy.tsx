import AppLayout from "@Components/AppLayout";
import { NextPage } from "next";
import React, { useState } from "react";
import { Grid, Divider } from "@material-ui/core";
import { DEFAULT_IMG_ALT } from "@Constants";
import { AccuracyStepper } from "@Components";
import Link from "next/link";
import styles from "./Private.module.scss";
import Image from "next/image";
import Img from "../../../../public/images/icon/img-accuracy.png";
import Background from "../../../../public/images/icon/bg.png";
import Avata from "../../../../public/images/icon/imgavata.png";
import Icon1 from "../../../../public/images/icon/ic_y.png";
// import Icon2 from "../../../../public/images/icon/ic_n.png";

const AccuracyPage: NextPage = () => {
  const [visibleAccountVerified, setVisibleAccountVerified] = useState<boolean>(false);

  setTimeout(() => {
    setVisibleAccountVerified(true);
  }, 3000);

  return (
    <main className="hng-app">
      <AppLayout>
        <div className={styles.accuracy}>
          <Grid container>
            <Grid item lg={4} className={styles.left}>
              <div className={styles.title}>XÁC THỰC TÀI KHOẢN</div>
              <div className={styles.des}>
                Để bắt đầu quá trình xác thực, vui lòng chuẩn bị giấy tờ tùy thân của bạn.
                <br />
                <br />( Thẻ căn cước, Hộ chiếu quốc tế )
              </div>
              <AccuracyStepper activeStep={3} />
            </Grid>
            <Grid item lg={8} className={styles.right}>
              {!visibleAccountVerified ? (
                <>
                  <div className={styles.image}>
                    <Image
                      src={Img}
                      alt={DEFAULT_IMG_ALT}
                      objectFit="contain"
                      width={150}
                      height={150}
                    />
                  </div>
                  <div className={styles.title}>Hệ thống đang xác thực</div>
                  <div className={styles.des}>
                    HanaGold đang xác thực danh tính và thông tin mà bạn đã
                    <br />
                    cung cấp, quá trình có thể mất vài ngày. Chúng tôi sẽ gửi kết
                    <br />
                    quả đến bạn trong thời gian ngắn nhất
                  </div>
                </>
              ) : (
                <>
                  <div className={styles.accountverified}>
                    <div className={styles.avata}>
                      <Image
                        src={Background}
                        alt={DEFAULT_IMG_ALT}
                        objectFit="contain"
                        width={442}
                        height={95}
                      />
                      <div className={styles.image}>
                        <Image
                          src={Avata}
                          alt={DEFAULT_IMG_ALT}
                          objectFit="contain"
                          width={150}
                          height={150}
                        />
                      </div>
                      <div className={styles.icon}>
                        <Image
                          src={Icon1}
                          alt={DEFAULT_IMG_ALT}
                          objectFit="contain"
                          width={54}
                          height={54}
                        />
                      </div>
                    </div>
                    <div className={styles.title}>Tài khoản đã được xác thực</div>
                    <div>
                      <div className={styles.text}>
                        Nhận diện mặt<span className={styles.passed}>Passed</span>
                      </div>
                      <Divider
                        style={{
                          backgroundColor: "#D4D4D4",
                          width: "31.5rem",
                          marginTop: "0.6rem",
                          marginBottom: "0.9rem",
                          opacity: "0.5",
                        }}
                      />
                      <div className={styles.text}>
                        Thông tin trùng khớp<span className={styles.passed}>Passed</span>
                      </div>
                    </div>
                  </div>

                  {/* <div className={styles.accountverified}>
                      <div className={styles.avata}>
                        <div className={styles.image}>
                          <Image
                            src={Avata}
                            alt={DEFAULT_IMG_ALT}
                            objectFit="contain"
                            width={150}
                            height={150}
                          />
                        </div>
                        <div className={styles.icon}>
                           <Image
                            src={Icon2}
                            alt={DEFAULT_IMG_ALT}
                            objectFit="contain"
                            width={54}
                            height={54}
                          />
                        </div>
                      </div>
                      <div className={styles.title}>Tài khoản không thể xác thực</div>
                      <div>
                        <div className={styles.text}>Nhận diện mặt<span className={styles.failed}>Failed</span></div>
                        <Divider
                          style={{
                            backgroundColor: "#D4D4D4",
                            width: "31.5rem",
                            marginTop: "0.6rem",
                            marginBottom: "0.9rem",
                            opacity: "0.5",
                          }}
                        />
                        <div className={styles.text}>Thông tin trùng khớp<span className={styles.failed}>Failed</span></div>
                      </div>
                    </div> */}
                </>
              )}
              <Link href={"/app"} passHref>
                <a>
                  <button className={styles.btn}>về trang chủ</button>
                </a>
              </Link>
              {/* <Link href={styles./app/settings/private" passHref>
                    <a>
                      <button className={styles.btn}>XÁC THỰC LẠI TÀI KHOẢN</button>
                    </a>
                  </Link> */}
            </Grid>
          </Grid>
        </div>
      </AppLayout>
    </main>
  );
};

export default AccuracyPage;
