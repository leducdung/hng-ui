import AppLayout from "@Components/AppLayout";
import { Form, Field } from "react-final-form";
import { NextPage } from "next";
import React from "react";
import { Grid, Select, OutlinedInput, InputAdornment } from "@material-ui/core";
import { FormUIUtil } from "@Utils";
import { TextField } from "@material-ui/core";
import { AccuracyStepper } from "@Components";
import Link from "next/link";
import { MuiPickersUtilsProvider, DatePicker } from "@material-ui/pickers";
import { ArrowDropDown as ArrowDropDownIcon } from "@material-ui/icons";
import DateFnsUtils from "@date-io/date-fns";
import styles from "./Private.module.scss";

const PrivatePage: NextPage = () => {
  return (
    <main className="hng-app">
      <AppLayout>
        <div className={styles.private}>
          <Grid container>
            <Grid item lg={4} className={styles.left}>
              <div className={styles.title}>XÁC THỰC TÀI KHOẢN</div>
              <div className={styles.des}>
                Để bắt đầu quá trình xác thực, vui lòng chuẩn bị giấy tờ tùy thân của bạn.
                <br />
                <br />( Thẻ căn cước, Hộ chiếu quốc tế )
              </div>
              <AccuracyStepper activeStep={0} />
            </Grid>
            <Grid item lg={8} className={styles.right}>
              <div className={styles.title}>XÁC THỰC TÀI KHOẢN</div>
              <div className={styles.des}>
                Vui lòng cung cấp đầy đủ thông tin cá nhân của bạn để tiếp tục
              </div>

              <Form
                initialValuesEqual={() => true}
                onSubmit={async (values) => {
                  console.log(values);
                }}
                render={({ handleSubmit }) => {
                  return (
                    <form onSubmit={handleSubmit}>
                      <Grid container spacing={2}>
                        <Grid item lg={6}>
                          <Field name="nationality">
                            {({ input, meta, ...rest }) => {
                              return FormUIUtil.renderFormItem(
                                "Quốc tịch",
                                <Select
                                  {...input}
                                  {...rest}
                                  // native
                                  value={input.value}
                                  onChange={(e) => {
                                    input.onChange(e.target.value);
                                  }}
                                  error={meta.error && meta.touched}
                                  fullWidth
                                  input={<OutlinedInput className={styles.formtextfield} />}
                                  variant="outlined"
                                  // renderValue={(selected) => {
                                  // const selectedCity = LOCATIONS.find((city) => city.id === selected);

                                  // return selectedCity?.name;
                                  // }}

                                  className={styles.formselectfield}>
                                  {/* <MenuItem className="form-select-field-item">Việt Nam</MenuItem> */}
                                </Select>
                              );
                            }}
                          </Field>
                          <Field name="name">
                            {({ input, meta, ...rest }) => {
                              return FormUIUtil.renderFormItem(
                                "Họ tên",
                                <TextField
                                  {...input}
                                  {...rest}
                                  fullWidth
                                  placeholder={""}
                                  className={styles.formtextfield}
                                  inputProps={{ className: "input" }}
                                  variant="outlined"
                                  onChange={(e) => input.onChange(e.target.value)}
                                  helperText={meta.touched ? meta.error : ""}
                                  error={meta.error && meta.touched}
                                />
                              );
                            }}
                          </Field>
                          <Field name="birthDate">
                            {({ input, meta }) => {
                              return FormUIUtil.renderFormItem(
                                "Ngày sinh",
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                  <DatePicker
                                    inputVariant="outlined"
                                    value={input.value}
                                    onChange={(value) => {
                                      if (value) {
                                        input.onChange(value.toISOString());
                                      }
                                    }}
                                    fullWidth
                                    format="dd-MM-yyyy"
                                    className={styles.formtextfield}
                                    inputProps={{ className: "input" }}
                                    InputProps={{
                                      endAdornment: (
                                        <InputAdornment position="end">
                                          <ArrowDropDownIcon
                                            style={{ fontSize: "2.2rem", color: "#D8D8D8" }}
                                          />
                                        </InputAdornment>
                                      ),
                                    }}
                                    helperText={meta.touched ? meta.error : ""}
                                    error={meta.error && meta.touched}
                                  />
                                </MuiPickersUtilsProvider>
                              );
                            }}
                          </Field>
                        </Grid>
                        <Grid item lg={6}>
                          <Field name="cate">
                            {({ input, meta, ...rest }) => {
                              return FormUIUtil.renderFormItem(
                                "Loại",
                                <Select
                                  {...input}
                                  {...rest}
                                  // native
                                  value={input.value}
                                  onChange={(e) => {
                                    input.onChange(e.target.value);
                                  }}
                                  error={meta.error && meta.touched}
                                  fullWidth
                                  input={<OutlinedInput className={styles.formtextfield} />}
                                  variant="outlined"
                                  // renderValue={(selected) => {
                                  // const selectedCity = LOCATIONS.find((city) => city.id === selected);

                                  // return selectedCity?.name;
                                  // }}
                                  className={styles.formselectfield}>
                                  {/* {LOCATIONS.map((city) => (
                        <MenuItem key={city.id} value={city.id} className="form-select-field-item">
                          {city.name}
                        </MenuItem>
                      ))} */}
                                </Select>
                              );
                            }}
                          </Field>
                          <Field name="number">
                            {({ input, meta, ...rest }) => {
                              return FormUIUtil.renderFormItem(
                                "Mã số thẻ",
                                <TextField
                                  {...input}
                                  {...rest}
                                  fullWidth
                                  placeholder={""}
                                  className={styles.formtextfield}
                                  inputProps={{ className: "input" }}
                                  variant="outlined"
                                  onChange={(e) => input.onChange(e.target.value)}
                                  helperText={meta.touched ? meta.error : ""}
                                  error={meta.error && meta.touched}
                                />
                              );
                            }}
                          </Field>
                          <Field name="sex">
                            {({ input, meta, ...rest }) => {
                              return FormUIUtil.renderFormItem(
                                "Giới tính",
                                <Select
                                  {...input}
                                  {...rest}
                                  // native
                                  value={input.value}
                                  onChange={(e) => {
                                    input.onChange(e.target.value);
                                  }}
                                  error={meta.error && meta.touched}
                                  fullWidth
                                  input={<OutlinedInput className={styles.formtextfield} />}
                                  variant="outlined"
                                  // renderValue={(selected) => {
                                  // const selectedCity = LOCATIONS.find((city) => city.id === selected);

                                  // return selectedCity?.name;
                                  // }}
                                  className={styles.formselectfield}>
                                  {/* {LOCATIONS.map((city) => (
                        <MenuItem key={city.id} value={city.id} className="form-select-field-item">
                          {city.name}
                        </MenuItem>
                      ))} */}
                                </Select>
                              );
                            }}
                          </Field>
                        </Grid>
                      </Grid>
                      <Link href="/app/settings/private/verify" passHref>
                        <a>
                          <button className={styles.btn}>tiếp tục</button>
                        </a>
                      </Link>
                    </form>
                  );
                }}
              />
            </Grid>
          </Grid>
        </div>
      </AppLayout>
    </main>
  );
};

export default PrivatePage;
