import AppLayout from "@Components/AppLayout";
import { NextPage } from "next";
import React from "react";
import { Grid } from "@material-ui/core";
import { DEFAULT_IMG_ALT } from "@Constants";
import { AccuracyStepper } from "@Components";
import Link from "next/link";
import styles from "./Private.module.scss";
import Image from "next/image";
import Img from "../../../../public/images/icon/imgavata.png";
import IconCame from "../../../../public/images/icon/ic_came.png";
import IconFile from "../../../../public/images/icon/ic_file.png";

const AvataPage: NextPage = () => {
  return (
    <main className="hng-app">
      <AppLayout>
        <div className={styles.avata}>
          <Grid container>
            <Grid item lg={4} className={styles.left}>
              <div className={styles.title}>XÁC THỰC TÀI KHOẢN</div>
              <div className={styles.des}>
                Để bắt đầu quá trình xác thực, vui lòng chuẩn bị giấy tờ tùy thân của bạn.
                <br />
                <br />( Thẻ căn cước, Hộ chiếu quốc tế )
              </div>
              <AccuracyStepper activeStep={2} />
            </Grid>
            <Grid item lg={8} className={styles.right}>
              <div className={styles.title}>Chụp ảnh nhận diện mặt</div>
              <div className={styles.des}>Vui lòng chụp ảnh chân dung để xác thực tài khoản</div>
              <div className={styles.image}>
                <div className={styles.item}>
                  <Image src={Img} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                  <div className={styles.icon}>
                    <Image
                      src={IconCame}
                      alt={DEFAULT_IMG_ALT}
                      objectFit="contain"
                      width={54}
                      height={54}
                    />
                    <Image
                      src={IconFile}
                      alt={DEFAULT_IMG_ALT}
                      objectFit="contain"
                      width={54}
                      height={54}
                    />
                  </div>
                </div>
                <Link href={"/app/settings/private/accuracy"} passHref>
                  <a>
                    <button className={styles.btn}>tiếp tục</button>
                  </a>
                </Link>
              </div>
            </Grid>
          </Grid>
        </div>
      </AppLayout>
    </main>
  );
};

export default AvataPage;
