import AppLayout from "@Components/AppLayout";
import { Search as SearchIcon } from "@material-ui/icons";
import { NextPage } from "next";
import React from "react";
import { TextField, InputAdornment } from "@material-ui/core";
import { GoogleMap, LoadScript, InfoWindow, Circle } from "@react-google-maps/api";
import { INFO_STORES, DEFAULT_IMG_ALT } from "@Constants";
import { Branch } from "mock";
import styles from "./Stores.module.scss";
import Image from "next/image";
import Icon from "../../../public/images/icon/ic_location.png";
import Logo from "../../../public/images/branch/branch-logo.png";

const branchMarkerStyle = {
  strokeColor: "#fff",
  strokeWeight: 2,
  fillColor: "rgba(46, 169, 54, 0.4)",
  fillOpacity: 1,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  zIndex: 2,
};

const StoresPage: NextPage = () => {
  return (
    <main className="hng-app">
      <AppLayout>
        <div className={styles.stores}>
          <div className={styles.left}>
            <TextField
              fullWidth
              placeholder="Tìm cửa hàng"
              variant="outlined"
              className={styles.searchinput}
              inputProps={{ className: styles.input }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon style={{ fontSize: "2.2rem", color: "#9E9E9E" }} />
                  </InputAdornment>
                ),
              }}
            />
            <div className={styles.list}>
              {Branch.map((store, idx) => {
                return (
                  <article key={idx} className={styles.item}>
                    <div className={styles.image}>
                      <Image alt={DEFAULT_IMG_ALT} src={store.img} objectFit="contain" />
                    </div>
                    <div className={styles.location}>
                      <div className={styles.info}>
                        <div className={styles.name}>{store.name}</div>
                        <div className={styles.text}>{store.address}</div>
                      </div>
                      <Image alt={DEFAULT_IMG_ALT} src={Icon} objectFit="contain" />
                    </div>
                  </article>
                );
              })}
            </div>
          </div>
          <div className={styles.right}>
            <div className={styles.ggmap}>
              <LoadScript googleMapsApiKey="AIzaSyCxEoQrFNLnWW7xUJTbvwhgmg23F5fo7RQ">
                <GoogleMap
                  id={`branch-map`}
                  mapContainerClassName="branch-map"
                  mapContainerStyle={{ width: "100%", height: "100%" }}
                  center={INFO_STORES[0].coordinate}
                  zoom={10}>
                  {INFO_STORES.map((store, idx) => {
                    return (
                      <React.Fragment key={idx}>
                        <Circle center={store.coordinate} options={branchMarkerStyle} radius={10} />
                        {/* <Marker position={store.coordinate} options={branchMarkerStyle} /> */}
                        <InfoWindow position={store.coordinate}>
                          <div>
                            <Image src={Logo} alt={DEFAULT_IMG_ALT} height={43} width={17} />
                          </div>
                        </InfoWindow>
                      </React.Fragment>
                    );
                  })}
                </GoogleMap>
              </LoadScript>
            </div>
          </div>
        </div>
      </AppLayout>
    </main>
  );
};

export default StoresPage;
