import AppLayout from "@Components/AppLayout";
import { NextPage, GetServerSideProps } from "next";
import React from "react";
import { Divider } from "@material-ui/core";
import { DEFAULT_IMG_ALT } from "@Constants";
import { isValidToken, parseCookie, parseJwtToken } from "lib/authentication/cookie-service";
import { Store, StoreItem } from "mock";
// import Link from "next/link";
import { BtnGroup } from "@Components";
import styles from "./HomeApp.module.scss";
import Image from "next/image";
import Img1 from "../../public/images/hng-app/dongxu.png";
import Img2 from "../../public/images/hng-app/heo.png";
import IconView from "../../public/images/hng-app/Vector.png";
import Icon1 from "../../public/images/hng-app/ic_nap.png";
import Icon2 from "../../public/images/hng-app/ic_muavang.png";
import Icon3 from "../../public/images/hng-app/ic_rutvang.png";

const HanaGoldApp: NextPage = () => {
  return (
    <main className="hng-app">
      <AppLayout hasBackground>
        <div className={styles.homepage}>
          <div className={styles.table}>
            <div className={styles.item}>
              <div className={styles.icon}>
                <Image
                  alt={DEFAULT_IMG_ALT}
                  src={IconView}
                  width={28}
                  height={28}
                  objectFit="contain"
                />
              </div>

              <div className={styles.imagepig}>
                <Image alt={DEFAULT_IMG_ALT} src={Img1} objectFit="contain" />
              </div>
              <Divider
                style={{
                  width: "6.2rem",
                  backgroundColor: "#5A5A5A",
                  marginTop: "1.4rem",
                  marginBottom: "2.6rem",
                }}
              />
              <div className={styles.itemcontent}>
                <div className={styles.text}> Vàng hiện có</div>
                <div className={styles.price}>12 chỉ</div>
              </div>
            </div>
            <div className={styles.item}>
              <div className={styles.icon}>
                <Image
                  alt={DEFAULT_IMG_ALT}
                  src={IconView}
                  width={28}
                  height={28}
                  objectFit="contain"
                />
              </div>
              <div className={styles.imagepig}>
                <Image alt={DEFAULT_IMG_ALT} src={Img2} objectFit="contain" />
              </div>
              <Divider
                style={{
                  width: "6.2rem",
                  backgroundColor: "#5A5A5A",
                  marginTop: "1.4rem",
                  marginBottom: "2.6rem",
                }}
              />
              <div className={styles.itemcontent}>
                <div className={styles.text}> Tiền hiện có</div>
                <div className={styles.price}>124,000,000 đ</div>
              </div>
            </div>

            <div className={styles.right}>
              <div className={styles.itemicon}>
                <div className={styles.imageicon}>
                  <Image alt={DEFAULT_IMG_ALT} src={Icon1} />
                </div>
                Nạp tiền
              </div>
              <div className={styles.itemicon}>
                <div className={styles.imageicon}>
                  <Image alt={DEFAULT_IMG_ALT} src={Icon2} />
                </div>
                Mua vàng
              </div>
              {/* <Link href="/app/agency" passHref>
                <a> */}
              <div className={styles.itemicon}>
                <div className={styles.imageicon}>
                  <Image alt={DEFAULT_IMG_ALT} src={Icon3} />
                </div>
                rút vàng
              </div>
              {/* </a>
              </Link> */}
            </div>
          </div>
          <div className={styles.list}>
            <BtnGroup<StoreItem>
              list={Store}
              renderBtnLabel={(i) => {
                return (
                  <div key={i.id} className={styles.imageintroduction}>
                    <Image src={i.img} alt={DEFAULT_IMG_ALT} />
                  </div>
                );
              }}
            />
          </div>
        </div>
      </AppLayout>
    </main>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const cookie = parseCookie(req);
  const [token] = parseJwtToken(cookie.token);
  const [authenticated, data] = isValidToken(token);
  if (authenticated) {
    return {
      props: {
        user: data,
      },
    };
  }
  return {
    props: {},
    redirect: {
      statusCode: 302,
      destination: "/customer/signin",
    },
  };
};

export default HanaGoldApp;
