import React, { useState } from "react";
import { NextPage } from "next";
import Image from "next/image";
import Banner from "../../public/images/banner/bannernews.png";
import { DEFAULT_IMG_ALT } from "@Constants";
import styles from "./News.module.scss";
import { HtmlHeader, PageFooter, PageHeader } from "@Widgets";
import { BtnGroup } from "@Components";
import { HotNews, News, NewsCategory } from "mock";
import Link from "next/link";
import Icon from "../../public/images/mock/news/icon.png";
import { useBreakpoints } from "hooks";

export type CategoryItem = {
  id: string;
  name: string;
};
export type NewItem = {
  id: string;
  img: StaticImageData;
  name: string;
  category: string;
  time: string;
  view: string;
};
const NewsPage: NextPage = () => {
  const breakpoints = useBreakpoints();
  const [selectCategory, setSelectCategory] = useState<CategoryItem | null>(
    NewsCategory[""] ?? null
  );
  const activeCategory = NewsCategory.find((cate) => cate.id === selectCategory?.id);
  return (
    <>
      <HtmlHeader />
      <PageHeader />
      <div className={styles.news}>
        <div className={styles.banner}>
          <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="fill" objectFit="cover" />
          <div className={styles.content}>
            <div className={styles.title}>TIN TỨC</div>
            <div className={styles.text}> TIN NỔI BẬT</div>
            <div className={styles.para}>
              {breakpoints.lg ? (
                <>
                  <div className={styles.left}>
                    {HotNews.map((h) => {
                      if (h.id === "1")
                        return (
                          <Link key={h.name} href="/news/slug" passHref>
                            <a>
                              <div className={styles.image}>
                                <Image
                                  src={h.img}
                                  alt={DEFAULT_IMG_ALT}
                                  objectFit="cover"
                                  layout="fill"
                                />
                                <div className={styles.icon}>
                                  <Image
                                    src={Icon}
                                    alt={DEFAULT_IMG_ALT}
                                    objectFit="contain"
                                    width={13}
                                    height={3}
                                  />
                                </div>
                                <div className={styles.des}>
                                  <div className={styles.cate}> {h.category}</div>
                                  <div className={styles.name}> {h.name}</div>
                                  <div className={styles.view}>
                                    <span>{h.view} lượt xem • </span>
                                    {h.time} tiếng trước
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        );
                    })}
                  </div>
                  <div className={styles.right}>
                    {HotNews.slice(1, HotNews.length).map((h) => {
                      return (
                        <Link key={h.name} href="/news/slug" passHref>
                          <a>
                            <div className={styles.item}>
                              <div className={styles.image}>
                                <Image
                                  src={h.img}
                                  alt={DEFAULT_IMG_ALT}
                                  objectFit="cover"
                                  width={157}
                                  height={111}
                                />
                              </div>
                              <div className={styles.des}>
                                <div className={styles.cate}> {h.category}</div>
                                <div className={styles.name}> {h.name}</div>
                                <div className={styles.view}>
                                  <span>
                                    {h.view} lượt xem • {h.time} tiếng trước
                                  </span>
                                  <Image
                                    src={Icon}
                                    alt={DEFAULT_IMG_ALT}
                                    objectFit="contain"
                                    width={13}
                                    height={3}
                                  />
                                </div>
                              </div>
                            </div>
                          </a>
                        </Link>
                      );
                    })}
                  </div>
                </>
              ) : (
                <BtnGroup<NewItem>
                  list={HotNews}
                  renderBtnLabel={(n) => {
                    return (
                      <div className={styles.itemMobile}>
                        <div className={styles.image}>
                          <Image
                            src={n.img}
                            alt={DEFAULT_IMG_ALT}
                            objectFit="cover"
                            height={200}
                            width={306}
                          />
                          <Link key={n.id} href="/news/slug" passHref>
                            <a>
                              <div className={styles.icon}>
                                <Image
                                  src={Icon}
                                  alt={DEFAULT_IMG_ALT}
                                  objectFit="contain"
                                  width={13}
                                  height={3}
                                />
                              </div>
                            </a>
                          </Link>
                        </div>
                        <div className={styles.cate}>{n.category}</div>
                        <div className={styles.name}>{n.name}</div>
                        <div className={styles.view}>
                          <span>{n.view} lượt xem • </span>
                          {n.time} tiếng trước
                        </div>
                      </div>
                    );
                  }}
                />
              )}
            </div>
          </div>
        </div>
        <div className={styles.body}>
          <div className={styles.listCate}>
            <BtnGroup<CategoryItem>
              list={[{ id: "", name: "TẤT CẢ DANH MỤC" }, ...NewsCategory]}
              onClick={(cate) => {
                setSelectCategory(cate);
              }}
              renderBtnLabel={(cate) => {
                // return <div className="product-listing__list__category__item">{cate.name}</div>;

                if (!cate.id) {
                  return (
                    <div
                      className={
                        !activeCategory ? `${styles.itemcate} ${styles.active}` : styles.itemcate
                      }
                      onClick={() => {
                        // props.onChangeCategory(cate);
                      }}>
                      TẤT CẢ DANH MỤC
                    </div>
                  );
                }
                return (
                  <div
                    className={
                      cate.id === activeCategory?.id
                        ? `${styles.itemcate} ${styles.active}`
                        : styles.itemcate
                    }>
                    {cate.name}
                  </div>
                );
              }}
            />
          </div>
          <div className={styles.list}>
            {News.map((n) => {
              return (
                <Link href="/news/slug" passHref key={n.name}>
                  <a>
                    <div className={styles.item}>
                      <div className={styles.image}>
                        <Image src={n.img} alt={DEFAULT_IMG_ALT} objectFit="cover" layout="fill" />
                        {breakpoints.md && (
                          <div className={styles.icon}>
                            <Image
                              src={Icon}
                              alt={DEFAULT_IMG_ALT}
                              objectFit="contain"
                              width={13}
                              height={3}
                            />
                          </div>
                        )}
                      </div>
                      <div className={styles.des}>
                        <div className={styles.cate}> {n.category}</div>
                        <div className={styles.name}> {n.name}</div>
                        <div className={styles.view}>
                          <span>
                            {n.view} lượt xem • {n.time} tiếng trước
                          </span>

                          {!breakpoints.md && (
                            <Image
                              src={Icon}
                              alt={DEFAULT_IMG_ALT}
                              objectFit="contain"
                              width={13}
                              height={3}
                            />
                          )}
                        </div>
                      </div>
                    </div>
                  </a>
                </Link>
              );
            })}
          </div>
          <div className={styles.btn}>
            <button className={styles.submit}>Xem thêm</button>
          </div>
        </div>
      </div>
      <PageFooter withoutDivider />
    </>
  );
};

export default NewsPage;
