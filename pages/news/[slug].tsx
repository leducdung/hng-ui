import React from "react";
import { NextPage } from "next";
import { DEFAULT_IMG_ALT } from "@Constants";
import { HtmlHeader, PageFooter, PageHeader } from "@Widgets";
import { HotNews, RELATED_NEWS } from "mock";
import Link from "next/link";
import styles from "./NewDetail.module.scss";
import Image from "next/image";
import Icon from "../../public/images/icon/default.png";
import Img1 from "../../public/images/mock/news/5.png";
import Img2 from "../../public/images/mock/news/ndimg2.png";
import { Divider } from "@material-ui/core";
import {
  ArrowBackRounded as ArrowBackRoundedIcon,
  ShareOutlined as ShareOutlinedIcon,
} from "@material-ui/icons";
import { useBreakpoints } from "hooks";
import { BtnGroup } from "@Components";
import { FacebookIcon, InstagramIcon } from "@Components/Icons";
export type NewItem = {
  id: string;
  img: StaticImageData;
  name: string;
  category: string;
  time: string;
  view: string;
};
const NewDetailPage: NextPage = () => {
  const breakpoints = useBreakpoints();
  return (
    <>
      <HtmlHeader />
      <PageHeader />
      <div className={styles.newdetail}>
        {!breakpoints.lg && (
          <Link href="/news" passHref>
            <a>
              <div className={styles.link}>
                <span>
                  <ArrowBackRoundedIcon style={{ fontSize: "2.2rem" }} />
                  &ensp;Tin Tức
                </span>
                <ShareOutlinedIcon style={{ fontSize: "2.2rem" }} />
              </div>
            </a>
          </Link>
        )}
        <div className={styles.content}>
          <div className={styles.left}>
            <div className={styles.leftTop}>
              <div className={styles.image}>
                <Image src={Img1} alt={DEFAULT_IMG_ALT} width={753} height={420} />
              </div>
              <div>
                <div className={styles.cate}>Danh mục 1</div>
                <div className={styles.name}>
                  {breakpoints.lg && (
                    <Link href="/news" passHref>
                      <a>
                        <Image src={Icon} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                        &ensp;
                      </a>
                    </Link>
                  )}
                  So sánh nhẫn cưới bạch kim với nhẫn cưới làm bằng vàng trắng
                </div>
                <div className={styles.view}>30 lượt xem • 2 tiếng trước</div>
              </div>
            </div>
            {!breakpoints.lg && (
              <Divider
                style={{ marginTop: "1.3rem", marginBottom: "1.2rem", backgroundColor: "#373737" }}
              />
            )}
            <div className={styles.text}>
              Tuy cùng có màu trắng nhưng chất liệu, giá cả hai loại nhẫn cưới bạch kim và vàng
              trắng đều khác nhau.
              <br />
              <br />
              Xu hướng chọn nhẫn cưới màu trắng được các cặp uyên ương hiện nay ưa chuộng. Vì sắc
              màu trắng dễ kết hợp quần áo, phù hợp với đa số nước da cũng như bàn tay của cô dâu
              chú rể. Uyên ương có thể cân nhắc chọn giữa nhẫn cưới bạch kim và nhẫn vàng trắng. Mỗi
              loại nhẫn đều có những đặc điểm nhất định, trong đó nhẫn bạch kim cao cấp hơn nhẫn làm
              từ vàng trắng.
              <br /> <br />
              1. Kim loại vàng và bạch kim
              <br />
              <br />- Nhẫn bạch kim (hay còn gọi là nhẫn Platinum) được làm từ bạch kim nguyên chất,
              không pha thêm các kim loại khác vào khi gia công. <br />- Nhẫn vàng trắng (còn gọi là
              nhẫn White Gold) cũng có màu trắng sáng như bạch kim, nhưng được làm từ hợp kim vàng
              với một số kim loại quý khác nhau, sau đó được phủ lên một lớp kim loại Rhodium để tạo
              vẻ sáng bóng.
            </div>
            <div className={styles.image}>
              <Image src={Img2} alt={DEFAULT_IMG_ALT} width={749} height={275} />
            </div>
            <div className={styles.text}>
              2. Độ cứng và độ nặng
              <br />
              <br />
              - Nhẫn bạch kim có khối lượng cao hơn khoảng 50% so với nhẫn vàng trắng vì độ tinh
              chất cao, tỷ trọng lớn.
              <br />
              <br />- Nhẫn bạch kim thường ít bị méo hay biến dạng theo thời gian, va đập vì bạch
              kim cứng hơn vàng trắng.
              <br />
              <br />
              <br />
            </div>
            <div className={styles.group}>
              <div>
                <span className={styles.tag}>#vàng trắng</span>
                <span className={styles.tag}>#bạch kim</span>
              </div>
              {breakpoints.lg && (
                <div className={styles.groupIcon}>
                  <span className={styles.icon}>
                    <FacebookIcon size={[9, 17]} viewBox={[9, 17]} color="#6C778D" />
                  </span>
                  <span className={styles.icon}>
                    <InstagramIcon size={[17, 17]} viewBox={[17, 17]} color="#6C778D" />
                  </span>
                </div>
              )}
            </div>
          </div>
          {!breakpoints.lg && (
            <Divider
              style={{ marginTop: "3.6rem", marginBottom: "3.2rem", backgroundColor: "#373737" }}
            />
          )}
          <div className={styles.right}>
            <div className={styles.titleRelated}>
              TIN LIÊN QUAN
              {!breakpoints.lg && (
                <Link href="/news" passHref>
                  <a>Xem tất cả &gt;</a>
                </Link>
              )}
            </div>
            {breakpoints.lg ? (
              <>
                {RELATED_NEWS.map((r) => {
                  return (
                    <div key={r.name} className={styles.item}>
                      <div>
                        <div className={styles.name}> {r.name}</div>
                        <div className={styles.view}>
                          <span>{r.view} lượt xem • </span>
                          {r.time} tiếng trước
                        </div>
                      </div>
                      <div className={styles.image}>
                        <Image src={r.img} alt={DEFAULT_IMG_ALT} width={90} height={90} />
                      </div>
                    </div>
                  );
                })}
              </>
            ) : (
              <BtnGroup<NewItem>
                list={HotNews}
                renderBtnLabel={(n) => {
                  return (
                    <div className={styles.itemMobile}>
                      <div className={styles.image}>
                        <Image
                          src={n.img}
                          alt={DEFAULT_IMG_ALT}
                          objectFit="cover"
                          height={120}
                          width={164}
                        />
                      </div>

                      <div className={styles.name}>{n.name}</div>
                    </div>
                  );
                }}
              />
            )}
          </div>
        </div>
      </div>
      <PageFooter withoutDivider />
    </>
  );
};

export default NewDetailPage;
