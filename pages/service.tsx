import React from "react";
import { ServicePage } from "../widgets";
import { NextSeo } from "next-seo";
import Head from "next/head";
import { useImgCDN } from "lib/url-generator";

const AboutUsPage = () => {
  return (
    <React.Fragment>
      <NextSeo
        title="HanaGold - Dịch Vụ"
        description="Mua Bán, Chuyển Đổi, Hợp Tác Kinh Doanh, Tích Luỹ, Cầm Đồ"
        canonical="https://hanagold.unibiz.io/service"
        openGraph={{
          title: "HanaGold - Dịch Vụ",
          description:
            "HanaGold là nền tảng kết nối tiệm kim hoàn và người đi mua vàng, tại đó người dùng có thể chọn lựa mẫu mã, mua vàng tích lũy online.",
          url: "https://hanagold.unibiz.io",
          type: "website",
          images: [
            {
              url: `${useImgCDN("/images/aboutus/img3.png")}`,
              alt: "Hana Gold Logo",
            },
          ],
        }}
      />
      <Head>
        <meta property="og:image:secure_url" content={`${useImgCDN("/images/aboutus/img3.png")}`} />
        <meta property="og:image:type" content="image/png" />
      </Head>
      <ServicePage />
    </React.Fragment>
  );
};

export default AboutUsPage;
