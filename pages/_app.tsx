import React from "react";
import "../styles/globals.scss";
import { AppProps /* AppContext */ } from "next/app";
// import App from "next/app";
import { useStore } from "../redux/store";
import theme from "../styles/theme";
import { ThemeProvider } from "@material-ui/core/styles";
import { Provider } from "react-redux";
import { useApollo } from "lib/apollo-client";
import { ApolloProvider } from "@apollo/client";
//import { createUploadLink } from "apollo-upload-client"

// const blackList = [
//   "Opera",
//   "OPR",
//   "Opera Mini",
//   "Opera/9.80 (Android; Opera Mini/12.0.1987/37.7327; U; pl) Presto/2.12.423 Version/12.16",
//   "Mozilla/5.0 (Linux; U; Android 8.1.0; zh-CN; EML-AL00 Build/HUAWEIEML-AL00) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.108 baidu.sogo.uc.UCBrowser/11.9.4.974 UWS/2.13.1.48 Mobile Safari/537.36 AliApp(DingTalk/4.5.11) com.alibaba.android.rimet/10487439 Channel/227200 language/zh-CN",
//   "Mozilla/5.0 (Linux; U; Android 6.0.1; zh-CN; F5121 Build/34.0.A.1.247) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/40.0.2214.89 UCBrowser/11.5.1.944 Mobile Safari/537.36",
//   "Mozilla/5.0 (Linux; U; Android 4.4.2; zh-CN; HUAWEI MT7-TL00 Build/HuaweiMT7-TL00) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/40.0.2214.89 UCBrowser/11.3.8.909 Mobile Safari/537.36",
//   "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)",
//   "Mozilla/5.0 (Windows NT 10.0; Trident/7.0; rv:11.0) like Gecko",
//   "Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-G960U) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/10.2 Chrome/71.0.3578.99 Mobile Safari/537.36",
//   "Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-G960U) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/10.1 Chrome/71.0.3578.99 Mobile Safari/537.36",
//   "Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-G950U) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/10.2 Chrome/71.0.3578.99 Mobile Safari/537.36",
// ];

const WrappedApp = ({ Component, pageProps }: AppProps) => {
  //   if (pageProps.error) {
  //     return <Error statusCode={pageProps.error.statusCode} title={pageProps.error.message} />;
  //   }
  const store = useStore(pageProps.initialReduxState);
  const client = useApollo(pageProps.initialApolloState);
  // const client = new ApolloClient({
  //   cache: new InMemoryCache(),
  //   link: createUploadLink({ uri: process.env.NEXT_PUBLIC_APOLLO_API_ENDPOINT + "/graphql" }),
  // });
  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement?.removeChild(jssStyles);
    }
  }, []);
  return (
    <ApolloProvider client={client}>
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <Component {...pageProps} />
        </ThemeProvider>
      </Provider>
    </ApolloProvider>
  );
};

// WrappedApp.getInitialProps = async (appContext: AppContext) => {
//   const appProps = await App.getInitialProps(appContext);
//   const userAgent = appContext.ctx.req?.headers["user-agent"];
//   console.log(userAgent)
//   if (
//     userAgent &&
//     blackList.some((item) => userAgent.includes(item)) &&
//     appContext.router.pathname !== "/not-supported"
//   ) {
//     appContext.ctx.res?.writeHead(302, {
//       location: "/not-supported",
//     });
//     appContext.ctx.res?.end();
//   }
//   return { ...appProps };
// };

export default WrappedApp;
