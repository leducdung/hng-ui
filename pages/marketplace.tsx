import React from "react";
import { AgencyPage } from "../widgets";
import { NextSeo } from "next-seo";
import Head from "next/head";
import { useImgCDN } from "lib/url-generator";

const Agency = () => {
  return (
    <React.Fragment>
      <NextSeo
        title="HanaGold - Đồng vàng HanaGold 24k"
        description="CƠ HỘI TRỞ THÀNH CHỦ TIỆM KIM HOÀN 4.0"
        canonical="https://hanagold.unibiz.io/marketplace"
        openGraph={{
          title: "HanaGold - Đồng vàng HanaGold 24k",
          description: "CƠ HỘI TRỞ THÀNH CHỦ TIỆM KIM HOÀN 4.0",
          url: "https://hanagold.unibiz.io/marketplace",
          type: "website",
          images: [
            {
              url: `${useImgCDN("/images/banner/banner1.png")}`,
              alt: "HanaGold Đại Lý",
            },
          ],
        }}
      />
      <Head>
        <meta
          property="og:image:secure_url"
          content={`${useImgCDN("/images/banner/banner1.png")}`}
        />
        <meta property="og:image:type" content="image/png" />
      </Head>
      <AgencyPage />
    </React.Fragment>
  );
};

export default Agency;
