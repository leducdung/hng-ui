import React, { useEffect, useState } from "react";
import { Field, Form } from "react-final-form";
import { TextField, Checkbox, InputAdornment, IconButton } from "@material-ui/core";
import { NextPage, GetServerSideProps } from "next";
import { DEFAULT_IMG_ALT, APP_ROUTES, DEFAULT_LOGO_IMG_ALT } from "@Constants";
import Link from "next/link";
import Image from "next/image";
import { FormUtil } from "@Utils";
import { useMutation } from "@apollo/client";
import CustomerQuery from "../../lib/queries/Customer.graphql";
import { useRouter } from "next/router";
import { isValidToken, parseCookie, parseJwtToken } from "lib/authentication/cookie-service";
import {
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from "@material-ui/icons";
import Banner from "../../public/images/aboutus/bg2.png";
import BannerBottom from "../../public/images/banner/bannerbottom.png";
// import { serverSideTranslations } from "next-i18next/serverSideTranslations";

// @TODO implement remember me

const SigninFormPage: NextPage = () => {
  const router = useRouter();
  const [signinErrorMessage, setSigninErrorMessage] = useState<string | null>(null);
  const [checked, setChecked] = React.useState(true);
  const [signIn, { loading }] = useMutation(CustomerQuery.SignIn);
  const [showPassword, setShowPassword] = useState<boolean>(false);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.checked);
  };

  useEffect(() => {
    if (signinErrorMessage) {
      setTimeout(() => {
        setSigninErrorMessage(null);
      }, 2000);
    }

    // eslint-disable-next-line
  }, [signinErrorMessage]);
  const handleSubmit = async (values) => {
    try {
      await signIn({ variables: values });
      router.push("/app");
      return {};
    } catch (error) {
      const errors: Record<string, unknown> = {};
      console.error(error.message);
      if (error.message.includes("INCORRECT PASSWORD")) {
        errors.password = "Email hoặc mật khẩu không chính xác";
        errors.email = "Email hoặc mật khẩu không chính xác";
      }
      if (error.message.includes("NOT FOUND")) {
        errors.password = "Email hoặc mật khẩu không chính xác";
        errors.email = "Email hoặc mật khẩu không chính xác";
      }
      return errors;
    }
  };
  return (
    <div className="auth-form">
      <div className="auth-form__bg">
        <Image alt={DEFAULT_IMG_ALT} src={Banner} />
      </div>
      <div className="auth-form__content">
        <Image height="100%" width="100%" alt={DEFAULT_LOGO_IMG_ALT} src={"/images/app-icon.png"} />
        <div className="auth-form__title">ĐĂNG NHẬP</div>
        <div className="auth-form__description">
          Bạn chưa có tài khoản ở HanaGold?&#160;
          <Link href={APP_ROUTES.SIGNUP}>
            <a>Đăng ký ngay</a>
          </Link>
        </div>
        <Form
          onSubmit={handleSubmit}
          render={({ handleSubmit }) => {
            return (
              <form onSubmit={handleSubmit} className="auth-form__form">
                <Field name="email" validate={FormUtil.Rule.required("Xin nhập email")}>
                  {({ input, meta, ...rest }) => {
                    return (
                      <TextField
                        {...input}
                        {...rest}
                        fullWidth
                        type="email"
                        autoComplete="email"
                        placeholder={"Nhập email"}
                        className="form-text-field"
                        inputProps={{ className: "input" }}
                        variant="outlined"
                        onChange={(e) => input.onChange(e.target.value)}
                        helperText={meta.touched ? meta.error || meta.submitError : ""}
                        error={(meta.error || meta.submitError) && meta.touched}
                      />
                    );
                  }}
                </Field>

                <Field name="password" validate={FormUtil.Rule.required("Xin nhập mật khẩu")}>
                  {({ input, meta, ...rest }) => {
                    return (
                      <TextField
                        {...input}
                        {...rest}
                        fullWidth
                        autoComplete="current-password"
                        placeholder={"Nhập mật khẩu"}
                        className="form-text-field"
                        inputProps={{ className: "input" }}
                        variant="outlined"
                        onChange={(e) => input.onChange(e.target.value)}
                        helperText={meta.touched ? meta.error || meta.submitError : ""}
                        error={(meta.error || meta.submitError) && meta.touched}
                        InputProps={{
                          type: showPassword ? "text" : "password",
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton onClick={() => setShowPassword((show) => !show)}>
                                {showPassword ? (
                                  <VisibilityIcon style={{ fontSize: "2rem", color: "#959595" }} />
                                ) : (
                                  <VisibilityOffIcon
                                    style={{ fontSize: "2rem", color: "#959595" }}
                                  />
                                )}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                      />
                    );
                  }}
                </Field>
                <div className="auth-form__form__control">
                  <Checkbox
                    checked={checked}
                    onChange={handleChange}
                    className="auth-form__form__checkbox"
                    inputProps={{ "aria-label": "primary checkbox" }}
                  />
                  Ghi nhớ
                </div>

                <button type="submit" className="auth-form__form__submit-btn" disabled={loading}>
                  ĐĂNG NHẬP
                </button>
                <div className="auth-form__form__forgetpw">
                  <Link href={APP_ROUTES.FORGETPASSWORD} passHref>
                    <a>Quên mật khẩu?</a>
                  </Link>
                </div>
              </form>
            );
          }}
        />
      </div>
      <div className="auth-form__bg-bottom">
        <Image alt={DEFAULT_IMG_ALT} src={BannerBottom} layout="fill" />
      </div>
      <div className="auth-form__text">
        &#42;By Signing up, you agree to our Terms of Use & Privacy Policy
      </div>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const cookie = parseCookie(req);
  const [token] = parseJwtToken(cookie.token);
  const [authenticated, data] = isValidToken(token);
  if (authenticated) {
    return {
      props: {
        user: data,
      },
      redirect: {
        statusCode: 302,
        destination: "/app",
      },
    };
  }
  return {
    props: {},
  };
};

export default SigninFormPage;
