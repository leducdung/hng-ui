import React, { useEffect, useState } from "react";
import { Field, Form } from "react-final-form";
import { FORM_ERROR } from "final-form";
import { FormUtil } from "@Utils";
import { IconButton, InputAdornment, TextField, Select, MenuItem } from "@material-ui/core";
import { NextPage } from "next";
import Link from "next/link";
import Image from "next/image";
import { DEFAULT_IMG_ALT, APP_ROUTES, REGEX, DEFAULT_LOGO_IMG_ALT } from "@Constants";
import {
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from "@material-ui/icons";
import CustomerQuery from "../../lib/queries/Customer.graphql";
import { useMutation /*ApolloError*/ } from "@apollo/client";
import { useRouter } from "next/router";
import { COUNTRY_PHONE_CODES } from "@Constants/country-code";
import { validateMobilePhone } from "lib/validator";
import { makeStyles } from "@material-ui/core/styles";
import NotificationModal from "@Components/NotificationModal";
import Banner from "../../public/images/aboutus/bg2.png";
import BannerBottom from "../../public/images/banner/bannerbottom.png";
// import { serverSideTranslations } from "next-i18next/serverSideTranslations";
// https://www.w3schools.com/html/html_entities.asp
//
const useStyles = makeStyles({
  select: {
    "& ul": {
      backgroundColor: "#282828",
    },
    "& li": {
      fontSize: 12,
      color: "#a6a6a6",
      "&:hover": {
        backgroundColor: "#474747",
      },
    },
  },
});

const SignupFormPage: NextPage = () => {
  const router = useRouter();
  const [mounted, setMounted] = useState(false);
  const [signupErrorMessage, setSignupErrorMessage] = useState<string | null>(null);
  const [visiblePassword, setVisiblePassword] = useState<boolean>(false);
  const [visibleConfirmPassword, setVisibleConfirmPassword] = useState<boolean>(false);
  const [signUp, { loading }] = useMutation(CustomerQuery.CreateAccount);
  const [showNotificationModal, setShownotificationModal] = useState<boolean>(false);
  const classes = useStyles();
  useEffect(() => {
    setMounted(true);
  }, []);
  const toggleVisiblePassword = () => {
    setVisiblePassword(!visiblePassword);
  };

  const toggleVisibleConfirmPassword = () => {
    setVisibleConfirmPassword(!visibleConfirmPassword);
  };

  useEffect(() => {
    let timer: null | ReturnType<typeof setTimeout> = null;
    if (signupErrorMessage) {
      timer = setTimeout(() => {
        setSignupErrorMessage(() => null);
      }, 2000);
    }
    return () => {
      if (timer) clearTimeout(timer);
    };
  }, [signupErrorMessage]);

  const handleSubmit = async (values) => {
    try {
      await signUp({
        variables: {
          ...values,
          phoneNumber: `${values.phoneCode}${values.phoneNumber}`,
        },
      });
      const timeout = setTimeout(() => {
        if (mounted) {
          router.push("/customer/signin");
        } else {
          clearTimeout(timeout);
        }
      }, 5000);
      setShownotificationModal(true);
    } catch (error) {
      const errors: Record<string, unknown> = {
        [FORM_ERROR]: "Đã có lỗi xảy ra xin hãy kiểm tra lại thông tin",
      };
      if (error.message.includes("PHONENUMBER EXIST"))
        errors.phoneNumber = "Số điện thoại đã tồn tại!";
      if (error.message.includes("EMAIL EXIST")) errors.email = "Địa chỉ email đã tồn tại!";
      if (error.message.includes("phoneNumber must be a valid phone number"))
        errors.phoneNumber = "Số điện thoại không hợp lệ";
      return errors;
    }
  };

  return (
    <React.Fragment>
      <div className="auth-form">
        <div className="auth-form__bg">
          <Image alt={DEFAULT_IMG_ALT} src={Banner} />
        </div>
        <div className="auth-form__content">
          <Image
            height="100%"
            width="100%"
            alt={DEFAULT_LOGO_IMG_ALT}
            src={"/images/app-icon.png"}
          />
          <div className="auth-form__title">ĐĂNG KÝ</div>
          <div className="auth-form__description">
            Bạn đã có tài khoản ở HanaGold?&#160;
            <Link href={APP_ROUTES.SIGNIN}>
              <a>Đăng nhập ngay</a>
            </Link>
          </div>
          <Form
            initialValuesEqual={() => true}
            initialValues={{ phoneCode: "+84" }}
            validate={(values) => {
              const errors: Record<string, unknown> = {};

              if (!values.confirmPassword) {
                errors.confirmPassword = "Xin nhập xác thực mật khẩu";
              } else if (values.confirmPassword !== values.password) {
                errors.confirmPassword = "Xác thực mật khẩu không chính xác";
              }
              if (!validateMobilePhone("Vui lòng nhập số điện thoại", values.phoneCode)) {
                errors.phoneNumber = "số điện thoại không hợp lệ";
                errors.phoneCode = "&#42;";
              }
              return errors;
            }}
            onSubmit={async (values) => handleSubmit(values)}
            render={({ handleSubmit }) => {
              return (
                <form onSubmit={handleSubmit} className="auth-form__form">
                  <Field
                    name="fullName"
                    validate={FormUtil.composeValidators([
                      FormUtil.Rule.required("Vui lòng nhập họ tên"),
                    ])}>
                    {({ input, meta, ...rest }) => {
                      return (
                        <TextField
                          {...input}
                          {...rest}
                          fullWidth
                          placeholder={"Họ Tên"}
                          className="form-text-field"
                          inputProps={{ className: "input" }}
                          variant="outlined"
                          onChange={(e) => input.onChange(e.target.value)}
                          helperText={meta.touched ? meta.error : ""}
                          error={meta.error && meta.touched}
                        />
                      );
                    }}
                  </Field>
                  <Field
                    name="email"
                    validate={FormUtil.composeValidators([
                      FormUtil.Rule.required("Vui lòng nhập email"),
                      FormUtil.Rule.pattern(REGEX.EMAIL, { errorMessage: "Email không hợp lệ" }),
                    ])}>
                    {({ input, meta, ...rest }) => {
                      return (
                        <TextField
                          {...input}
                          {...rest}
                          fullWidth
                          type="email"
                          autoComplete="email"
                          placeholder={"Địa chỉ email"}
                          className="form-text-field"
                          inputProps={{ className: "input" }}
                          variant="outlined"
                          onChange={(e) => input.onChange(e.target.value)}
                          helperText={meta.touched ? meta.error || meta.submitError : ""}
                          error={(meta.error || meta.submitError) && meta.touched}
                        />
                      );
                    }}
                  </Field>
                  <div className="phone-input-group">
                    <Field
                      name="phoneCode"
                      validate={FormUtil.composeValidators([
                        FormUtil.Rule.required("Vui lòng nhập số điện thoại"),
                      ])}>
                      {({ input, meta, ...rest }) => {
                        return (
                          <Select
                            {...input}
                            {...rest}
                            fullWidth
                            className="form-select-field"
                            variant="outlined"
                            renderValue={(value) => {
                              return value ? (value as string) : "";
                            }}
                            MenuProps={{
                              classes: { paper: classes.select },
                            }}
                            inputProps={{ className: "input" }}
                            onChange={(e) => input.onChange(e.target.value)}
                            error={meta.error && meta.touched}>
                            {COUNTRY_PHONE_CODES.map(({ label, value }, index: number) => (
                              <MenuItem key={index} value={value}>
                                {label}
                              </MenuItem>
                            ))}
                          </Select>
                        );
                      }}
                    </Field>
                    <Field
                      name="phoneNumber"
                      validate={FormUtil.composeValidators([
                        FormUtil.Rule.required("Vui lòng nhập số điện thoại"),
                      ])}>
                      {({ input, meta, ...rest }) => {
                        return (
                          <TextField
                            {...input}
                            {...rest}
                            type="tel"
                            autoComplete="tel tel-national"
                            fullWidth
                            placeholder={"Số điện thoại"}
                            className="form-text-field"
                            inputProps={{ className: "input" }}
                            variant="outlined"
                            onChange={(e) => input.onChange(e.target.value)}
                            helperText={meta.touched ? meta.error || meta.submitError : ""}
                            error={(meta.error || meta.submitError) && meta.touched}
                          />
                        );
                      }}
                    </Field>
                  </div>

                  <div className="auth-form__pw">
                    <Field
                      name="password"
                      validate={FormUtil.composeValidators([
                        FormUtil.Rule.required("Vui lòng nhập mật khẩu"),
                        FormUtil.Rule.minLength(8, { errorMessage: "Độ dài tối thiểu 8 ký tự" }),
                      ])}>
                      {({ input, meta, ...rest }) => {
                        return (
                          <TextField
                            {...input}
                            {...rest}
                            fullWidth
                            autoComplete="new-password"
                            placeholder={"Mật khẩu"}
                            className="form-text-field"
                            inputProps={{ className: "input" }}
                            variant="outlined"
                            onChange={(e) => input.onChange(e.target.value)}
                            helperText={meta.touched ? meta.error : ""}
                            error={meta.error && meta.touched}
                            InputProps={{
                              type: visiblePassword ? "text" : "password",
                              endAdornment: (
                                <InputAdornment position="end">
                                  <IconButton onClick={toggleVisiblePassword}>
                                    {visiblePassword ? (
                                      <VisibilityIcon
                                        style={{ fontSize: "2rem", color: "#959595" }}
                                      />
                                    ) : (
                                      <VisibilityOffIcon
                                        style={{ fontSize: "2rem", color: "#959595" }}
                                      />
                                    )}
                                  </IconButton>
                                </InputAdornment>
                              ),
                            }}
                          />
                        );
                      }}
                    </Field>
                    <Field name="confirmPassword">
                      {({ input, meta, ...rest }) => {
                        return (
                          <TextField
                            {...input}
                            {...rest}
                            fullWidth
                            autoComplete="new-password"
                            placeholder={"Xác thực mật khẩu"}
                            className="form-text-field"
                            inputProps={{ className: "input" }}
                            variant="outlined"
                            onChange={(e) => input.onChange(e.target.value)}
                            helperText={meta.touched ? meta.error : ""}
                            error={meta.error && meta.touched}
                            InputProps={{
                              type: visibleConfirmPassword ? "text" : "password",
                              endAdornment: (
                                <InputAdornment position="end">
                                  <IconButton onClick={toggleVisibleConfirmPassword}>
                                    {visibleConfirmPassword ? (
                                      <VisibilityIcon
                                        style={{ fontSize: "2rem", color: "#959595" }}
                                      />
                                    ) : (
                                      <VisibilityOffIcon
                                        style={{ fontSize: "2rem", color: "#959595" }}
                                      />
                                    )}
                                  </IconButton>
                                </InputAdornment>
                              ),
                            }}
                          />
                        );
                      }}
                    </Field>
                  </div>
                  <button disabled={loading} type="submit" className="auth-form__form__submit-btn">
                    TẠO TÀI KHOẢN
                  </button>
                </form>
              );
            }}
          />
        </div>
        <div className="auth-form__bg-bottom">
          <Image alt={DEFAULT_IMG_ALT} src={BannerBottom} layout="fill" />
        </div>
        <div className="auth-form__text">
          &#42;By Signing up, you agree to our Terms of Use & Privacy Policy
        </div>
      </div>
      <NotificationModal
        show={showNotificationModal}
        title={"Tạo tài khoản thành công!"}
        content="Vui lòng đăng nhập để thực hiện các giao dịch">
        <button onClick={() => router.push("/customer/signin")} className="confirm">
          đăng nhập
        </button>
      </NotificationModal>
    </React.Fragment>
  );
};

// export const getStaticProps = async ({ locale }) => ({
//   props: {
//     ...(await serverSideTranslations(locale, ["common"])),
//   },
// });

export default SignupFormPage;
