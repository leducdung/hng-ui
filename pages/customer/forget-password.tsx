import React from "react";
import { NextPage } from "next";
import { ForgetPasswordForm } from "../../widgets";

const ForgetpasswordPage: NextPage = () => {
  return <ForgetPasswordForm />;
};

export default ForgetpasswordPage;
