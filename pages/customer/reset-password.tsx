import React from "react";
import { NextPage } from "next";
import { ResetPasswordForm } from "../../widgets";

const ResetpasswordPage: NextPage = () => {
  return <ResetPasswordForm />;
};

export default ResetpasswordPage;
