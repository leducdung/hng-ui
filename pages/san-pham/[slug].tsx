import React, { useState, useEffect } from "react";
import { PageFooter, PageHeader } from "@Widgets";
import { useBreakpoints } from "../../hooks";
import { Swiper, SwiperSlide } from "swiper/react";
import {
  Select,
  MenuItem,
  Grid,
  Divider,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Dialog,
} from "@material-ui/core";
import { CartPlusIcon } from "@Components/Icons";
import { ProductCard1, QuantityBtnGroup, SizeInstructions } from "@Components";
import { ArrowDropDown as ArrowDropDownIcon } from "@material-ui/icons";
import SwiperCore, { Navigation } from "swiper";
import { initializeApollo } from "@Lib/apollo-client";
import { GetServerSideProps } from "next";
import GetProduct from "@Lib/queries/Product.graphql";
import { useQuery } from "@apollo/client";
import { currencyFormat } from "@Utils";
import Image from "next/image";
import { SIZE } from "mock/product.mock";
import { addToCart } from "@Lib/cart-module";
import { useRouter } from "next/router";
import { APP_ROUTES } from "@Constants";
// import { DEFAULT_IMG_ALT } from "@Constants";
// import Banner3 from "../../public/images/banner/imgdt3.png";
// import Banner2 from "../../public/images/banner/imgdt2.png";
// import Banner1 from "../../public/images/banner/imgdt1.png";

SwiperCore.use([Navigation]);

export type ImgItem = {
  path: string;
};

const ProductDetail = ({ product }) => {
  const breakpoints = useBreakpoints();
  const router = useRouter();
  const [quantity, setQuantity] = useState<number>(1);
  const [visibleViewerModal, setVisibleViewerModal] = useState<boolean>(false);
  const [selectImg, setSelectImg] = useState<ImgItem>(product.images[0]);
  const { data: productsData } = useQuery(GetProduct.GetProductByCategorySlug, {
    variables: { slug: product.categories[0].category.slug, pageNum: 1, pageSize: 10 },
  });
  useEffect(() => {
    setQuantity(1);
  }, [product]);
  useEffect(() => {
    product.images.length && setSelectImg(() => product.images[0]);
  }, [product]);
  const toggleViewerModal = () => {
    setVisibleViewerModal(!visibleViewerModal);
  };

  return (
    <>
      <PageHeader />
      <div className="product-detail">
        <div className="product-detail-page">
          <div className="product-detail__info">
            <div className="product-detail__info__list">
              <div className="product-detail__info__img-show">
                <Image
                  loader={({ src }) => src}
                  src={selectImg.path || "/app-icon.png"}
                  alt={product.name}
                  layout="fill"
                />
              </div>

              <Swiper
                breakpoints={{
                  375: {
                    slidesPerView: 4,
                  },
                  576: {
                    slidesPerView: 5,
                  },
                  992: {
                    slidesPerView: 4,
                  },
                }}
                navigation>
                {product.images.map((i: ImgItem, index: number) => {
                  return (
                    <SwiperSlide key={i.path + index.toString()}>
                      <div className="product-detail__swiper__img" onClick={() => setSelectImg(i)}>
                        <Image
                          src={i.path || "/app-icon.png"}
                          alt={product.name + " " + index.toString()}
                          layout="fill"
                        />
                      </div>
                    </SwiperSlide>
                  );
                })}
              </Swiper>
            </div>
            <div className="product-detail__order">
              <div className="product-detail__order__text-category">
                {product.categories[0].category.name || "danh mục sản phẩm"}
              </div>
              <div className="product-detail__order__name">{product.name}</div>
              {!breakpoints.lg && (
                <>
                  <div className=" product-detail__order__group__title product-detail__order__group__title__price">
                    {product.price === 0 ? "Liên hệ" : `${currencyFormat(product.price)} đ`}
                  </div>
                  <Divider style={{ backgroundColor: "#6C6C6C", marginBottom: "3rem" }} />
                  <Grid container className="product-detail__order__table">
                    <Grid item xs={6}>
                      <div className="product-detail__order__table__name">Mã sản phẩm</div>
                      <div className="product-detail__order__table__name">Loại sản phẩm</div>
                      {/* <div className="product-detail__order__table__name">Chất liệu</div> */}
                    </Grid>
                    <Grid item xs={6}>
                      <div className="product-detail__order__table__description">
                        {product.code}
                      </div>
                      <div className="product-detail__order__table__description">
                        {product.categories[0].category.name || "danh mục sản phẩm"}
                      </div>
                      {/* <div className="product-detail__order__table__description">Vàng</div> */}
                    </Grid>
                  </Grid>
                  <Select
                    variant="outlined"
                    className="product-detail__order__select"
                    renderValue={() => <span>Chọn kích cỡ</span>}
                    displayEmpty
                    MenuProps={{
                      className: "product-detail__order__select__menu",
                      anchorOrigin: {
                        vertical: 60,
                        horizontal: "left",
                      },
                      transformOrigin: {
                        vertical: "top",
                        horizontal: "left",
                      },
                      getContentAnchorEl: null,
                    }}>
                    <Grid container>
                      <Grid item lg={5} className="product-detail__order__select__menu-col">
                        {SIZE.slice(0, Math.floor(SIZE.length / 2)).map((s) => {
                          return (
                            <MenuItem key={s.size} style={{ fontSize: "1.2rem" }}>
                              {s.size}
                            </MenuItem>
                          );
                        })}
                      </Grid>
                      <Grid item lg={5} className="product-detail__order__select__menu-col">
                        {SIZE.slice(Math.floor(SIZE.length / 2), SIZE.length).map((s) => {
                          return (
                            <MenuItem key={s.size} style={{ fontSize: "1.2rem" }}>
                              {s.size}
                            </MenuItem>
                          );
                        })}
                      </Grid>
                    </Grid>
                  </Select>
                </>
              )}
              {breakpoints.lg && (
                <>
                  <Grid container className="product-detail__order__group">
                    <Grid item xs={6} lg={4}>
                      <div className="product-detail__order__group__title">Mã sản phẩm</div>
                      {/* <div className="product-detail__order__group__title">Kích cỡ</div> */}
                    </Grid>
                    <Grid item xs={6} lg={8}>
                      <div className="product-detail__order__group__title">{product.code}</div>

                      {/* <div className="product-detail__order__group__title"> */}
                      {/*   <Select */}
                      {/*     variant="outlined" */}
                      {/*     className="product-detail__order__select" */}
                      {/*     renderValue={() => <span>Chọn kích cỡ</span>} */}
                      {/*     displayEmpty */}
                      {/*     MenuProps={{ */}
                      {/*       className: "product-detail__order__select__menu", */}
                      {/*       anchorOrigin: { */}
                      {/*         vertical: 60, */}
                      {/*         horizontal: "left", */}
                      {/*       }, */}
                      {/*       transformOrigin: { */}
                      {/*         vertical: "top", */}
                      {/*         horizontal: "left", */}
                      {/*       }, */}
                      {/*       getContentAnchorEl: null, */}
                      {/*     }}> */}
                      {/*     <Grid container> */}
                      {/*       <Grid item lg={5} className="product-detail__order__select__menu-col"> */}
                      {/*         {SIZE.slice(0, Math.floor(SIZE.length / 2)).map((s) => { */}
                      {/*           return ( */}
                      {/*             <MenuItem key={s.size} style={{ fontSize: "1.2rem" }}> */}
                      {/*               <span>{s.size}</span> */}
                      {/*             </MenuItem> */}
                      {/*           ); */}
                      {/*         })} */}
                      {/*       </Grid> */}
                      {/*       <Grid item lg={5} className="product-detail__order__select__menu-col"> */}
                      {/*         {SIZE.slice(Math.floor(SIZE.length / 2), SIZE.length).map((s) => { */}
                      {/*           return ( */}
                      {/*             <MenuItem key={s.size} style={{ fontSize: "1.2rem" }}> */}
                      {/*               <span>{s.size}</span> */}
                      {/*             </MenuItem> */}
                      {/*           ); */}
                      {/*         })} */}
                      {/*       </Grid> */}
                      {/*     </Grid> */}
                      {/*   </Select> */}
                      {/* </div> */}
                      {/* <div */}
                      {/*   className="product-detail__order__group__title product-detail__order__group__title__size" */}
                      {/*   onClick={toggleViewerModal} */}
                      {/*   style={{ cursor: "pointer" }}> */}
                      {/*   Hướng dẫn chọn kích cỡ */}
                      {/* </div> */}
                    </Grid>
                  </Grid>

                  <Divider style={{ backgroundColor: "#6C6C6C", marginBottom: "3rem" }} />
                  <Grid
                    container
                    className="product-detail__order__group product-detail__order__actions">
                    <Grid item lg={4}>
                      <div className="product-detail__order__group__title">Giá sản phẩm</div>
                      <div className="product-detail__order__actions__quantity">
                        <QuantityBtnGroup
                          quantity={quantity}
                          disableMinusBtn={quantity == 1}
                          onChangeQuantity={(value) => {
                            setQuantity(value);
                          }}
                        />
                      </div>
                    </Grid>

                    <Grid item lg={8}>
                      <div className=" product-detail__order__group__title product-detail__order__group__title__price">
                        {product.price !== 0 ? `${currencyFormat(product.price)} đ` : "Liên hệ"}
                      </div>
                      <div className="product-detail__order__actions-right">
                        <button
                          className={`product-detail__order__btn`}
                          onClick={() => {
                            addToCart(product.id, product, quantity);
                            router.push(APP_ROUTES.ORDER);
                          }}>
                          MUA NGAY
                        </button>

                        <button
                          className="product-detail__order__add-to-cart"
                          onClick={() => {
                            addToCart(product.id, product, quantity);
                          }}>
                          <CartPlusIcon size={[24, 24]} viewBox={[24, 24]} color="#A6A6A6" />
                        </button>
                      </div>
                    </Grid>
                  </Grid>
                </>
              )}
            </div>
          </div>
          <Grid container className="product-detail__describe">
            <Grid item sm={12} lg={!!product.specification ? 5 : 12}>
              <Accordion defaultExpanded={true}>
                <AccordionSummary
                  expandIcon={<ArrowDropDownIcon className="product-detail__describe__icon" />}
                  className="product-detail__title">
                  MÔ TẢ
                </AccordionSummary>
                <AccordionDetails className="product-detail__describe__description">
                  {product.description
                    ? product.description
                    : ` Nếu trang phục tôn lên vóc dáng và định hình phong cách thì trang sức sẽ đóng vai
                  trò thể hiện tính cách và đẳng cấp. Tùy vào sở thích cũng như loại nữ trang mà bạn
                  ưa chuộng mà chúng sẽ sở hữu vẻ đẹp khác nhau. Nhằm đem đến cho các cô gái những
                  mẫu trang sức đính đá lấp lánh, PNJ ra mắt sản phẩm nhẫn được chế tác trên chất
                  liệu vàng 18K cùng điểm nhấn đính đá CZ sang trọng, giúp cho quý cô trông thời
                  thượng và hiện đại. PNJ luôn trân quý và tôn vinh vẻ đẹp đặc trưng và duyên dáng
                  của phụ nữ Việt trong từng thiết kế trang sức. Ngoài ra, xu hướng thời trang ngày
                  càng phát triển, nữ trang vàng PNJ cũng vì vậy mà trở nên độc đáo và tinh tế hơn,
                  tô điểm thêm vẻ hiện đại cho chủ sở hữu, giúp nàng thể hiện sự tự tin và phong
                  cách của mình.
                `}
                </AccordionDetails>
              </Accordion>
              {!breakpoints.lg && !!product.specification && (
                <Accordion defaultExpanded={true}>
                  <AccordionSummary
                    expandIcon={<ArrowDropDownIcon className="product-detail__describe__icon" />}
                    className="product-detail__title">
                    THÔNG SỐ KỸ THUẬT
                  </AccordionSummary>
                  <AccordionDetails className="product-detail__describe__description">
                    <Grid container className="product-detail__describe__table">
                      <Grid item xs={6}>
                        {product.specification.specificationGroups[0].specificationGroupValues.map(
                          (n, index: number) => {
                            return (
                              <div key={index} className="product-detail__describe__table__name">
                                {n.name}
                              </div>
                            );
                          }
                        )}
                      </Grid>
                      <Grid item xs={6}>
                        {product.specification.specificationGroups[0].specificationGroupValues.map(
                          (d, index: number) => {
                            return (
                              <div
                                key={index}
                                className="product-detail__describe__table__description">
                                {d.value}
                              </div>
                            );
                          }
                        )}
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
              )}
              <Accordion>
                <AccordionSummary
                  expandIcon={<ArrowDropDownIcon className="product-detail__describe__icon" />}
                  className="product-detail__title">
                  CHÍNH SÁCH BẢO HÀNH
                </AccordionSummary>
                <AccordionDetails className="product-detail__describe__description"></AccordionDetails>
              </Accordion>
              <Accordion>
                <AccordionSummary
                  expandIcon={<ArrowDropDownIcon className="product-detail__describe__icon" />}
                  className="product-detail__title">
                  THANH TOÁN VÀ GIAO HÀNG
                </AccordionSummary>
                <AccordionDetails className="product-detail__describe__description"></AccordionDetails>
              </Accordion>
            </Grid>
            {breakpoints.lg && !!product.specification && (
              <Grid item lg={6} className="product-detail__describe__right">
                <React.Fragment>
                  <div className="product-detail__title">THÔNG SỐ KỸ THUẬT</div>
                  <Divider
                    style={{
                      backgroundColor: "#6C6C6C",
                      marginBottom: "3.3rem",
                      marginTop: "1rem",
                    }}
                  />
                  <Grid container className="product-detail__describe__table">
                    <Grid item lg={6}>
                      {product.specification.specificationGroups[0].specificationGroupValues.map(
                        (n, index: number) => {
                          return (
                            <div key={index} className="product-detail__describe__table__name">
                              {n.name}
                            </div>
                          );
                        }
                      )}
                    </Grid>
                    <Grid item lg={6}>
                      {product.specification.specificationGroups[0].specificationGroupValues.map(
                        (d, index: number) => {
                          return (
                            <div
                              key={index}
                              className="product-detail__describe__table__description">
                              {d.value}
                            </div>
                          );
                        }
                      )}
                    </Grid>
                  </Grid>
                </React.Fragment>
              </Grid>
            )}
          </Grid>
          <div className="product-detail__favorite">
            <div className="product-detail__title">SẢN PHẨM ĐƯỢC ƯA CHUỘNG</div>
            <div className="product-detail__favorite__list">
              <Swiper
                breakpoints={{
                  375: {
                    slidesPerView: 2,
                  },
                  576: {
                    slidesPerView: 3,
                  },
                  768: {
                    slidesPerView: 4,
                  },
                  992: {
                    slidesPerView: 3,
                  },
                  1200: {
                    slidesPerView: 3.5,
                  },
                  1440: {
                    slidesPerView: 4,
                  },
                }}>
                {!!productsData &&
                  productsData.result.products.map((prod) => {
                    return (
                      <SwiperSlide key={prod.slug}>
                        <ProductCard1
                          id={prod.id}
                          slug={prod.slug}
                          title={prod.name + " " + prod.code}
                          img={prod.images[0].path}
                          price={prod.price > 0 ? `${currencyFormat(prod.price)} đ` : "Liên hệ"}
                          discount={prod.discount ?? 0}
                        />
                      </SwiperSlide>
                    );
                  })}
              </Swiper>
            </div>
          </div>
        </div>
        {!breakpoints.lg && (
          <div className="product-detail__group-order">
            <button
              className="product-detail__order__add-to-cart"
              onClick={() => {
                // onAddToCart();
              }}>
              <CartPlusIcon size={[24, 24]} viewBox={[24, 24]} color="#A6A6A6" />
            </button>
            <button
              className={`product-detail__order__btn`}
              onClick={() => {
                // onAddToCart(true);
              }}>
              MUA NGAY
            </button>
          </div>
        )}
      </div>
      <PageFooter withoutDivider />

      <Dialog
        open={visibleViewerModal}
        onClose={() => {
          toggleViewerModal();
        }}
        fullScreen={!breakpoints.lg ? true : false}
        className="size-instructions__dialog">
        <SizeInstructions
          onClose={() => {
            toggleViewerModal();
          }}
        />
      </Dialog>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  try {
    const client = initializeApollo();
    const { data } = await client.query({
      query: GetProduct.GetProductBySlug,
      variables: {
        slug: params?.slug,
      },
    });
    return {
      props: {
        params,
        product: { ...data.product, price: parseInt(data.product.price) },
      },
    };
  } catch (error) {
    console.error(error);
    return {
      props: {
        params,
      },
    };
  }
};
export default ProductDetail;
