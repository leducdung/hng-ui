import React from "react";
import { NextPage } from "next";

const NotSupportedPage: NextPage = () => {
  return (
    <div style={{ width: "100%", height: "100%", margin: "0 auto" }}>
      <h1>Trình duyệt này không được hỗ trợ</h1>
    </div>
  );
};

export default NotSupportedPage;
