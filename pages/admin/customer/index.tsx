import React from "react";
import { NextPage } from "next";
import styles from "./Customer.module.scss";
import {
  Search as SearchIcon,
  MoreHoriz as MoreHorizIcon,
  List as ListIcon,
  Add as AddIcon,
} from "@material-ui/icons";
import { Grid, InputAdornment, TextField } from "@material-ui/core";
import Image from "next/image";
import Link from "next/link";
import { useQuery } from "@apollo/client";
import GetCustomer from "@Lib/queries/Customer.graphql";
import AdminLayout from "@Components/AdminLayout";

const CustomerPage: NextPage = () => {
  const { data: customerData } = useQuery(GetCustomer.GetData);

  return (
    <AdminLayout>
      <div className={styles.customer}>
        <div className={styles.header}>
          <div className={styles.title}>Tài khoản</div>
          <div className={styles.groupSearch}>
            <TextField
              fullWidth
              placeholder="Tìm kiếm"
              variant="outlined"
              className={styles.search}
              inputProps={{ className: "input" }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon style={{ fontSize: "2.2rem", color: "#272B2F" }} />
                  </InputAdornment>
                ),
              }}
            />
            <button className={styles.btnFilter}>
              <ListIcon style={{ fontSize: "2.2rem", color: "#272B2F", marginRight: "1.1rem" }} />
              Lọc
            </button>
            <MoreHorizIcon style={{ fontSize: "2.2rem", color: "#272B2F" }} />
          </div>
        </div>
        <Grid container className={styles.listHeader}>
          <Grid item xs={3}>
            Khách hàng / ID
          </Grid>
          <Grid item xs={2}>
            Số điện thoại
          </Grid>
          <Grid item xs={2}>
            E-mail
          </Grid>
          <Grid item xs={2}>
            Trạng thái
          </Grid>
          <Grid item xs={3}>
            Trạng thái Xác thực
          </Grid>
        </Grid>
        {customerData && (
          <Link href={"/admin/customer/" + customerData.user.id} passHref>
            <a className={styles.item}>
              <Grid container alignItems="center">
                <Grid item container xs={3}>
                  <Image
                    loader={({ src }) => src}
                    loading="lazy"
                    src={customerData.user.avata || "/images/customer-avatar.png"}
                    alt={"avata"}
                    width={40}
                    height={40}
                    objectFit="contain"
                  />
                  <div className={styles.name}>
                    {customerData.user.fullName}
                    <br />
                    <span>{customerData.user.code}</span>
                  </div>
                </Grid>
                <Grid item xs={2}>
                  {customerData.user.phoneNumber}
                </Grid>
                <Grid item xs={2}>
                  {customerData.user.email}
                </Grid>
                <Grid item xs={2} alignItems="center">
                  {customerData.user.status === true ? (
                    <div>
                      <span className={styles.activity}>&#8226;&ensp;</span>
                      Hoạt động
                    </div>
                  ) : (
                    <div>
                      <span className={styles.invalid}>&#8226;&ensp;</span>
                      Vô hiệu
                    </div>
                  )}
                </Grid>
                <Grid item xs={3}>
                  <span className={styles.unconfirmed}>&#8226;&ensp;</span>
                  Chưa xác thực
                </Grid>
              </Grid>
            </a>
          </Link>
        )}
      </div>
      <div />
      <div className={styles.floatingBtnItem}>
        <AddIcon
          style={{
            fontSize: "2.2rem",
            color: "#fff",
          }}
        />
      </div>
    </AdminLayout>
  );
};

export default CustomerPage;
