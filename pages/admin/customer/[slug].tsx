import React, { useState } from "react";
import styles from "./Profile.module.scss";
import {
  ArrowDropDown as ArrowDropDownIcon,
  Cancel as CancelIcon,
  Error as ErrorIcon,
} from "@material-ui/icons";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  FormControlLabel,
  Grid,
  Switch,
  Popover,
} from "@material-ui/core";
// import CheckCircleRoundedIcon from "@material-ui/icons/CheckCircleRounded";
import Image from "next/image";
// import { NavHeader } from "@Components";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/client";
import GetCustomer from "@Lib/queries/Customer.graphql";
import { NavHeader } from "@Components";

const ProfileCustomerPage = () => {
  const [anchorUserEl, setAnchorUserEl] = useState<HTMLElement | null>(null);
  const [onChangeCheck, setOnChangeCheck] = useState<boolean>(false);
  const [, setAccuracy] = useState<{
    visible: boolean;
    newStatus: boolean;
  }>({
    visible: false,
    newStatus: true,
  });

  const handleOpenCheckMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorUserEl(event.currentTarget);
  };
  const openAccuracy = (newStatus: boolean) => {
    setAccuracy({ visible: true, newStatus });
  };
  const handleCloseCheckMenu = () => {
    setAnchorUserEl(null);
  };

  const renderValue = (value?: string) => {
    return <div className={styles.value}> {value ? value : "--/--"}</div>;
  };

  const router = useRouter();
  const { slug } = router.query;
  const id = parseInt(slug as string);
  const { data: customerDetailData } = useQuery(GetCustomer.GetCustomerById, {
    variables: { id: id },
  });
  // const { data: customerVerificationData } = useQuery(GetCustomer.GetCustomerVerification);

  return (
    <>
      {customerDetailData && (
        <div className={styles.customerDetail}>
          <NavHeader
            activeStep={0}
            name={customerDetailData.customer.fullName}
            code={customerDetailData.customer.code}
            id={customerDetailData.customer.id}
          />
          <div className={styles.bodyInfo}>
            <Grid container spacing={2}>
              <Grid item xs={3} className={styles.left}>
                <div className={styles.borderImg}>
                  <div className={styles.image}>
                    <Image
                      loader={({ src }) => src}
                      loading="lazy"
                      src={customerDetailData.customer.avata || "/images/customer-avatar.png"}
                      alt={"avata"}
                      layout="fill"
                    />
                  </div>
                </div>
                <div className={styles.name}>{customerDetailData.customer.fullName}</div>
                <button className={styles.btnEdit}>Chỉnh sửa</button>
              </Grid>
              <Grid item xs={6} className={styles.center}>
                <div className={styles.box}>
                  <div className={styles.title}>Thông tin chung</div>
                  <br />
                  <br />
                  <div>Trạng thái</div>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={customerDetailData.customer.status}
                        onChange={(e) => {
                          openAccuracy(e.target.checked);
                        }}
                        className="app-switch-box"
                      />
                    }
                    label={
                      <div className="label" style={{ marginBottom: 0 }}>
                        {customerDetailData.customer.status ? "Đang hoạt động" : "Vô hiệu"}
                      </div>
                    }
                  />
                  <br />
                  <br />
                  <Grid container>
                    <Grid item xs={6}>
                      Tên tài khoản
                      {renderValue(customerDetailData.customer.user)}
                    </Grid>
                    <Grid item xs={6}>
                      Giới tính
                      <div className={styles.value}>
                        {renderValue(customerDetailData.customer.gender)}
                      </div>
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={6}>
                      Số điện thoại
                      <div className={styles.value}>
                        {renderValue(customerDetailData.customer.phoneNumber)}
                      </div>
                    </Grid>
                    <Grid item xs={6}>
                      Email
                      <div className={styles.value}>
                        {renderValue(customerDetailData.customer.email)}
                      </div>
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={6}>
                      Ngày giao dịch gần nhất<div className={styles.value}>--/--</div>
                    </Grid>
                    <Grid item xs={6}>
                      Ghi chú<div className={styles.value}>--/--</div>
                    </Grid>
                  </Grid>
                </div>
                <div className={styles.box}>
                  <div className={styles.title}>Thông tin xác thực</div>
                  {/* <div className={`${styles.check} ${styles.check1}`}>
                    <CheckCircleRoundedIcon
                      style={{ color: "#4EAA39", fontSize: "2.6rem", marginRight: "0.9rem" }}
                    />
                    Đã được xác thực
                  </div> */}
                  {onChangeCheck ? (
                    <div className={`${styles.check} ${styles.check2}`}>
                      <CancelIcon
                        style={{ color: "#E53935", fontSize: "2.6rem", marginRight: "0.9rem" }}
                      />
                      Không thể xác thực
                    </div>
                  ) : (
                    <div
                      className={`${styles.check} ${styles.check3}`}
                      onClick={handleOpenCheckMenu}>
                      <ErrorIcon
                        style={{ color: "#FFB300", fontSize: "2.6rem", marginRight: "0.9rem" }}
                      />
                      Chưa xác thực
                    </div>
                  )}

                  <Grid container>
                    <Grid item xs={6}>
                      Họ tên<div className={styles.value}>Lucy McQueen</div>
                    </Grid>
                    <Grid item xs={6}>
                      Ngày sinh<div className={styles.value}>--/--</div>
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={6}>
                      Quốc tịch<div className={styles.value}>--/--</div>
                    </Grid>
                    <Grid item xs={6}>
                      Loại<div className={styles.value}>--/--</div>
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={6}>
                      Mã số thẻ<div className={styles.value}>--/--</div>
                    </Grid>
                    <Grid item xs={6}></Grid>
                  </Grid>

                  <div className={styles.listCard}>
                    <div className={styles.card}>
                      <Image
                        loader={({ src }) => src}
                        loading="lazy"
                        src={"/images/account/card1.png"}
                        alt={"card1"}
                        width={215}
                        height={112}
                      />
                      <div>Căn cước công dân</div>
                      <div className={styles.value}>Mặt trước</div>
                    </div>

                    <div className={styles.card}>
                      <Image
                        loader={({ src }) => src}
                        loading="lazy"
                        src={"/images/account/card2.png"}
                        alt={"card2"}
                        width={215}
                        height={112}
                      />
                      <div>Căn cước công dân</div>
                      <div className={styles.value}>Mặt sau</div>
                    </div>

                    <div className={styles.card}>
                      <Image
                        loader={({ src }) => src}
                        loading="lazy"
                        src={"/images/account/avtnone.png"}
                        alt={"card1"}
                        width={116}
                        height={116}
                      />
                      <div>Nhận dạng</div>
                      <div className={styles.value}>Khuôn mặt</div>
                    </div>
                  </div>
                </div>
                <Accordion defaultExpanded={true} className={styles.drop}>
                  <AccordionSummary
                    expandIcon={<ArrowDropDownIcon style={{ color: "#000000" }} />}
                    className={styles.title}>
                    Lịch sử xác thực
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item container>
                        <Grid item xs={4}>
                          {/* --/-- */}
                        </Grid>
                        <Grid item xs={8}>
                          {/* Admin thay đổi Thông tin xác thực từ “Chưa xác thực” sang “Đã xác thực” */}
                        </Grid>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
              </Grid>
              <Grid item xs={3} className={styles.right}>
                <div className={styles.box}>
                  <div className={styles.title}>Thông tin ngân hàng</div>
                  <br />
                  <br />
                  <div>
                    <div>Tên ngân hàng</div>
                    <div className={styles.value}>--/--</div>
                  </div>
                  <div>
                    <div>Tên thụ hưởng</div>
                    <div className={styles.value}>--/--</div>
                  </div>
                  <div>
                    <div>Số tài khoản</div>
                    <div className={styles.value}>--/--</div>
                  </div>
                  <div>
                    <div>Ngày hết hạn</div>
                    <div className={styles.value}>--/--</div>
                  </div>
                </div>
              </Grid>
            </Grid>
          </div>
        </div>
      )}

      <Popover
        open={Boolean(anchorUserEl)}
        anchorEl={anchorUserEl}
        onClose={handleCloseCheckMenu}
        onClick={handleCloseCheckMenu}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}>
        <div className={styles.popover}>
          <div
            className={styles.menuItem}
            onClick={() => {
              setOnChangeCheck(false);
            }}>
            <span style={{ color: "#F7AB2F", fontSize: "3rem" }}>&#8226;&ensp;</span>Chưa xác thực
          </div>
          <div
            className={styles.menuItem}
            onClick={() => {
              setOnChangeCheck(true);
            }}>
            <span style={{ color: "#E53935", fontSize: "3rem" }}>&#8226;&ensp;</span>Không thể xác
            thực
          </div>
          <div
            className={styles.menuItem}
            onClick={() => {
              setOnChangeCheck(true);
            }}>
            <span style={{ color: "#4EAA39", fontSize: "3rem" }}>&#8226;&ensp;</span>Đã được xác
            thực
          </div>
        </div>
      </Popover>
    </>
  );
};

export default ProfileCustomerPage;
