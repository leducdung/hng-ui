import React, { useState } from "react";
import { NextPage } from "next";
import { Dialog, Grid, InputAdornment, TextField } from "@material-ui/core";
import {
  Search as SearchIcon,
  List as ListIcon,
  Add as AddIcon,
  MoreHoriz as MoreHorizIcon,
} from "@material-ui/icons";
import styles from "./GoldPrice.module.scss";
import { useQuery } from "@apollo/client";
import GoldPriceQuery from "@Lib/queries/GoldPrice.graphql";
import moment from "moment";
import AdminLayout from "@Components/AdminLayout";
import { PriceOutputModel } from "@Models/price";
import GoldPriceUpdateForm from "./GoldPriceUpdateForm";
import GoldPriceCreateForm from "./GoldPriceCreateForm";
import InfiniteScroll from "react-infinite-scroller";

const GoldPricePage: NextPage = () => {
  const { data: priceGoldData } = useQuery(GoldPriceQuery.GetGoldPriceHNG);
  const { data: priceApply } = useQuery(GoldPriceQuery.GetGoldPriceHNGApply);
  const [visibleCreateModal, setVisibleCreateModal] = useState<boolean>(false);
  const [visibleUpdateModal, setVisibleUpdateModal] = useState<PriceOutputModel | null>(null);

  const renderStatus = (status: string) => {
    switch (status) {
      case "EXPIRED":
        return (
          <span>
            <span style={{ color: "#F84948" }}>&#8226;&ensp;</span>Hết hiệu lực
          </span>
        );
      case "PENDING":
        return (
          <span>
            <span style={{ color: "#FFD74B" }}>&#8226;&ensp;</span>Chờ áp dụng
          </span>
        );
      default:
        return (
          <span>
            <span style={{ color: "#4DD22C" }}>&#8226;&ensp;</span>Đang áp dụng
          </span>
        );
    }
  };
  return (
    <AdminLayout>
      <div className={styles.goldpricePage}>
        <div className={styles.content}>
          <div className={styles.header}>
            <div className={styles.title}>
              Loại thông số ({priceGoldData && priceGoldData.prices.length})
            </div>
            <div className={styles.actions}>
              {priceApply && (
                <div className={styles.apply}>
                  Giá vàng đang áp dụng
                  <span>Giá mua {priceApply.price.buyPrice.toLocaleString()} đ</span>
                  <span>Giá bán {priceApply.price.sellPrice.toLocaleString()} đ</span>
                </div>
              )}

              <TextField
                placeholder="Tìm kiếm"
                variant="outlined"
                className={styles.search}
                inputProps={{ className: "input" }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon className="icon" />
                    </InputAdornment>
                  ),
                }}
                onChange={() => {
                  /* props.onSearch(e.target.value); */
                }}
              />
              <button className={styles.btnFilter}>
                <ListIcon style={{ fontSize: "2.2rem", color: "#272B2F", marginRight: "1.1rem" }} />
                Lọc
              </button>
              <MoreHorizIcon style={{ fontSize: "2.2rem", color: "#272B2F" }} />
            </div>
          </div>

          {priceGoldData && (
            <div className={styles.list}>
              <div className={styles.header}>
                <Grid container alignItems="center">
                  <Grid item xs={3}>
                    Ngày giờ Áp dụng
                  </Grid>
                  <Grid item xs={3}>
                    Giá mua
                  </Grid>
                  <Grid item xs={3}>
                    Giá bán
                  </Grid>
                  <Grid item xs={3}>
                    Trạng thái
                  </Grid>
                </Grid>
              </div>
              <InfiniteScroll
                useWindow={false}
                pageStart={1}
                initialLoad={false}
                // hasMore={}
                loadMore={() => {
                  // props.onLoadMore();
                }}
                style={{ width: "100%", overflow: "auto" }}>
                {priceGoldData.prices.map((price) => {
                  return (
                    <div
                      key={price.id}
                      onClick={() => {
                        setVisibleUpdateModal(price);
                      }}>
                      <Grid container alignItems="center" className={styles.item}>
                        <Grid item xs={3}>
                          {moment(new Date(parseInt(price.updatedAt))).format(
                            "DD/MM/yyyy hh:MM:ss"
                          )}
                        </Grid>
                        <Grid item xs={3}>
                          {price.buyPrice.toLocaleString()} đ
                        </Grid>
                        <Grid item xs={3}>
                          {price.sellPrice.toLocaleString()} đ
                        </Grid>
                        <Grid item xs={3}>
                          {renderStatus(price.status)}
                        </Grid>
                      </Grid>
                    </div>
                  );
                })}
              </InfiniteScroll>
            </div>
          )}
        </div>
      </div>
      <div className={styles.floatingBtnItem}>
        <AddIcon
          style={{
            fontSize: "2.2rem",
            color: "#fff",
          }}
          onClick={() => {
            setVisibleCreateModal(true);
          }}
        />
      </div>
      <Dialog maxWidth="sm" open={Boolean(visibleUpdateModal!)}>
        <GoldPriceUpdateForm
          handleCloseForm={() => {
            setVisibleUpdateModal(null);
          }}
          data={visibleUpdateModal!}
        />
      </Dialog>
      <Dialog maxWidth="sm" open={visibleCreateModal}>
        <GoldPriceCreateForm
          handleCloseForm={() => {
            setVisibleCreateModal(false);
          }}
        />
      </Dialog>
    </AdminLayout>
  );
};

export default GoldPricePage;
