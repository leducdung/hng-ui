import React from "react";
import { NextPage } from "next";
import styles from "./GoldPrice.module.scss";
import { Form, Field } from "react-final-form";
import { FORM_ERROR } from "final-form";
import { FormUIUtil, FormUtil } from "@Utils";
import { InputAdornment, TextField } from "@material-ui/core";
import { Clear as ClearIcon, ArrowDropDown as ArrowDropDownIcon } from "@material-ui/icons";
import { useMutation } from "@apollo/client";
import GoldPriceQuery from "@Lib/queries/GoldPrice.graphql";
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";

type Props = {
  handleCloseForm: () => void;
};

const GoldPriceCreateForm: NextPage<Props> = ({ handleCloseForm }) => {
  const [createPrice] = useMutation(GoldPriceQuery.CreateGoldPriceHNG, {
    onCompleted: () => {
      window.location.reload();
    },
  });

  const handleSubmit = async (values) => {
    try {
      await createPrice({
        variables: {
          ...values,
          sellPrice: parseInt(values.sellPrice),
          buyPrice: parseInt(values.buyPrice),
          createdAt: new Date(),
        },
      });
    } catch (error) {
      const errors: Record<string, unknown> = {
        [FORM_ERROR]: "Đã có lỗi xảy ra xin hãy kiểm tra lại thông tin",
      };
      return errors;
    }
  };

  return (
    <div className={styles.goldPriceForm}>
      <div className={styles.title}>Tạo mới giá vàng</div>
      <div onClick={handleCloseForm} className={styles.icon}>
        <ClearIcon style={{ fontSize: "2.2rem", cursor: "pointer" }} />
      </div>
      <Form
        onSubmit={handleSubmit}
        render={({ handleSubmit }) => {
          return (
            <form onSubmit={handleSubmit} className={styles.form}>
              <Field
                name="sellPrice"
                validate={FormUtil.composeValidators([
                  FormUtil.Rule.required("Vui lòng nhập Giá bán"),
                ])}>
                {({ input, meta, ...rest }) => {
                  return FormUIUtil.renderFormItem(
                    "Giá bán",
                    <TextField
                      {...input}
                      {...rest}
                      fullWidth
                      placeholder={"Nhập giá bán..."}
                      className={styles.formtextfield}
                      inputProps={{ className: "input" }}
                      variant="outlined"
                      onChange={(e) => input.onChange(e.target.value)}
                      helperText={meta.touched ? meta.error : ""}
                      error={meta.error && meta.touched}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <span>đ</span>
                          </InputAdornment>
                        ),
                      }}
                    />
                  );
                }}
              </Field>
              <Field
                name="buyPrice"
                validate={FormUtil.composeValidators([
                  FormUtil.Rule.required("Vui lòng nhập Giá mua"),
                ])}>
                {({ input, meta, ...rest }) => {
                  return FormUIUtil.renderFormItem(
                    "Giá mua",
                    <TextField
                      {...input}
                      {...rest}
                      fullWidth
                      placeholder={"Nhập giá mua..."}
                      className={styles.formtextfield}
                      inputProps={{ className: "input" }}
                      variant="outlined"
                      onChange={(e) => input.onChange(e.target.value)}
                      helperText={meta.touched ? meta.error : ""}
                      error={meta.error && meta.touched}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <span>đ</span>
                          </InputAdornment>
                        ),
                      }}
                    />
                  );
                }}
              </Field>

              <Field name="createdAt">
                {({ input, meta }) => {
                  return FormUIUtil.renderFormItem(
                    "Thời gian áp dụng",
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <DateTimePicker
                        inputVariant="outlined"
                        value={input.value}
                        onChange={(value) => {
                          if (value) {
                            input.onChange(value.toISOString());
                          }
                        }}
                        fullWidth
                        format="yyyy-MM-dd hh:mm"
                        className={styles.formtextfield}
                        inputProps={{ className: "input" }}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <ArrowDropDownIcon style={{ fontSize: "2.2rem", color: "#D8D8D8" }} />
                            </InputAdornment>
                          ),
                        }}
                        helperText={meta.touched ? meta.error : ""}
                        error={meta.error && meta.touched}
                      />
                    </MuiPickersUtilsProvider>
                  );
                }}
              </Field>
              <div className={styles.groupBtn}>
                <button className={styles.btn} onClick={handleCloseForm}>
                  Hủy
                </button>
                <button type="submit" className={`${styles.btn} ${styles.btnUpdate}`}>
                  Lưu
                </button>
              </div>
            </form>
          );
        }}
      />
    </div>
  );
};

export default GoldPriceCreateForm;
