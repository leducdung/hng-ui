import React, { useState } from "react";
import { NextPage } from "next";
import { useQuery } from "@apollo/client";
import GoldLocationQuery from "@Lib/queries/Location.graphql";
import { Dialog, Grid, InputAdornment, TextField } from "@material-ui/core";
import {
  Search as SearchIcon,
  Edit as EditIcon,
  DeleteOutlineOutlined as DeleteOutlineOutlinedIcon,
  ArrowDropDown as ArrowDropDownIcon,
  Add as AddIcon,
} from "@material-ui/icons";
import styles from "./Location.module.scss";
import Image from "next/image";
import LocationFormCreate from "./LocationFormCreate";
import AdminLayout from "@Components/AdminLayout";
import LocationFormUpdate from "./LocationFormUpdate";

const LocationPage: NextPage = () => {
  const { data: locationData } = useQuery(GoldLocationQuery.GetLocationStore);
  // const [removeLocation] = useMutation(GoldLocationQuery.DeleteLocation);
  const [openCreateLocation, setOpenCreateLocation] = useState<boolean>(false);
  const [openUpdateLocation, setOpenUpdateLocation] = useState<number>(0);
  const handleOpen = (id: number) => {
    setOpenUpdateLocation(id);
  };
  // const handleremove = async (values) => {
  //   try {
  //     await removeLocation({
  //       variables: {
  //         id: id,
  //       },
  //     });
  //   } catch (error) {}
  // };
  return (
    <AdminLayout settingsPage>
      {locationData && (
        <div className={styles.locationPage}>
          <div className={styles.content}>
            <div className={styles.header}>
              <div className={styles.title}>Chi nhánh</div>
              <div className={styles.actions}>
                <TextField
                  placeholder="Tìm kiếm"
                  variant="outlined"
                  className="search-input"
                  inputProps={{ className: "input" }}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <SearchIcon className="icon" />
                      </InputAdornment>
                    ),
                  }}
                  onChange={() => {
                    /* props.onSearch(e.target.value); */
                  }}
                />
              </div>
            </div>
            <div className={styles.list}>
              <div className={styles.listHeader}>
                <div className={styles.text}>{locationData.locations.length} Chi nhánh</div>
                <div className={styles.text}>
                  Sort by
                  <span>
                    &nbsp;A to Z<ArrowDropDownIcon style={{ marginLeft: "1.1rem" }} />
                  </span>
                </div>
              </div>

              {locationData.locations.map((loca) => {
                return (
                  <div key={loca.id}>
                    <Grid container className={styles.item} justify="space-between">
                      <Grid item lg={4} className={styles.name}>
                        <div className={styles.image}>
                          <Image
                            loader={({ src }) => src}
                            loading="lazy"
                            src={loca.image || "/images/customer-avatar.png"}
                            alt={"avata"}
                            layout="fill"
                            objectFit="cover"
                          />
                        </div>
                        <div>
                          {loca.name}
                          <br />
                          {loca.status === true ? (
                            <span>
                              <span style={{ color: "#40DD7F" }}>&#8226;&ensp;</span>Hoạt động
                            </span>
                          ) : (
                            <span>
                              <span style={{ color: "#F84948" }}>&#8226;&ensp;</span>Không hoạt động
                            </span>
                          )}
                        </div>
                      </Grid>
                      <Grid item lg={6}>
                        {loca.address}
                      </Grid>
                      <Grid item lg={1} className={styles.iconGroup}>
                        <EditIcon
                          style={{ marginRight: "5rem" }}
                          className="cursor-pointer"
                          fontSize="large"
                          onClick={() => {
                            handleOpen(loca.id);
                          }}
                        />
                        <DeleteOutlineOutlinedIcon fontSize="large" />
                      </Grid>
                    </Grid>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      )}
      <div className={styles.floatingBtnItem} onClick={() => setOpenCreateLocation(true)}>
        <AddIcon
          style={{
            fontSize: "2.2rem",
            color: "#fff",
          }}
        />
      </div>
      <Dialog fullScreen open={openCreateLocation}>
        <LocationFormCreate
          handleCloseForm={() => {
            setOpenCreateLocation(false);
          }}
        />
      </Dialog>
      <Dialog fullScreen open={Boolean(openUpdateLocation!)}>
        <LocationFormUpdate
          handleCloseForm={() => {
            setOpenUpdateLocation(0);
          }}
          id={openUpdateLocation}
        />
      </Dialog>
    </AdminLayout>
  );
};

export default LocationPage;
