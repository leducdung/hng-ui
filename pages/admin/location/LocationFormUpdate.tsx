import React, { useState } from "react";
import { NextPage } from "next";
import LocationQuery from "@Lib/queries/Location.graphql";
import { FormControlLabel, Grid, Switch, TextField } from "@material-ui/core";
import styles from "./Location.module.scss";
import { Field, Form } from "react-final-form";
import { FormUIUtil, FormUtil } from "@Utils";
import { FORM_ERROR } from "final-form";
import NotificationModal from "@Components/NotificationModal";
import { useMutation } from "@apollo/client";
import IconAdd from "../../../public/images/icon/iconAdd.png";
import Image from "next/image";
import { DEFAULT_IMG_ALT } from "@Constants";
import SimpleModalHeader from "@Components/SimpleModalHeader";

type Props = {
  handleCloseForm: () => void;
  id;
};

const LocationFormUpdatePage: NextPage<Props> = ({ handleCloseForm, id }) => {
  const [showNotificationModal, setShownotificationModal] = useState<boolean>(false);
  const [updateLocation, { loading }] = useMutation(LocationQuery.UpdateLocationStore);

  const handleSubmit = async (values) => {
    try {
      await updateLocation({
        variables: {
          ...values,
          id: id,
        },
      });
    } catch (error) {
      const errors: Record<string, unknown> = {
        [FORM_ERROR]: "Đã có lỗi xảy ra xin hãy kiểm tra lại thông tin",
      };

      return errors;
    }
  };
  return (
    <div className={styles.locationFrom}>
      <SimpleModalHeader title={"Chỉnh sửa chi nhánh"} onClose={handleCloseForm} />
      <div className={styles.container}>
        <div className={styles.box}>
          <Form
            onSubmit={handleSubmit}
            render={({ handleSubmit }) => {
              return (
                <form onSubmit={handleSubmit}>
                  <Grid container spacing={6} alignItems="center" justify="center">
                    <div className={styles.image}>
                      <div className={styles.icon}>
                        <Image src={IconAdd} alt={DEFAULT_IMG_ALT} width={24} height={24} />
                      </div>
                    </div>
                  </Grid>
                  <div className={styles.title}>Thông tin chung</div>

                  <Grid container spacing={6} justify="space-between">
                    <Grid item xs={6} md={6}>
                      <Field
                        name="name"
                        validate={FormUtil.composeValidators([
                          FormUtil.Rule.required("Vui lòng nhập Tên chi nhánh"),
                        ])}>
                        {({ input, meta, ...rest }) => {
                          return FormUIUtil.renderFormItem(
                            "Tên chi nhánh",
                            <TextField
                              {...input}
                              {...rest}
                              fullWidth
                              placeholder={"Nhập Tên chi nhánh..."}
                              className={styles.formtextfield}
                              inputProps={{ className: "input" }}
                              variant="outlined"
                              onChange={(e) => input.onChange(e.target.value)}
                              helperText={meta.touched ? meta.error : ""}
                              error={meta.error && meta.touched}
                            />
                          );
                        }}
                      </Field>
                      <Field
                        name="phoneNumber"
                        validate={FormUtil.composeValidators([
                          FormUtil.Rule.required("Vui lòng nhập Số điện thoại"),
                        ])}>
                        {({ input, meta, ...rest }) => {
                          return FormUIUtil.renderFormItem(
                            "Số điện thoại",
                            <TextField
                              {...input}
                              {...rest}
                              fullWidth
                              placeholder={"Nhập Số điện thoại..."}
                              className={styles.formtextfield}
                              inputProps={{ className: "input" }}
                              variant="outlined"
                              onChange={(e) => input.onChange(e.target.value)}
                              helperText={meta.touched ? meta.error : ""}
                              error={meta.error && meta.touched}
                            />
                          );
                        }}
                      </Field>
                    </Grid>
                    <Grid item xs={6} md={6}>
                      <Field name="status" type="checkbox">
                        {({ input }) => {
                          return FormUIUtil.renderFormItem(
                            "Trạng thái",
                            <FormControlLabel
                              control={
                                <Switch
                                  checked={!!input.checked}
                                  onChange={() => {
                                    input.onChange(!input.checked);
                                  }}
                                  className="app-switch-box"
                                />
                              }
                              label={
                                <div className="label" style={{ marginBottom: 0 }}>
                                  {input.checked ? "Hoạt động" : "Vô hiệu"}
                                </div>
                              }
                            />
                          );
                        }}
                      </Field>
                      <Field
                        name="email"
                        validate={FormUtil.composeValidators([
                          FormUtil.Rule.required("Vui lòng nhập Email"),
                        ])}>
                        {({ input, meta, ...rest }) => {
                          return FormUIUtil.renderFormItem(
                            "Email",
                            <TextField
                              {...input}
                              {...rest}
                              fullWidth
                              placeholder={"Nhập Email..."}
                              className={styles.formtextfield}
                              inputProps={{ className: "input" }}
                              variant="outlined"
                              onChange={(e) => input.onChange(e.target.value)}
                              helperText={meta.touched ? meta.error : ""}
                              error={meta.error && meta.touched}
                            />
                          );
                        }}
                      </Field>
                    </Grid>
                  </Grid>
                  <Grid container spacing={6} justify="space-between">
                    <Grid item xs={12} md={12}>
                      <Field
                        name="address"
                        validate={FormUtil.composeValidators([
                          FormUtil.Rule.required("Vui lòng nhập Địa chỉ"),
                        ])}>
                        {({ input, meta, ...rest }) => {
                          return FormUIUtil.renderFormItem(
                            "Địa chỉ",
                            <TextField
                              {...input}
                              {...rest}
                              fullWidth
                              placeholder={"Nhập Địa chỉ..."}
                              className={styles.formtextfield}
                              inputProps={{ className: "input" }}
                              variant="outlined"
                              onChange={(e) => input.onChange(e.target.value)}
                              helperText={meta.touched ? meta.error : ""}
                              error={meta.error && meta.touched}
                            />
                          );
                        }}
                      </Field>
                    </Grid>
                  </Grid>
                  <div className={styles.appFormFooter}>
                    <button
                      className={`${styles.btn} ${styles.cancel}`}
                      onClick={(e) => {
                        e.preventDefault();
                        handleCloseForm();
                      }}>
                      Hủy
                    </button>
                    <button
                      type="submit"
                      disabled={loading}
                      className={`${styles.btn} ${styles.save}`}>
                      Lưu
                    </button>
                  </div>
                </form>
              );
            }}
          />
        </div>
      </div>
      <NotificationModal
        show={showNotificationModal}
        title={"Chỉnh sửa chi nhánh thành công!"}
        content="">
        <button
          onClick={() => {
            setShownotificationModal(false);
            handleCloseForm();
          }}
          className="confirm">
          Xác nhận
        </button>
      </NotificationModal>
    </div>
  );
};

export default LocationFormUpdatePage;
