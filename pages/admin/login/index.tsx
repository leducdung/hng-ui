import React, { useState } from "react";
import { NextPage, GetServerSideProps } from "next";
import Image from "next/image";
import { FormUtil } from "@Utils";
import { Form, Field } from "react-final-form";
import { InputAdornment, IconButton, TextField } from "@material-ui/core";
import {
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from "@material-ui/icons";
import { ADMIN_BASE, REGEX } from "@Constants";
import { useMutation } from "@apollo/client";
import HNGIcon from "../../../public/app-icon.png";
import AdminQuery from "@Lib/queries/Admin.graphql";
import { isValidToken, parseCookie, parseJwtToken } from "@Lib/authentication/cookie-service";
import { useRouter } from "next/router";

const AdminLoginPage: NextPage = () => {
  const router = useRouter();
  const [visiblePassword, toggleVisiblePassword] = useState<boolean>(false);
  const [login] = useMutation(AdminQuery.Login, {
    onCompleted: () => {
      router.push(ADMIN_BASE);
    },
  });
  const handleSubmit = async (values) => {
    try {
      await login({ variables: values });
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <div className="login-page">
      <div className="main">
        <Image src={HNGIcon} alt="HanaGold icon" />
        <div>
          <h1 className="title">Đăng nhập</h1>

          <div className="group">
            <button disabled className="btn gg">
              Google
            </button>
            <button disabled className="btn fb">
              Facebook
            </button>
            <button disabled className="btn ap">
              Apple
            </button>
          </div>

          <div className="divider">
            <div className="line"></div>or
            <div className="line"></div>
          </div>
          <Form
            initialValuesEqual={() => true}
            onSubmit={handleSubmit}
            render={({ handleSubmit }) => {
              return (
                <form onSubmit={handleSubmit} className="login-form">
                  <Field
                    name="email"
                    validate={FormUtil.composeValidators([
                      FormUtil.Rule.required("Xin nhập email"),
                      FormUtil.Rule.pattern(REGEX.EMAIL, { errorMessage: "Email không hợp lệ" }),
                    ])}>
                    {({ input, meta, ...rest }) => {
                      return (
                        <TextField
                          {...input}
                          {...rest}
                          fullWidth
                          autoComplete="email"
                          placeholder="Email"
                          variant="outlined"
                          className="form-text-field"
                          inputProps={{ className: "input" }}
                          onChange={(e) => input.onChange(e.target.value)}
                          helperText={meta.touched ? meta.error : ""}
                          error={meta.error && meta.touched}
                        />
                      );
                    }}
                  </Field>

                  <Field name="password" validate={FormUtil.Rule.required("Xin nhập password")}>
                    {({ input, meta, ...rest }) => {
                      return (
                        <TextField
                          {...input}
                          {...rest}
                          fullWidth
                          autoComplete="current-password"
                          placeholder="Password"
                          variant="outlined"
                          className="form-text-field"
                          inputProps={{ className: "input" }}
                          onChange={(e) => input.onChange(e.target.value)}
                          helperText={meta.touched ? meta.error : ""}
                          error={meta.error && meta.touched}
                          InputProps={{
                            type: "password",
                            endAdornment: (
                              <InputAdornment position="end">
                                <IconButton onClick={() => toggleVisiblePassword(false)}>
                                  {visiblePassword ? (
                                    <VisibilityIcon style={{ fontSize: "2rem" }} />
                                  ) : (
                                    <VisibilityOffIcon style={{ fontSize: "2rem" }} />
                                  )}
                                </IconButton>
                              </InputAdornment>
                            ),
                          }}
                        />
                      );
                    }}
                  </Field>

                  <button type="button" className="btn password-recovery">
                    Quên mật khẩu?
                  </button>
                  <button type="submit" className="btn login">
                    Đăng nhập
                  </button>
                </form>
              );
            }}
          />
        </div>
        <div className="provision">
          &#42;Đăng nhập, bạn đồng ý với
          <span className="link"> Điều khoản sử dụng </span>&#38;
          <span className="link"> Chính sách bảo mật </span>
        </div>
      </div>
      <div className="side"></div>
    </div>
  );
};
export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const cookie = parseCookie(req);
  const [token] = parseJwtToken(cookie["__token"]);
  const [authenticated] = isValidToken(token);
  if (authenticated) {
    return {
      props: {},
      redirect: {
        statusCode: 302,
        destination: ADMIN_BASE,
      },
    };
  }
  return {
    props: {},
  };
};
export default AdminLoginPage;
