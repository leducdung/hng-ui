import React from "react";
import { NextPage, GetServerSideProps } from "next";
import AdminLayout from "@Components/AdminLayout";
import { isValidToken, parseCookie, parseJwtToken } from "@Lib/authentication/cookie-service";
import { ADMIN_ROUTES } from "@Constants";

const AdminPage: NextPage = () => {
  return (
    <AdminLayout>
      <div></div>
    </AdminLayout>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const cookie = parseCookie(req);
  const [token] = parseJwtToken(cookie["__token"]);
  const [authenticated, data] = isValidToken(token);
  if (authenticated) {
    return {
      props: {
        user: data,
      },
    };
  }
  return {
    props: {},
    redirect: {
      statusCode: 302,
      destination: ADMIN_ROUTES.LOGIN,
    },
  };
};

export default AdminPage;
