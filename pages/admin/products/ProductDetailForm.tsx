import { DEFAULT_IMG_ALT } from "@Constants";
import { NextPage } from "next";
import React, { useState } from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import clsx from "clsx";
import styles from "./ProductDetail.module.scss";
import ProductQuery from "@Lib/queries/Product.graphql";
import { useQuery } from "@apollo/client";
import { ProductDetailOutputModel } from "@Models";
import { Dot } from "@Components";
import { Dialog } from "@material-ui/core";
import ProductEditForm from "./ProductEditForm";
import SimpleModalHeader from "@Components/ImageUpload/SimpleModalHeader";

export type P_Props = {
  slug: string;
  onClose: () => void;
};

const ProductDetailForm: NextPage<P_Props> = (props) => {
  //   const { data } = props.fetchedProduct;
  const [productData, setProductData] = useState<ProductDetailOutputModel>({
    id: 0,
    price: 0,
    status: false,
    name: "",
    slug: "",
    code: "",
    description: "",
    images: [],
    categories: [],
  });
  const [visibleEditModal, setVisibleEditModal] = useState<string | null>(null);

  const setEditModal = (slug: string | null) => {
    setVisibleEditModal(slug);
  };

  const renderInfoBox = (
    title: string,
    value: JSX.Element,
    config?: { className?: string; style?: React.CSSProperties }
  ) => {
    const classes = clsx({
      [config?.className || ""]: Boolean(config?.className),
    });

    return (
      <div className={`${styles.body__infoBox} ${classes}`} style={config?.style}>
        <div className={styles.body__subTitle}>{title}</div>
        {value}
      </div>
    );
  };

  const { data } = useQuery(ProductQuery.GetProductBySlug, {
    variables: { slug: props.slug },
    onCompleted: () => {
      setProductData(data.product);
    },
  });

  return (
    <React.Fragment>
      <div className={styles.productDetail}>
        <SimpleModalHeader
          title="Chi tiết sản phẩm"
          onClose={() => {
            props.onClose();
          }}
        />
        <div className={styles.productDetail__body}>
          <div className={styles.productDetail__body__left}>
            <div className={styles.productDetail__body__left__product}>
              <div className={styles.productDetail__body__left__product__img}>
                <LazyLoadImage
                  alt={DEFAULT_IMG_ALT}
                  // src={ImageUtil.buildProductImgSrc(
                  //   data!.product.images.find((img) => img.picture)
                  // )}
                  src={"/images/app-icon.png"}
                />
              </div>
              <div className={styles.productDetail__body__left__product__name}>
                {productData.name}
              </div>
            </div>
            <div className={styles.productDetail__body__left__actions}>
              <div>
                <button
                  type="button"
                  className={styles.edit}
                  onClick={() => {
                    setEditModal(productData.slug);
                  }}>
                  Chỉnh sửa
                </button>
              </div>
            </div>
          </div>
          <div className={styles.productDetail__body__center}>
            <div className={styles.productDetail__body__center__summary}>
              <div className={styles.productDetail__body__center__summary__title}>
                Thông tin chung
              </div>

              <div className={styles.body__group_info_box} style={{ alignItems: "flex-start" }}>
                <div className={styles.productDetail__body__center__summary__col_1}>
                  <div style={{ display: "block" }} className={styles.body__group_info_box}>
                    {renderInfoBox(
                      "Mã sản phẩm",
                      <div className={styles.body__text}>{productData.code}</div>,
                      {
                        style: { flex: 1 },
                      }
                    )}
                    {renderInfoBox(
                      "Danh mục",
                      <div className={styles.body__text}>
                        {productData.categories!.map((cate) => cate.category.name).join(", ")}
                      </div>,
                      {
                        style: { flex: 1 },
                      }
                    )}
                  </div>
                  {renderInfoBox(
                    "Trạng thái",
                    <div
                      className={`${styles.body__text} ${styles.productDetail__body__right__setting__publish__status}`}>
                      <Dot
                        style={{
                          backgroundColor: productData.status ? "#2AC769" : "#F0F3F8",
                          marginRight: "0.5rem",
                        }}
                      />
                      <span>{productData.status ? "Hiển thị" : "Ẩn"}</span>
                    </div>
                  )}
                </div>
                <div className={styles.productDetail__body__center__summary__col_2}>
                  {renderInfoBox(
                    "Tên sản phẩm",
                    <div
                      className={styles.body__text}
                      style={{ display: "flex", alignItems: "center" }}>
                      <span>{productData.name}</span>
                    </div>
                  )}
                  {renderInfoBox(
                    "Thời gian hiển thị",
                    <div className={styles.body__text}>
                      {/* {data.product.timePublication
                          ? moment(data.product.timePublication).format("DD/MM/YYYY, hh:mm")
                          : TEXT.NOT_AVAILABLE} */}
                    </div>
                  )}
                </div>
              </div>
              <div className={styles.productDetail__body__center__summary__image}>
                {renderInfoBox(
                  "Mô tả ngắn",
                  <div className={styles.body__text}>{productData.description}</div>
                )}
              </div>

              <div className={styles.productDetail__body__center__summary__image}>
                {renderInfoBox(
                  "Hình ảnh sản phẩm",
                  <div className={styles.productDetail__body__center__summary__image__list}>
                    {/* {data.product.images.map((image) => {
                        return (
                          <div key={image.id} className="img">
                            <LazyLoadImage
                              alt={DEFAULT_IMG_ALT}
                              src={ImageUtil.buildProductImgSrc(image)}
                            />
                          </div>
                        );
                      })} */}
                  </div>
                )}
              </div>
              {renderInfoBox(
                "Mô tả",
                <div
                  style={{
                    cursor: "pointer",
                    color: "#6658F3",
                    textDecoration: "underline",
                  }}
                  className={styles.body__text}
                  onClick={() => {
                    //   props.toggleViewFullDescription();
                  }}>
                  Xem chi tiết
                </div>
              )}

              <div className={styles.productDetail__body__center__summary__title}>SEO</div>
              {/* {data.product.pageTitle &&
                  renderInfoBox(
                    "Tiêu đề trang",
                    <div className={styles.body__text}> {data.product.pageTitle}</div>
                  )}

                {data.product.metaDescription &&
                  renderInfoBox(
                    "Mô tả trang",
                    <div className={styles.body__text}> {data.product.metaDescription}</div>
                  )} */}

              {/* {renderInfoBox(
                  "Đường dẫn",
                  <a
                    className={styles.body__text}
                    href={ProductUtil.buildCustomerProductDetailPath(data.product)}
                    target="_blank"
                    rel="noreferrer">
                    {ProductUtil.buildCustomerProductDetailPath(data.product)}
                  </a>
                )} */}
            </div>
          </div>
          <div className={styles.productDetail__body__right}>
            <div className={styles.productDetail__body__right__setting}>
              <div className={styles.productDetail__body__right__setting__title}>Thiết lập</div>
              <div className={styles.productDetail__body__right__setting__publish}>
                {renderInfoBox(
                  "Loại sản phẩm",
                  <div
                    className={`${styles.body__text} ${styles.productDetail__body__right__setting__publish__status}`}>
                    {productData.specification
                      ? productData.specification.specificationGroups
                          .map((spec) => spec.name)
                          .join(", ")
                      : ""}
                  </div>
                )}

                {renderInfoBox(
                  "Thương hiệu",
                  <div className={styles.body__text}>{/* {data.product.brand || "-"} */}</div>
                )}

                {renderInfoBox(
                  "Xuất xứ",
                  <div className={styles.body__text}>{/* {data.product.origin} */}</div>
                )}
                {renderInfoBox(
                  "Nơi hiển thị",
                  <div className={styles.body__text}>Website Eoty • Eoty POS</div>
                )}
              </div>
            </div>
            <div className={styles.productDetail__body__right__attributes}>
              <div className={styles.productDetail__body__right__attributes__title}>Thuộc tính</div>
              {/* {attributes.map((a) => {
                  return (
                    <React.Fragment key={a.id}>
                      <div className={styles.productDetail__body__right__attributes__item}>
                        {getDetailByLanguage(a.details, selectedLang)?.name}
                      </div>
                      <div className={styles.productDetail__body__right__attributes__item__value}>
                        {a.variantValues
                          .filter((v) => v.countryCode === selectedLang)
                          .map((v) => {
                            return (
                              <div key={v.id} className="attribute-value">
                                {v.value}
                              </div>
                            );
                          })}
                      </div>
                    </React.Fragment>
                  );
                })} */}
            </div>
          </div>
        </div>
      </div>
      {productData && (
        <Dialog fullScreen open={Boolean(visibleEditModal)}>
          <ProductEditForm
            bindingData={productData}
            onCancel={() => {
              setVisibleEditModal(null);
              //   , {
              //     success: () => {
              //       props.onReloadProductDetail();
              //     },
              //   });
            }}
          />
        </Dialog>
      )}
    </React.Fragment>
  );
};

export default ProductDetailForm;
