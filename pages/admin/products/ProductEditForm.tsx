import React, { useState, useEffect } from "react";
import { Grid, TextField, Checkbox, InputAdornment } from "@material-ui/core";
import { Form } from "react-final-form";
import { FormUIUtil, loadCallback } from "@Utils";
import { NextPage } from "next";
import { FormSelect, FormSwitch, FormField, Switch, DeleteModal, ImageUpload } from "@Components";
import { Autocomplete } from "@material-ui/lab";
import {
  Event as EventIcon,
  CheckBoxOutlineBlank as CheckBoxOutlineBlankIcon,
  CheckBox as CheckBoxIcon,
  Clear as ClearIcon,
  InfoOutlined as InfoOutlinedIcon,
} from "@material-ui/icons";
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import styles from "./ProductEdit.module.scss";
import { useMutation, useQuery } from "@apollo/client";
import ProductQuery from "@Lib/queries/Product.graphql";
import { OutputImageModel, ProductDetailOutputModel } from "@Models";

export type Props = {
  bindingData: ProductDetailOutputModel;
  onCancel: () => void;
};
export type CategoryItem = {
  id: string;
  name: string;
  code: string;
  status: boolean;
};

const ProductEditForm: NextPage<Props> = (props) => {
  const [showDeleteModal, setShowDeleteModal] = useState<boolean>(false);
  // const [labels, setLabels] = useState<string[]>([]);
  const [productStatus, setProductStatus] = useState<{
    status: boolean;
    timePublication: string | null;
  }>({
    status: true,
    timePublication: null,
  });
  const [cateList, setCateList] = useState<CategoryItem[]>([]);

  const [uploadImage] = useMutation(ProductQuery.UpdateProduct, {
    onCompleted: (data) => {
      console.log(data);
    },
    onError: (err) => {
      console.log(err);
    },
  });

  const onChangeTimePublication = (date: string | null) => {
    setProductStatus((prevState) => {
      return {
        ...prevState,
        timePublication: date,
      };
    });
  };

  let submit;
  const { bindingData } = props;

  const [productData, setProductData] = useState<ProductDetailOutputModel>({
    id: 0,
    price: 0,
    status: false,
    name: "",
    slug: "",
    code: "",
    description: "",
    images: [],
    categories: [],
  });

  useEffect(() => {
    setProductData(bindingData);
  }, [bindingData]);

  const [productPictures, setProductPictures] = useState<OutputImageModel[]>(
    productData &&
      productData.images.map((img) => {
        return {
          path: img.path,
          __typename: "ProductImage",
        };
      })
  );

  const onUploadProductImg = (data) => {
    uploadImage({ variables: { productId: bindingData.id, files: [data] } });
  };

  const onRemoveProductImg = (path: string) => {
    //Temporarily dont have delete api
    const pictures = productPictures.filter((p) => p.path !== path);
    setProductPictures(pictures);
    // uploadImage({ variables: { productId: bindingData.id, files: pictures } });
  };

  const { data: catesData } = useQuery(ProductQuery.GetCategories, {
    onCompleted: () => {
      setCateList(catesData.categories);
    },
  });

  return (
    <React.Fragment>
      <div className={styles.productUpdate}>
        <div className={styles.appFormHeader}>
          <div className={styles.title}>Sửa Sản Phẩm</div>

          <button
            type="button"
            className={styles.close}
            onClick={() => {
              props.onCancel();
            }}>
            <span aria-hidden className={styles.closeIcon} onClick={() => props.onCancel()}>
              <ClearIcon style={{ fontSize: "2.2rem", cursor: "pointer" }} />
            </span>
          </button>
        </div>
        <Form
          initialValues={productData}
          onSubmit={async () => {
            // props.onSubmit({ val: values, attributeStateArray });
          }}>
          {({ handleSubmit }) => {
            submit = handleSubmit;

            return (
              <form onSubmit={handleSubmit} className={styles.productUpdateForm}>
                <Grid container justify="center" className={styles.productUpdate__container}>
                  <Grid item xl={2} lg={2} md={12} className={styles.productUpdate__left}></Grid>

                  <Grid item xl={5} lg={5} md={12} className={styles.productUpdate__center}>
                    <div className={styles.productUpdate__center__title}>Thông tin chung</div>

                    <Grid container spacing={6} justify="space-between">
                      <Grid item xs={12} md={6}>
                        <FormField
                          name="code"
                          label="Mã sản phẩm"
                          placeholder="Nhập mã sản phẩm"
                          requiredMessage="Xin nhập mã sản phẩm"
                        />
                      </Grid>
                      <Grid item xs={12} md={6}>
                        <FormField
                          name="name"
                          label="Tên sản phẩm"
                          placeholder="Nhập tên sản phẩm"
                          requiredMessage="Xin nhập tên sản phẩm"
                        />
                      </Grid>
                    </Grid>

                    <Grid container spacing={6} justify="space-between">
                      <Grid item xs={12} md={6}>
                        <FormSelect
                          name="categoryId"
                          label="Danh mục sản phẩm"
                          requiredMessage="Xin chọn danh mục"
                          selectItems={cateList.map((c: CategoryItem) => ({
                            value: c.id,
                            label: c.name,
                          }))}
                        />
                      </Grid>
                      <Grid item xs={12} md={6}>
                        <FormField
                          name="barcode"
                          label="Barcode"
                          placeholder="Nhập barcode"
                          requiredMessage="Xin nhập tên sản phẩm"
                        />
                      </Grid>
                    </Grid>

                    <Grid container spacing={6} justify="space-between">
                      <Grid item xs={12} md={6}>
                        <FormField
                          name="price"
                          label="Nhập giá bán"
                          requiredMessage="Xin nhập giá bán"
                          placeholder="Nhập giá bán sản phẩm"
                        />
                      </Grid>
                      <Grid item xs={12} md={6}>
                        <FormField
                          name="importPrice"
                          label="Giá nhập"
                          placeholder="Nhập giá nhập"
                          requiredMessage="Xin nhập giá nhập"
                          numberRequiredMessage="Chỉ được nhập số"
                        />
                      </Grid>
                    </Grid>
                    <FormField
                      name="shortDes"
                      label="Mô tả ngắn"
                      placeholder="Nhập mô tả ngắn"
                      requiredMessage=""
                    />

                    {FormUIUtil.renderFormItem(
                      "Hình ảnh sản phẩm",
                      <ImageUpload
                        fileList={productPictures}
                        max={10}
                        onChange={onUploadProductImg}
                        onDelete={onRemoveProductImg}
                        config={{ uploadModal: true }}
                      />
                    )}
                    {/* <Divider /> */}

                    <div className={`${styles.productUpdate__center__title}`}>
                      <div>Tối ưu SEO</div>
                      <div style={{ alignItems: "center" }} className={styles.flex}>
                        <div style={{ fontSize: "16px" }}>Chỉnh sửa SEO</div>
                        <Switch />
                      </div>
                    </div>
                    <FormField
                      name="pageTitle"
                      label="Tiêu đề trang"
                      placeholder="Tiêu đề trang"
                      requiredMessage="Xin nhập tiêu đề trang"
                    />
                    <FormField
                      name="metaDescription"
                      label="Mô tả trang"
                      placeholder="Nhập mô tả trang"
                      requiredMessage=""
                      fieldConfig={{
                        multiline: true,
                        rows: 4,
                        className: "form-textarea-field",
                      }}
                    />
                    <FormField
                      name="slug"
                      label="Đường dẫn"
                      placeholder="Tiêu đề trang"
                      requiredMessage="Xin nhập Đường dẫn"
                    />
                  </Grid>
                  <Grid item xl={5} lg={5} md={12} className={styles.productUpdate__right}>
                    <FormSelect
                      label="Loại thông số kỹ thuật"
                      name="spec"
                      requiredMessage=""
                      selectItems={[]}
                      placeholder=""
                    />
                    <FormField
                      name="Brand"
                      label="Thương hiệu"
                      placeholder="Nhập thương hiệu"
                      requiredMessage=""
                    />
                    <FormField
                      name="origin"
                      label="Xuất xứ"
                      placeholder="Nhập xuất xứ"
                      requiredMessage=""
                    />

                    <Autocomplete
                      limitTags={5}
                      fullWidth
                      multiple
                      options={["abc", "bcd"]}
                      getOptionLabel={(option) => option}
                      filterSelectedOptions
                      className={styles.form_autocomplete_select_field}
                      renderInput={(params) => {
                        return (
                          <div className={styles.labelTextField}>
                            <span className={styles.form_autocomplete_select_field_label}>
                              Nhãn
                            </span>
                            <TextField
                              {...params}
                              className={styles.labelTextField__root}
                              variant="outlined"
                              placeholder="Type and enter to create new label"
                              // onKeyPress={(e) => {
                              //   if (e.key === "Enter") {
                              //     e.preventDefault();
                              //     props.onCreateTag(inputTagsRef.current?.value || "", {
                              //       success: (data) => {
                              //         input.onChange([
                              //           ...input.value,
                              //           { name: data.name, id: data.id },
                              //         ]);
                              //       },
                              //     });
                              //   }
                              // }}
                            />
                          </div>
                        );
                      }}
                      onInputChange={() => {
                        // setSearchValue(value);
                      }}
                      onChange={() => {
                        // setLabels(value);
                      }}
                    />

                    <FormSwitch
                      label="Trạng thái"
                      name="status"
                      stateMessages={["Hoạt động", "Không hoạt động"]}
                    />

                    <div className={styles.productUpdate__right__publish}>
                      <div className={styles.productUpdate__right__publish__allow}>
                        <Checkbox
                          className="app-check-box"
                          checked={productStatus.timePublication !== null}
                          onChange={(e) => {
                            const isChecked = e.target.checked;

                            if (isChecked) {
                              onChangeTimePublication(new Date().toISOString());
                            } else {
                              onChangeTimePublication(null);
                            }
                          }}
                          icon={<CheckBoxOutlineBlankIcon style={{ fontSize: "2.2rem" }} />}
                          checkedIcon={<CheckBoxIcon style={{ fontSize: "2.2rem" }} />}
                        />
                        <div className="form-label" style={{ margin: "0 1rem" }}>
                          Thiết lập ngày hiển thị{" "}
                          <InfoOutlinedIcon style={{ marginLeft: "0.5rem" }} />
                        </div>
                      </div>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <DateTimePicker
                          disabled={productStatus.timePublication === null}
                          ampm={false}
                          inputVariant="outlined"
                          value={productStatus.timePublication}
                          onChange={(value) => {
                            if (value) {
                              onChangeTimePublication(value.toISOString());
                            }
                          }}
                          format="dd/MM/yyyy hh:mm"
                          className="form-text-field"
                          fullWidth
                          inputProps={{ className: "input" }}
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position="end">
                                <EventIcon style={{ fontSize: "2.2rem" }} />
                              </InputAdornment>
                            ),
                          }}
                        />
                      </MuiPickersUtilsProvider>
                    </div>
                  </Grid>
                </Grid>
              </form>
            );
          }}
        </Form>
        <div className={styles.appFormFooter}>
          <button
            className={`${styles.btn} ${styles.delete}`}
            onClick={(e) => {
              e.preventDefault();
              setShowDeleteModal(true);
              // toggleDeleteModal();
            }}>
            Xóa Sản Phẩm
          </button>
          <button
            className={`${styles.btn} ${styles.cancel}`}
            onClick={(e) => {
              e.preventDefault();
              props.onCancel();
            }}>
            Hủy
          </button>
          <button
            type="submit"
            className={`${styles.btn} ${styles.save}`}
            onClick={(e) => {
              loadCallback(submit, e);
            }}>
            Lưu
          </button>
        </div>
      </div>
      <DeleteModal
        show={showDeleteModal}
        title={"Xóa sản phẩm"}
        content={`Bạn có chắc muốn xóa sản phẩm "${productData.name}"`}
        cancelEvent={() => {
          setShowDeleteModal(false);
        }}
        confirmEvent={() => {
          //   deleteProduct(bindingData.product.id);
        }}
      />
    </React.Fragment>
  );
};

export default ProductEditForm;
