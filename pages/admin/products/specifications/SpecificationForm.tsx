import React, { useState } from "react";
import styles from "./Specifications.module.scss";
import { Form, Field } from "react-final-form";
import { Divider, Grid, TextField } from "@material-ui/core";
import { Clear as ClearIcon, Add as AddIcon } from "@material-ui/icons";
import { FormUIUtil, FormUtil } from "@Utils";
import { v4 as uuidv4 } from "uuid";

type Props = {
  handleCloseForm: () => void;
  updateModel?: boolean;
};

const SpecificationFrom: React.FC<Props> = ({ handleCloseForm, updateModel }) => {
  const [valueList, setValueList] = useState<string[]>([uuidv4()]);
  const removeValue = (id: string) => setValueList((list) => list.filter((uuid) => uuid !== id));
  const addValue = () => setValueList((list) => [...list, uuidv4()]);
  return (
    <div className={styles.specificationFrom}>
      <div className={styles.appFormHeader}>
        <div className={styles.title}>
          {updateModel ? "Chỉnh sửa loại thông số" : "Tạo Loại thông số"}
        </div>

        <button
          type="button"
          className={styles.close}
          onClick={() => {
            handleCloseForm;
          }}>
          <span aria-hidden className={styles.closeIcon} onClick={handleCloseForm}>
            <ClearIcon style={{ fontSize: "2.2rem", cursor: "pointer" }} />
          </span>
        </button>
      </div>
      <Form
        initialValuesEqual={() => true}
        onSubmit={async (values) => {
          console.log(values);
        }}
        render={({ handleSubmit }) => {
          return (
            <form onSubmit={handleSubmit} className={styles.form}>
              <Field name="type" validate={FormUtil.Rule.required("Xin nhập loại thông số")}>
                {({ input, meta, ...rest }) => {
                  return FormUIUtil.renderFormItem(
                    "Loại Thông Số",
                    <TextField
                      {...input}
                      {...rest}
                      fullWidth
                      placeholder="Nhập tên loại thông số"
                      className="form-text-field"
                      inputProps={{ className: "input" }}
                      variant="outlined"
                      onChange={(e) => input.onChange(e.target.value)}
                      helperText={meta.touched ? meta.error : ""}
                      error={meta.error && meta.touched}
                    />
                  );
                }}
              </Field>
              <div className={styles.group}>
                <Grid container alignItems="center" spacing={1}>
                  <Grid item xs={4} className={styles.label}>
                    Nhóm thông số
                  </Grid>
                  <Grid item xs={7}>
                    <Field
                      name="group"
                      validate={FormUtil.Rule.required("Xin nhập tên nhóm thông số")}
                      subscription={{
                        touched: true,
                        error: true,
                        value: true,
                      }}>
                      {({ input, meta, ...rest }) => {
                        return (
                          <TextField
                            {...input}
                            {...rest}
                            fullWidth
                            size="small"
                            placeholder="Nhập tên nhóm thông số"
                            className=""
                            inputProps={{ className: "input" }}
                            variant="outlined"
                            onChange={(e) => input.onChange(e.target.value)}
                            helperText={meta.touched ? meta.error : ""}
                            error={meta.error && meta.touched}
                          />
                        );
                      }}
                    </Field>
                  </Grid>
                  <Grid container item xs={1} justify="flex-end">
                    <ClearIcon
                      style={{ color: "#BDC6D7", cursor: "pointer" }}
                      //   onClick={() => removeValue(uuid)}
                    />
                  </Grid>
                </Grid>
                <div className={styles.line}></div>
                {valueList.map((uuid, idx) => (
                  <Grid
                    container
                    alignItems="center"
                    className={styles.item}
                    spacing={1}
                    key={uuid}>
                    <Grid item xs={4} className={styles.label}>
                      Thông số {idx + 1}
                    </Grid>
                    <Grid item xs={7}>
                      <Field
                        name="value"
                        validate={FormUtil.Rule.required("Nhập giá trị thuộc tính...")}
                        subscription={{
                          touched: true,
                          error: true,
                          value: true,
                        }}>
                        {({ input, meta, ...rest }) => {
                          return (
                            <TextField
                              {...input}
                              {...rest}
                              size="small"
                              fullWidth
                              placeholder="Nhập giá trị thuộc tính..."
                              className=""
                              inputProps={{ className: "input" }}
                              variant="outlined"
                              onChange={(e) => input.onChange(e.target.value)}
                              helperText={meta.touched ? meta.error : ""}
                              error={meta.error && meta.touched}
                            />
                          );
                        }}
                      </Field>
                    </Grid>
                    <Grid container item xs={1} justify="flex-end">
                      <ClearIcon
                        style={{
                          color: "#BDC6D7",
                          cursor: "pointer",
                        }}
                        onClick={() => removeValue(uuid)}
                      />
                    </Grid>
                  </Grid>
                ))}
                <Divider style={{ marginBottom: "1.3rem" }} />
                <div className={styles.add} onClick={() => addValue()}>
                  <AddIcon style={{ fontSize: "2rem", color: "#6C778D", marginRight: "1.5rem" }} />
                  Thêm thông số
                </div>
                <Divider style={{ marginTop: "1.3rem" }} />
              </div>
              <div className={`${styles.add} ${styles.addValue}`}>
                <AddIcon style={{ fontSize: "2rem", color: "#6658F3", marginRight: "1rem" }} />
                Thêm nhóm thông số mới
              </div>
            </form>
          );
        }}
      />
      <div className={styles.appFormFooter}>
        {updateModel && (
          <button
            className={`${styles.btn} ${styles.delete}`}
            onClick={(e) => {
              e.preventDefault();
              // toggleDeleteModal();
            }}>
            Xóa
          </button>
        )}
        <button
          className={`${styles.btn} ${styles.cancel}`}
          onClick={(e) => {
            e.preventDefault();
            handleCloseForm();
          }}>
          Hủy
        </button>
        <button
          type="submit"
          className={`${styles.btn} ${styles.save}`}
          onClick={() => {
            // loadCallback(submit, e);
          }}>
          Lưu
        </button>
      </div>
    </div>
  );
};

export default SpecificationFrom;
