import React, { useState } from "react";
import { NextPage } from "next";
import styles from "./Specifications.module.scss";
import {
  Search as SearchIcon,
  MoreHoriz as MoreHorizIcon,
  List as ListIcon,
  Add as AddIcon,
} from "@material-ui/icons";
import { Dialog, InputAdornment, TextField } from "@material-ui/core";
import SpecificationFrom from "./SpecificationForm";
import AdminLayout from "@Components/AdminLayout";

const SpecificationsPage: NextPage = () => {
  const [openCreateSpecification, setOpenCreateSpecification] = useState<boolean>(false);
  const [openUpdateSpecification, setOpenUpdateSpecification] = useState<boolean>(false);

  return (
    <AdminLayout>
      <div className={styles.specification}>
        <div className={styles.header}>
          <div className={styles.title}>Loại thông số (20)</div>
          <div className={styles.groupSearch}>
            <TextField
              fullWidth
              placeholder="Tìm kiếm"
              variant="outlined"
              className={styles.search}
              inputProps={{ className: "input" }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon style={{ fontSize: "2.2rem", color: "#272B2F" }} />
                  </InputAdornment>
                ),
              }}
            />
            <button className={styles.btnFilter}>
              <ListIcon style={{ fontSize: "2.2rem", color: "#272B2F", marginRight: "1.1rem" }} />
              Lọc
            </button>
            <MoreHorizIcon style={{ fontSize: "2.2rem", color: "#272B2F" }} />
          </div>
        </div>
        <div className={`${styles.list} ${styles.listHeader}`}>
          <div className={styles.name}>Tên loại thông số</div>
          <div className={styles.group}>Nhóm thông số</div>
          <div className={styles.quantity}>Số lượng thông số</div>
        </div>
        <div
          className={`${styles.list} ${styles.item}`}
          onClick={() => {
            setOpenUpdateSpecification(true);
          }}>
          <div className={styles.name}>Tên loại thông số</div>
          <div className={styles.group}>Nhóm thông số</div>
          <div className={styles.quantity}>Số lượng thông số</div>
        </div>
      </div>
      <div />
      <div className={styles.floatingBtnItem} onClick={() => setOpenCreateSpecification(true)}>
        <AddIcon
          style={{
            fontSize: "2.2rem",
            color: "#fff",
          }}
        />
      </div>
      <Dialog fullScreen open={openCreateSpecification || openUpdateSpecification}>
        <SpecificationFrom
          handleCloseForm={() => {
            setOpenCreateSpecification(false);
            setOpenUpdateSpecification(false);
          }}
          updateModel={openUpdateSpecification}
        />
      </Dialog>
    </AdminLayout>
  );
};

export default SpecificationsPage;
