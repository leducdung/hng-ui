import React, { useEffect, useState } from "react";
import { currencyFormat } from "../../../utils";
import { Switch } from "@Components";
import styles from "./ProductListing.module.scss";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { DEFAULT_IMG_ALT } from "@Constants";
import { ProductOutputModel } from "@Models";

type Props = {
  prod: ProductOutputModel;
  openProductDetailModal: (slug: string) => void;
};

const ProductListingItem = ({ prod, openProductDetailModal }: Props) => {
  const [productData, setProductData] = useState<ProductOutputModel>({
    id: 0,
    price: 0,
    status: false,
    name: "",
    slug: "",
    code: "",
    description: "",
  });

  useEffect(() => {
    setProductData(prod);
  }, [prod]);
  return (
    <div
      className={styles.productListing__content__list__item}
      onClick={() => {
        openProductDetailModal(productData.slug);
      }}>
      <div className={`${styles.productText} ${styles.productListing__content__list__code}`}>
        {productData.code}
      </div>
      <div className={`${styles.productListing__content__list__name}`}>
        <div className={styles.avatar}>
          <LazyLoadImage
            alt={DEFAULT_IMG_ALT}
            // src={ImageUtil.buildProductImgSrc(productData.images[0])}
            src={"/images/app-icon.png"}
          />
        </div>
        <div className={`${styles.productText} ${styles.label}`}>{productData.name}</div>
      </div>
      <div className={`${styles.productText} ${styles.productListing__content__list__variants}`}>
        {/* {productData.productVariants.length} */}0
      </div>
      <div className={`${styles.productText} ${styles.productListing__content__list__status}`}>
        <Switch name="" checked={productData.status} />
        <div className={styles.productStatus}>{productData.status ? "Hiển thị" : "Ẩn"}</div>
      </div>
      <div className={`${styles.productText} ${styles.productListing__content__list__price}`}>
        {productData.price ? `${currencyFormat(productData.price)}đ` : 0}
      </div>
    </div>
  );
};

export default React.memo(ProductListingItem);
