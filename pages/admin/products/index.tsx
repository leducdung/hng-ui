import { Avatar, Dialog, Divider, TextField, InputAdornment } from "@material-ui/core";
import { NextPage } from "next";
import React, { useState } from "react";
import InfiniteScroll from "react-infinite-scroller";
import ProductCreateForm from "./ProductCreateForm";
import ProductDetailForm from "./ProductDetailForm";
import GetProduct from "@Lib/queries/Product.graphql";
import ProductListingItem from "./ProductListingItem";
import { useLazyQuery, useQuery } from "@apollo/client";
import styles from "./ProductListing.module.scss";
import { CategoryCard1, Loading } from "@Components";
import {
  Search as SearchIcon,
  Add as AddIcon,
  FilterList as FilterListIcon,
} from "@material-ui/icons";
import AdminLayout from "@Components/AdminLayout";

export type P_Props = {
  onCompleteCreateProduct: () => void;
};

export type ProductItem = {
  id: string;
  name: string;
  price: number;
  status: boolean;
  slug: string;
  images: {
    path: string;
  };
  discount: string | null;
};

const ProductListingPresenter: NextPage<P_Props> = (props) => {
  const [visibleProductCreateModal, setVisibleProductCreateModal] = useState<boolean>(false);
  const [visibleProductDetailSlug, setVisibleProductDetailSlug] = useState<string>("");
  // const { data: productsData, loading } = useQuery(ProductQuery.GetProductByCategory, {
  //   variables: { categoryId: 17, pageNum: 1, pageSize: 100 },
  // });
  const [activeCate, setActiveCate] = useState<number>(0);

  const { data: catesData, loading: cateLoading } = useQuery(GetProduct.GetCategories);
  const [getProdByCate, { data: productsData, loading: prodLoading }] = useLazyQuery(
    GetProduct.GetProductByCategory
  );

  const onSelectCategory = (id) => {
    getProdByCate({ variables: { categoryId: id, pageNum: 1, pageSize: 100 } });
    setActiveCate(id);
  };

  const openProductDetailModal = (slug: string) => {
    setVisibleProductDetailSlug(slug);
  };

  return (
    <AdminLayout>
      <div className={styles.productListing}>
        <div className={styles.productListing__category}>
          <div className={styles.productListing__category__header}>
            <div className={styles.title}>Danh mục</div>
          </div>
          <div
            className={`${styles.productListing__category__item} ${
              activeCate === 0 && styles.active
            }`}
            onClick={() => {
              onSelectCategory(0);
            }}>
            <Avatar alt="All" src="/static/images/avatar/1.jpg">
              T
            </Avatar>
            <div className={styles.productListing__category__item__name}>Tất cả</div>
          </div>
          <Divider style={{ width: "100%", margin: "1.6rem 0" }} />
          {cateLoading && <Loading />}
          <div className={styles.productListing__category__list}>
            {!cateLoading &&
              catesData.categories.map((cate) => {
                const isActive = activeCate === cate.id;
                return (
                  <CategoryCard1
                    key={cate.id}
                    name={cate.name}
                    onClick={() => {
                      onSelectCategory(cate.id);
                    }}
                    picture={cate.picture || undefined}
                    selected={isActive}
                    onActionClick={() => {
                      // openCategoryPopup(e, cate);
                    }}
                    moreIcon={true}
                    isAvatar={true}
                  />
                );
              })}
          </div>
        </div>
        <div className={styles.productListing__content}>
          <div className={styles.productListing__content__header}>
            {catesData && (
              <div className={styles.productListing__content__header__title}>
                {catesData.categories.find((e) => e.id === activeCate)?.name || "Tất cả"} (
                {productsData ? productsData.getProductByCategory.totalItems : "0"})
              </div>
            )}
            <div className={styles.productListing__content__header__actions}>
              <TextField
                placeholder="Tìm kiếm"
                variant="outlined"
                className={styles[`search-input`]}
                inputProps={{ className: styles.input }}
                InputProps={{
                  // style: { fontSize: "1.4rem" },
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon className={styles.icon} fontSize="large" />
                    </InputAdornment>
                  ),
                  classes: { notchedOutline: styles[`notchedOutline`] },
                }}
                onChange={() => {
                  // props.onSearch(e.target.value);
                }}
              />
              <button className={styles.productListing__content__header__actions__filter}>
                <FilterListIcon style={{ fontSize: "2.2rem", marginRight: "1rem" }} />
                Filter
              </button>
            </div>
          </div>
          <div className={styles.productListing__content__list}>
            <div className={styles.productListing__content__list__header}>
              <div className={`${styles.text} ${styles.productListing__content__list__code}`}>
                Mã Sản Phẩm
              </div>
              <div
                className={`${styles.text} ${styles.productListing__content__list__name} ${styles.info}`}>
                Tên Sản Phẩm
              </div>
              <div className={`${styles.text} ${styles.productListing__content__list__variants}`}>
                Biến thể
              </div>
              <div className={`${styles.text} ${styles.productListing__content__list__status}`}>
                Trạng thái
              </div>
              <div className={`${styles.text} ${styles.productListing__content__list__price}`}>
                Giá bán
              </div>
            </div>
          </div>
          <InfiniteScroll
            useWindow={false}
            pageStart={1}
            initialLoad={false}
            // hasMore={
            // !props.fetchedProduct.loading &&
            // props.fetchedProduct.totalPages > props.fetchedProduct.page
            // }
            loadMore={() => {
              // props.onLoadMore();
            }}
            style={{ width: "100%", overflow: "auto" }}>
            {prodLoading && <Loading />}
            {productsData &&
              productsData.getProductByCategory.products.map((prod, idx: number) => {
                return (
                  <ProductListingItem
                    key={idx}
                    prod={prod}
                    openProductDetailModal={openProductDetailModal}
                  />
                );
              })}
          </InfiniteScroll>
        </div>
      </div>

      <div
        className={styles.floatingBtnItem}
        onClick={(e) => {
          e.preventDefault();
          setVisibleProductCreateModal(true);
        }}>
        <AddIcon
          style={{
            fontSize: "2.2rem",
            color: "#fff",
          }}
        />
      </div>

      <Dialog fullScreen open={visibleProductCreateModal}>
        <ProductCreateForm
          //   allCategories={props.all_category_list.data}
          onCancel={() => {
            setVisibleProductCreateModal(false);
          }}
          onComplete_Create={() => {
            setVisibleProductCreateModal(false);
            props.onCompleteCreateProduct();
          }}
        />
      </Dialog>
      <Dialog fullScreen open={Boolean(visibleProductDetailSlug)}>
        <ProductDetailForm
          // categories={props.all_category_list.data}
          slug={visibleProductDetailSlug!}
          onClose={() => {
            setVisibleProductDetailSlug("");
          }}
          // onComplete_Delete={() => {
          //   props.closeProductDetailModal();
          // }}
        />
      </Dialog>
    </AdminLayout>
  );
};

export default ProductListingPresenter;
