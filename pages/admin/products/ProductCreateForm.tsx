import React, { useEffect, useState } from "react";
import { NextPage } from "next";
import { Form } from "react-final-form";
import { Checkbox, Grid, InputAdornment, TextField } from "@material-ui/core";
import { FormField, FormSelect, FormSwitch, Switch, NotificationModal } from "@Components";
import styles from "./ProductCreate.module.scss";
import {
  Event as EventIcon,
  CheckBoxOutlineBlank as CheckBoxOutlineBlankIcon,
  CheckBox as CheckBoxIcon,
  InfoOutlined as InfoOutlinedIcon,
  Clear as ClearIcon,
} from "@material-ui/icons";
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { Autocomplete } from "@material-ui/lab";
import { loadCallback } from "@Utils";
import { useMutation, useQuery } from "@apollo/client";
import ProductQuery from "@Lib/queries/Product.graphql";
import { useRouter } from "next/router";
import { FORM_ERROR } from "final-form";

export type P_Props = {
  onCancel: () => void;
  onComplete_Create?: () => void;
};
export type CategoryItem = {
  id: string;
  name: string;
  code: string;
  status: boolean;
};

const ProductPage: NextPage<P_Props> = (props) => {
  let submit;
  const router = useRouter();
  const [mounted, setMounted] = useState(false);
  const [productStatus, setProductStatus] = useState<{
    status: boolean;
    timePublication: string | null;
  }>({
    status: true,
    timePublication: null,
  });
  const [showNotificationModal, setShownotificationModal] = useState<boolean>(false);
  const [labels, setLabels] = useState<string[]>([]);
  const [cateList, setCateList] = useState<CategoryItem[]>([]);
  useEffect(() => {
    setMounted(true);
  }, []);

  const onChangeTimePublication = (date: string | null) => {
    setProductStatus((prevState) => {
      return {
        ...prevState,
        timePublication: date,
      };
    });
  };

  const [createProduct] = useMutation(ProductQuery.CreateProduct);

  const handleSubmit = async (values) => {
    try {
      await createProduct({
        variables: {
          ...values,
          tags: labels,
          price: parseInt(values.price),
          importPrice: parseInt(values.importPrice),
        },
      });
      const timeout = setTimeout(() => {
        if (mounted) {
          router.push("/admin/product");
        } else {
          clearTimeout(timeout);
        }
      }, 5000);
      setShownotificationModal(true);
    } catch (error) {
      const errors: Record<string, unknown> = {
        [FORM_ERROR]: "Đã có lỗi xảy ra xin hãy kiểm tra lại thông tin",
      };
      return errors;
    }
  };

  const { data: catesData } = useQuery(ProductQuery.GetCategories, {
    onCompleted: () => {
      setCateList(catesData.categories);
    },
  });

  return (
    <React.Fragment>
      <div className={styles.productCreate}>
        <div className={styles.appFormHeader}>
          <div className={styles.title}>Tạo Sản Phẩm</div>

          <button
            type="button"
            className={styles.close}
            onClick={() => {
              props.onCancel();
            }}>
            <span aria-hidden className={styles.closeIcon} onClick={() => props.onCancel()}>
              <ClearIcon style={{ fontSize: "2.2rem", cursor: "pointer" }} />
            </span>
          </button>
        </div>
        <Form
          onSubmit={async (values) => {
            handleSubmit(values);
          }}>
          {({ handleSubmit }) => {
            submit = handleSubmit;
            return (
              <form onSubmit={handleSubmit} className={styles.productCreateForm}>
                <Grid container justify="center" className={styles.productCreate__container}>
                  <Grid item xl={5} lg={5} md={12} className={styles.productCreate__center}>
                    <div className={styles.productCreate__title}>Thông tin chung</div>
                    <Grid container spacing={6} justify="space-between">
                      <Grid item xs={12} md={6}>
                        <FormField
                          name="code"
                          label="Mã sản phẩm"
                          placeholder="Nhập mã sản phẩm"
                          requiredMessage="Xin nhập mã sản phẩm"
                        />
                      </Grid>
                      <Grid item xs={12} md={6}>
                        <FormField
                          name="name"
                          label="Tên sản phẩm"
                          placeholder="Nhập tên sản phẩm"
                          requiredMessage="Xin nhập tên sản phẩm"
                        />
                      </Grid>
                    </Grid>
                    <Grid container spacing={6} justify="space-between">
                      <Grid item xs={12} md={6}>
                        <FormSelect
                          name="categoryId"
                          label="Danh mục sản phẩm"
                          requiredMessage="Xin chọn danh mục"
                          selectItems={cateList.map((c: CategoryItem) => ({
                            value: c.id,
                            label: c.name,
                          }))}
                        />
                      </Grid>
                      <Grid item xs={12} md={6}>
                        <FormField
                          name="barcode"
                          label="Barcode"
                          placeholder="Nhập barcode"
                          requiredMessage="Xin nhập tên sản phẩm"
                        />
                      </Grid>
                    </Grid>
                    <Grid container spacing={6} justify="space-between">
                      <Grid item xs={12} md={6}>
                        <FormField
                          name="price"
                          label="Giá bán"
                          placeholder="Nhập giá bán"
                          requiredMessage="Xin nhập giá bán"
                          numberRequiredMessage="Chỉ được nhập số"
                        />
                      </Grid>
                      <Grid item xs={12} md={6}>
                        <FormField
                          name="importPrice"
                          label="Giá nhập"
                          placeholder="Nhập giá nhập"
                          requiredMessage="Xin nhập giá nhập"
                          numberRequiredMessage="Chỉ được nhập số"
                        />
                      </Grid>
                    </Grid>
                    <FormField
                      name="shortDes"
                      label="Mô tả ngắn"
                      placeholder="Nhập mô tả ngắn"
                      requiredMessage=""
                    />

                    <div className={styles.productCreate__title}>
                      <div>Tối ưu SEO</div>
                      <div style={{ alignItems: "center" }} className="flex">
                        <div style={{ fontSize: "16px" }}>Chỉnh sửa SEO</div>
                        <Switch />
                      </div>
                    </div>
                    <FormField
                      name="seoTitle"
                      label="Tiêu đề trang"
                      placeholder="Tiêu đề trang"
                      requiredMessage="Xin nhập tiêu đề trang"
                    />
                    <FormField
                      name="seoDescription"
                      label="Mô tả trang"
                      placeholder="Nhập mô tả trang"
                      requiredMessage="Xin nhập mô tả trang"
                      fieldConfig={{
                        multiline: true,
                        rows: 4,
                        className: "form-textarea-field",
                      }}
                    />
                    <FormField
                      name="slug"
                      label="Đường dẫn"
                      placeholder="Tiêu đề trang"
                      requiredMessage=""
                    />
                  </Grid>
                  <Grid item xl={4} lg={5} md={12} className={styles.productCreate__right}>
                    <FormSelect
                      label="Loại thông số kỹ thuật"
                      name="spec"
                      requiredMessage=""
                      selectItems={[]}
                      placeholder=""
                    />
                    <FormField
                      name="Brand"
                      label="Thương hiệu"
                      placeholder="Nhập thương hiệu"
                      requiredMessage=""
                    />
                    <FormField
                      name="origin"
                      label="Xuất xứ"
                      placeholder="Nhập xuất xứ"
                      requiredMessage=""
                    />

                    <Autocomplete
                      limitTags={5}
                      fullWidth
                      multiple
                      options={["abc", "bcd"]}
                      getOptionLabel={(option) => option}
                      filterSelectedOptions
                      className={styles.form_autocomplete_select_field}
                      renderInput={(params) => {
                        return (
                          <div className={styles.labelTextField}>
                            <span className={styles.form_autocomplete_select_field_label}>
                              Nhãn
                            </span>
                            <TextField
                              {...params}
                              className={styles.labelTextField__root}
                              variant="outlined"
                              placeholder="Type and enter to create new label"
                              // onKeyPress={(e) => {
                              //   if (e.key === "Enter") {
                              //     e.preventDefault();
                              //     props.onCreateTag(inputTagsRef.current?.value || "", {
                              //       success: (data) => {
                              //         input.onChange([
                              //           ...input.value,
                              //           { name: data.name, id: data.id },
                              //         ]);
                              //       },
                              //     });
                              //   }
                              // }}
                            />
                          </div>
                        );
                      }}
                      onInputChange={(_, value) => {
                        console.log(value);
                        // setSearchValue(value);
                      }}
                      onChange={(_, value) => {
                        setLabels(value);
                      }}
                    />

                    <FormSwitch
                      label="Trạng thái"
                      name="status"
                      stateMessages={["Hoạt động", "Không hoạt động"]}
                    />

                    <div className={styles.productCreate__right__publish}>
                      <div className={styles.productCreate__right__publish__allow}>
                        <Checkbox
                          className="app-check-box"
                          checked={productStatus.timePublication !== null}
                          onChange={(e) => {
                            const isChecked = e.target.checked;

                            if (isChecked) {
                              onChangeTimePublication(new Date().toISOString());
                            } else {
                              onChangeTimePublication(null);
                            }
                          }}
                          icon={<CheckBoxOutlineBlankIcon style={{ fontSize: "2.2rem" }} />}
                          checkedIcon={<CheckBoxIcon style={{ fontSize: "2.2rem" }} />}
                        />
                        <div className="form-label" style={{ margin: "0 1rem" }}>
                          Thiết lập ngày hiển thị{" "}
                          <InfoOutlinedIcon style={{ marginLeft: "0.5rem" }} />
                        </div>
                      </div>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <DateTimePicker
                          disabled={productStatus.timePublication === null}
                          ampm={false}
                          inputVariant="outlined"
                          value={productStatus.timePublication}
                          onChange={(value) => {
                            if (value) {
                              onChangeTimePublication(value.toISOString());
                            }
                          }}
                          format="dd/MM/yyyy hh:mm"
                          className="form-text-field"
                          fullWidth
                          inputProps={{ className: "input" }}
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position="end">
                                <EventIcon style={{ fontSize: "2.2rem" }} />
                              </InputAdornment>
                            ),
                          }}
                        />
                      </MuiPickersUtilsProvider>
                    </div>
                  </Grid>
                </Grid>
              </form>
            );
          }}
        </Form>
        <div className={styles.appFormFooter}>
          <button
            className={`${styles.btn} ${styles.cancel}`}
            onClick={(e) => {
              e.preventDefault();
              props.onCancel();
            }}>
            Hủy
          </button>
          <button
            type="submit"
            className={`${styles.btn} ${styles.save}`}
            onClick={(e) => {
              loadCallback(submit, e);
            }}>
            Lưu
          </button>
        </div>
      </div>

      <NotificationModal show={showNotificationModal} title={"Tạo sản phẩm thành công!"} content="">
        <button
          onClick={() => {
            setShownotificationModal(false);
            props.onCancel();
          }}
          className="confirm">
          Xác nhận
        </button>
      </NotificationModal>
    </React.Fragment>
  );
};

export default ProductPage;
