import { ADMIN_ROUTES, DEFAULT_PRODUCT_IMG_PLACEHOLDER } from "@Constants";
import { currencyFormat } from "@Utils";
import moment from "moment";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { GoldWithdrawListOutput } from ".";
import styles from "./GoldTransactionListItem.module.scss";
import { Dot } from "@Components";
import Link from "next/link";
import { useEffect, useState } from "react";

export type Props = {
  transaction: GoldWithdrawListOutput;
};

const GoldTransactionListItem = (props: Props) => {
  // const transactionData: GoldWithdrawListOutput = props.transaction;
  const [transactionData, setTransactionItem] = useState<GoldWithdrawListOutput>({
    id: 0,
    code: "",
    walletId: 0,
    nameCustomer: "",
    amountGold: 0,
    status: "",
    storeId: 0,
    fee: 0,
    addressCustomer: "",
    amountGoldDelivered: 0,
    updatedAt: "",
    createdAt: "",
  });

  const switchState = (state: string) => {
    switch (state) {
      case "NEW":
        return { color: "#08C4E3", content: "Mới" };
      default:
        return { color: "#08C4E3", content: "" };
    }
  };

  const { transaction } = props;
  useEffect(() => {
    setTransactionItem(transaction);
  }, [transaction]);

  return (
    <Link href={ADMIN_ROUTES.GOLD_WITHDRAWAL + "/" + transactionData.id} passHref>
      <div className={styles.goldTrans_list_item} key={transactionData.id}>
        <div className={styles.text}>{transactionData.id || ""}</div>
        <div className={styles.avatar_group}>
          <div className={styles.avatar}>
            <LazyLoadImage alt={""} src={DEFAULT_PRODUCT_IMG_PLACEHOLDER} />
          </div>
          <div className={styles.info_group}>
            <div className={`${styles.text} ${styles.title}`}>{transactionData.nameCustomer}</div>
            <div className={styles.text}>{/* {transactionData.customer.phone || ""} */}</div>
          </div>
        </div>
        <div className={styles.text}>{transactionData.amountGold}</div>
        <div className={styles.text}>
          {transactionData.fee ? `${currencyFormat(transactionData.fee)}đ` : 0}
        </div>
        <div className={styles.text}>{/* Cửa hàng rút */}</div>
        <div className={styles.text}>
          {moment(new Date(parseInt(transactionData.createdAt))).format("DD/MM/yyyy • hh:MM:ss")}
        </div>
        <div className={`${styles.text} ${styles.status}`}>
          <Dot
            style={{
              backgroundColor: switchState(transactionData.status).color,
              marginRight: "1.5rem",
            }}
          />
          <div className={styles.text}>{switchState(transactionData.status).content}</div>
        </div>
      </div>
    </Link>
  );
};

export default GoldTransactionListItem;
