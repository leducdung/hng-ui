import { Loading } from "@Components";
import { InputAdornment, TextField } from "@material-ui/core";
import React, { useState } from "react";
import InfiniteScroll from "react-infinite-scroller";
import styles from "./GoldTransactionListing.module.scss";
import GoldTransactionListItem from "./GoldTransactionListItem";
import { Search as SearchIcon, FilterList as FilterListIcon } from "@material-ui/icons";
import { useQuery } from "@apollo/client";
import TransactionQuery from "@Lib/queries/Transaction.graphql";
import AdminLayout from "@Components/AdminLayout";

export type GoldWithdrawListOutput = {
  id: number;
  code: string;
  walletId: number;
  nameCustomer: string;
  amountGold: number;
  status: string;
  storeId: number;
  fee: number;
  addressCustomer: string;
  amountGoldDelivered: 0;
  updatedAt: string;
  createdAt: string;
};

const GoldTransactionList = () => {
  const [goldWithdrawList, setGoldWithdrawList] = useState<GoldWithdrawListOutput[]>([]);

  const { data } = useQuery(TransactionQuery.getGoldWithdrawList, {
    onCompleted: () => {
      setGoldWithdrawList(data.getAllTransactionGoldWithDraw);
    },
  });
  return (
    <AdminLayout>
      <div className={styles.goldTrans_page}>
        <div className={styles.goldTrans_page__content}>
          <div className={styles.header}>
            <div className={styles.title}>Tất cả ({goldWithdrawList.length})</div>
            <div className={styles.actions}>
              <TextField
                placeholder="Tìm kiếm"
                variant="outlined"
                className={styles[`search-input`]}
                inputProps={{ className: styles.input }}
                InputProps={{
                  // style: { fontSize: "1.4rem" },
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon className={styles.icon} fontSize="large" />
                    </InputAdornment>
                  ),
                  classes: { notchedOutline: styles[`notchedOutline`] },
                }}
                onChange={() => {
                  // props.onSearch(e.target.value);
                }}
              />
              <button className={styles.filter}>
                <FilterListIcon style={{ fontSize: "2.2rem", marginRight: "1rem" }} />
                Filter
              </button>
            </div>
          </div>
          <div className={styles.goldTrans_page__content__list}>
            <div className={styles.header}>
              <div className={styles.text}>Mã giao dịch</div>
              <div className={styles.text}>Khách hàng</div>
              <div className={styles.text}>Số Lượng (Chỉ)</div>
              <div className={styles.text}>Phí Giao Dịch</div>
              <div className={styles.text}>Cửa Hàng Rút</div>
              <div className={styles.text}>Thời Gian Giao Dịch</div>
              <div className={`${styles.text} ${styles.status}`}>Trạng Thái</div>
            </div>
          </div>
          <InfiniteScroll
            pageStart={0}
            loadMore={(e) => {
              console.log(e);
            }}
            //   hasMore={hasMore}
            useWindow={false}
            loader={<Loading />}
            style={{ width: "100%", overflow: "auto" }}>
            {goldWithdrawList.map((wd: GoldWithdrawListOutput) => (
              <GoldTransactionListItem key={wd.id} transaction={wd} />
            ))}
          </InfiniteScroll>
        </div>
      </div>
      {/* <FloatingButton
          visible={false}
          onClickPrimary={() => history.push(APP_ROUTES.STORAGE.CREATE_SUPPLIER)}
          btns={[]}
        /> */}
    </AdminLayout>
  );
};

export default GoldTransactionList;
