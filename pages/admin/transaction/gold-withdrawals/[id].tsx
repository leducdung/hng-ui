import { initializeApollo } from "@Lib/apollo-client";
import { GetServerSideProps } from "next";
import TransactionQuery from "@Lib/queries/Transaction.graphql";
import React from "react";
import styles from "./WithdrawItem.module.scss";
import { Clear as ClearIcon, ArrowDropUp as ArrowDropUpIcon } from "@material-ui/icons";
import router from "next/router";
import moment from "moment";
import { MultiSteps } from "@Components";
import { Grid } from "@material-ui/core";
import clsx from "clsx";
import { currencyFormat } from "@Utils";

export enum OrderStatus {
  NEW = "NEW",
  CONFIRMED = "CONFIRMED",
  DELIVERING = "DELIVERING",
  COMPLETED = "COMPLETED",
  CANCEL = "CANCEL",
  CASHBACK = "CASHBACK",
}

const ProductDetail = ({ withdraw }) => {
  const onCancelEdit = () => {
    router.push("/admin/transaction/gold-withdrawals");
  };

  const renderInfoBox = (
    title: string,
    value: JSX.Element,
    config?: { className?: string; style?: React.CSSProperties }
  ) => {
    const classes = clsx({
      [config?.className || ""]: Boolean(config?.className),
    });

    return (
      <div className={`${styles.body__infoBox} ${classes}`} style={config?.style}>
        <div className={styles.body__subTitle}>{title}</div>
        {value}
      </div>
    );
  };

  return (
    <div className={styles["withdraw-update"]}>
      <div className={styles.appFormHeader}>
        <div className={styles.title}>Rút Vàng</div>

        <button
          type="button"
          className={styles.close}
          onClick={(e) => {
            e.preventDefault();
            onCancelEdit();
          }}>
          <span aria-hidden className={styles.closeIcon}>
            <ClearIcon style={{ fontSize: "2.2rem", cursor: "pointer" }} />
          </span>
        </button>
      </div>
      <div className={styles.container}>
        <div className={styles.container__header}>
          <div className={styles.container__header__info}>
            <div className={styles.container__header__info__title}>{withdraw.code}</div>
            <div className={styles[`container__header__info__sub-title`]}>
              {moment(new Date(parseInt(withdraw.createdAt))).format("DD/MM/yyyy • hh:MM:ss")}
            </div>
          </div>
          {withdraw.status === OrderStatus.CANCEL ? (
            <MultiSteps initialState={["Đã Huỷ"]} currentState={"Đã Huỷ"} />
          ) : (
            <MultiSteps
              initialState={["Mới", "Xác nhận", "Giao vàng", "Hoàn thành"]}
              currentState={(() => {
                switch (withdraw.status) {
                  case OrderStatus.NEW:
                    return "Mới";
                  case OrderStatus.CONFIRMED:
                    return "Xác nhận";
                  case OrderStatus.DELIVERING: {
                    return "Giao vàng";
                  }
                  case OrderStatus.COMPLETED:
                    return "Hoàn thành";
                  default:
                    return "";
                }
              })()}
            />
          )}
        </div>
        <Grid container spacing={2} className={styles.container__body}>
          <Grid item xs={4}>
            <div className={styles.summary}>
              <div className={styles.summary__title}>Thông tin Rút vàng</div>
              <Grid container>
                <Grid item xs={6}>
                  {renderInfoBox(
                    "Khách hàng",
                    <a className={styles.body__text}>{withdraw.nameCustomer}</a>,
                    {
                      style: { flex: 1 },
                    }
                  )}
                  {renderInfoBox(
                    "Số lượng rút (Chỉ)",
                    <div className={styles.body__text} style={{ fontWeight: "bold" }}>
                      {withdraw.amountGold}
                    </div>,
                    {
                      style: { flex: 1 },
                    }
                  )}
                </Grid>
                <Grid item xs={6}>
                  {renderInfoBox("Cửa hàng rút", <a className={styles.body__text}>abc</a>, {
                    style: { flex: 1 },
                  })}
                  {renderInfoBox(
                    "Phí giao dịch",
                    <div className={styles.body__text} style={{ fontWeight: "bold" }}>
                      {withdraw.fee ? `${currencyFormat(withdraw.fee)}đ` : 0}
                    </div>,
                    {
                      style: { flex: 1 },
                    }
                  )}
                </Grid>
              </Grid>
            </div>
          </Grid>
          <Grid item xs={8}>
            <div className={styles.transactionList}>
              <div className={styles.transactionList__title}>Danh sách vàng đã giao (15)</div>
            </div>
          </Grid>
        </Grid>
      </div>
      <div className={styles.appFormFooter}>
        <button
          className={`${styles.btn} ${styles.add}`}
          onClick={(e) => {
            e.preventDefault();
          }}>
          Thêm thao tác
          <ArrowDropUpIcon className={styles.icon} />
        </button>
        <button
          className={`${styles.btn} ${styles[`confirm-payment`]}`}
          onClick={(e) => {
            e.preventDefault();
          }}>
          Xác nhận thanh toán
        </button>
        <button
          type="submit"
          className={`${styles.btn} ${styles[`confirm-deli`]}`}
          onClick={(e) => {
            e.preventDefault();
            //   loadCallback(submit, e);
          }}>
          Xác nhận giao vàng
        </button>
      </div>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  try {
    const client = initializeApollo();
    const withdrawId = params ? (Array.isArray(params.id) ? params.id[0] : params.id) : "0"; //Note: [0] to validate case param could have 2 queries
    const { data } = await client.query({
      query: TransactionQuery.getGoldWithdrawById,
      variables: {
        id: parseInt(withdrawId ?? "0"),
      },
    });
    return {
      props: {
        params,
        withdraw: { ...data.getTransactionGoldWithDrawById },
      },
    };
  } catch (error) {
    console.error(error);
    return {
      props: {
        params,
      },
    };
  }
};
export default ProductDetail;
