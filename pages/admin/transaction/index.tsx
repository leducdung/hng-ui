import { Loading } from "@Components";
import { InputAdornment, TextField } from "@material-ui/core";
import React, { useState } from "react";
import InfiniteScroll from "react-infinite-scroller";
import styles from "./MoneyTransactionListing.module.scss";
import MoneyTransactionListItem from "./MoneyTransactionListItem";
import { Search as SearchIcon, FilterList as FilterListIcon } from "@material-ui/icons";
import { useQuery } from "@apollo/client";
import TransactionQuery from "@Lib/queries/Transaction.graphql";
import AdminLayout from "@Components/AdminLayout";

export type PaymentTransactionOutput = {
  id: number;
  customerId: string;
  payCode: string;
  partnerTransactionCode: string;
  transactionCode: string;
  state: string;
  amount: number;
  status: boolean;
  description: string;
  createdAt: string;
  updatedAt: string;
  customer: {
    userName: string;
    fullName: string;
    phoneNumber: string;
  };
};

const MoneyTransactionList = () => {
  const [transactionList, setTransactionList] = useState<PaymentTransactionOutput[]>([]);

  const { data } = useQuery(TransactionQuery.getMoneyTransactionList, {
    onCompleted: () => {
      const newArr = data.getAllTransactionPaymentGateWay.filter((e) => e.status === true);
      setTransactionList(newArr);
    },
  });
  return (
    <AdminLayout>
      <div className={styles.moneyTrans_page}>
        <div className={styles.moneyTrans_page__content}>
          <div className={styles.header}>
            <div className={styles.title}>Tất cả ({transactionList.length})</div>
            <div className={styles.actions}>
              <TextField
                placeholder="Tìm kiếm"
                variant="outlined"
                className={styles[`search-input`]}
                inputProps={{ className: styles.input }}
                InputProps={{
                  // style: { fontSize: "1.4rem" },
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon className={styles.icon} fontSize="large" />
                    </InputAdornment>
                  ),
                  classes: { notchedOutline: styles[`notchedOutline`] },
                }}
                onChange={() => {
                  // props.onSearch(e.target.value);
                }}
              />
              <button className={styles.filter}>
                <FilterListIcon style={{ fontSize: "2.2rem", marginRight: "1rem" }} />
                Filter
              </button>
            </div>
          </div>
          <div className={styles.moneyTrans_page__content__list}>
            <div className={styles.header}>
              <div className={styles.text}>Mã giao dịch</div>
              <div className={styles.text}>Khách hàng</div>
              <div className={styles.text}>Hoạt động</div>
              <div className={styles.text}>Giá trị nạp tiền</div>
              <div className={styles.text}>Thời gian giao dịch</div>
              <div className={styles.text}>Trạng thái</div>
            </div>
          </div>
          <InfiniteScroll
            pageStart={0}
            loadMore={(e) => {
              console.log(e);
            }}
            //   hasMore={hasMore}
            useWindow={false}
            loader={<Loading />}
            style={{ width: "100%", overflow: "auto" }}>
            {transactionList.map((sup: PaymentTransactionOutput) => (
              <MoneyTransactionListItem key={sup.id} transaction={sup} />
            ))}
          </InfiniteScroll>
        </div>
      </div>
      {/* <FloatingButton
          visible={false}
          onClickPrimary={() => history.push(APP_ROUTES.STORAGE.CREATE_SUPPLIER)}
          btns={[]}
        /> */}
    </AdminLayout>
  );
};

export default MoneyTransactionList;
