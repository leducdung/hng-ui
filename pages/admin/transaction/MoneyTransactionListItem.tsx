import { DEFAULT_PRODUCT_IMG_PLACEHOLDER } from "@Constants";
import { currencyFormat } from "@Utils";
import { LazyLoadImage } from "react-lazy-load-image-component";
import styles from "./MoneyTransactionListItem.module.scss";
import { PaymentTransactionOutput } from "./index";
import { Dot } from "@Components";
import moment from "moment";
import { useEffect, useState } from "react";

export type Props = {
  transaction: PaymentTransactionOutput;
};

const MoneyTransactionListItem = (props: Props) => {
  const [transactionData, setTransactionItem] = useState<PaymentTransactionOutput>({
    id: 0,
    customerId: "",
    payCode: "",
    partnerTransactionCode: "",
    transactionCode: "",
    state: "",
    amount: 0,
    status: false,
    description: "",
    createdAt: "",
    updatedAt: "",
    customer: {
      userName: "",
      fullName: "",
      phoneNumber: "",
    },
  });

  // const transactionData: PaymentTransactionOutput = props.transaction;

  const { transaction } = props;
  useEffect(() => {
    setTransactionItem(transaction);
  }, [transaction]);

  const switchState = (state: string) => {
    switch (state) {
      case "CANCELED":
      case "":
      case "EXPIRED":
        return { color: "#E53935", content: "Ngừng giao dịch" };
      case "PENDING":
        return { color: "#08C4E3", content: "Đang giao dịch" };
      case "SUCCEEDED":
        return { color: "#4DD22C", content: "Hoàn thành" };
      default:
        return { color: "#E53935", content: "Ngừng giao dịch" };
    }
  };

  // const valueColor = transactionData.activity === "Nạp tiền" ? "#4EAA39" : "#E53935";
  return (
    <div className={styles.moneyTrans_list_item} key={transactionData.id}>
      <div className={styles.text}>{transactionData.id || ""}</div>
      <div className={styles.avatar_group}>
        <div className={styles.avatar}>
          <LazyLoadImage alt={transactionData.customerId} src={DEFAULT_PRODUCT_IMG_PLACEHOLDER} />
        </div>
        <div className={styles.info_group}>
          <div className={`${styles.text} ${styles.title}`}>
            {" "}
            {transactionData.customer.fullName || ""}
          </div>
          <div className={styles.text}>{transactionData.customer.phoneNumber}</div>
        </div>
      </div>
      <div className={styles.text}>Nạp tiền</div>
      <div className={styles.text} style={{ color: "#4EAA39" }}>
        + {transactionData.amount ? `${currencyFormat(transactionData.amount)}đ` : 0}
      </div>
      <div className={styles.text}>
        {moment(new Date(parseInt(transactionData.createdAt))).format("DD/MM/yyyy • hh:MM:ss")}
      </div>
      <div className={`${styles.text} ${styles.status}`}>
        <Dot
          style={{
            backgroundColor: switchState(transactionData.state).color,
            marginRight: "1.5rem",
          }}
        />
        <div className={styles.text}>{switchState(transactionData.state).content}</div>
      </div>
    </div>
  );
};

export default MoneyTransactionListItem;
