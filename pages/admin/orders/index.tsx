import React from "react";
import moment from "moment";
import { GetServerSideProps } from "next";
import { TextField, InputAdornment } from "@material-ui/core";
import {
  Search as SearchIcon,
  GetApp as GetAppIcon,
  CheckCircle as CheckCircleIcon,
} from "@material-ui/icons";
import AdminLayout from "@Components/AdminLayout";
import { isValidToken, parseCookie, parseJwtToken } from "@Lib/authentication/cookie-service";
import { ADMIN_ROUTES, DEFAULT_FETCHING, envTable } from "@Constants";
import { DatePicker } from "antd";
import styles from "./index.module.scss";
import { FilterIcon } from "@Components/Icons/FilterIcon";
import clsx from "clsx";
import { useQuery } from "@apollo/client";
import OrderQuery from "@Lib/queries/Cart.graphql";
import useDateRange from "hooks/useDateRange";
import InfiniteScroll from "react-infinite-scroller";
import { usePaginateState } from "hooks/usePaginateState";
import { OrderOutputModel, OrderStatus, PaymentStatus } from "@Models/order";
import Loading from "@Components/Loading";

const { RangePicker } = DatePicker;
const OrderListingPage = () => {
  const [state, dispatch] = usePaginateState<OrderOutputModel>([]);
  const [range, setRange] = useDateRange();
  const { refetch } = useQuery(OrderQuery.getOrdersBydate, {
    variables: {
      input: {
        pageLimit: DEFAULT_FETCHING.LIMIT,
        pageNumber: DEFAULT_FETCHING.PAGE_NUMBER,
        startDate: range.startTime.format("yyyy-MM-DD"),
        endDate: range.endTime.format("yyyy-MM-DD"),
      },
    },
    onCompleted: (result) => {
      dispatch({
        type: "UPDATE",
        payload: {
          data: result.data.orders as Array<OrderOutputModel>,
          pageNumber: result.data.pageNumber,
          totalData: result.data.totalOrders,
          totalPages: result.data.totalPages,
        },
      });
    },
  });

  const hasMore = state.data.length < state.totalData && state.pageNumber < state.totalPages;
  return (
    <AdminLayout>
      <div className={styles.wrapper}>
        <div className={styles.content}>
          <div className={styles.controller}>
            <div>
              <RangePicker
                value={[range.startTime, range.endTime]}
                onChange={(val) => {
                  if (val && val.length) {
                    {
                      setRange(moment(val[0]), moment(val[1]));
                    }
                  }
                }}
                className=""
              />
            </div>
            <div className={styles["action-list"]}>
              <TextField
                placeholder="Tìm kiếm"
                variant="outlined"
                className={clsx("form-text-field", styles["search-input"])}
                inputProps={{ className: "input" }}
                InputProps={{
                  // style: { fontSize: "1.4rem" },
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon className="icon" />
                    </InputAdornment>
                  ),
                }}
                onChange={(e) => {
                  console.log(e.target.value);
                }}
              />
              <button
                className={clsx("btn-action-style")}
                onClick={() => {
                  {
                    /* props.onRefresh(); */
                  }
                }}>
                <FilterIcon color="#272B2F" size={[22, 22]} viewBox={[22, 22]} />
                Lọc
              </button>
              <button className={clsx("btn-action-style")}>
                <GetAppIcon style={{ fontSize: "2.2rem", marginRight: "1rem" }} />
                Xuất file
              </button>
            </div>
          </div>
          <section className={styles.table}>
            <div className={styles.header}>
              <div className={styles.code}>Mã đơn hàng</div>
              <div className={styles.status}>Giao hàng</div>
              <div className={styles.source}>Nguồn</div>
              <div className={styles.create}>Ngày đặt</div>
              <div className={styles.customer}>Khách hàng</div>
              <div className={styles.paid}>Thanh toán</div>
              <div className={styles.total}>Tổng tiền</div>
            </div>
            <ul className={styles.body}>
              <InfiniteScroll
                pageStart={0}
                loadMore={() =>
                  refetch({
                    input: {
                      pageLimit: state.pageLimit,
                      pageNumber: state.pageNumber + 1,
                      startDate: range.startTime.format("yyyy-MM-DD"),
                      endDate: range.endTime.format("yyyy-MM-DD"),
                    },
                  })
                }
                hasMore={hasMore}
                useWindow={false}
                loader={<Loading />}>
                {state.data.map((order) => {
                  return (
                    <li key={order.id} className={styles.item}>
                      <div className={styles.code}>{order.code}</div>
                      <div className={styles.status}>
                        {(() => {
                          switch (order.status) {
                            case OrderStatus.CREATED:
                              return (
                                <span>
                                  <span style={{ color: "#2B94F8" }}>&#8226;&ensp;</span>Mới
                                </span>
                              );
                            case OrderStatus.CONFIRMED:
                              return (
                                <span>
                                  <span style={{ color: "#6658F3" }}>&#8226;&ensp;</span>Xác nhận
                                </span>
                              );
                            case OrderStatus.DELIVERING:
                              return (
                                <span>
                                  <span style={{ color: "#F56837" }}>&#8226;&ensp;</span>Đang giao
                                  hàng
                                </span>
                              );
                            case OrderStatus.DELIVERED:
                              return (
                                <span>
                                  <span style={{ color: "#F56837" }}>&#8226;&ensp;</span>Đã giao
                                  hàng
                                </span>
                              );
                            case OrderStatus.COMPLETED:
                              return (
                                <span>
                                  <span style={{ color: "#08C4E3" }}>&#8226;&ensp;</span>Hoàn thành
                                </span>
                              );
                            case OrderStatus.CANCELLED:
                              return (
                                <span>
                                  <span style={{ color: "#FFB300" }}>&#8226;&ensp;</span>Hủy bỏ
                                </span>
                              );
                            default:
                              return "";
                          }
                        })()}
                      </div>
                      <div className={styles.source}>{order.orderFrom}</div>
                      <div className={styles.create}>
                        {moment(order.createdAt).format("DD/MM/YYYY•hh:mm")}
                      </div>
                      <div className={styles.customer}>{order.customer.fullName}</div>
                      <div className={styles.paid}>
                        {(() => {
                          switch (order.paymentStatus) {
                            case PaymentStatus.COMPLETED:
                              return (
                                <CheckCircleIcon style={{ fontSize: "2.5rem", color: "#4DD22C" }} />
                              );
                            default:
                              return (
                                <CheckCircleIcon style={{ fontSize: "2.5rem", color: "#F0F3F8" }} />
                              );
                          }
                        })()}
                      </div>
                      <div className={styles.total}>
                        {order.details.reduce(
                          (total: number, { price = 0, amount = 0 }) => total + price * amount,
                          0
                        )}
                        đ
                      </div>
                    </li>
                  );
                })}
              </InfiniteScroll>
            </ul>
          </section>
        </div>
      </div>
    </AdminLayout>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const cookie = parseCookie(req);
  const [token] = parseJwtToken(cookie[envTable.admin]);
  const [authenticated, data] = isValidToken(token);
  if (authenticated) {
    return {
      props: {
        user: data,
      },
    };
  }
  return {
    props: {},
    redirect: {
      statusCode: 302,
      destination: ADMIN_ROUTES.LOGIN,
    },
  };
};

export default OrderListingPage;
