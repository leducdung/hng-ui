import React from "react";
import { IntroductionStonePage } from "../widgets";
import { NextSeo } from "next-seo";
import Head from "next/head";
import { useImgCDN } from "lib/url-generator";

const IntroductionStone = () => {
  return (
    <React.Fragment>
      <NextSeo
        title="Bộ Sưu Tập Viên Đá Hạnh Phúc"
        description="HanaGold là nền tảng kết nối tiệm kim hoàn và người đi mua vàng, tại đó người dùng có thể chọn lựa mẫu mã, mua vàng tích lũy online."
        canonical="https://hanagold.unibiz.io/introduction-stone"
        openGraph={{
          title: "Bộ Sưu Tập Viên Đá Hạnh Phúc",
          description:
            "HanaGold là nền tảng kết nối tiệm kim hoàn và người đi mua vàng, tại đó người dùng có thể chọn lựa mẫu mã, mua vàng tích lũy online.",
          url: "https://hanagold.unibiz.io/introduction-stone",
          type: "website",
          images: [
            {
              url: `${useImgCDN("/images/aboutus/img3.png")}`,
              alt: "Happy Stone",
            },
          ],
        }}
      />
      <Head>
        <meta property="og:image:secure_url" content={`${useImgCDN("/images/aboutus/img3.png")}`} />
        <meta property="og:image:type" content="image/png" />
      </Head>
      <IntroductionStonePage />
    </React.Fragment>
  );
};

export default IntroductionStone;
