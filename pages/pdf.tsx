import { DEFAULT_IMG_ALT } from "@Constants";
import React from "react";
import Image from "next/image";
import Img1 from "../public/images/tccs/1.jpg";
import Img2 from "../public/images/tccs/2.jpg";
import Img3 from "../public/images/tccs/3.jpg";
import Img4 from "../public/images/tccs/4.jpg";
import Img5 from "../public/images/tccs/5.jpg";

const Pdf: React.FC = () => {
  return (
    <div style={{ background: "#000000", display: "flex", flexDirection: "column" }}>
      <br />
      <Image src={Img1} alt={DEFAULT_IMG_ALT} objectFit="contain" width={794} height={1122} />
      <br />
      <Image src={Img2} alt={DEFAULT_IMG_ALT} objectFit="contain" width={794} height={1122} />
      <br />
      <Image src={Img3} alt={DEFAULT_IMG_ALT} objectFit="contain" width={794} height={1122} />
      <br />
      <Image src={Img4} alt={DEFAULT_IMG_ALT} objectFit="contain" width={794} height={1122} />
      <br />
      <Image src={Img5} alt={DEFAULT_IMG_ALT} objectFit="contain" width={794} height={1122} />
      <br />
    </div>
  );
};

export default Pdf;
