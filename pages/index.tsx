import React from "react";
import { NextPage } from "next";
import { P_Props } from "../widgets/Home/Home.type";
import { Home as HomePage } from "@Widgets";
import { NextSeo } from "next-seo";
import Head from "next/head";

// @TODO: have to specify og:image and og:image:secure_url to work with https
//  https://stackoverflow.com/questions/12178164/can-i-put-both-ogimagesecure-url-and-ogimage-on-page-for-linkedin-and-faceboo
const Home: NextPage<P_Props> = () => {
  return (
    <React.Fragment>
      <NextSeo
        title="HanaGold - Đồng vàng HanaGold 24k"
        description="HanaGold là nền tảng kết nối tiệm kim hoàn và người đi mua vàng, tại đó người dùng có thể chọn lựa mẫu mã, mua vàng tích lũy online."
        canonical="https://hanagold.unibiz.io"
        openGraph={{
          title: "HanaGold - Đồng vàng HanaGold 24k",
          description:
            "HanaGold là nền tảng kết nối tiệm kim hoàn và người đi mua vàng, tại đó người dùng có thể chọn lựa mẫu mã, mua vàng tích lũy online.",
          url: "https://hanagold.unibiz.io",
          type: "website",
          images: [
            {
              url: "https://res.cloudinary.com/unibiz/image/upload/v1629102840/HNG/Screen_Shot_2021-08-16_at_3.32.51_PM_vog2s6.png",
              alt: "Hana Gold Logo",
            },
          ],
        }}
      />
      <Head>
        <meta
          property="og:image:secure_url"
          content="https://res.cloudinary.com/unibiz/image/upload/v1629102840/HNG/Screen_Shot_2021-08-16_at_3.32.51_PM_vog2s6.png"
        />
        <meta property="og:image:type" content="image/png" />
      </Head>
      <HomePage />
    </React.Fragment>
  );
};

export default Home;
