import React from "react";
import { NextPage } from "next";
import styles from "./PolicyPage.module.scss";
import { DEFAULT_IMG_ALT } from "@Constants";
import { HtmlHeader, PageHeader, PageFooter } from "@Widgets";
import Image from "next/image";
import Banner from "../../../public/images/guide/banner-pl.png";
import Num1 from "../../../public/images/guide/1.png";
import Num2 from "../../../public/images/guide/2.png";
import Num3 from "../../../public/images/guide/3.png";
import Num4 from "../../../public/images/guide/4.png";
import Num5 from "../../../public/images/guide/5.png";

const PolicyPage: NextPage = () => {
  return (
    <>
      <HtmlHeader />
      <PageHeader />
      <div className={styles.policy}>
        <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="responsive" objectFit="cover" />
        <div className={styles.content}>
          <div className={styles.title}>CHÍNH SÁCH GIAO NHẬN HÀNG</div>
          <div className={styles.item}>
            <Image src={Num1} alt={DEFAULT_IMG_ALT} objectFit="contain" />
            <div className={styles.text}>
              <div className={styles.name}>
                Giao nhận trực tiếp tại cửa hàng HanaGold, các showroom Đại lý HanaGold
              </div>
              Quý khách mua hàng tại cửa hàng HanaGold và các showroom Đại lý sẽ được giao nhận tại
              thời điểm hoàn thành thanh toán đơn hàng. Sản phẩm luôn được kiểm tra và xác nhận
              trước khi nhận hàng và rời khỏi cửa hàng.
            </div>
          </div>
          <div className={styles.item}>
            <Image src={Num2} alt={DEFAULT_IMG_ALT} objectFit="contain" />

            <div className={styles.text}>
              <div className={styles.name}>Giao tận nhà</div>
              Các sản phẩm hỗ trợ giao hàng tại nhà:
              <div className={styles.paragraph}>
                &#8226;&ensp; Sản phẩm trang sức bạc
                <br />
                &#8226;&ensp; Sản phẩm quà tặng kim hoàn bao gồm các vật phẩm phong thủy mạ vàng,
                cúp lưu niệm, huân chương mạ vàng, tranh mạ vàng,…
              </div>
              <span className={styles.name}>*Lưu ý:</span> HanaGold giao hàng trên toàn quốc cho tất
              cả các đơn hàng trên, thời gian nhận hàng và chi phí vận chuyển phụ thuộc vào đơn vị
              vận chuyển
            </div>
          </div>
          <div className={styles.item}>
            <Image src={Num3} alt={DEFAULT_IMG_ALT} objectFit="contain" />
            <div className={styles.text}>
              <div className={styles.name}>DỊCH VỤ BẢO HIỂM HÀNG HÓA</div>
              <div className={styles.name}>Dịch vụ Bảo hiểm hàng hóa và giao hàng bảo đảm 100%</div>
              <div className={styles.paragraph}>
                &#8226;&ensp;Khi kiểm tra hàng , quý khách nên Quay lại Video để những vấn đề phát
                sinh được dễ dàng xử lý.
                <br />
                &#8226;&ensp;Luôn cam kết đền bù 100% giá trị hàng hóa trong trường hợp thất lạc.
              </div>
              <br />
              <div className={styles.name}>Quy cách đóng gói hàng hóa tại HanaGold</div>
              <div className={styles.paragraph}>
                &#8226;&ensp;Toàn bộ sản phẩm HanaGold đã được kiểm tra chất lượng trước khi đóng
                gói niêm phong 2 lớp.
                <br />
                &#8226;&ensp;<span className={styles.name}>Lớp 1: </span>Sản phẩm của quý khách được
                đặt vào hộp trang sức HanaGold và được niêm phong lớp 1 bằng Tem vỡ* HanaGold.
                <br />
                &#8226;&ensp;<span className={styles.name}>Lớp 2: </span>Hộp vận chuyển sẽ được gói
                kín, niêm phong bằng 02 Tem vỡ* HanaGold ở giữa mỗi nắp hộp và dán băng keo cẩn thận
              </div>
              <div className={styles.name}>Lưu ý:</div>
              <div className={styles.paragraph}>
                &#8226;&ensp;Để bảo vệ quyền lợi của Khách hàng khi mua trang sức giá trị cao, toàn
                bộ sản phẩm của HanaGold thông qua đối tác vận chuyển đều được HanaGold mua bảo hiểm
                hàng hoá.
                <br />
                &#8226;&ensp;*Tem vỡ: Là một loại tem đặc biệt dùng để dán lên các sản phẩm nhằm mục
                đích đảm bảo hàng hóa chưa bị bóc niêm phong. Đặc điểm của nó khá giòn, khi dán tem
                lên sản phẩm sẽ ko thể bóc ra, nếu cố tình bóc thì tem sẽ vỡ vụn ra.
              </div>
            </div>
          </div>
          <div className={styles.item}>
            <Image src={Num4} alt={DEFAULT_IMG_ALT} objectFit="contain" />
            <div className={styles.text}>
              <div className={styles.name}>Thời gian giao hàng</div>
              <div className={styles.paragraph}>
                &#8226;&ensp; Khu vực Tp.HCM, Hà Nội: 1-4 ngày.
                <br />
                &#8226;&ensp; Khu vực tỉnh thành khác: 2-6 ngày.
                <br />
                &#8226;&ensp; Thời gian giao hàng được bắt đầu tính từ lúc tư vấn viên HanaGold liên
                hệ xác nhận đơn hàng thành công
                <br />
                &#8226;&ensp; Thời gian giao hàng trên chỉ mang tính chất tham khảo, thực tế có thể
                dao động sớm hoặc muộn hơn tùy theo tình trạng tồn kho sản phẩm, địa chỉ giao hàng
                và một số điều kiện khách quan mà HanaGold không thể kiểm soát được (Ví dụ: thiên
                tai, lũ lụt, hỏng hóc phương tiện...).
              </div>
            </div>
          </div>
          <div className={styles.item}>
            <Image src={Num5} alt={DEFAULT_IMG_ALT} objectFit="contain" />
            <div className={styles.text}>
              <div className={styles.name}>
                Chính sách xem và kiểm tra hàng hóa trước khi nhận hàng
              </div>
              Nhằm đáp ứng nhu cầu và bảo vệ tối đa quyền lợi khách hàng khi sử dụng dịch vụ
              HanaGold đã triển khai chính sách hỗ trợ việc xem và kiểm tra hàng hóa khi giao hàng.
              <br />
              <br />
              Khách hàng khi nhận hàng từ nhân viên vận chuyển: vui lòng kiểm tra hộp hàng còn
              nguyên vẹn 02 tem niêm phong, khách hàng có thể mở hàng để kiểm tra hàng hóa bên trong
              hộp hàng. Khuyến khích khách hàng quay lại Video trong suốt quá trình kiểm tra hàng để
              những vấn đề phát sinh được dễ dàng xử lý.
              <br />
              <br />
              HanaGold đảm bảo sản phẩm gửi đến khách hàng đã được kiểm tra chất lượng. Trong trường
              hợp hiếm hoi sản phẩm khách hàng nhận được có khiếm khuyết, hư hỏng hoặc không như mô
              tả, HanaGold cam kết bảo vệ khách hàng bằng chính sách đổi trả và bảo hành.
            </div>
          </div>
        </div>
      </div>

      <PageFooter withoutDivider />
    </>
  );
};

export default PolicyPage;
