import React from "react";
import { NextPage } from "next";
import styles from "./ShoppingGuidePage.module.scss";
import { DEFAULT_IMG_ALT } from "@Constants";
import { HtmlHeader, PageHeader, PageFooter } from "@Widgets";
import { ShoppingGuide } from "@Constants/guide";
import Image from "next/image";
import Banner from "../../../public/images/guide/banner-sp.png";

const ShoppingGuidePage: NextPage = () => {
  return (
    <>
      <HtmlHeader />
      <PageHeader />
      <div className={styles.shopping}>
        <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="responsive" objectFit="cover" />
        <div className={styles.content}>
          <div className={styles.title}>HƯỚNG DẪN MUA HÀNG</div>
          <div className={styles.des}>
            Với phương châm An toàn - Tiện lợi - Nhanh chóng HanaGold mang đến những trải nghiệm mua
            bán trang sức, mỹ nghệ bằng công nghệ cho khách hàng tốt nhất. Khách hàng có thể sở hữu
            các sản phẩm HanaGold bằng các cách sau:
          </div>
          {ShoppingGuide.map((s) => {
            return (
              <div key={s.title} className={styles.item}>
                <div>
                  <Image src={s.img} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                </div>
                <div className={styles.text}>
                  <div className={styles.name}> {s.title}</div>
                  {s.description}
                </div>
              </div>
            );
          })}
        </div>
      </div>
      <PageFooter withoutDivider />
    </>
  );
};

export default ShoppingGuidePage;
