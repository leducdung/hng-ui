import React from "react";
import { NextPage } from "next";
import { HtmlHeader, PageHeader, PageFooter } from "@Widgets";
import styles from "./JewelryPreservationPage.module.scss";
import { DEFAULT_IMG_ALT } from "@Constants";
import { JewelryPreservation } from "@Constants/guide";
import Image from "next/image";
import Banner from "../../../public/images/guide/banner-jewelry.png";

const JewelryPreservationPage: NextPage = () => {
  return (
    <>
      <HtmlHeader />
      <PageHeader />
      <div className={styles.jewelry}>
        <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="responsive" objectFit="cover" />
        <div className={styles.content}>
          <div className={styles.title}>HƯỚNG DẪN bảo quản trang sức</div>
          {JewelryPreservation.map((j) => {
            return (
              <div key={j.name} className={styles.item}>
                <Image src={j.img} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                <div className={styles.text}>
                  <div className={styles.name}>{j.name}</div>
                  <div>
                    {j.text}
                    {j.des?.map((d) => {
                      return (
                        <>
                          {d.des}
                          <br />
                        </>
                      );
                    })}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>

      <PageFooter withoutDivider />
    </>
  );
};

export default JewelryPreservationPage;
