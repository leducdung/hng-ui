import React from "react";
import { NextPage } from "next";
import styles from "./WarrantyProvisionsPage.module.scss";
import { DEFAULT_IMG_ALT } from "@Constants";
import { HtmlHeader, PageHeader, PageFooter } from "@Widgets";
import { Warranty, Exchange48H } from "@Constants/guide";
import Image from "next/image";
import Banner from "../../../public/images/guide/banner-w.png";

const WarrantyProvisionsPage: NextPage = () => {
  return (
    <>
      <HtmlHeader />
      <PageHeader />
      <div className={styles.warranty}>
        <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="responsive" objectFit="cover" />
        <div className={styles.content}>
          <div className={styles.title}>QUY ĐỊNH BẢO HÀNH</div>
          <div className={styles.des}>
            *Việc bảo hành sản phẩm HanaGold có thể thay đổi theo chính sách của HanaGold theo từng
            thời điểm
          </div>
          <div>
            <div className={`${styles.table} ${styles.name}`}>
              <div className={styles.left}>Loại sản phẩm</div>
              <div className={styles.col}>Bảo hành</div>
              <div className={styles.col}>Lưu ý</div>
            </div>
            {Warranty.map((w) => {
              return (
                <div key={w.cate} className={styles.table}>
                  <div className={styles.left}>{w.cate}</div>
                  <div className={styles.col}>
                    {w.guarantee.map((g) => {
                      return (
                        <>
                          {g.gua}
                          <br />
                        </>
                      );
                    })}
                  </div>
                  <div className={styles.col}>
                    {w.note.map((n) => {
                      return (
                        <>
                          {n.not}
                          <br />
                        </>
                      );
                    })}
                  </div>
                </div>
              );
            })}
          </div>

          <div className={styles.text}>Đổi hàng trong vòng 48H: </div>
          {Exchange48H.map((e) => {
            return (
              <>
                {e.text}
                <br />
              </>
            );
          })}
        </div>
      </div>

      <PageFooter withoutDivider />
    </>
  );
};

export default WarrantyProvisionsPage;
