import React from "react";
import { NextPage } from "next";
import styles from "./SizeGuidePage.module.scss";
import { DEFAULT_IMG_ALT } from "@Constants";
import { HtmlHeader, PageHeader, PageFooter } from "@Widgets";
import { SizeRing, SizeBracelet, SizeBangle, SizeNecklace } from "@Constants/guide";
import Image from "next/image";

const SizeGuidePage: NextPage = () => {
  return (
    <>
      <HtmlHeader />
      <PageHeader />
      <div className={styles.size}>
        <div>
          <Image
            src="/images/guide/banner-size.png"
            alt="banner"
            width={1400}
            height={400}
            layout="responsive"
            objectFit="cover"
          />
        </div>

        <div className={styles.content}>
          <div className={styles.title}>HƯỚNG DẪN đo size trang sức</div>
          {SizeRing.map((r) => {
            return (
              <div key={r.title} className={styles.item}>
                <div className={styles.imgicon}>
                  <Image src={r.img} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                </div>
                <div className={styles.text}>
                  <div className={styles.name}>{r.title}</div>
                  {r.des}
                  <br />
                  {r.step.map((s) => {
                    return (
                      <div key={s.name}>
                        <span className={styles.name}>{s.name}</span>
                        {s.des}
                      </div>
                    );
                  })}
                  <div className={styles.imgRing}>
                    <Image
                      src="/images/guide/imgs1.png"
                      alt={DEFAULT_IMG_ALT}
                      width={229}
                      height={119}
                      objectFit="contain"
                    />
                  </div>
                  <div className={styles.imgSize}>
                    <Image
                      src="/images/guide/img-ring.png"
                      alt={DEFAULT_IMG_ALT}
                      width="736"
                      height="1167"
                    />
                  </div>
                  <div className={styles.name}>Đo size nhẫn bằng tờ giấy và thước</div>
                  {r.step1.map((step1) => {
                    return (
                      <div key={step1.name}>
                        <span className={styles.name}>{step1.name}</span>
                        {step1.des}
                      </div>
                    );
                  })}
                  <br />
                  <div className={styles.name}>Những lưu ý khi đo</div>
                  {r.step2.map((step2) => {
                    return (
                      <>
                        <span className={styles.name}>{step2.name}</span>
                        {step2.des}

                        {step2.des2}
                      </>
                    );
                  })}
                </div>
              </div>
            );
          })}
          {SizeBangle.map((b) => {
            return (
              <div key={b.title} className={styles.item}>
                <div className={styles.imgicon}>
                  <Image src={b.img} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                </div>
                <div className={styles.text}>
                  <div className={styles.name}>{b.title}</div>
                  <div className={styles.imgRuler}>
                    <Image
                      src="/images/guide/t1.png"
                      alt={DEFAULT_IMG_ALT}
                      width="679"
                      height="51"
                    />
                  </div>
                  <div>
                    <Image
                      src="/images/guide/vector1.png"
                      alt={DEFAULT_IMG_ALT}
                      width="625"
                      height="14"
                    />
                  </div>
                  <div className={styles.imgcord}>
                    <Image
                      src="/images/guide/d1.png"
                      alt={DEFAULT_IMG_ALT}
                      width="626"
                      height="14"
                    />
                  </div>
                  {b.des}
                  <br />
                  <br />
                  {b.step.map((step) => {
                    return (
                      <div key={step.name} className={styles.tableitem}>
                        <span className={styles.name}>{step.name}</span> <br />
                        {step.des}
                      </div>
                    );
                  })}
                  <br />
                  {b.step2.map((step2) => {
                    return (
                      <div key={step2.name}>
                        <span className={styles.name}>{step2.name}</span>
                        {step2.des}
                      </div>
                    );
                  })}
                  {b.note.map((note) => {
                    return (
                      <>
                        {" "}
                        <br />
                        <div className={styles.name}>{note.title}</div>
                        {note.note1}
                        <br />
                        {note.note2}
                        <br />
                      </>
                    );
                  })}
                </div>
              </div>
            );
          })}
          {SizeBracelet.map((b) => {
            return (
              <div key={b.title} className={styles.item}>
                <div className={styles.imgicon}>
                  <Image src={b.img} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                </div>
                <div className={styles.text}>
                  <div className={styles.name}>{b.title}</div>

                  <div className={styles.imgBracelet}>
                    <Image
                      src="/images/guide/imgs2.png"
                      alt={DEFAULT_IMG_ALT}
                      width={230}
                      height={122}
                      objectFit="contain"
                    />
                  </div>
                  {b.des}
                  <br />
                  <br />
                  {b.step.map((step) => {
                    return (
                      <div key={step.name} className={styles.tableitem}>
                        <span className={styles.name}>{step.name}</span> <br />
                        {step.des}
                      </div>
                    );
                  })}
                  <br />
                  <div className={styles.name}>Đo dựa vào vòng: </div>
                  {b.step1.map((step1) => {
                    return (
                      <div key={step1.name}>
                        {step1.name}
                        {step1.des}
                      </div>
                    );
                  })}
                  <br />
                  <div className={styles.name}>Cách đo thủ công: </div>
                  <div className={styles.name}>Vòng tròn: </div>
                  {b.step2.map((step2) => {
                    return (
                      <div key={step2.name}>
                        <span className={styles.name}>{step2.name}</span>
                        {step2.des}
                      </div>
                    );
                  })}
                  <br />
                  <div className={styles.name}>Vòng Oval : </div>
                  <span>
                    Bạn có thể đo chiều ngang cổ tay
                    <br />
                    Kích thước vòng tay của bạn chính là size số ghi dưới vòng tròn.
                  </span>
                </div>
              </div>
            );
          })}
          {SizeNecklace.map((n) => {
            return (
              <div key={n.title} className={styles.item}>
                <div className={styles.imgicon}>
                  <Image src={n.img} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                </div>
                <div className={styles.text}>
                  <div className={styles.name}>{n.title}</div>
                  <div className={styles.imgRuler}>
                    <Image
                      src="/images/guide/t2.png"
                      alt={DEFAULT_IMG_ALT}
                      width="722"
                      height="44"
                    />
                  </div>
                  <div>
                    <Image
                      src="/images/guide/vector2.png"
                      alt={DEFAULT_IMG_ALT}
                      width="710"
                      height="14"
                    />
                  </div>
                  <div className={styles.imgcord}>
                    <Image
                      src="/images/guide/d2.png"
                      alt={DEFAULT_IMG_ALT}
                      width="700"
                      height="10"
                    />
                  </div>
                  {n.des}
                  <br />
                  <br />
                  <div className={styles.tableitem}>
                    <span className={styles.name}>Kích thước dây chuyền phổ biến</span> <br />
                    40 cm - 41 cm - 42 cm - 43 cm - 44 cm - 45 cm
                  </div>
                  <br />
                  <div>
                    <Image
                      src="/images/guide/imgchain.png"
                      alt={DEFAULT_IMG_ALT}
                      width="736"
                      height="403"
                    />
                  </div>
                  <br />
                  {n.step.map((s) => {
                    return (
                      <>
                        {s.des}
                        <br />
                      </>
                    );
                  })}
                  <br />
                  <div className={styles.name}>Đo thủ công :</div>
                  {n.step1.map((s1) => {
                    return (
                      <>
                        {s1.des}
                        <br />
                      </>
                    );
                  })}
                </div>
              </div>
            );
          })}
        </div>
      </div>

      <PageFooter withoutDivider />
    </>
  );
};

export default SizeGuidePage;
