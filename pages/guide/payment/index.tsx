import React from "react";
import { NextPage } from "next";
import styles from "./PaymentGuidePage.module.scss";
import { HtmlHeader, PageHeader, PageFooter } from "@Widgets";
import { DEFAULT_IMG_ALT } from "@Constants";
import Image from "next/image";
import Banner from "../../../public/images/guide/banner-pm.png";
import Num1 from "../../../public/images/guide/1.png";
import Num2 from "../../../public/images/guide/2.png";
import Num3 from "../../../public/images/guide/3.png";

const PaymentGuidePage: NextPage = () => {
  return (
    <>
      <HtmlHeader />
      <PageHeader />
      <div className={styles.payment}>
        <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="responsive" objectFit="cover" />
        <div className={styles.content}>
          <div className={styles.title}>HƯỚNG DẪN THANH TOÁN</div>
          <div className={styles.des}>
            Tại HanaGold, khách hàng được cung cấp nhiều cách thức thanh toán trực tuyến, linh hoạt
            thuận tiện cho nhu cầu mua bán sản phẩm, dịch vụ như: Thanh toán tiền mặt trực tiếp,
            thanh toán qua thẻ ATM (có đăng ký Internet Banking) và thanh toán khi nhận hàng (COD).
          </div>
          <div className={styles.item}>
            <Image src={Num1} alt={DEFAULT_IMG_ALT} objectFit="contain" />

            <div className={styles.text}>
              <div className={styles.name}>Thanh toán tiền mặt</div>
              Phương thức này áp dụng cho các khách hàng mua hàng hóa trực tiếp tại cửa hàng
              HanaGold. Nhân viên thu ngân sẽ hỗ trợ tận tình cho quý khách.
            </div>
          </div>
          <div className={styles.item}>
            <Image src={Num2} alt={DEFAULT_IMG_ALT} objectFit="contain" />

            <div className={styles.text}>
              <div className={styles.name}>
                Thanh toán online qua thẻ ATM (Có đăng ký Internet Banking){" "}
              </div>
              Phương thức này áp dụng cho những khách hàng đặt hàng và mua sản phẩm trang sức, mỹ
              nghệ trên website HanaGold. Quy trình như sau:
              <br />
              <span className={styles.name}>Bước 1:</span> Quý khách chọn mẫu mã sản phẩm yêu thích
              và thêm vào giỏ hàng
              <br />
              <span className={styles.name}>Bước 2:</span> Chọn hình thức thanh toán online qua
              internet banking
              <br />
              <span className={styles.name}>Bước 3:</span> Tiến hành điền thông tin theo yêu cầu và
              xác thực giao dịch
              <br />
              <span className={styles.name}>Bước 4:</span> Hoàn thành thanh toán đơn hàng và chờ xác
              nhận đơn hàng.
            </div>
          </div>
          <div className={styles.item}>
            <Image src={Num3} alt={DEFAULT_IMG_ALT} objectFit="contain" />
            <div className={styles.text}>
              <div className={styles.name}>QUY ĐỊNH VỀ HOÀN TRẢ TIỀN KHI THANH TOÁN TRỰC TUYẾN</div>
              <div className={styles.name}>Dư tiền, hoàn trả sản phẩm</div>
              Trong trường hợp quý khách hàng đã mua hàng & thanh toán trực tuyến thành công nhưng
              dư tiền, hoặc trả sản phẩm, HanaGold sẽ thực hiện hoàn tiền cho quý khách như sau:
              <br />
              <div className={styles.paragraph}>
                &#8226;&ensp;Đối với thẻ ATM: HanaGold sẽ hoàn lại vào tài khoản quý khách cung cấp
                và thời gian khách hàng nhận được tiền hoàn từ 05 - 07 ngày (trừ Thứ 7, Chủ Nhật và
                Ngày lễ) kể từ khi HanaGold nhận được đề nghị của khách hàng. Nếu qua thời gian trên
                không nhận được tiền, khách hàng liên hệ ngân hàng để giải quyết. HanaGold sẽ hỗ trợ
                cung cấp thông tin (nếu có).
                <br />
                &#8226;&ensp;Đối với thẻ Visa/MasterCard/JCB: HanaGold sẽ hoàn lại vào số thẻ của
                khách đã thanh toán. Thời gian khách hàng nhận được tiền hoàn từ 7-15 ngày làm việc
                kể từ khi HanaGold nhận được đề nghị của khách hàng. Nếu qua thời gian trên không
                nhận được tiền, khách hàng liên hệ ngân hàng để giải quyết. HanaGold sẽ hỗ trợ cung
                cấp thông tin (nếu có).
              </div>
              <br />
              <div className={styles.name}>
                HanaGold không nhận được tiền nhưng khách hàng bị trừ tiền
              </div>
              Trường hợp khách hàng thanh toán không thành công (phía ngân hàng nhận của HanaGold
              không ghi nhận được tiền) nhưng tài khoản của khách hàng bị trừ tiền. Khách hàng vui
              lòng liên hệ ngân hàng về số tiền thanh toán mua hàng đó.
              <br />
              <br />
              <div className={styles.name}>
                Khách hàng ghi sai nội dung, không phải là người thanh toán
              </div>
              Trường hợp khách hàng không phải là người thanh toán trực tiếp (khách hàng nhờ người
              thân thanh toán hộ) thì vui lòng ghi đúng hướng dẫn về nội dung hoặc mã đơn hàng (nếu
              có) để HanaGold xử lý đúng đơn hàng. Các trường hợp ghi sai nội dung hoặc mã đơn hàng
              sẽ tiếp nhận xử lý trong vòng 03 - 05 ngày làm việc (trừ Thứ 7, Chủ Nhật và Ngày lễ).
            </div>
          </div>
        </div>
      </div>

      <PageFooter withoutDivider />
    </>
  );
};

export default PaymentGuidePage;
