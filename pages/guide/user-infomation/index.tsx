import React from "react";
import { NextPage } from "next";
import styles from "./UserInfoPage.module.scss";
import { DEFAULT_IMG_ALT } from "@Constants";
import { HtmlHeader, PageHeader, PageFooter } from "@Widgets";
import { UserInfo } from "@Constants/guide";
import Image from "next/image";
import Banner from "../../../public/images/guide/banner-use.png";

const UserInfomationPage: NextPage = () => {
  return (
    <>
      <HtmlHeader />
      <PageHeader />
      <div className={styles.userinfo}>
        <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="responsive" objectFit="cover" />
        <div className={styles.title}>
          CHÍNH SÁCH BẢO VỆ THÔNG TIN CÁ NHÂN
          <br />
          NGƯỜI DÙNG
        </div>
        <div className={styles.content}>
          <div className={styles.des}>
            Cảm ơn bạn đã truy cập vào trang website được vận hành bởi Công ty cổ phần Vàng bạc đá
            quý HanaGold. Chúng tôi tôn trọng và cam kết sẽ bảo mật những thông tin mang tính riêng
            tư của bạn. Xin vui lòng đọc bản Chính sách bảo vệ thông tin cá nhân của người tiêu dùng
            dưới đây để hiểu hơn những cam kết mà chúng tôi thực hiện nhằm tôn trọng và bảo vệ quyền
            lợi của người truy cập.
            <br />
            <br />
            Bảo vệ thông tin cá nhân của người tiêu dùng và gây dựng được niềm tin cho bạn là vấn đề
            rất quan trọng với chúng tôi. Vì vậy, chúng tôi sẽ dùng tên và các thông tin khác liên
            quan đến bạn tuân thủ theo nội dung của chính sách này. Chúng tôi chỉ thu thập những
            thông tin cần thiết liên quan đến giao dịch mua bán.
            <br />
            <br />
            Người Tiêu Dùng hoặc Khách hàng sẽ được yêu cầu điền đầy đủ các thông tin theo các
            trường thông tin theo mẫu có sẵn trên Website
            <a href="https://hanagold.vn/"> https://hanagold.vn/</a> như: Họ và Tên, địa chỉ (nhà
            riêng hoặc văn phòng), địa chỉ email (công ty hoặc cá nhân), số điện thoại (di động, nhà
            riêng hoặc văn phòng), các chi tiết thẻ ATM (có đăng ký Internet banking) và trong mức
            độ có thể, các tuỳ chọn... Thông tin này được yêu cầu để đặt và hoàn tất việc đặt hàng
            online của Khách hàng (bao gồm gửi email xác nhận đặt hàng đến Khách hàng).
            <br /> &nbsp;&nbsp;&nbsp;&nbsp;• Về cookie và đèn báo hiệu web: Cookie là những thư mục
            dữ liệu được lưu trữ tạm thời hoặc lâu dài trong ổ cứng máy tính của Khách hàng. Các
            cookie được sử dụng để xác minh, truy tìm lượt (bảo vệ trạng thái) và duy trì thông tin
            cụ thể về việc sử dụng và người sử dụng Website
            <a href="https://hanagold.vn/"> https://hanagold.vn/</a> , như các tuỳ chọn cho trang
            web hoặc thông tin về giỏ hàng điện tử của họ. Những thư mục cookie cũng có thể được các
            công ty cung cấp dịch vụ quảng cáo đã ký kết Hợp đồng với HanaGold đặt trong máy tính
            của Khách hàng với mục đích nêu trên và dữ liệu được thu thập bởi những cookie này là
            hoàn toàn vô danh. Nếu không đồng ý, Khách hàng có thể xoá tất cả các cookie đã nằm
            trong ổ cứng máy tính của mình bằng cách tìm kiếm các thư mục với “cookie” trong tên của
            nó và xoá đi. Trong tương lai, Khách hàng có thể chỉnh sửa các lựa chọn trong trình
            duyệt của mình để các cookie (tương lai) bị chặn; Xin được lưu ý rằng: Nếu làm vậy,
            Khách hàng có thể không sử dụng được đầy đủ các tính năng của Website
            <a href="https://hanagold.vn/"> https://hanagold.vn/</a> . Để biết thêm thông tin về
            (cách sử dụng và không nhận) cookie, Khách hàng vui lòng truy cập vào website
            <a href="https://www.allaboutcookies.org"> www.allaboutcookies.org.</a>
            <br /> &nbsp;&nbsp;&nbsp;&nbsp; • Đèn báo hiệu web: Là các sợi mã phần mềm nhỏ tượng
            trưng cho yêu cầu về hình ảnh đồ thị trên 1 trang web hay email. Nó có thể có hoặc không
            có hình ảnh đồ thị nhìn thấy được liên quan đến đèn báo hiệu web, và thông thường hình
            ảnh được thiết kế để trộn lẫn vào nền của trang web hoặc email. Đèn báo hiệu web có thể
            được sử dụng vào các mục đích như: báo cáo lưu lượng truy cập trang web, số khách truy
            cập, kiểm tra và báo cáo quảng cáo, và tính cá nhân hoá. Đèn báo hiệu web mà HanaGold sử
            dụng chỉ để thu thập dữ liệu vô danh.
          </div>
          {UserInfo.map((u) => {
            return (
              <div key={u.name} className={styles.item}>
                <Image src={u.img} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                <div className={styles.text}>
                  <div className={styles.name}>{u.name}</div>
                  {u.text}
                  {u.texts?.map((t) => {
                    return (
                      <>
                        {t.des}
                        <br />
                      </>
                    );
                  })}
                  <div className={styles.para}>
                    {u.para?.map((d) => {
                      return (
                        <>
                          {d.des}
                          <br />
                        </>
                      );
                    })}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>

      <PageFooter withoutDivider />
    </>
  );
};

export default UserInfomationPage;
