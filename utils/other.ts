/* eslint-disable */
export const loadCallback = <T extends (...P: any[]) => any>(
  cb?: T,
  ...data: Parameters<T>
): ReturnType<T> => {
  return cb && cb(...data);
};

/**
 * Hiện giá trị theo định dạng tiền tệ của US
 * @param number kiểu dữ liệu number
 */
export const currencyFormat = (number: number): string => {
  // output: 5,000,000
  return new Intl.NumberFormat("en-US").format(number);

  // output: 5.0000.000
  // return new Intl.NumberFormat("en-DE").format(number);
};

/**
 * wrapper event listener
 * @param type
 * @param handler
 * @param target
 */
// eslint-disable-next-line
export const listener = (type: string, handler: Function, target: any = window) => {
  target.addEventListener(type, handler, { passive: false });
  return () => {
    target?.removeEventListener(type, handler);
  };
};

export function getFirstChar(str: string): string {
  return str.charAt(0);
}

export function capitalizeFirstLetter(str: string): string {
  return getFirstChar(str).toUpperCase() + str.slice(1);
}
