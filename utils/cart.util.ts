import { ProductCartOutputModel } from "@Models";

export const CartUtil = {
  calcCart,
  calcOrderProduct,
};

function calcCart(
  products: ProductCartOutputModel[],
  isVAT = false
): {
  data: ProductCartOutputModel[];
  quantity: number;
  provisionalTotal: number;
  totalDiscount: number;
  total: number;
} {
  let provisionalTotal = 0;
  let total = 0;
  let quantity = 0;
  const totalDiscount = 0;

  products.forEach((prod) => {
    // giá unit variant
    const prodVarUnitPrice = prod.productVariant.price;

    // giá của tất cả topping đã chọn
    const toppingsPrice = prod.toppings.reduce((sum, top) => {
      return top.topping.price * top.quantity + sum;
    }, 0);

    // giá của sp đã chọn
    const selectedProductPrice = prodVarUnitPrice + toppingsPrice;

    provisionalTotal += selectedProductPrice * prod.quantity;
    quantity += prod.quantity;
    total += selectedProductPrice * prod.quantity;
  });

  return {
    data: products,
    quantity,
    provisionalTotal,
    totalDiscount,
    total: isVAT ? total + total * 0.1 : total,
  };
}

function calcOrderProduct(
  product: Pick<ProductCartOutputModel, "productVariant" | "toppings">,
  quantity = 1
): number {
  const prodVarUnitPrice = product.productVariant.price;

  // giá của tất cả topping đã chọn
  const toppingsPrice = product.toppings.reduce((sum, top) => {
    return top.topping.price * top.quantity + sum;
  }, 0);

  return (prodVarUnitPrice + toppingsPrice) * quantity;
}
