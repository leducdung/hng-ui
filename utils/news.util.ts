import { APP_ROUTES } from "@Constants";

export const NewsUtil = {
  buildTags,
  buildNewsDetailPath,
  buildServiceDetailPath,
  buildProjectDetailPath,
};

function buildTags(tagStr: string | null | undefined, seperator = "|"): string[] {
  if (!tagStr) {
    return [];
  }

  let tags: string[] = tagStr.split(seperator);

  tags = tags.filter((tag) => {
    return !!tag.trim();
  });

  return tags;
}

function buildNewsDetailPath(data: { slugs: string[] }): string {
  const slug = data.slugs[data.slugs.length - 1];

  return slug ? `${APP_ROUTES.NEWS}/${slug}` : APP_ROUTES.NEWS;
}

function buildServiceDetailPath(data: { slugs: string[] }): string {
  const slug = data.slugs[data.slugs.length - 1];

  return slug ? `${APP_ROUTES.SERVICE}/${slug}` : APP_ROUTES.SERVICE;
}

function buildProjectDetailPath(data: { slugs: string[] }): string {
  const slug = data.slugs[data.slugs.length - 1];

  return slug ? `${APP_ROUTES.PROJECT}/${slug}` : APP_ROUTES.PROJECT;
}
