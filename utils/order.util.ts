import { APP_ROUTES, LOCATIONS } from "@Constants";
import { OrderStatus } from "@Models";

export const OrderUtil = {
  buildShippingAddress,
  getOrderStatusString,
  buildOrderDetailPath,
  getOrderStatusClassName,
};

function buildOrderDetailPath(data: { orderCode: string }): string {
  return data.orderCode
    ? `${APP_ROUTES.CUSTOMER_ORDER}/${data.orderCode}`
    : APP_ROUTES.CUSTOMER_ORDER;
}

function buildShippingAddress(
  shippingDestination:
    | {
        provinceId: number;
        districtId: number;
        wardId: number;
        address: string;
      }
    | undefined
): string {
  let address = "";

  if (shippingDestination) {
    address = shippingDestination.address;
    const province = LOCATIONS.find((pro) => shippingDestination.provinceId === pro.id);
    const district = province
      ? province.districts.find((dis) => shippingDestination.districtId === dis.id)
      : undefined;
    const ward = district
      ? district.wards.find((ward) => shippingDestination.wardId === ward.id)
      : undefined;

    address += ward ? ` ${ward.name}` : "";
    address += district ? `, ${district.name}` : "";
    address += province ? `, ${province.name}` : "";
  }

  return address;
}

function getOrderStatusString(status: OrderStatus): string {
  let statusStr = "";

  switch (status) {
    case OrderStatus.CREATED:
      statusStr = "Mới";
      break;
    case OrderStatus.CONFIRMED:
      statusStr = "Xác nhận";
      break;
    case OrderStatus.DELIVERING:
      statusStr = "Đang giao";
      break;
    case OrderStatus.COMPLETED:
      statusStr = "Giao thành công";
      break;
    case OrderStatus.CANCELLED:
      statusStr = "Hủy";
      break;
    // case OrderStatus.RETURN:
    //   statusStr = "Trả hàng";
    //   break;

    default:
      break;
  }

  return statusStr;
}

function getOrderStatusClassName(status: OrderStatus): string {
  let statusStr = "";

  switch (status) {
    case OrderStatus.CREATED:
      statusStr = "new";
      break;
    case OrderStatus.CONFIRMED:
      statusStr = "confirmed";
      break;
    case OrderStatus.DELIVERING:
      statusStr = "delivery";
      break;
    case OrderStatus.COMPLETED:
      statusStr = "complete";
      break;
    case OrderStatus.CANCELLED:
      statusStr = "cancel";
      break;
    // case OrderStatus.RETURN:
    //   statusStr = "Trả hàng";
    //   break;

    default:
      break;
  }

  return statusStr;
}
