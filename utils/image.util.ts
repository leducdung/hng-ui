import {
  DEFAULT_CUSTOMER_IMG_PLACEHOLDER,
  DEFAULT_NEWS_IMG_PLACEHOLDER,
  DEFAULT_PRODUCT_IMG_PLACEHOLDER,
  IMG_BASE_URL,
} from "@Constants";
import { ImageOutputModel } from "@Models";

class Util {
  buildProductImgSrc = (img: null | undefined | ImageOutputModel): string | undefined => {
    return img ? `${IMG_BASE_URL.PRODUCT}/${img.picture}` : DEFAULT_PRODUCT_IMG_PLACEHOLDER;
  };

  buildVariantImgSrc = (img: null | undefined | string): string | undefined => {
    return img ? `${IMG_BASE_URL.PRODUCT}/${img}` : DEFAULT_PRODUCT_IMG_PLACEHOLDER;
  };

  buildToppingImgSrc = (img: null | undefined | string): string | undefined => {
    return img ? `${IMG_BASE_URL.TOPPING}/${img}` : undefined;
  };

  buildNewsImgSrc = (img: null | undefined | string): string | undefined => {
    return img ? `${IMG_BASE_URL.NEWS}/${img}` : DEFAULT_NEWS_IMG_PLACEHOLDER;
  };

  buildCustomerImgSrc = (img: null | undefined | string): string | undefined => {
    return img ? `${IMG_BASE_URL.CUSTOMER}/${img}` : DEFAULT_CUSTOMER_IMG_PLACEHOLDER;
  };

  buildServiceImgSrc = (img: null | undefined | string): string | undefined => {
    return img ? `${IMG_BASE_URL.SERVICE}/${img}` : undefined;
  };

  buildProjectImgSrc = (img: null | undefined | string): string | undefined => {
    return img ? `${IMG_BASE_URL.PROJECT}/${img}` : undefined;
  };

  buildProductCategoryImgSrc = (img: string | undefined | null): string | undefined => {
    return img ? `${IMG_BASE_URL.PRODUCT_CATEGORIES}/${img}` : undefined;
  };
}

export const ImageUtil = new Util();
