// eslint-disable-next-line
function getOffsetToDocument(elem: any, dir: "left" | "top"): number {
  const offset = dir === "left" ? "offsetLeft" : "offsetTop";
  let value = 0;
  do {
    if (!isNaN(elem[offset])) {
      value += elem[offset];
    }
  } while ((elem = elem.offsetParent));
  return value;
}

export const DocumentUtil = {
  getOffsetToDocument,
};
