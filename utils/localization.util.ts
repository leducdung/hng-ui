/* eslint-disable */
import { DEFAULT_FALLBACK_LANGUAGE, MAPPING_LANGUAGES } from "@Constants";

export const LocalizationUtil = {
  translateKey,
};

function translateKey(
  input: {
    data: {};
    /**
     * key không có language ở sau
     */
    mainKey: string;
    currentLang?: string;
  },
  config?: { fallbackLang?: string }
): string {
  const currentLang = input.currentLang || config?.fallbackLang || DEFAULT_FALLBACK_LANGUAGE;
  let value = "";

  try {
    const mapLangStr: string =
      MAPPING_LANGUAGES[currentLang] || MAPPING_LANGUAGES[DEFAULT_FALLBACK_LANGUAGE];
    const objKey = input.mainKey + mapLangStr;
    value = input.data[objKey];

    if (value === undefined || value === null) {
      value = input.data[input.mainKey + MAPPING_LANGUAGES[DEFAULT_FALLBACK_LANGUAGE]];
    }
  } catch (error) {
    value = "";
  }

  if (!value) {
    value = "";
  }

  return value;
}
