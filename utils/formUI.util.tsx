import { InfoOutlined as InfoOutlinedIcon } from "@material-ui/icons";

function renderFormItem(label: string, value: React.ReactNode, withIcon = false): React.ReactNode {
  const icon = withIcon ? <InfoOutlinedIcon style={{ marginLeft: "0.5rem" }} /> : "";
  return (
    <div className="form-item">
      <div className="form-label">
        {label} {icon}
      </div>
      {value}
    </div>
  );
}

function renderFormLine(label: string, value: React.ReactNode, withIcon = false): React.ReactNode {
  const icon = withIcon ? <InfoOutlinedIcon style={{ marginLeft: "0.5rem" }} /> : "";

  return (
    <div className="form-line-item">
      <div className="form-line-label">
        {label} {icon}
      </div>
      {value}
    </div>
  );
}

export const FormUIUtil = {
  renderFormItem,
  renderFormLine,
};
