import { DEFAULT_FETCHING } from "@Constants";
import { useReducer } from "react";
import _ from "lodash";

type PaginateState<T> = {
  pageNumber: number;
  pageLimit: number;
  totalPages: number;
  totalData: number;
  data: Array<T>;
};

const DEFAULT_STATE = {
  data: [],
  pageNumber: DEFAULT_FETCHING.PAGE_NUMBER,
  pageLimit: DEFAULT_FETCHING.LIMIT,
  totalData: 0,
  totalPages: 1,
};

type PaginateAction = {
  type: "UPDATE" | "RESET";

  // eslint-disable-next-line
  payload: any;
};

type StateReducer<T> = (state: PaginateState<T>, action: PaginateAction) => PaginateState<T>;

function stateReducer<T>(state: PaginateState<T>, action: PaginateAction): PaginateState<T> {
  switch (action.type) {
    case "UPDATE":
      const data = _.uniqBy([...state.data, ...action.payload.data], "id");
      return {
        ...state,
        data,
        pageNumber: action.payload.pageNumber,
        totalData: action.payload.totalData,
        totalPages: action.payload.totalPages,
      };
    case "RESET":
      return DEFAULT_STATE;

    default:
      return state;
  }
}

function init<T>(initialData: PaginateState<T>): PaginateState<T> {
  return {
    ...DEFAULT_STATE,
    data: initialData.data,
    totalData: initialData.data.length,
  };
}

export const usePaginateState = <T>(initialData: Array<T>) => {
  const [state, dispatch] = useReducer<StateReducer<T>, PaginateState<T>>(
    stateReducer as StateReducer<T>,
    { ...DEFAULT_STATE, data: initialData },
    init
  );
  return [state, dispatch] as const;
};
