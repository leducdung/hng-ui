import React from "react";
import { CartProduct, getCart, saveCart } from "@Lib/cart-module";

export const useCart = () => {
  const [cart, setCart] = React.useState<CartProduct[]>([]);
  React.useEffect(() => {
    setCart(getCart());
  }, []);
  const changeItemQuantity = (id: number, quantity: number) => {
    let newCart = [...cart];
    const itemIndex = newCart.findIndex((item) => item.id === id);
    if (itemIndex !== -1) {
      newCart[itemIndex].quantity = quantity;
    }
    newCart = newCart.filter((item) => item.quantity > 0);
    setCart(() => [...newCart]);
    saveCart([...newCart]);
  };
  const totalProductPrices = React.useMemo(() => {
    const totalAmount = cart.reduce((total, item) => {
      return total + item.product.price * item.quantity;
    }, 0);
    return totalAmount;
  }, [cart]);

  const resetCart = () => {
    setCart([]);
    saveCart([]);
  };
  return [cart, changeItemQuantity, totalProductPrices, resetCart] as const;
};
