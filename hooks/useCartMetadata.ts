import React from "react";
import { CartMetadata, getInitialcartMetadata, saveCartMetadata } from "@Lib/cart-module";

export const useCartMetadata = () => {
  const [cartMetada, setCartMetadata] = React.useState<CartMetadata>(getInitialcartMetadata);
  const changeCartMetadata = (data: CartMetadata) => {
    setCartMetadata(() => ({ ...data }));
    saveCartMetadata({ ...data });
  };
  return [cartMetada, changeCartMetadata] as const;
};
