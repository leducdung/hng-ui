import { useMediaQuery } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";

export function useBreakpoints(): {
  sm: boolean;
  md: boolean;
  lg: boolean;
  xl: boolean;
} {
  const theme = useTheme();
  const breakpointSm = useMediaQuery(theme.breakpoints.up("sm"));
  const breakpointMd = useMediaQuery(theme.breakpoints.up("md"));
  const breakpointLg = useMediaQuery(theme.breakpoints.up("lg"));
  const breakpointXl = useMediaQuery(theme.breakpoints.up("xl"));

  return {
    /**
     * false < 576 < true
     */
    sm: breakpointSm,
    /**
     * false < 768 < true
     */
    md: breakpointMd,
    /**
     * false < 992 < true
     */
    lg: breakpointLg,
    /**
     * false < 1200 < true
     */
    xl: breakpointXl,
  };
}
