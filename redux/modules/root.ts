import { combineEpics } from "redux-observable";
import { combineReducers } from "redux";
import {
  AuthReducer,
  AuthEpic,
  MeReducer,
  MeEpic,
  CartEpic,
  CartReducer,
  NotificationReducer,
} from "../epics";

export const RootEpic = combineEpics(
  AuthEpic.signinEpic,
  MeEpic.loadMeEpic,
  ...Object.values(CartEpic)
);

export const RootReducer = combineReducers({
  Auth: AuthReducer,
  Me: MeReducer,
  Cart: CartReducer,
  Notifications: NotificationReducer,
});
