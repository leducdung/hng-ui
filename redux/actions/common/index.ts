import { ActionState } from "@Models";

export function createCommonType(type: string, payload: ActionState["payload"]): ActionState {
  return {
    type,
    payload,
  };
}

export const CommonActions = {
  createCommonType,
};
