import { AppState } from "@Models";

export const InitialAppNotificationState: AppState.AppNotifications = {
  list: [],
};
