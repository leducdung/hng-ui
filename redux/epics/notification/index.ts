/* eslint-disable */
import { map } from "rxjs/operators";
import { createCommonType, NotificationActionTypes } from "../../actions";
import { ActionState, AppState } from "@Models";
import { InitialAppNotificationState } from "./state";
import { v4 as uuidv4 } from "uuid";

export const NotificationEpic = {
  enqueueSnackbarEpic,
  removeSnackbarEpic,
};

function enqueueSnackbarEpic(action$) {
  return action$.pipe(
    map((action: ActionState) => {
      const newNotiId = uuidv4();

      return createCommonType(NotificationActionTypes.ENQUEUE_SNACKBAR, {
        data: {
          id: newNotiId,
          ...action.payload.data,
        },
      });
    })
  );
}

function removeSnackbarEpic(action$) {
  return action$.pipe(
    map((action: ActionState) => {
      return createCommonType(NotificationActionTypes.ENQUEUE_SNACKBAR, {
        data: {
          id: action.payload.data,
        },
      });
    })
  );
}

export function NotificationReducer(
  state = InitialAppNotificationState,
  action: ActionState
): AppState.AppNotifications {
  switch (action.type) {
    case NotificationActionTypes.ENQUEUE_SNACKBAR:
      return { ...state, list: [...state.list, action.payload.data] };
    case NotificationActionTypes.REMOVE_SNACKBAR: {
      let notis = state.list;
      notis = notis.filter((n) => n.id !== action.payload.data?.id);

      return { ...state, list: notis };
    }

    default:
      return state;
  }
}
