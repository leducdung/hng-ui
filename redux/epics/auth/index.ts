/* eslint-disable */
import { ofType } from "redux-observable";
import { concatMap, map, catchError, tap } from "rxjs/operators";
import { of, from } from "rxjs";
import { AuthActionTypes, createCommonType } from "../../actions";
import { ActionState, AppState } from "@Models";
import { AuthService } from "../../../services";
import { loadCallback } from "@Utils";
import { InitialAuthState } from "./state";

export const AuthEpic = {
  signinEpic,
  signoutEpic,
};

function signinEpic(action$) {
  return action$.pipe(
    ofType(AuthActionTypes.SIGNIN_START),
    concatMap((action: ActionState) =>
      from(AuthService.signin(action.payload.data)).pipe(
        map((res: any) => {
          if (res && (res.status === 200 || res.status === 201)) {
            return createCommonType(AuthActionTypes.SIGNIN_SUCCESS, { data: res.data });
          } else {
            loadCallback(action.payload.error, res);
            return createCommonType(AuthActionTypes.SIGNIN_FAILURE, { data: {} });
          }
        }),
        tap({
          complete: () => loadCallback(action.payload.complete),
        }),
        catchError((err) => {
          return of(createCommonType(AuthActionTypes.SIGNIN_FAILURE, { data: {} }));
        })
      )
    )
  );
}

function signoutEpic(action$) {
  return action$.pipe(ofType(AuthActionTypes.SIGNOUT));
}

export function AuthReducer(
  state = InitialAuthState,
  action: ActionState
): AppState.Authentication {
  switch (action.type) {
    case AuthActionTypes.SIGNIN_START:
      return { ...state, loading: true };
    case AuthActionTypes.SIGNIN_SUCCESS:
      return { ...state, loading: false, data: { ...action.payload.data } };
    case AuthActionTypes.SIGNIN_FAILURE:
      return { ...state, loading: false, data: null };

    case AuthActionTypes.SIGNOUT:
      return InitialAuthState;

    default:
      return state;
  }
}
