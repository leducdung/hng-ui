import { AppState } from "@Models";

export const InitialAuthState: AppState.Authentication = {
  data: null,
  loading: false,
};
