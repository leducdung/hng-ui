/* eslint-disable */
import { CartUtil } from "@Utils";
import { ofType } from "redux-observable";
import { map } from "rxjs/operators";
import { ActionState, AppState, ProductCartOutputModel } from "@Models";
import { CartActionTypes, createCommonType } from "../../actions";
import { InitialCartState } from "./state";

export const CartEpic = {
  addToCartEpic,
  removeItemInCartEpic,
  clearToCartEpic,
};

function addToCartEpic(action$) {
  return action$.pipe(
    ofType(CartActionTypes.ADD_TO_CART_START),
    map((action: ActionState) =>
      createCommonType(CartActionTypes.ADD_TO_CART_SUCCESS, { data: action.payload.data })
    )
  );
}

function removeItemInCartEpic(action$) {
  return action$.pipe(
    ofType(CartActionTypes.REMOVE_ITEM_IN_CART_START),
    map((action: ActionState) =>
      createCommonType(CartActionTypes.REMOVE_ITEM_IN_CART_SUCCESS, { data: action.payload.data })
    )
  );
}

function clearToCartEpic(action$) {
  return action$.pipe(
    ofType(CartActionTypes.CLEAR_CART_START),
    // mergeMap((action: ActionState) =>
    //   of(action.payload.data).pipe(
    //     map(tap({ next: () => loadCallback(action.payload.complete) }), (data) =>
    //       createCommonType(CartActionTypes.CLEAR_CART_SUCCESS, { data: {} })
    //     )
    //   )
    // )
    map((action: ActionState) => {
      return createCommonType(CartActionTypes.CLEAR_CART_SUCCESS, { data: {} });
    })
  );
}

export function CartReducer(
  state = InitialCartState,
  action: ActionState
): AppState.ProductCartState {
  switch (action.type) {
    case CartActionTypes.ADD_TO_CART_START:
      return { ...state, loading: true };
    case CartActionTypes.ADD_TO_CART_SUCCESS: {
      const data: ProductCartOutputModel[] = [...state.data, action.payload.data];
      return { ...state, loading: false, ...CartUtil.calcCart(data, state.isVAT) };
    }

    case CartActionTypes.REMOVE_ITEM_IN_CART_START:
      return { ...state, loading: true };
    case CartActionTypes.REMOVE_ITEM_IN_CART_SUCCESS: {
      let data = state.data;
      data = data.filter((prod) => prod.id !== action.payload.data?.id);

      return { ...state, loading: false, ...CartUtil.calcCart(data, state.isVAT) };
    }

    case CartActionTypes.CLEAR_CART_START:
      return { ...state, loading: true };
    case CartActionTypes.CLEAR_CART_SUCCESS:
      return { ...InitialCartState };

    default:
      return state;
  }
}
