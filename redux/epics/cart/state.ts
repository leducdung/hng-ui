import { AppState } from "@Models";

export const InitialCartState: AppState.ProductCartState = {
  data: [],
  quantity: 0,
  isVAT: false,
  provisionalTotal: 0,
  totalDiscount: 0,
  total: 0,
  loading: false,
};
