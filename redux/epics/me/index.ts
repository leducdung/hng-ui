/* eslint-disable */
import { ofType } from "redux-observable";
import { concatMap, map, catchError, tap } from "rxjs/operators";
import { of, from } from "rxjs";
import { createCommonType, MeActionTypes } from "../../actions";
import { ActionState, AppState } from "@Models";
import { AuthService } from "../../../services";
import { loadCallback } from "@Utils";
import { InitialMeState } from "./state";

export const MeEpic = {
  loadMeEpic,
  clearMeEpic,
};

function loadMeEpic(action$) {
  return action$.pipe(
    ofType(MeActionTypes.LOAD_ME_START),
    concatMap((action: ActionState) =>
      from(AuthService.loadMe()).pipe(
        map((res: any) => {
          if (res && (res.status === 200 || res.status === 201)) {
            return createCommonType(MeActionTypes.LOAD_ME_SUCCESS, { data: res.data.data });
          } else {
            loadCallback(action.payload.error, res);
            return createCommonType(MeActionTypes.LOAD_ME_FAILURE, { data: {} });
          }
        }),
        tap({
          complete: () => loadCallback(action.payload.complete),
        }),
        catchError((err) => {
          return of(createCommonType(MeActionTypes.LOAD_ME_FAILURE, { data: {} }));
        })
      )
    )
  );
}

function clearMeEpic(action$) {
  return action$.pipe(ofType(MeActionTypes.CLEAR_ME));
}

export function MeReducer(state = InitialMeState, action: ActionState): AppState.Me {
  switch (action.type) {
    case MeActionTypes.LOAD_ME_START:
      return { ...state, loading: true };
    case MeActionTypes.LOAD_ME_SUCCESS:
      return { ...state, loading: false, data: { ...action.payload.data } };
    case MeActionTypes.LOAD_ME_FAILURE:
      return { ...state, loading: false, data: null };

    case MeActionTypes.CLEAR_ME:
      return InitialMeState;

    default:
      return state;
  }
}
