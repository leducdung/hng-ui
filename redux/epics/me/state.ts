import { AppState } from "@Models";

export const InitialMeState: AppState.Me = {
  data: null,
  loading: false,
};
