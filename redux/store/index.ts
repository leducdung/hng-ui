import { useMemo } from "react";
import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { createEpicMiddleware } from "redux-observable";
import { RootEpic, RootReducer } from "../modules/root";
import { createLogger } from "redux-logger";
import { RootState } from "@Models";

// The config should be transparent, easy to add/remove middlewares, easy to config.
// any team members can config the redux store.

const initStore = (initialState: RootState) => {
  const epicMiddleware = createEpicMiddleware();
  const middlewares: Parameters<typeof applyMiddleware> = [];
  middlewares.push(epicMiddleware);
  if (process.env.NODE_ENV === "development") {
    const logger = createLogger({ collapsed: true });
    middlewares.push(logger);
  }
  const middlewareEnhancer = applyMiddleware(...middlewares);

  const store = createStore(RootReducer, initialState, composeWithDevTools(middlewareEnhancer));
  epicMiddleware.run(RootEpic);
  return store;
};

const storeFactory = () => {
  let store: undefined | ReturnType<typeof initStore>;
  return (preloadedState: RootState) => {
    let _store = store ?? initStore(preloadedState);
    if (preloadedState && store) {
      _store = initStore({
        ...store.getState(),
        ...preloadedState,
      });
      store = undefined;
    }

    if (typeof window === "undefined") return _store;
    if (!store) store = _store;

    return _store;
  };
};

export const useStore: (initialStore: RootState) => ReturnType<typeof initStore> = (
  initialState
) => {
  const store = useMemo(() => {
    const initializeStore = storeFactory();
    return initializeStore(initialState);
  }, [initialState]);
  return store;
};
