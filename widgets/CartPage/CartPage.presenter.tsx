import React, { useState } from "react";
import { useBreakpoints } from "../../hooks";
import { Button, InputAdornment, TextField, Dialog, Grid } from "@material-ui/core";
import { ShoppingCartOutlined as ShoppingCartOutlinedIcon } from "@material-ui/icons";
import { CheckoutStepper, ComingSoon, OrderProductCart1 } from "../../components";
import { APP_ROUTES, DEFAULT_COMING_SOON } from "../../constants";
// import { MOCK_DANCING_STONE_PRODUCTS } from "mock";
import styles from "./CartPage.module.scss";
import { useRouter } from "next/router";
import AppLayout from "@Components/AppLayout";
import { currencyFormat } from "@Utils";
import { useCartMetadata } from "hooks/useCartMetadata";
import { useCart } from "hooks/useCart";

const CartPagePresenter: React.FC = () => {
  const router = useRouter();
  const [cart, setCart, totalProductPrices] = useCart();
  const [cartMetada, setCartMetadata] = useCartMetadata();
  const breakpoints = useBreakpoints();
  const [visibleComingSoonModal, setVisibleComingSoonModal] = useState<boolean>(false);
  const toggleComingSoonModal = () => {
    setVisibleComingSoonModal(!visibleComingSoonModal);
  };

  return (
    <AppLayout title="Giỏ hàng của bạn">
      <div className={styles.cartPage}>
        <CheckoutStepper activeStep={0} />
        <Grid container spacing={2} className={styles.body}>
          <Grid item xs={9} className={styles.left}>
            <div className={styles.cartList}>
              {breakpoints.lg && (
                <div className={styles.cartIcon}>
                  <ShoppingCartOutlinedIcon
                    style={{ fontSize: "2.2rem", color: "#CACACA", marginRight: "2rem" }}
                  />
                  Giỏ hàng của tôi ({cart.length} sản phẩm)
                  {/* {t("CART_MY_SHOPPING_CART")} ({cart.length} {t("CART_ITEMS")}) */}
                </div>
              )}
              <div>
                {!!cart.length &&
                  cart.map((prodCart) => {
                    return (
                      <OrderProductCart1
                        changeItemQuantity={setCart}
                        key={prodCart.product.slug}
                        cartItem={prodCart}
                      />
                    );
                  })}
              </div>
              {!cart.length && (
                <div className={styles.noneProd}>
                  <div className={styles.content}>Không có sản phẩm trong giỏ</div>
                  <div className={styles.text}>Hãy cùng shopping nào...</div>
                </div>
              )}
            </div>

            <div className={styles.suggest}>
              {/* <div className={styles.suggest_header}>Thường được mua cùng</div> */}
              <Grid container spacing={4} className={styles.suggest__list}>
                {/* {MOCK_DANCING_STONE_PRODUCTS.slice(0, 4).map((pro) => { */}
                {/*   return ( */}
                {/*     <Grid item xs={3} key={pro.id}> */}
                {/*       <ProductCard1 */}
                {/*         key={pro.id} */}
                {/*         id={pro.id} */}
                {/*         title={pro.title} */}
                {/*         img={pro.img} */}
                {/*         price={pro.price} */}
                {/*         discount={pro.discount} */}
                {/*       /> */}
                {/*     </Grid> */}
                {/*   ); */}
                {/* })} */}
              </Grid>
            </div>
          </Grid>
          <Grid item xs={3} className={styles.right}>
            <div className={styles.rightAction}>
              <div className={styles.payment}>
                <div className={styles.summary}>Thành tiền</div>
                <Grid container justify="space-between" className={styles.provisional}>
                  <Grid item lg={4}>
                    <div>Tạm tính</div>
                  </Grid>
                  <Grid item lg={6} container justify="flex-end">
                    <div>
                      {currencyFormat(totalProductPrices)}
                      <span>đ</span>
                    </div>
                  </Grid>
                </Grid>
                <Grid container justify="space-between" className={styles.coupon}>
                  <Grid item lg={4}>
                    <div>Mã giảm giá</div>
                  </Grid>
                  <Grid item lg={6} container justify="flex-end" className={styles.coupon_value}>
                    <div>
                      0<sup>đ</sup>
                    </div>
                  </Grid>
                </Grid>
                <Grid container justify="space-between" className={styles.total}>
                  <Grid item lg={4}>
                    <div>Tổng cộng</div>
                  </Grid>
                  <Grid item lg={6} container justify="flex-end">
                    <div>
                      {currencyFormat(totalProductPrices)}
                      <sup>đ</sup>
                    </div>
                  </Grid>
                </Grid>
              </div>
              <div className={styles.coupon_group}>
                <TextField
                  placeholder="Mã giảm giá"
                  className={styles.form_text_field}
                  inputProps={{ className: styles.input }}
                  variant="outlined"
                  fullWidth
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <Button onClick={toggleComingSoonModal} className={styles.applyBtn}>
                          Áp dụng
                        </Button>
                      </InputAdornment>
                    ),
                    classes: {
                      notchedOutline: styles.noBorder,
                    },
                  }}
                />
              </div>

              <div className={styles.nextStep}>
                <button
                  className={styles.nextBtn}
                  onClick={() => {
                    router.push(APP_ROUTES.CONFIRM_SHIPPING);
                  }}>
                  Bước tiếp theo
                </button>
              </div>
            </div>
            <div className={styles.rightNote}>
              <div className={styles.rightNote_title}>Ghi chú giao hàng</div>
              <TextField
                placeholder="Placeholder"
                className={styles.rightNote_text}
                variant="outlined"
                value={cartMetada.userNote}
                onChange={(e) => {
                  setCartMetadata({ ...cartMetada, userNote: e.target.value as string });
                }}
                fullWidth
                rows={6}
                multiline
                inputProps={{ className: styles.input }}
              />
            </div>
          </Grid>
        </Grid>
      </div>

      <Dialog
        open={visibleComingSoonModal}
        onClose={() => {
          toggleComingSoonModal();
        }}
        maxWidth={breakpoints.lg ? "lg" : undefined}>
        <ComingSoon
          style={{ padding: "4rem" }}
          onClose={() => {
            toggleComingSoonModal();
          }}
          title={DEFAULT_COMING_SOON.TITLES.FEATURE}
          subTitle={DEFAULT_COMING_SOON.SUB_TITLES.FEATURE}
          modifyBackButton={{
            text: "Trở về trang chủ",
            onClick: () => {
              router.push(APP_ROUTES.HOME);
            },
          }}
        />
      </Dialog>
    </AppLayout>
  );
};

export default CartPagePresenter;
