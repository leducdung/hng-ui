import { AppState } from "@Models";
import { CommonActions } from "../../../redux/actions";

export type P_Props = {
  activeUrl?: string;
  actions: typeof CommonActions;
  onClickSignin: () => void;
  onClickSignup: () => void;
  onCartClick: () => void;
  Me: AppState.Me;
  signout: () => void;
  Cart: AppState.ProductCartState;
};
