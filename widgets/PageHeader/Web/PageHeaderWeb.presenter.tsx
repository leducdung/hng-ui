import React, { useState } from "react";
import Link from "next/link";
import AppIcon from "@Components/AppIcon";
import { AppMenus, APP_ROUTES, DEFAULT_IMG_ALT } from "@Constants";
import { P_Props } from "./PageHeaderWeb.type";
import clsx from "clsx";
import {
  ShoppingCartOutlined as ShoppingCartOutlinedIcon,
  ExitToApp as ExitToAppIcon,
  Person as PersonIcon,
  Description as DescriptionIcon,
  ArrowDropDown as ArrowDropDownIcon,
} from "@material-ui/icons";
import { Badge, Popover } from "@material-ui/core";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/client";
import ProductQuery from "@Lib/queries/Product.graphql";
import Image from "next/image";
import Icon from "../../../public/images/text-logo.png";
import { useCart } from "hooks/useCart";

const PageHeaderWeb: React.FC<P_Props> = (props) => {
  const router = useRouter();
  const [cart] = useCart();
  const [anchorCustomerMenuEl, setAnchorCustomerMenuEl] = React.useState<HTMLElement | null>(null);
  const {
    data,
  }: { data: { categories: Array<{ slug: string; name: string; status: boolean }> } | undefined } =
    useQuery(ProductQuery.getParentCategories);

  // const handlePopoverCustomerMenuOpen = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
  //   setAnchorCustomerMenuEl(event.currentTarget);
  // };
  const [visibleShowDropdown, setVisibleShowDropdown] = useState<boolean>(false);
  const handlePopoverCustomerMenuClose = () => {
    setAnchorCustomerMenuEl(null);
  };

  return (
    <React.Fragment>
      <div className="page-header__web">
        <div className="page-header__app-icon">
          <Link passHref href={APP_ROUTES.HOME}>
            <a>
              <AppIcon className="sub-icon" />
              <div className="main-icon">
                <Image src={Icon} alt={DEFAULT_IMG_ALT} objectFit="contain" />
              </div>
            </a>
          </Link>
        </div>
        <nav className="page-header__web__menu">
          {AppMenus.map((m) => {
            const isActive = m.url === props.activeUrl;
            const menuItemClasses = clsx({
              "page-header__web__menu__item": true,
              active: isActive,
            });

            return (
              <div className={menuItemClasses} key={m.url}>
                {m.urlType === "link" && (
                  <div className="page-header__web__menu__item__block">
                    <Link passHref href={m.url}>
                      <a className="page-header__web__menu__item__label">{m.title}</a>
                    </Link>
                  </div>
                )}

                {m.urlType === "select" && (
                  <React.Fragment>
                    <div
                      className="page-header__web__menu__item__block"
                      onClick={() => {
                        setVisibleShowDropdown(!visibleShowDropdown);
                      }}>
                      {/* <Link passHref href={m.url}> */}
                      <div className="page-header__web__menu__item__label">
                        {m.title}
                        <ArrowDropDownIcon
                          style={{ color: "#CBCBCB", marginLeft: "0.6rem", fontSize: "2.2rem" }}
                        />
                      </div>
                      {/* </Link> */}
                    </div>
                    {visibleShowDropdown && (
                      <div
                        className="page-header__web__menu__item__dropdown"
                        onClick={() => {
                          setVisibleShowDropdown(false);
                        }}>
                        {data &&
                          data.categories
                            .filter((c) => c.status)
                            .map((cate) => {
                              const slug =
                                cate.slug !== "trang-suc-happy-stone"
                                  ? "/product/" + cate.slug
                                  : "/" + cate.slug;
                              return (
                                <Link passHref key={slug} href={slug}>
                                  <a className="page-header__web__menu__item__dropdown__item item-label">
                                    {cate.name}
                                  </a>
                                </Link>
                              );
                            })}
                      </div>
                    )}
                  </React.Fragment>
                )}
              </div>
            );
          })}
        </nav>
        <div className="page-header__web__actions">
          <div className="page-header__web__menu__item__label">
            <Link href="/customer/signin" passHref>
              <a>Đăng ký&#160;&#47;&#160;Đăng Nhập</a>
            </Link>
          </div>
          <Badge
            variant="dot"
            badgeContent={cart.reduce((total, item) => {
              return total + item.quantity;
            }, 0)}
            color="secondary"
            className="page-header__cart">
            <button
              className="btn-no-style page-header__web__actions__btn cart"
              onClick={() => {
                router.push(APP_ROUTES.ORDER);
              }}>
              <ShoppingCartOutlinedIcon
                style={{ fontSize: "2.1rem", color: "#fff", backgroundColor: "#666" }}
              />
            </button>
          </Badge>
        </div>
      </div>
      <Popover
        open={Boolean(anchorCustomerMenuEl)}
        onClose={handlePopoverCustomerMenuClose}
        anchorEl={anchorCustomerMenuEl}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}>
        <div className="page-header__web__actions__customer-menu">
          <div
            className="item"
            onClick={() => {
              handlePopoverCustomerMenuClose();
              router.push(APP_ROUTES.CUSTOMER_PROFILE);
            }}>
            <PersonIcon style={{ fontSize: "2.2rem", color: "#3C3B3B", marginRight: "0.8rem" }} />
            <span>Hồ sơ cá nhân</span>
          </div>
          <div
            className="item"
            onClick={() => {
              handlePopoverCustomerMenuClose();
              router.push(APP_ROUTES.CUSTOMER_ORDER);
            }}>
            <DescriptionIcon
              style={{ fontSize: "2.2rem", color: "#3C3B3B", marginRight: "0.8rem" }}
            />
            <span>Lịch sử đơn hàng</span>
          </div>
          <div
            className="item"
            onClick={() => {
              handlePopoverCustomerMenuClose();
              props.signout();
            }}>
            <ExitToAppIcon
              style={{ fontSize: "2.2rem", color: "#3C3B3B", marginRight: "0.8rem" }}
            />
            <span>Đăng xuất</span>
          </div>
        </div>
      </Popover>
    </React.Fragment>
  );
};

export default PageHeaderWeb;
