import React, { Component } from "react";
import PageHeaderPresenter from "./PageHeader.presenter";
import { bindActionCreators, Dispatch } from "redux";
import { connect } from "react-redux";
import {
  MapStateToProps,
  MapDispatchToProps,
  C_Props,
  MergedProps,
  C_States,
} from "./PageHeader.type";
import { AuthActionTypes, CommonActions, MeActionTypes } from "../../redux/actions";
import { RootState } from "@Models";
import { RequestUtil } from "@Utils";

class PageHeaderContainer extends Component<MergedProps, C_States> {
  signout = () => {
    RequestUtil.removeAuthHeader();
    this.props.actions.createCommonType(MeActionTypes.CLEAR_ME, { data: {} });
    this.props.actions.createCommonType(AuthActionTypes.SIGNOUT, { data: {} });
  };

  render() {
    return <PageHeaderPresenter {...this.props} {...this.state} signout={this.signout} />;
  }
}

export default connect<MapStateToProps, MapDispatchToProps, C_Props, RootState>(
  (state: RootState) => ({
    Me: state.Me,
    Cart: state.Cart,
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(CommonActions, dispatch),
  })
)(PageHeaderContainer);
