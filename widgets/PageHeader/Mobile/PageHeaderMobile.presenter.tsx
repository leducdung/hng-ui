import React, { useState } from "react";
import Link from "next/link";
import { MenuIcon } from "@Components/Icons";
import { AppMenus, APP_ROUTES, DEFAULT_IMG_ALT } from "@Constants";
import { IconButton, Drawer, Badge, Dialog } from "@material-ui/core";
import {
  ShoppingCartOutlined as ShoppingCartOutlinedIcon,
  Close as CloseIcon,
  ArrowRight as ArrowRightIcon,
  ArrowLeft as ArrowLeftIcon,
} from "@material-ui/icons";
import clsx from "clsx";
import { P_Props } from "./PageHeaderMobile.type";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { DownloadApp } from "@Components";
import { useQuery } from "@apollo/client";
import ProductQuery from "@Lib/queries/Product.graphql";
import Image from "next/image";
import IconApp from "../../../public/images/app-icon.png";
import { useCart } from "hooks/useCart";

export type AppMenu = {
  url: string;
  urlType: "select" | "link";
  title: string;
};
const PageHeaderMobile: React.FC<P_Props> = (props) => {
  const [cart] = useCart();
  const [visibleMobileMenu, setVisibleMobileMenu] = useState<boolean>(false);
  const [visibleDropdownMenu, setVisibleDropdownMenu] = useState<AppMenu | null>(null);
  const [visibleOpenDownPage, setVisibleOpenDownPage] = useState<boolean>(false);
  const {
    data,
  }: { data: { categories: Array<{ slug: string; name: string; status: boolean }> } | undefined } =
    useQuery(ProductQuery.getParentCategories);

  const toggleMobileMenu = () => {
    setVisibleMobileMenu(!visibleMobileMenu);
  };

  const toggleOpenDownPage = () => {
    setVisibleOpenDownPage(!visibleOpenDownPage);
  };

  return (
    <>
      <div className="page-header__mobile">
        <div className="page-header__mobile__app-icon">
          <button
            className="btn-no-style page-header__mobile__actions__btn menu"
            onClick={(e) => {
              e.preventDefault();
              toggleMobileMenu();
            }}>
            <MenuIcon color="#ffffff" viewBox={[32, 32]} size={[32, 32]} />
          </button>
          <Link href={APP_ROUTES.HOME}>
            <a>
              {/* <AppIcon className="sub-icon" /> */}
              <div className="mobile-icon">
                <Image src={IconApp} alt={DEFAULT_IMG_ALT} objectFit="cover" layout="fill" />
              </div>
            </a>
          </Link>
        </div>
        <div className="page-header__mobile__actions">
          <Badge
            variant="dot"
            badgeContent={cart.length}
            color="secondary"
            className="page-header__cart">
            <button
              className="btn-no-style page-header__mobile__actions__btn cart"
              onClick={() => {
                toggleOpenDownPage();
                // props.onCartClick();
              }}>
              <ShoppingCartOutlinedIcon style={{ fontSize: "2rem", color: "#fff" }} />
            </button>
          </Badge>
        </div>
      </div>
      <Drawer
        anchor="left"
        open={visibleMobileMenu}
        onClose={() => {
          toggleMobileMenu();
        }}
        ModalProps={{
          className: "page-header__mobile__drawer__bg",
        }}
        // onOpen={() => {
        //   toggleMobileMenu();
        // }}
      >
        <div className="page-header__mobile__drawer">
          <div className="page-header__mobile__drawer__header">
            <IconButton
              style={{ padding: "0" }}
              onClick={() => {
                toggleMobileMenu();
              }}>
              <CloseIcon style={{ fontSize: "2.2rem", color: "#fff" }} />
            </IconButton>
            <Link href={APP_ROUTES.HOME}>
              <a
                onClick={() => {
                  toggleMobileMenu();
                }}>
                {/* <AppIcon/> */}
                <div className="mobile-icon">
                  <Image src={IconApp} alt={DEFAULT_IMG_ALT} objectFit="cover" layout="fill" />
                </div>
              </a>
            </Link>
            {/* <IconButton
              onClick={() => {
                props.onCartClick();
              }}
            >
              <ShoppingCartOutlinedIcon style={{ fontSize: "2.2rem", color: "#3C3B3B" }} />
            </IconButton> */}
          </div>
          <div className="page-header__mobile__drawer__menu">
            {visibleDropdownMenu ? (
              // @TODO fix this
              // eslint-disable-next-line
              <>
                {visibleDropdownMenu.urlType === "select" && (
                  <>
                    <div className="page-header__mobile__drawer__menu__item__block">
                      <div
                        className="page-header__mobile__drawer__menu__item__label"
                        onClick={() => {
                          setVisibleDropdownMenu(null);
                        }}>
                        <ArrowLeftIcon style={{ fontSize: "2.2rem" }} />
                        {visibleDropdownMenu.title}
                      </div>
                    </div>
                    <div className="page-header__mobile__drawer__menu__item__dropdown">
                      {data &&
                        data.categories
                          .filter((c) => c.status)
                          .map((cate) => {
                            const slug =
                              cate.slug !== "trang-suc-happy-stone"
                                ? "/product/" + cate.slug
                                : cate.slug;
                            return (
                              <Link passHref key={cate.slug} href={slug}>
                                <a className="page-header__mobile__drawer__menu__item__dropdown__item item-label">
                                  {cate.name}
                                </a>
                              </Link>
                            );
                          })}
                    </div>
                  </>
                )}
              </>
            ) : (
              <>
                <div
                  className="page-header__mobile__drawer__menu__item label-signin"
                  onClick={toggleOpenDownPage}>
                  Đăng ký / Đăng nhập
                </div>
                {AppMenus.map((m) => {
                  const isActive = m.url === props.activeUrl;
                  const menuItemClasses = clsx({
                    "page-header__mobile__drawer__menu__item": true,
                    active: isActive,
                  });

                  return (
                    <div className={menuItemClasses} key={m.url}>
                      {m.urlType === "link" && (
                        <div className="page-header__mobile__drawer__menu__item__block">
                          <Link href={m.url}>
                            <a
                              className="page-header__mobile__drawer__menu__item__label"
                              onClick={() => {
                                toggleMobileMenu();
                              }}>
                              {m.title}
                            </a>
                          </Link>
                        </div>
                      )}

                      {m.urlType === "select" && (
                        <div
                          className="page-header__mobile__drawer__menu__item__label"
                          onClick={() => {
                            setVisibleDropdownMenu(m);
                          }}>
                          {m.title}
                          <ArrowRightIcon style={{ fontSize: "2.2rem" }} />
                        </div>
                      )}
                    </div>
                  );
                })}
              </>
            )}
          </div>

          <div className="page-header__mobile__drawer__menu__btn-group">
            <div className="page-header__mobile__drawer__menu__btn-group__text">
              Tải ứng dụng HanaGold để tối ưu trải nghiệm
            </div>
            <Link href={"https://play.google.com/store/apps/details?id=com.unibiz.hanagold"}>
              <a className="page-header__mobile__drawer__menu__btn">
                <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/iconggplay.png"} />
                <div>
                  <div className="page-header__mobile__drawer__menu__btn__get">GET IT ON</div>
                  <div className="page-header__mobile__drawer__menu__btn__name">Google Play</div>
                </div>
              </a>
            </Link>
            <Link href={"https://testflight.apple.com/join/RoqAkqPl"}>
              <a className="page-header__mobile__drawer__menu__btn">
                <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/iconapple.png"} />
                <div>
                  <div className="page-header__mobile__drawer__menu__btn__get">Download on the</div>
                  <div className="page-header__mobile__drawer__menu__btn__name">App Store</div>
                </div>
              </a>
            </Link>
          </div>
        </div>
      </Drawer>

      <Dialog
        open={visibleOpenDownPage}
        fullScreen={true}
        onClose={() => {
          toggleOpenDownPage();
        }}>
        <DownloadApp
          onClose={() => {
            toggleOpenDownPage();
            setVisibleMobileMenu(false);
          }}
        />
      </Dialog>
    </>
  );
};

export default PageHeaderMobile;
