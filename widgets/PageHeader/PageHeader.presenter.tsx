import React, { useEffect, useRef, useState } from "react";
import clsx from "clsx";
import { P_Props } from "./PageHeader.type";
import PageHeaderWeb from "./Web";
import PageHeaderMobile from "./Mobile";
import { listener, loadCallback } from "@Utils";
import { useRouter } from "next/router";
import { APP_ROUTES } from "@Constants";
import { AuthTabPosition } from "@Models";

const PageHeader: React.FC<P_Props> = (props) => {
  const router = useRouter();
  const [, setAuthModalState] = useState<{
    visible: boolean;
    initTab: AuthTabPosition;
  }>({
    visible: false,
    initTab: AuthTabPosition.SIGN_IN,
  });
  const classes = clsx({ "page-header": true, [props.className || ""]: Boolean(props.className) });
  const headerRef: React.RefObject<HTMLDivElement | null | undefined> = useRef();

  const findActivePath = (): string | undefined => {
    let active: string | undefined = undefined;

    if (router.asPath === APP_ROUTES.HOME) {
      active = APP_ROUTES.HOME;
    } else {
      active = Object.values(APP_ROUTES).find(
        (r) => r !== APP_ROUTES.HOME && router.asPath.includes(r)
      );
    }

    return active;
  };

  const onClickSignin = () => {
    setAuthModalState({ visible: true, initTab: AuthTabPosition.SIGN_IN });
  };

  const onClickSignup = () => {
    setAuthModalState({ visible: true, initTab: AuthTabPosition.SIGN_UP });
  };

  const onCartClick = () => {
    router.push(APP_ROUTES.ORDER);
  };

  useEffect(() => {
    let eventListener;

    if (!props.removeScrollEvent) {
      eventListener = listener(
        "scroll",
        () => {
          if (headerRef && headerRef.current) {
            const scrollY = window.scrollY;
            if (!scrollY) {
              headerRef.current.classList.remove("scrolling");
            } else {
              headerRef.current.classList.add("scrolling");
            }
          }
        },
        window
      );
    }

    return () => {
      loadCallback(eventListener);
    };
    // eslint-disable-next-line
  }, []);

  const activeUrl = findActivePath();
  return (
    // eslint-disable-next-line
    <header className={classes} style={props.style} ref={headerRef as any}>
      <PageHeaderWeb
        activeUrl={activeUrl}
        actions={props.actions}
        onClickSignin={onClickSignin}
        onClickSignup={onClickSignup}
        onCartClick={onCartClick}
        Me={props.Me}
        signout={props.signout}
        Cart={props.Cart}
      />
      <PageHeaderMobile
        activeUrl={activeUrl}
        actions={props.actions}
        onClickSignin={onClickSignin}
        onClickSignup={onClickSignup}
        onCartClick={onCartClick}
        Me={props.Me}
        signout={props.signout}
        Cart={props.Cart}
      />
    </header>
  );
};

export default PageHeader;
