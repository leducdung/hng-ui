import React from "react";
import { AppState } from "@Models";
import { CommonActions } from "../../redux/actions";
import { FullPageApiRef } from "./../../components/FullPageSroll/FullPageSroll.type";

export type C_Props = {
  className?: string;
  style?: React.CSSProperties;
  FullPageApiRef?: FullPageApiRef;

  removeScrollEvent?: boolean;
};
export type C_States = Record<string, unknown>;

export type MapStateToProps = {
  Me: AppState.Me;
  Cart: AppState.ProductCartState;
};

export type MapDispatchToProps = {
  actions: typeof CommonActions;
};

export type MergedProps = C_Props & MapStateToProps & MapDispatchToProps;

export type P_Props = MergedProps &
  C_States & {
    signout: () => void;
  };
