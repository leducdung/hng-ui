import React, { useState } from "react";
import { PageFooter, PageHeader } from "..";
import { APP_ROUTES, Online, Offline, ImgService } from "@Constants";
import Link from "next/link";
import { useBreakpoints } from "../../hooks";
import { Dialog, Grid } from "@material-ui/core";
import { ComingSoon } from "@Components";
import Image from "next/image";
// import { useQuery, useMutation, gql } from "@apollo/client";
// import GetCustomer from "../../lib/queries/Customer.graphql";

const ServicePagePresenter: React.FC = () => {
  const breakpoints = useBreakpoints();
  const [visibleViewerModal, setVisibleViewerModal] = useState<boolean>(false);
  // const { data, loading, error } = useQuery(GetCustomer);
  // const [login] = useMutation(gql`
  //   mutation Test {
  //     customerLogin(loginInput: { email: "test1@gmail.com", password: "unibiz@123" }) {
  //       id
  //     }
  //   }
  // `);
  // try {
  //   login()
  // } catch (error) {
  //   console.error(error)
  // }
  const toggleViewerModal = () => {
    setVisibleViewerModal(!visibleViewerModal);
  };
  return (
    <>
      <PageHeader />
      <div className="service-page">
        <div className="service-page__title">dịch vụ</div>
        <div className="service-page__des">ƯU ĐIỂM NỔI BẬT</div>
        <div className="service-page__category">
          <div className="service-page__category__row">
            {ImgService.slice(0, Math.floor(ImgService.length / 2)).map((i) => {
              return (
                <div key={i.label} className="service-page__category__item">
                  <div className="service-page__category__img">
                    <Image src={i.img} alt={`${i.label} icon`} />
                  </div>
                  <div className="service-page__category__text">{i.label}</div>
                </div>
              );
            })}
          </div>
          <div className="service-page__category__row">
            {ImgService.slice(Math.floor(ImgService.length / 2), ImgService.length).map((i) => {
              return (
                <div key={i.label} className="service-page__category__item">
                  <div className="service-page__category__img">
                    <Image src={i.img} alt={`${i.label} icon`} />
                  </div>
                  <div className="service-page__category__text">
                    {!breakpoints.md ? i.labelmb : i.label}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="service-page__list">
          <div className="service-page__list__layout">
            <div className="service-page__list__left">
              <div className="service-page__list__title">
                Mua sản phẩm trang sức, mỹ nghệ
                <br />
                tích lũy online
              </div>
              {Online.map((o) => {
                return (
                  <Grid key={o.number} container className="service-page__list__item">
                    <Grid item xs={3} className="service-page__list__item__img">
                      <Image src={o.img} alt={o.title} placeholder="blur" />
                    </Grid>
                    <Grid item xs={9} className="service-page__list__item__content">
                      <div className="service-page__list__item__number">{o.number}</div>
                      <div className="service-page__list__item__title">{o.title}</div>
                    </Grid>
                  </Grid>
                );
              })}
              {/* <button className="service-page__btn-submit" onClick={toggleViewerModal}>
                MUA VÀNG NGAY
              </button> */}
            </div>
            <div className="service-page__list__right">
              <div className="service-page__list__title">
                Mua sản phẩm trang sức, mỹ nghệ
                <br />
                offline tại showroom HanaGold
              </div>
              {Offline.map((o) => {
                return (
                  <Grid key={o.number} container className="service-page__list__item">
                    <Grid item xs={3} className="service-page__list__item__img">
                      <Image src={o.img} alt={o.title} placeholder="blur" />
                    </Grid>
                    <Grid item xs={9} className="service-page__list__item__content">
                      <div className="service-page__list__item__number">{o.number}</div>
                      <div className="service-page__list__item__title">{o.title}</div>
                    </Grid>
                  </Grid>
                );
              })}
              <Link href={APP_ROUTES.MAP}>
                <a>
                  <button className="service-page__btn-submit">XEM CỬA HÀNG GẦN NHẤT</button>
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
      <PageFooter withoutDivider />

      <Dialog
        open={visibleViewerModal}
        onClose={() => {
          toggleViewerModal();
        }}
        fullScreen={!breakpoints.lg ? true : false}
        className="comingsoon-comp__dialog">
        <ComingSoon
          onClose={() => {
            toggleViewerModal();
          }}
        />
      </Dialog>
    </>
  );
};

export default ServicePagePresenter;
