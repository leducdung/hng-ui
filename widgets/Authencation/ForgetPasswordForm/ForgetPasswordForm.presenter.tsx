import React, { useEffect, useState } from "react";
import { Field, Form } from "react-final-form";
import { FormUtil } from "@Utils";
import { TextField } from "@material-ui/core";
import { APP_ROUTES, DEFAULT_IMG_ALT, REGEX } from "@Constants";
// import { P_Props } from "./ForgetPasswordForm.type";
// import { AppNotifications } from "../../../model";
// import { AuthService } from "../../../services";
// import { NotificationActionTypes } from "../../../redux/actions";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Link from "next/link";
import Image from "next/image";
import Banner from "../../../public/images/aboutus/bg2.png";
import BannerBottom from "../../../public/images/banner/bannerbottom.png";

const ForgetPasswordFormPresenter: React.FC = () => {
  const [errorMessage, setErrorMessage] = useState<string | null>(null);

  const onSubmit = (values) => {
    console.log(values);
    // AuthService.forgetPassword(values.email)
    //   .then((res) => {
    //     if (res && (res.status === 200 || res.status === 201)) {
    //       props.actions.createCommonType(NotificationActionTypes.ENQUEUE_SNACKBAR, {
    //         data: {
    //           type: AppNotifications.SUCCESS,
    //           message: "Vui lòng kiểm tra hộp thư đến để nhận Email quên mật khẩu",
    //         },
    //       });
    //       loadCallback(props.onComplete_Forget);
    //     } else if (res) {
    //       if (res.status === 404) {
    //         setErrorMessage("ERR_USERNAME_NOT_EXIST");
    //       } else {
    //         setErrorMessage("ERR_DEFAULT");
    //       }
    //     }
    //   })
    //   .catch((err) => {
    //     console.error(err);
    //     setErrorMessage("ERR_DEFAULT");
    //   });
  };

  useEffect(() => {
    if (errorMessage) {
      setTimeout(() => {
        setErrorMessage(null);
      }, 2000);
    }

    // eslint-disable-next-line
  }, [errorMessage]);

  return (
    <div className="auth-form">
      <div className="auth-form__bg">
        <Image alt={DEFAULT_IMG_ALT} src={Banner} />
      </div>
      <div className="auth-form__content">
        <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/app-icon.png"} />
        <div className="auth-form__title">QUÊN MẬT KHẨU</div>
        <div className="auth-form__description">
          Nhập vào E-mail bạn dùng để đăng ký tài khoản,
          <br />
          chúng tôi sẽ giúp bạn đặt lại mật khẩu.
        </div>
        <Form
          onSubmit={async (values) => {
            onSubmit(values);
          }}
          render={({ handleSubmit }) => {
            return (
              <form onSubmit={handleSubmit} className="auth-form__form">
                <Field
                  name="email"
                  type="email"
                  validate={FormUtil.composeValidators([
                    FormUtil.Rule.required("ERR_REQUIRED_EMAIL"),
                    FormUtil.Rule.pattern(REGEX.EMAIL, { errorMessage: "ERR_EMAIL_INVALID" }),
                  ])}>
                  {({ input, meta, ...rest }) => {
                    return (
                      <TextField
                        {...input}
                        {...rest}
                        fullWidth
                        placeholder={"Nhập địa chỉ email"}
                        className="form-text-field"
                        inputProps={{ className: "input" }}
                        variant="outlined"
                        onChange={(e) => input.onChange(e.target.value)}
                        helperText={meta.touched ? meta.error : ""}
                        error={meta.error && meta.touched}
                      />
                    );
                  }}
                </Field>

                <button type="submit" className="auth-form__form__submit-btn">
                  <Link href={APP_ROUTES.RESETPASSWORD}>
                    <a>Đặt lại mật khẩu</a>
                  </Link>
                </button>
                <div className="auth-form__form__forgetpw">
                  Bạn chưa có tài khoản?
                  <Link href={APP_ROUTES.SIGNUP}>
                    <a> ĐĂNG KÝ</a>
                  </Link>
                </div>
              </form>
            );
          }}
        />
        {/* <div className="auth-form__block__footer auth-form__signin__footer">
        <div className="auth-form__signin__footer__line2">
          {"DO_NOT_HAVE_AN_ACCOUNT"}
          <button
            className="sign-up"
            onClick={() => {
              props.onChangeTab(AuthTabPosition.SIGN_UP);
            }}
          >
            {"REGISTER_NOW"}
          </button>
        </div>
      </div> */}
      </div>
      <div className="auth-form__bg-bottom">
        <Image alt={DEFAULT_IMG_ALT} src={BannerBottom} layout="fill" />
      </div>
    </div>
  );
};

export default ForgetPasswordFormPresenter;
