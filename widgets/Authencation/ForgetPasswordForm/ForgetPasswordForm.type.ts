import { AuthTabPosition } from "@Models";
import { CommonActions } from "../../../redux/actions";

export type P_Props = {
  actions: typeof CommonActions;
  onChangeTab: (tabPos: AuthTabPosition) => void;
  onClose: () => void;
  onComplete_Forget?: () => void;
};
