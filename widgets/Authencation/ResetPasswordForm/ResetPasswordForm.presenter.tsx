import React, { useState } from "react";
import { HtmlHeader } from "../..";
import { Field, Form } from "react-final-form";
import { FormUtil } from "@Utils";
import { IconButton, InputAdornment, TextField } from "@material-ui/core";
import {
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from "@material-ui/icons";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { DEFAULT_IMG_ALT } from "@Constants";
import Image from "next/image";
import Banner from "../../../public/images/aboutus/bg2.png";
import BannerBottom from "../../../public/images/banner/bannerbottom.png";

const ResetPasswordFormPresenter: React.FC = () => {
  const [visiblePassword, setVisiblePassword] = useState<boolean>(false);
  const [visibleConfirmPassword, setVisibleConfirmPassword] = useState<boolean>(false);

  const toggleVisiblePassword = () => {
    setVisiblePassword(!visiblePassword);
  };

  const toggleVisibleConfirmPassword = () => {
    setVisibleConfirmPassword(!visibleConfirmPassword);
  };

  return (
    <>
      <HtmlHeader />
      <div className="auth-form">
        <div className="auth-form__bg">
          <Image alt={DEFAULT_IMG_ALT} src={Banner} />
        </div>
        <div className="auth-form__content">
          <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/app-icon.png"} />
          <div className="auth-form__title">ĐẶT LẠI MẬT KHẨU</div>
          <div className="auth-form__description">
            Bạn vui lòng nhập mẩu khẩu mới để tiếp tục đăng
            <br />
            nhập vào hệ thống.
          </div>
          <Form
            onSubmit={async () => {
              // signin(values);
            }}
            render={({ handleSubmit }) => {
              // submit = handleSubmit;
              return (
                <form onSubmit={handleSubmit} className="auth-form__form">
                  <Field name="password" validate={FormUtil.Rule.required("ERR_REQUIRED_PWD")}>
                    {({ input, meta, ...rest }) => {
                      return (
                        <TextField
                          {...input}
                          {...rest}
                          fullWidth
                          placeholder={"Mật khẩu mới"}
                          className="form-text-field"
                          inputProps={{ className: "input" }}
                          variant="outlined"
                          onChange={(e) => input.onChange(e.target.value)}
                          helperText={meta.touched ? meta.error : ""}
                          error={meta.error && meta.touched}
                          InputProps={{
                            type: visiblePassword ? "text" : "password",
                            endAdornment: (
                              <InputAdornment position="end">
                                <IconButton onClick={toggleVisiblePassword}>
                                  {visiblePassword ? (
                                    <VisibilityIcon
                                      style={{ fontSize: "2rem", color: "#262525" }}
                                    />
                                  ) : (
                                    <VisibilityOffIcon
                                      style={{ fontSize: "2rem", color: "#262525" }}
                                    />
                                  )}
                                </IconButton>
                              </InputAdornment>
                            ),
                          }}
                        />
                      );
                    }}
                  </Field>

                  <Field
                    name="confirmPassword"
                    validate={FormUtil.Rule.required("ERR_REQUIRED_PWD")}>
                    {({ input, meta, ...rest }) => {
                      return (
                        <TextField
                          {...input}
                          {...rest}
                          fullWidth
                          placeholder="Nhập lại mật khẩu mới"
                          className="form-text-field"
                          inputProps={{ className: "input" }}
                          variant="outlined"
                          onChange={(e) => input.onChange(e.target.value)}
                          helperText={meta.touched ? meta.error : ""}
                          error={meta.error && meta.touched}
                          InputProps={{
                            type: visibleConfirmPassword ? "text" : "password",
                            endAdornment: (
                              <InputAdornment position="end">
                                <IconButton onClick={toggleVisibleConfirmPassword}>
                                  {visibleConfirmPassword ? (
                                    <VisibilityIcon
                                      style={{ fontSize: "2rem", color: "#262525" }}
                                    />
                                  ) : (
                                    <VisibilityOffIcon
                                      style={{ fontSize: "2rem", color: "#262525" }}
                                    />
                                  )}
                                </IconButton>
                              </InputAdornment>
                            ),
                          }}
                        />
                      );
                    }}
                  </Field>

                  <button
                    type="submit"
                    className="auth-form__form__submit-btn"
                    onClick={(e) => {
                      e.preventDefault();
                      // loadCallback(submit, e);
                    }}
                    // disabled={props.Authenication.loading}
                  >
                    Đặt lại mật khẩu
                  </button>
                </form>
              );
            }}
          />
        </div>
        <div className="auth-form__bg-bottom">
          <Image alt={DEFAULT_IMG_ALT} src={BannerBottom} layout="fill" />
        </div>
      </div>
    </>
  );
};

export default ResetPasswordFormPresenter;
