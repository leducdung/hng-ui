import React from "react";
import Head from "next/head";
import { P_Props } from "./HtmlHeader.type";
import { DEFAULT_HTML } from "@Constants";

const HtmlHeader: React.FC<P_Props> = (props) => {
  return (
    <Head>
      <meta property="og:title" content={props.metaTitle || DEFAULT_HTML.META_TITLE} />
      <meta
        property="og:description"
        content={props.metaDescription || DEFAULT_HTML.META_DESCRIPTION}
      />
      <meta property="og:image" content="/images/aboutus/img3.png" />
      <meta name="description" content={props.metaDescription || DEFAULT_HTML.META_DESCRIPTION} />

      <title>{props.pageTitle || DEFAULT_HTML.TITLE}</title>
    </Head>
  );
};

export default HtmlHeader;
