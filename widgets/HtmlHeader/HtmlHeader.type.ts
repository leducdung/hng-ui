export type P_Props = {
  pageTitle?: string | null;
  metaTitle?: string | null;
  metaDescription?: string | null;
};
