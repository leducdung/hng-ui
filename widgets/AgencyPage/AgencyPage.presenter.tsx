import React, { useState } from "react";
import SwiperCore, { Autoplay, Navigation, Controller, Thumbs } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import { PageFooter, PageHeader } from "..";
import { DEFAULT_IMG_ALT } from "@Constants";
import Image from "next/image";
import {
  DistributorRegisterBlock,
  Registration,
  ShowRoomPackages,
  SpecialProducts,
  SpecialOffer,
  Opportunity,
} from "@Components";
import { useBreakpoints } from "../../hooks";
import SwiperClass from "swiper/types/swiper-class";
import { Dialog } from "@material-ui/core";
import Banner1 from "../../public/images/banner/banner1.png";
import Banner2 from "../../public/images/banner/banner2.png";
import Banner3 from "../../public/images/banner/banner3.png";
import Banner4 from "../../public/images/banner/banner4.png";
import Banner5 from "../../public/images/banner/banner5.png";

SwiperCore.use([Navigation, Autoplay, Controller, Thumbs]);

const BANNER = [
  { id: "1", img: Banner1 },
  { id: "2", img: Banner2 },
  { id: "3", img: Banner3 },
  { id: "4", img: Banner4 },
  { id: "5", img: Banner5 },
];

const AgencyPagePresenter: React.FC = () => {
  const breakpoints = useBreakpoints();

  const [thumbsSwiper, setThumbsSwiper] = useState<SwiperClass>();

  const [visibleViewerModal, setVisibleViewerModal] = useState<boolean>(false);

  const toggleViewerModal = () => {
    setVisibleViewerModal(!visibleViewerModal);
  };

  return (
    <>
      <PageHeader />
      <div className="agency-page">
        <div className="agency-page__banner">
          <Swiper
            slidesPerView={1}
            autoplay={{ delay: 3000 }}
            loop
            thumbs={{ swiper: thumbsSwiper }}
            controller={{ control: thumbsSwiper }}>
            {BANNER.map((banner) => {
              return (
                <SwiperSlide key={banner.id}>
                  <Image
                    src={banner.img}
                    alt={DEFAULT_IMG_ALT}
                    placeholder="blur"
                    layout="responsive"
                  />
                </SwiperSlide>
              );
            })}
          </Swiper>
        </div>

        <div className="agency-page__opportunity">
          <div className="swiper">
            <Swiper slidesPerView={3} navigation onSwiper={setThumbsSwiper}>
              {BANNER.map((banner) => {
                return (
                  <SwiperSlide key={banner.id}>
                    <div className="swiper__img">
                      <Image
                        src={banner.img}
                        alt={DEFAULT_IMG_ALT}
                        placeholder="blur"
                        objectFit="cover"
                        layout="fill"
                      />
                    </div>
                  </SwiperSlide>
                );
              })}
            </Swiper>
          </div>
          <Opportunity />
        </div>

        <div className="agency-page__special-offer">
          <SpecialOffer />
        </div>
        <div className="agency-page__special-products">
          <SpecialProducts />
        </div>
        <div className="agency-page__showroom">
          <ShowRoomPackages />
        </div>
      </div>
      <DistributorRegisterBlock />
      <PageFooter withoutDivider />
      <Dialog
        open={visibleViewerModal}
        onClose={() => {
          toggleViewerModal();
        }}
        fullScreen={!breakpoints.lg ? true : false}
        className="registration__dialog">
        <Registration
          onClose={() => {
            toggleViewerModal();
          }}
        />
      </Dialog>
    </>
  );
};

export default AgencyPagePresenter;
