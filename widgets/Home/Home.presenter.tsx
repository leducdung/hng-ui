import React, { useEffect, useState } from "react";
import { PageFooter, PageHeader } from "..";
import { P_Props } from "./Home.type";
import { Swiper, SwiperSlide } from "swiper/react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Zoom from "react-reveal/Zoom";
import Fade from "react-reveal/Fade";
import {
  AboutUs,
  BranchMap,
  BtnGroup,
  DistributorRegisterBlock,
  IntroDownApp,
  TableValue,
  WhyUs,
  Collection,
  HomeProduct,
  Partner,
} from "../../components";
import { APP_ROUTES, DEFAULT_IMG_ALT } from "@Constants";
import { NewsOutputModel } from "@Models";
import { NewsService } from "../../services";
import Link from "next/link";
import { NewsUtil } from "@Utils";
import { useBreakpoints } from "../../hooks";
import Image from "next/image";
import Banner from "../../public/images/aboutus/bg2.png";
import Advisory from "@Components/Advisory";

const HomePresenter: React.FC<P_Props> = () => {
  const breakpoints = useBreakpoints();
  const [news, setNews] = useState<NewsOutputModel[]>([]);

  useEffect(() => {
    NewsService.getNews()
      .then((res) => {
        if (res && (res.status === 200 || res.status === 201)) {
          setNews(res.data.data);
        }
      })
      .catch((err) => {
        console.error(err);
      });
  }, []);

  return (
    <>
      <PageHeader />
      <main className="home-page">
        <div className="home-page__banner__content">
          <Zoom>
            <div className="home-page__banner__content__title">
              HanaGold
              <br />
              Tiệm kim hoàn 4.0
            </div>
          </Zoom>
          <Fade bottom>
            {/* <div className="home-page__banner__content__description">
              HanaGold là nền tảng kết nối tiệm kim hoàn và người đi mua vàng, tại đó người dùng có
              thể chọn lựa mẫu mã, mua vàng tích lũy online theo giá trị sản phẩm và nhận vàng
              offline tại chuỗi cửa hàng HanaGold và hệ thống kim hoàn liên kết trên toàn quốc
            </div> */}
          </Fade>
        </div>
        <Swiper
          slidesPerView={1}
          // loop={true}
        >
          <div className="home-page__bg">
            <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="fill" objectFit="cover" />
          </div>
          <SwiperSlide>
            <div className="home-page__banner">
              <div className="home-page__bg__banner">
                <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/bgbanner.png"} />
              </div>
              <div className="home-page__bg__image">
                <Zoom>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/imgbanner.png"} />
                </Zoom>
              </div>
              <div className="home-page__bg__image__top-left img-item">
                <Fade left>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/imgtopleft.png"} />
                </Fade>
              </div>
              <div className="home-page__bg__image__bottom-left img-item">
                <Fade left>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/imgbottomright.png"} />
                </Fade>
              </div>
              <div className="home-page__bg__image__top-right img-item">
                <Fade right>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/imgtopright.png"} />
                </Fade>
              </div>
              <div className="home-page__bg__image__bottom-right img-item">
                <Fade right>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/imgbottomright.png"} />
                </Fade>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="home-page__banner">
              <div className="home-page__bg__banner">
                <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/bgbanner.png"} />
              </div>
              <div className="home-page__bg__image">
                <Zoom>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/imgbanner.png"} />
                </Zoom>
              </div>
              <div className="home-page__bg__image__top-left img-item">
                <Fade left>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/imgtopleft.png"} />
                </Fade>
              </div>
              <div className="home-page__bg__image__bottom-left img-item">
                <Fade left>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/imgbottomright.png"} />
                </Fade>
              </div>
              <div className="home-page__bg__image__top-right img-item">
                <Fade right>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/imgtopright.png"} />
                </Fade>
              </div>
              <div className="home-page__bg__image__bottom-right img-item">
                <Fade right>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/imgbottomright.png"} />
                </Fade>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="home-page__banner">
              <div className="home-page__bg__banner">
                <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/bgbanner.png"} />
              </div>
              <div className="home-page__bg__image">
                <Zoom>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/imgbanner.png"} />
                </Zoom>
              </div>
              <div className="home-page__bg__image__top-left img-item">
                <Fade left>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/imgtopleft.png"} />
                </Fade>
              </div>
              <div className="home-page__bg__image__bottom-left img-item">
                <Fade left>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/imgbottomright.png"} />
                </Fade>
              </div>
              <div className="home-page__bg__image__top-right img-item">
                <Fade right>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/imgtopright.png"} />
                </Fade>
              </div>
              <div className="home-page__bg__image__bottom-right img-item">
                <Fade right>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/imgbottomright.png"} />
                </Fade>
              </div>
            </div>
          </SwiperSlide>
        </Swiper>

        <Zoom>
          <TableValue />
        </Zoom>
        <WhyUs />
        <IntroDownApp />
        <HomeProduct />
        <Collection />
        <BranchMap />
        <AboutUs />
        <Advisory />
        <Partner />
        {Boolean(news.length) && (
          <section className="home-page__block home-page__news">
            <div className="home-page__news__header">
              <div style={{ margin: "0" }} className="home-page__block__title">
                tin tức
              </div>
              {breakpoints.lg && (
                <Link href={APP_ROUTES.NEWS}>
                  <a className="home-page__news__list-link">XEM TẤT CẢ</a>
                </Link>
              )}
            </div>

            <BtnGroup<NewsOutputModel>
              list={news}
              renderBtnLabel={(item) => {
                return (
                  <Link href={NewsUtil.buildNewsDetailPath(item)}>
                    <a href="#" className="home-page__news__item">
                      <div className="home-page__news__item__img">
                        {/* <LazyLoadImage alt={DEFAULT_IMG_ALT} src={ImageUtil.buildNewsImgSrc(item.imageFeatured)} /> */}
                        <Zoom ssrReveal>
                          <LazyLoadImage alt={DEFAULT_IMG_ALT} src={item.imageFeatured!} />
                        </Zoom>
                      </div>

                      <Fade top>
                        <div className="home-page__news__item__title">{item.title}</div>
                      </Fade>
                      <Fade bottom>
                        <p className="home-page__news__item__description">
                          {item.shortDescription}
                        </p>
                      </Fade>
                    </a>
                  </Link>
                );
              }}
              className="home-page__news__container"
            />
          </section>
        )}
        <DistributorRegisterBlock />
      </main>
      <PageFooter withoutDivider />
    </>
  );
};

export default HomePresenter;
