import React, { useState } from "react";
import PageHeader from "@Widgets/PageHeader";
import PageFooter from "@Widgets/PageFooter";
import { APP_ROUTES, DEFAULT_IMG_ALT, CategoryStone } from "@Constants";
import Link from "next/link";
import { useBreakpoints } from "../../hooks";
import { LinearProgress } from "@material-ui/core";
import { ProductCard1, BtnGroup } from "@Components";
// import { Search as SearchIcon } from "@material-ui/icons";
import { useQuery } from "@apollo/client";
import GetProduct from "@Lib/queries/Product.graphql";
import { currencyFormat } from "@Utils/other";
import Image from "next/image";
import Banner from "../../public/images/banner/banner-stone.png";
import BannerText from "../../public/images/banner/logostone.png";
// import Icon from "../../public/images/icon/icon-sort.png";

export type CategoryItem = {
  id: string;
  name: string;
};

const DancingStonePagePresenter: React.FC = () => {
  const breakpoints = useBreakpoints();
  const [, setSelectCategory] = useState<CategoryItem | null>(CategoryStone[0] ?? null);
  const activeCategory = CategoryStone.find((cate) => cate.id === "");
  const { data: productsData } = useQuery(GetProduct.GetProductByCategorySlug, {
    variables: { slug: "trang-suc-happy-stone", pageNum: 1, pageSize: 100 },
  });
  const numberOfProducts = productsData
    ? productsData.result.products.filter((p) => p.status).length
    : 0;
  return (
    <>
      <PageHeader />
      <div className="dancing-store-page">
        <div className="dancing-store-page__banner">
          <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="fill" objectFit="cover" />
          <div className="dancing-store-page__banner__text">
            <div className="dancing-store-page__banner__logo">
              <Image src={BannerText} alt={DEFAULT_IMG_ALT} objectFit="contain" />
            </div>
            <Link href={APP_ROUTES.INTRODUCTION_STONE}>
              <a>
                <button className="dancing-store-page__banner__btn">Tìm hiểu thêm</button>
              </a>
            </Link>
          </div>
        </div>
        <div className="dancing-store-page__filter">
          {breakpoints.lg && (
            <div className="dancing-store-page__filter__count">{numberOfProducts} Sản phẩm</div>
          )}
          {/* <div className="dancing-store-page__filter-right">
            <TextField
              fullWidth
              placeholder="Bạn đang tìm sản phẩm nào?"
              variant="outlined"
              className={`dancing-store-page__filter__search-input`}
              inputProps={{ className: "input" }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon style={{ fontSize: "2.2rem", color: "#9E9E9E" }} />
                  </InputAdornment>
                ),
              }}
              onChange={() => {
                // setSearchValue(e.target.value);
                // props.onSearch(e.target.value);
              }}
            />

            {breakpoints.lg && (
              <>
                <Select
                  variant="outlined"
                  className="dancing-store-page__filter__select"
                  renderValue={() => <span>Danh mục</span>}
                  displayEmpty
                  MenuProps={{
                    className: "dancing-store-page__filter__select__menu",
                    anchorOrigin: {
                      vertical: 60,
                      horizontal: "left",
                    },
                    transformOrigin: {
                      vertical: "top",
                      horizontal: "left",
                    },
                    getContentAnchorEl: null,
                  }}>
                  {CategoryStone.map((cate) => {
                    return (
                      <MenuItem key={cate.id} style={{ fontSize: "1.4rem" }}>
                        {cate.name}
                      </MenuItem>
                    );
                  })}
                </Select>

                <Select
                  onChange={() => {
                    // props.onChangeFilterValue(e.target.value as any);
                  }}
                  variant="outlined"
                  className="dancing-store-page__filter__select"
                  renderValue={() => <span>Giá</span>}
                  displayEmpty
                  MenuProps={{
                    className: "dancing-store-page__filter__select__menu",
                    anchorOrigin: {
                      vertical: 60,
                      horizontal: "left",
                    },
                    transformOrigin: {
                      vertical: "top",
                      horizontal: "left",
                    },
                    getContentAnchorEl: null,
                  }}> */}
          {/* {Price.map((p) => {
                    return (
                      <MenuItem key={p.price} style={{ fontSize: "1.4rem" }}>
                        {p.price}
                      </MenuItem>
                    );
                  })} */}
          {/* </Select>

                <Select
                  onChange={() => {
                    // props.onChangeFilterValue(e.target.value as any);
                  }}
                  variant="outlined"
                  className="dancing-store-page__filter__select"
                  renderValue={() => <span>Chất liệu</span>}
                  displayEmpty
                  MenuProps={{
                    className: "dancing-store-page__filter__select__menu",
                    anchorOrigin: {
                      vertical: 60,
                      horizontal: "left",
                    },
                    transformOrigin: {
                      vertical: "top",
                      horizontal: "left",
                    },
                    getContentAnchorEl: null,
                  }}> */}
          {/* {Price.map((p) => {
                        return (
                          <MenuItem key={p.price} style={{ fontSize: "1.4rem" }}>
                            {p.price}
                          </MenuItem>
                        );
                      })} */}
          {/* </Select>
              </>
            )}
          </div>
          {!breakpoints.lg && (
            <div className="dancing-store-page__filter__sort">
              <Image src={Icon} alt={DEFAULT_IMG_ALT} objectFit="contain" />
            </div>
          )} */}
        </div>
        {!breakpoints.lg && (
          <div className="dancing-store-page__list__category">
            <BtnGroup<CategoryItem>
              list={[{ id: "", name: "Tất cả" }, ...CategoryStone]}
              onClick={(cate) => {
                setSelectCategory(cate);
              }}
              renderBtnLabel={(cate) => {
                // return <div className="dancing-store-page__list__category__item">{cate.name}</div>;

                if (!cate.id) {
                  return (
                    <div
                      className={
                        !activeCategory
                          ? "dancing-store-page__list__category__item active"
                          : "dancing-store-page__list__category__item"
                      }
                      onClick={() => {
                        // props.onChangeCategory(cate);
                      }}>
                      Tất cả
                    </div>
                  );
                }
                return (
                  <div
                    className={
                      cate.id === activeCategory?.id
                        ? "dancing-store-page__list__category__item active"
                        : "dancing-store-page__list__category__item"
                    }>
                    {cate.name}
                  </div>
                );
              }}
            />
          </div>
        )}
        <div className="dancing-store-page__list">
          {productsData &&
            productsData.result.products
              .filter((p) => p.status)
              .map((pro) => {
                return (
                  <ProductCard1
                    key={pro.id}
                    slug={APP_ROUTES.SAN_PHAM + "/" + pro.slug}
                    id={pro.id}
                    title={pro.name}
                    img={pro.images[0].path}
                    price={parseInt(pro.price) > 0 ? `${currencyFormat(pro.price)} đ` : "Liên hệ"}
                    discount={pro.discount ?? 0}
                  />
                );
              })}
        </div>
        <div className="dancing-store-page__list__btn">
          {breakpoints.lg && (
            <>
              <div className="dancing-store-page__list__text">
                {numberOfProducts} kết quả của {numberOfProducts} sản phẩm
              </div>
              <LinearProgress
                variant="determinate"
                value={100}
                style={{ width: "24rem", marginTop: "2.2rem" }}
              />
            </>
          )}
          {/* <button className="dancing-store-page__list__btn-submit">Xem thêm</button> */}
        </div>
      </div>
      <PageFooter withoutDivider />
    </>
  );
};

export default DancingStonePagePresenter;
