import React, { useState } from "react";
import { PageFooter, PageHeader } from "@Widgets";
import { Category, DEFAULT_IMG_ALT, Price } from "@Constants";
import { useBreakpoints } from "../../hooks";
import {
  InputAdornment,
  TextField,
  Select,
  MenuItem,
  Grid,
  LinearProgress,
} from "@material-ui/core";
import { Search as SearchIcon } from "@material-ui/icons";
import { BtnGroup, ProductCard1 } from "@Components";
import { Zoom, Fade } from "react-reveal";
import { useQuery } from "@apollo/client";
import GetProduct from "../../lib/queries/Product.graphql";
import { currencyFormat } from "@Utils";
import Image from "next/image";
import Icon from "../../public/images/icon/icon-sort.png";

export type CategoryItem = {
  id: string;
  name: string;
};

const ProductListingPresenter: React.FC = () => {
  const breakpoints = useBreakpoints();
  const [selectCategory, setSelectCategory] = useState<CategoryItem | null>(Category[0] ?? null);
  const activeCategory = Category.find((cate) => cate.id === selectCategory?.id);

  const { data: productsData } = useQuery(GetProduct.GetProductByCategory, {
    variables: { categoryId: 27, pageNum: 1, pageSize: 10 },
  });

  const productQuantity = productsData ? productsData.getProductByCategory.products.length : 0;

  return (
    <>
      <PageHeader />
      <div className="product-listing">
        <div className="product-listing__banner">
          {breakpoints.lg && (
            <Image
              src="/images/banner/banner-product.png"
              width="1408"
              height="737"
              alt={DEFAULT_IMG_ALT}
            />
          )}

          <Grid
            container
            direction="row"
            justify="center"
            alignItems="stretch"
            spacing={1}
            className="product-listing__banner__list">
            <Grid item container lg={6} className="product-listing__banner__img-left">
              <Grid item direction="column">
                <Zoom>
                  <Image
                    src="/images/banner/banner-product1.png"
                    width="550"
                    height="426"
                    alt={DEFAULT_IMG_ALT}
                  />
                </Zoom>
              </Grid>
            </Grid>
            {breakpoints.lg && (
              <Grid
                item
                container
                lg={6}
                spacing={2}
                className="product-listing__banner__img-right">
                <Grid item container lg={6} spacing={2} direction="column">
                  <Grid item>
                    <Fade top>
                      <Image
                        width={264}
                        height={202}
                        src="/images/banner/banner-product2.png"
                        alt={DEFAULT_IMG_ALT}
                      />
                    </Fade>
                  </Grid>
                  <Grid item>
                    <Fade bottom>
                      <Image
                        width={264}
                        height={202}
                        src="/images/banner/banner-product3.png"
                        alt={DEFAULT_IMG_ALT}
                      />
                    </Fade>
                  </Grid>
                </Grid>
                <Grid item container lg={6} spacing={2} direction="column">
                  <Grid item>
                    <Fade top>
                      <Image
                        width={264}
                        height={202}
                        src="/images/banner/banner-product4.png"
                        alt={DEFAULT_IMG_ALT}
                      />
                    </Fade>
                  </Grid>
                  <Grid item>
                    <Fade bottom>
                      <Image
                        width={264}
                        height={202}
                        src="/images/banner/banner-product5.png"
                        alt={DEFAULT_IMG_ALT}
                      />
                    </Fade>
                  </Grid>
                </Grid>
              </Grid>
            )}
          </Grid>
        </div>
        <div className="product-listing__filter">
          {breakpoints.lg && (
            <div className="product-listing__filter__count">{productQuantity} Sản phẩm</div>
          )}
          <div className="product-listing__filter-right">
            <TextField
              fullWidth
              placeholder="Bạn đang tìm sản phẩm nào?"
              variant="outlined"
              className={`product-listing__filter__search-input`}
              inputProps={{ className: "input" }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon style={{ fontSize: "2.2rem", color: "#9E9E9E" }} />
                  </InputAdornment>
                ),
              }}
            />

            {breakpoints.lg && (
              <>
                <Select
                  variant="outlined"
                  className="product-listing__filter__select"
                  renderValue={() => <span>Danh mục</span>}
                  displayEmpty
                  MenuProps={{
                    className: "product-listing__filter__select__menu",
                    anchorOrigin: {
                      vertical: 60,
                      horizontal: "left",
                    },
                    transformOrigin: {
                      vertical: "top",
                      horizontal: "left",
                    },
                    getContentAnchorEl: null,
                  }}>
                  {Category.map((cate) => {
                    return (
                      <MenuItem key={cate.id} style={{ fontSize: "1.4rem" }}>
                        {cate.name}
                      </MenuItem>
                    );
                  })}
                </Select>

                <Select
                  variant="outlined"
                  className="product-listing__filter__select"
                  renderValue={() => <span>Giá</span>}
                  displayEmpty
                  MenuProps={{
                    className: "product-listing__filter__select__menu",
                    anchorOrigin: {
                      vertical: 60,
                      horizontal: "left",
                    },
                    transformOrigin: {
                      vertical: "top",
                      horizontal: "left",
                    },
                    getContentAnchorEl: null,
                  }}>
                  {Price.map((p) => {
                    return (
                      <MenuItem key={p.price} style={{ fontSize: "1.4rem" }}>
                        {p.price}
                      </MenuItem>
                    );
                  })}
                </Select>

                <Select
                  variant="outlined"
                  className="product-listing__filter__select"
                  renderValue={() => <span>Chất liệu</span>}
                  displayEmpty
                  MenuProps={{
                    className: "product-listing__filter__select__menu",
                    anchorOrigin: {
                      vertical: 60,
                      horizontal: "left",
                    },
                    transformOrigin: {
                      vertical: "top",
                      horizontal: "left",
                    },
                    getContentAnchorEl: null,
                  }}>
                  {/* {Price.map((p) => {
                        return (
                          <MenuItem key={p.price} style={{ fontSize: "1.4rem" }}>
                            {p.price}
                          </MenuItem>
                        );
                      })} */}
                </Select>
              </>
            )}
          </div>
          {!breakpoints.lg && (
            <div className="product-listing__filter__sort">
              <Image src={Icon} alt={DEFAULT_IMG_ALT} objectFit="contain" />
            </div>
          )}
        </div>
        {!breakpoints.lg && (
          <div className="product-listing__list__category">
            <BtnGroup<CategoryItem>
              list={[{ id: "", name: "Tất cả" }, ...Category]}
              onClick={(cate) => {
                setSelectCategory(cate);
              }}
              renderBtnLabel={(cate) => {
                if (!cate.id) {
                  return (
                    <div
                      className={
                        !activeCategory
                          ? "product-listing__list__category__item active"
                          : "product-listing__list__category__item"
                      }
                      onClick={() => {
                        // props.onChangeCategory(cate);
                      }}>
                      Tất cả
                    </div>
                  );
                }
                return (
                  <div
                    className={
                      cate.id === activeCategory?.id
                        ? "product-listing__list__category__item active"
                        : "product-listing__list__category__item"
                    }>
                    {cate.name}
                  </div>
                );
              }}
            />
          </div>
        )}

        {productsData && (
          <Grid container spacing={4} className="product-listing__list">
            {productsData.getProductByCategory.products.map((pro) => {
              return (
                <Grid item xs={3} key={pro.id}>
                  <ProductCard1
                    id={pro.id}
                    title={pro.name}
                    img={pro.images[0].path}
                    price={parseInt(pro.price) > 0 ? `${currencyFormat(pro.price)} đ` : "Liên hệ"}
                    discount={pro.discount ?? 0}
                  />
                </Grid>
              );
            })}
          </Grid>
        )}

        {productsData && (
          <div className="product-listing__list__btn">
            {breakpoints.lg && (
              <>
                <div className="product-listing__list__text">
                  {productQuantity} kết quả của {productQuantity} sản phẩm
                </div>

                <LinearProgress
                  variant="determinate"
                  value={productQuantity < 15 ? 100 : 30}
                  style={{ width: "24rem", marginTop: "2.2rem" }}
                />
              </>
            )}
            <button className="product-listing__list__btn-submit">Xem thêm</button>
          </div>
        )}
      </div>

      <PageFooter withoutDivider />
    </>
  );
};

export default ProductListingPresenter;
