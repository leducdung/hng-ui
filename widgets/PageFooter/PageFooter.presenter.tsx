import React from "react";
import Link from "next/link";
import { FacebookIcon, InstagramIcon, YoutubeIcon } from "@Components/Icons";
import { APP_ROUTES, DEFAULT_STORE_INFOS } from "@Constants";
import { P_Props } from "./PageFooter.type";
import Fade from "react-reveal/Fade";

const PageFooterPresenter: React.FC<P_Props> = (props) => {
  const socialBtnGroup = (
    <div className="page-footer__social-info__btn-group">
      <Link passHref href={"https://www.facebook.com/VangHanaGold/"}>
        <a>
          <FacebookIcon size={[9, 17]} viewBox={[9, 17]} color="#605D5A" />
        </a>
      </Link>
      <Link passHref href={"https://www.instagram.com/trangsuchappystone"}>
        <a>
          <InstagramIcon size={[17, 17]} viewBox={[17, 17]} color="#605D5A" />
        </a>
      </Link>
      <Link passHref href={"http://www.youtube.com/channel/UC-R1xz8ZCkJTDLLjBD9YKfQ"}>
        <a>
          <YoutubeIcon size={[19, 15]} viewBox={[19, 15]} color="#605D5A" />
        </a>
      </Link>
    </div>
  );

  return (
    <>
      {!props.withoutDivider && <div className="page-footer__divider"></div>}
      <footer id="footer" className="page-footer">
        <div className="page-footer__content">
          <div className="page-footer__url-info">
            <Fade top>
              <div className="page-footer__view">
                <div className="page-footer__view__item">
                  <div className="page-footer__title">HANAGOLD</div>
                  <Link href={APP_ROUTES.ABOUT_US} passHref>
                    <a className="page-footer__url-info__link">
                      Giới thiệu {DEFAULT_STORE_INFOS.NAME}
                    </a>
                  </Link>
                  <Link href={APP_ROUTES.MARKETPLACE} passHref>
                    <a className="page-footer__url-info__link">Hệ thống cửa hàng</a>
                  </Link>
                </div>

                <div className="page-footer__view__item page-footer__view__item__right">
                  <Link href={APP_ROUTES.DANCING_STONE} passHref>
                    <a className="page-footer__url-info__link">Sản phẩm</a>
                  </Link>
                  <div className="page-footer__social-info">{socialBtnGroup}</div>
                </div>
              </div>
            </Fade>
          </div>
          <Fade top>
            <div className="page-footer__url-info page-footer__instruction">
              <div className="page-footer__title">HƯỚNG DẪN</div>
              <div className="page-footer__view">
                <div className="page-footer__view__item">
                  <Link href={APP_ROUTES.SHOPPING_GUIDE} passHref>
                    <a className="page-footer__url-info__link">Hướng dẫn mua hàng</a>
                  </Link>
                  <Link href={APP_ROUTES.PAYMENT_GUIDE} passHref>
                    <a className="page-footer__url-info__link">Hướng dẫn thanh toán</a>
                  </Link>
                </div>
                <div className="page-footer__view__item">
                  <Link href={APP_ROUTES.JEWELRY_GUIDE} passHref>
                    <a className="page-footer__url-info__link">Hướng dẫn bảo quản trang sức </a>
                  </Link>
                  <Link href={APP_ROUTES.SIZE_GUIDE} passHref>
                    <a className="page-footer__url-info__link">Hướng dẫn đo size trang sức</a>
                  </Link>
                </div>
              </div>
            </div>
          </Fade>
          <Fade top>
            <div className="page-footer__url-info page-footer__provision">
              <div className="page-footer__title">ĐIỀU KHOẢN CHÍNH SÁCH</div>
              <div className="page-footer__view">
                <div className="page-footer__view__item">
                  {/* <a
                    href={DEFAULT_STORE_INFOS.FILE}
                    download="tieu-chuan.pdf"
                    target="_blank"
                    rel="noreferrer"
                    className="page-footer__url-info__link">
                    Công bố tiêu chuẩn cơ sở
                  </a> */}
                  <Link href={APP_ROUTES.PDF} passHref>
                    <a target="_blank" className="page-footer__url-info__link">
                      Công bố tiêu chuẩn cơ sở
                    </a>
                  </Link>
                  <Link href={APP_ROUTES.POLICY_GUIDE} passHref>
                    <a className="page-footer__url-info__link">Chính sách giao nhận</a>
                  </Link>
                  <Link href={APP_ROUTES.WARRANTY_GUIDE} passHref>
                    <a className="page-footer__url-info__link">Quy định bảo hành</a>
                  </Link>
                  <Link href={APP_ROUTES.USER_INFORMATION_GUIDE} passHref>
                    <a className="page-footer__url-info__link">Chính sách bảo mật</a>
                  </Link>
                </div>
              </div>
            </div>
          </Fade>

          <Fade top>
            <div className="page-footer__general-info">
              <div className="page-footer__title">LIÊN HỆ</div>
              <div className="page-footer__general-info__store">
                <div className="page-footer__general-info__store__block__value value-name">
                  {DEFAULT_STORE_INFOS.FULL_NAME}
                </div>
                <a
                  href={DEFAULT_STORE_INFOS.URL.GG_MAP}
                  target="_blank"
                  className="page-footer__general-info__store__block"
                  rel="noreferrer">
                  <span className="page-footer__general-info__store__block__label">HCM: </span>
                  <span className="page-footer__general-info__store__block__value">
                    {DEFAULT_STORE_INFOS.ADDRESS.HCM}
                  </span>
                </a>
                {/* <a
                  href={DEFAULT_STORE_INFOS.URL.GG_MAP}
                  target="_blank"
                  className="page-footer__general-info__store__block"
                  rel="noreferrer">
                  <span className="page-footer__general-info__store__block__label">Cần Thơ: </span>
                  <span className="page-footer__general-info__store__block__value">
                    {DEFAULT_STORE_INFOS.ADDRESS.CONTHO}
                  </span>
                </a>
                <a
                  href={DEFAULT_STORE_INFOS.URL.GG_MAP}
                  target="_blank"
                  className="page-footer__general-info__store__block"
                  rel="noreferrer">
                  <span className="page-footer__general-info__store__block__label">
                    Bình Dương:
                  </span>
                  <span className="page-footer__general-info__store__block__value">
                    {DEFAULT_STORE_INFOS.ADDRESS.BINHDUONG}
                  </span>
                </a> */}
                <div className="page-footer__general-info__store__block">
                  <span className="page-footer__general-info__store__block__label">Email: </span>
                  <span className="page-footer__general-info__store__block__value">
                    {DEFAULT_STORE_INFOS.EMAIL}
                  </span>
                </div>
                <a
                  className="page-footer__general-info__store__block"
                  href={`tel:${DEFAULT_STORE_INFOS.PHONE_NUMBER_TO_CALL}`}>
                  <span className="page-footer__general-info__store__block__label">Hotline: </span>
                  <span className="page-footer__general-info__store__block__value">
                    {DEFAULT_STORE_INFOS.PHONE_NUMBER}
                  </span>
                </a>
                <div className="page-footer__general-info__store__block">
                  <span className="page-footer__general-info__store__block__label">
                    MST: 0316531254&ensp;•&ensp;
                  </span>
                  <span className="page-footer__general-info__store__block__value">
                    Sở Kế hoạch và Đầu tư TP Hồ Chí Minh cấp ngày 09/10/2020
                  </span>
                </div>
              </div>
            </div>
          </Fade>
        </div>
      </footer>
    </>
  );
};

export default PageFooterPresenter;
