import React, { useState } from "react";
import { Dialog, Grid } from "@material-ui/core";
import { ProductCard1 } from "@Components";
import { APP_ROUTES, DEFAULT_IMG_ALT } from "@Constants";
import { MOCK_DANCING_STONE_PRODUCTS } from "../../mock";
import Link from "next/link";
import { useBreakpoints } from "../../hooks";
import PageHeader from "@Widgets/PageHeader";
import PageFooter from "@Widgets/PageFooter";
import Image from "next/image";
import Banner from "../../public/images/dancing-stone/introduction/banner-background.png";
import BannerImg from "../../public/images/dancing-stone/introduction/banner-ring.gif";
import BannerText from "../../public/images/dancing-stone/introduction/banner-text.png";
import ImgCt1 from "../../public/images/dancing-stone/introduction/content1-1.png";
import Background1 from "../../public/images/dancing-stone/introduction/product-background.png";
import Background2 from "../../public/images/dancing-stone/introduction/content2-1.png";
import ImgCt2 from "../../public/images/dancing-stone/introduction/content2-2.png";
import ImgBtn from "../../public/images/dancing-stone/introduction/content2-2-btn.png";
import Banner2 from "../../public/images/dancing-stone/introduction/banner2.png";
import Img3a from "../../public/images/dancing-stone/introduction/content3-1.png";
import Img3b from "../../public/images/dancing-stone/introduction/content3-2.png";

const IntroductionStonePagePresenter: React.FC = () => {
  // const introImgs = "images/dancing-stone/introduction/banner-background.png";
  const [youtubeVisible, setYoutubeVisible] = useState<boolean>(false);
  const breakpoints = useBreakpoints();

  const toggleYoutube = () => {
    setYoutubeVisible(!youtubeVisible);
  };

  return (
    <>
      <PageHeader />
      <div className="dancing-stone-intro">
        <div className="dancing-stone-intro__bannerGroup">
          <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="fill" objectFit="cover" />

          <div className="dancing-stone-intro__bannerGroup__content">
            <div className="bannerRing">
              <Image src={BannerImg} alt={DEFAULT_IMG_ALT} />
            </div>
            <div className="bannerText">
              <Image src={BannerText} alt={DEFAULT_IMG_ALT} />
            </div>
          </div>
        </div>

        <Grid
          container
          justify="space-between"
          alignItems="center"
          className="dancing-stone-intro__content-1">
          <Grid item xs={12} lg={5} className="dancing-stone-intro__content-1__left">
            {!breakpoints.lg && (
              <div className="dancing-stone-intro__title">
                Sắc đẹp vĩnh cửu của
                <br />
                trang sức hiện đại
              </div>
            )}
            <div className="dancing-stone-intro__content-1__img">
              <Image src={ImgCt1} alt={DEFAULT_IMG_ALT} layout="fill" placeholder="blur" />
            </div>
          </Grid>
          <Grid item xs={12} lg={6}>
            {breakpoints.lg && (
              <div className="dancing-stone-intro__title">
                Sắc đẹp vĩnh cửu của
                <br />
                trang sức hiện đại
              </div>
            )}
            <div className="dancing-stone-intro__content-1__text">
              Lấy cảm hứng từ trái tim yêu thương, HanaGold cho ra đời một dòng trang sức độc đáo
              mang tên “Happy Stone” – có nghĩa là viên đá hạnh phúc. Đây chính là một món quà dành
              cho các quý cô thời thượng, hiện đại và bản lĩnh.
              <br /> <br />
              Điểm đặc biệt ở dòng trang sức này chính là sự chuyển động, lấp lánh không ngừng của
              viên đá tựa như những ngôi sao tỏa sáng trên bầu trời đem lại cho người đối diện một
              sức hút mãnh liệt và ấn tượng sâu sắc ngay từ cái nhìn đầu tiên. Đặc biệt, dòng trang
              sức này làm tôn lên vẻ đẹp thanh thoát, thể hiện được sự độc lập, tự tin tỏa sáng của
              người phụ nữ hiện đại.
            </div>
            {breakpoints.lg && (
              <Link href={APP_ROUTES.DANCING_STONE}>
                <a>
                  <button className="dancing-stone-intro__btn">Xem sản phẩm</button>
                </a>
              </Link>
            )}
          </Grid>
        </Grid>

        <div className="dancing-stone-intro__product-list">
          {breakpoints.lg && (
            <Image src={Background1} alt={DEFAULT_IMG_ALT} objectFit="cover" layout="fill" />
          )}
          <div className="dancing-stone-intro__product-list__content">
            <div className="dancing-stone-intro__title">bộ sưu tập viên đá hạnh phúc</div>
            <div className="dancing-stone-intro__product-list__listing">
              {MOCK_DANCING_STONE_PRODUCTS.slice(0, 8).map((pro) => {
                return (
                  <ProductCard1
                    key={pro.id}
                    id={pro.id}
                    title={pro.title}
                    img={pro.img}
                    price={pro.price}
                    discount={pro.discount}
                  />
                );
              })}
            </div>
            <div className="dancing-stone-intro__product-list__btn">
              <button className="dancing-stone-intro__btn">
                {breakpoints.lg ? "Xem tất cả sản phẩm" : "Xem thêm"}
              </button>
            </div>
          </div>
        </div>

        <div className="dancing-stone-intro__content-2">
          <div className="dancing-stone-intro__title">
            Bước đột phá về chế tác
            <br />
            trang sức từ HanaGold
          </div>
          <div className="dancing-stone-intro__content-2__wrapper">
            <Image src={Background2} alt={DEFAULT_IMG_ALT} objectFit="cover" layout="fill" />
            {!breakpoints.lg && (
              <div className="dancing-stone-intro__content-2__wrapper__item">
                <div className="dancing-stone-intro__content-2__wrapper__item__img">
                  <Image src={ImgCt2} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                  <div
                    className="dancing-stone-intro__content-2__wrapper__item__img-btn"
                    onClick={toggleYoutube}>
                    <Image src={ImgBtn} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                  </div>
                </div>
              </div>
            )}
            {breakpoints.lg && (
              <Grid container className="dancing-stone-intro__content-2__content">
                <Grid item xs={12} lg={5}>
                  <div>
                    Với sự khởi đầu là một doanh nghiệp trẻ trong ngành kim hoàn, HanaGold đã cố
                    gắng tìm tòi, học hỏi và tiên phong đi theo con đường khởi nghiệp đổi mới sáng
                    tạo, sáng tạo trong mô hình kinh doanh, sáng tạo trong sản phẩm và cách truyền
                    tải thông điệp đến khách hàng. <br />
                    <br />
                    Bắt nhịp với sự vận động không ngừng của nhịp sống hiện đại, HanaGold mang tinh
                    thần của những người phụ nữ hiện đại, năng động truyền tải vào dòng sản phẩm
                    trang sức “Happy Stone”, khi cơ thể càng chuyển động, viên đá sẽ càng nhảy theo
                    vũ điệu của cuộc sống. Khi cảm xúc bạn thăng hoa, viên đá cũng vui mừng hạnh
                    phúc mà nhảy nhót không ngừng. Happy Stone không chỉ là trang sức mà còn đồng
                    điệu về tâm hồn.
                  </div>
                  <button className="dancing-stone-intro__btn">Xem bộ sưu tập</button>
                </Grid>
                <Grid item lg={6}>
                  <div className="dancing-stone-intro__content-2__wrapper__item">
                    <div className="dancing-stone-intro__content-2__wrapper__item__img">
                      <Image src={ImgCt2} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                      <div
                        className="dancing-stone-intro__content-2__wrapper__item__img-btn"
                        onClick={toggleYoutube}>
                        <Image src={ImgBtn} alt={DEFAULT_IMG_ALT} objectFit="contain" />
                      </div>
                    </div>
                  </div>
                </Grid>
              </Grid>
            )}
          </div>
          {!breakpoints.lg && (
            <div className="dancing-stone-intro__content-2__content">
              Với sự khởi đầu là một doanh nghiệp trẻ trong ngành kim hoàn, HanaGold đã cố gắng tìm
              tòi, học hỏi và tiên phong đi theo con đường khởi nghiệp đổi mới sáng tạo, sáng tạo
              trong mô hình kinh doanh, sáng tạo trong sản phẩm và cách truyền tải thông điệp đến
              khách hàng. <br />
              <br />
              Bắt nhịp với sự vận động không ngừng của nhịp sống hiện đại, HanaGold mang tinh thần
              của những người phụ nữ hiện đại, năng động truyền tải vào dòng sản phẩm trang sức
              “Happy Stone”, khi cơ thể càng chuyển động, viên đá sẽ càng nhảy theo vũ điệu của cuộc
              sống. Khi cảm xúc bạn thăng hoa, viên đá cũng vui mừng hạnh phúc mà nhảy nhót không
              ngừng. Happy Stone không chỉ là trang sức mà còn đồng điệu về tâm hồn.
            </div>
          )}
        </div>

        <div className="dancing-stone-intro__banner2">
          <Image src={Banner2} alt={DEFAULT_IMG_ALT} objectFit="contain" />
        </div>

        <div className="dancing-stone-intro__content-3">
          <div className="dancing-stone-intro__title">
            TỰ TIN KHẲNG ĐỊNH PHONG CÁCH
            <br />
            VỚI TRANG SỨC HANAGOLD
          </div>
          <Grid container justify="center">
            <Grid item xs={12} lg={6} className="dancing-stone-intro__content-3__item">
              <div className="dancing-stone-intro__content-3__img">
                <Image src={Img3a} alt={DEFAULT_IMG_ALT} />
              </div>
            </Grid>
            <Grid item xs={12} lg={6} className="dancing-stone-intro__content-3__item">
              <div className="dancing-stone-intro__content-3__img">
                <Image src={Img3b} alt={DEFAULT_IMG_ALT} />
              </div>
            </Grid>
          </Grid>
          <Grid container justify="center">
            <Grid item xs={12} lg={6}>
              <div className="dancing-stone-intro__content-3__text">
                Với sự nỗ lực không ngừng nghỉ, HanaGold tự hào về đội ngũ nghiên cứu, chế tác, sản
                xuất dòng trang sức “Happy Stone” độc đáo này. Trải qua quy trình nghiêm nghặt và
                cực kì khắt khe, sự tiến bộ vượt trội của dòng sản phẩm này không chỉ góp phần cho
                sự phát triển của nền trang sức mà còn cho cả toàn ngành kim hoàn tại Việt Nam.
                Chúng tôi, đang từng ngày kiến tạo một nền móng vững chắc để tiến bước xa hơn và
                vươn ra thế giới.
              </div>
            </Grid>
            <Grid item xs={12} lg={6}>
              <div className="dancing-stone-intro__content-3__text">
                Với sự tỉ mỉ trong từng thiết kế, sự tinh tế đặt trọn cả chữ “tâm” của người thợ kim
                hoàn, mỗi sản phẩm trang sức “Happy Stone” mà HanaGold mang đến như một làn gió mới,
                làm đa dạng thêm bộ sưu tập trang sức của bạn. Hãy để HanaGold giúp bạn gia tăng
                thêm niềm vui cuộc sống, tô điểm cho màu sắc cá tính, trải nghiệm trở thành một quý
                cô sang chảnh và quý phái trong bất kì một thời khắc nào của cuộc sống.
              </div>
            </Grid>
          </Grid>
        </div>
      </div>

      <PageFooter withoutDivider />
      <Dialog className="dancing-stone-intro__dialog" open={youtubeVisible} onClose={toggleYoutube}>
        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/4E00d5wCm88"></iframe>
      </Dialog>
    </>
  );
};

export default IntroductionStonePagePresenter;
