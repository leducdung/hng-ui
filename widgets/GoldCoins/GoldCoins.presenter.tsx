import React, { useState } from "react";
import PageHeader from "@Widgets/PageHeader";
import PageFooter from "@Widgets/PageFooter";
import { DEFAULT_IMG_ALT } from "@Constants";
import { useBreakpoints } from "../../hooks";
import Image from "next/image";
import Banner from "../../public/images/banner/bannergold.png";
// import BannerMobile from "../../public/images/banner/bannergoldmb.png";
import Img1 from "../../public/images/banner/Img1gold.png";
import Img2 from "../../public/images/banner/Img2gold.png";
import Img1mb from "../../public/images/banner/Img1goldmb.png";
import Img2mb from "../../public/images/banner/Img2goldmb.png";
import ImgIcon1 from "../../public/images/service/Img1.png";
import ImgIcon2 from "../../public/images/service/Img2.png";
import ImgIcon3 from "../../public/images/service/Img3.png";
import Banner2 from "../../public/images/banner/bannergold2.png";
import Iphone from "../../public/images/banner/iphonegoldpage.png";
import Pulse from "react-reveal/Pulse";
import Fade from "react-reveal/Fade";
import Num1 from "../../public/images/guide/1.png";
import Num2 from "../../public/images/guide/2.png";
import Num3 from "../../public/images/guide/3.png";
import Num4 from "../../public/images/guide/4.png";
import Bg from "../../public/images/banner/bgtable.png";

import styles from "./GoldCoins.module.scss";
import { Dialog, Grid } from "@material-ui/core";
import { Registration } from "@Components";

const GoldCoinsPresenter: React.FC = () => {
  const breakpoints = useBreakpoints();
  const [visibleViewerModal, setVisibleViewerModal] = useState<boolean>(false);

  const toggleViewerModal = () => {
    setVisibleViewerModal(!visibleViewerModal);
  };

  return (
    <>
      <PageHeader />
      <div className={styles.goldCoins}>
        <div className={styles.cover}>
          <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="responsive" width={1440} height={644} />
        </div>
        <div className={styles.content}>
          <div className={styles.title}>
            ĐỒNG VÀNG
            <br />
            KIM KHỔNG TƯỚC
          </div>
          <div className={styles.des}>
            Đồng vàng kim khổng tước với họa tiết logo hình{!breakpoints.sm ? <br /> : " "}chú chim
            khổng tước tượng trưng cho sự
            {!breakpoints.sm ? <br /> : " "}
            {breakpoints.lg && <br />}
            Hạnh phúc - Cao quý - May mắn - Bình an{!breakpoints.sm ? <br /> : " "}cho chủ nhân sở
            hữu.
            {!breakpoints.sm ? <br /> : " "}
            Đồng vàng hình tròn biểu trưng{breakpoints.lg && <br />} cho sự tròn đầy
            {!breakpoints.sm ? <br /> : " "}trong cuộc sống, và vàng 9999 luôn là sự lựa chọn
            {!breakpoints.sm ? <br /> : " "}an tâm nhất{breakpoints.lg && <br />} vào giá trị bền
            vững cùng với{!breakpoints.sm ? <br /> : " "}HanaGold.
          </div>
          <Pulse delay={1500} forever>
            <button className={styles.btn} onClick={toggleViewerModal}>
              Đăng kí tư vấn
            </button>
          </Pulse>
          <div className={styles.bannerGroup}>
            {!breakpoints.sm && (
              <Image
                src={Banner2}
                alt={DEFAULT_IMG_ALT}
                width={375}
                height={602}
                objectFit="cover"
              />
            )}
            <div className={styles.imgGroup}>
              <div className={styles.imgLeft}>
                <Image
                  src={breakpoints.lg ? Img1 : Img1mb}
                  alt={DEFAULT_IMG_ALT}
                  width={breakpoints.lg ? 346 : 164}
                  height={breakpoints.lg ? 522 : 248}
                  objectFit="contain"
                />
              </div>

              <div className={styles.imgRight}>
                <Image
                  src={breakpoints.lg ? Img2 : Img2mb}
                  alt={DEFAULT_IMG_ALT}
                  width={breakpoints.lg ? 346 : 164}
                  height={breakpoints.lg ? 522 : 248}
                  objectFit="contain"
                />
              </div>
            </div>
          </div>
          <div className={styles.bgTable}>
            {!breakpoints.sm && (
              <Image src={Bg} alt={DEFAULT_IMG_ALT} width={375} height={636} objectFit="cover" />
            )}
            <Grid container className={styles.table} justify="space-between">
              <Grid item xs={3} lg={2}>
                <Image
                  src={ImgIcon1}
                  alt={DEFAULT_IMG_ALT}
                  width={152}
                  height={120}
                  objectFit="contain"
                />
              </Grid>
              <Grid item xs={9} lg={10}>
                <div className={styles.label}>Tích lũy vàng với thu nhập thấp</div>Tại HanaGold, bất
                kỳ ai cũng có thể mua vàng với thu nhập thấp, mua tích lũy theo zem, ly, phân vàng,
                nghĩa là chỉ từ 100 ngàn đồng, bạn cũng có thể mua vàng để tiết kiệm cho các mục
                tiêu tương lai
              </Grid>
              <Grid item xs={3} lg={2}>
                <Image
                  src={ImgIcon2}
                  alt={DEFAULT_IMG_ALT}
                  width={152}
                  height={120}
                  objectFit="contain"
                />
              </Grid>
              <Grid item xs={9} lg={10}>
                <div className={styles.label}>Giá trị đầu tư vàng vật chất</div>Đồng vàng mang
                thương hiệu HanaGold với các định lượng 1 chỉ, 2 chỉ, 5 chỉ là sản phẩm chủ lực khi
                khách hàng có nhu cầu chuyển đổi từ vàng tích lũy sang vàng vật chất tại showroom
                HanaGold và các showroom đại lý trên toàn quốc
              </Grid>
              <Grid item xs={3} lg={2}>
                <Image
                  src={ImgIcon3}
                  alt={DEFAULT_IMG_ALT}
                  width={152}
                  height={120}
                  objectFit="contain"
                />
              </Grid>
              <Grid item xs={9} lg={10}>
                <div className={styles.label}>Ứng dụng công nghệ</div>Trên mỗi đồng vàng kim khổng
                tước ép vỉ đều có tích hợp chip NFC và mã QR code giúp dễ dàng mua bán, trao đổi,
                đồng thời cũng giúp dễ kiểm soát, tránh giả mạo sản phẩm HanaGold
              </Grid>
            </Grid>
          </div>
        </div>
        <div className={styles.bg}>
          <Image src={Banner2} alt={DEFAULT_IMG_ALT} layout="fill" />
          <div className={styles.contentBuy}>
            <Fade ssrReveal bottom>
              <div className={styles.iPhone}>
                <Image src={Iphone} alt={DEFAULT_IMG_ALT} layout="fill" objectFit="cover" />
              </div>
            </Fade>
            <div className={styles.right}>
              <div className={styles.title}>mua sản phẩm trang sức, mỹ nghệ tích lũy online</div>
              <div className={styles.item}>
                <Image
                  src={Num1}
                  alt={DEFAULT_IMG_ALT}
                  width={40}
                  height={40}
                  objectFit="contain"
                />
                <span>ĐĂNG KÝ THÀNH VIÊN HANAGOLD</span>
              </div>
              <div className={styles.item}>
                <Image
                  src={Num2}
                  alt={DEFAULT_IMG_ALT}
                  width={40}
                  height={40}
                  objectFit="contain"
                />
                <span>NẠP TIỀN</span>
              </div>
              <div className={styles.item}>
                <Image
                  src={Num3}
                  alt={DEFAULT_IMG_ALT}
                  width={40}
                  height={40}
                  objectFit="contain"
                />
                <span>Mua tích lũy mỗi ngày theo zem, ly, phân, chỉ vàng</span>
              </div>
              <div className={styles.item}>
                <Image
                  src={Num4}
                  alt={DEFAULT_IMG_ALT}
                  width={40}
                  height={40}
                  objectFit="contain"
                />
                <span>Nhận trực tiếp sản phẩm trang sức, mỹ nghệ tại tiệm vàng HanaGold</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <PageFooter withoutDivider />

      <Dialog
        open={visibleViewerModal}
        onClose={() => {
          toggleViewerModal();
        }}
        fullScreen={!breakpoints.lg ? true : false}
        className="registration__dialog">
        <Registration
          onClose={() => {
            toggleViewerModal();
          }}
        />
      </Dialog>
    </>
  );
};

export default GoldCoinsPresenter;
