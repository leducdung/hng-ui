export const APP_ROUTES = {
  HOME: "/",
  // GOLD: "/gold-4",
  ABOUT_US: "/#about-us",
  PRODUCT: "/tuong-phong-thuy",
  REDIRECT_PRODUCT: "/product/tuong-phong-thuy",
  MAP: "/#map",
  SERVICE: "/service",
  DANCING_STONE: "/trang-suc-happy-stone",
  PROJECT: "/project",
  NEWS: "/news",
  CONTACT: "/contact",
  ORDER: "/app/checkout",
  INTRODUCTION_STONE: "/introduction-stone",
  RESULT_ORDER: "/order/result",
  CUSTOMER_PROFILE: "/customer/profile",
  CUSTOMER_ORDER: "/customer/order",
  CUSTOMER_SHIPPING_ADDRESS: "/customer/shipping-address",
  CUSTOMER_PASSWORD: "/customer/password",
  GOLD_COINS: "/dong-vang-kim-khong-tuoc",
  MARKETPLACE: "/marketplace",
  SIGNIN: "/customer/signin",
  SIGNUP: "/customer/signup",
  FORGETPASSWORD: "/customer/forget-password",
  RESETPASSWORD: "/customer/reset-password",
  CONFIRM_SHIPPING: "/app/checkout/confirm-shipping",
  CONFIRM_ORDER: "/app/checkout/confirm-order",
  SHOPPING_GUIDE: "/guide/shopping",
  PAYMENT_GUIDE: "/guide/payment",
  POLICY_GUIDE: "/guide/policy",
  WARRANTY_GUIDE: "/guide/warranty",
  JEWELRY_GUIDE: "/guide/jewelry",
  USER_INFORMATION_GUIDE: "/guide/user-infomation",
  SIZE_GUIDE: "/guide/size",
  SAN_PHAM: "/san-pham",
  PDF: "/pdf",
} as const;

export const ADMIN_BASE = "/admin";
export const ADMIN_ROUTES = {
  ORDER: ADMIN_BASE + "/orders",
  LOGIN: ADMIN_BASE + "/login",
  BILLING: ADMIN_BASE + "/billing",
  PRODUCT: ADMIN_BASE + "/products",
  CUSTOMER: ADMIN_BASE + "/customer",
  GOLD_PRICE: ADMIN_BASE + "/gold-price",
  PARTNER: ADMIN_BASE + "/partner",
  PAYMENT: ADMIN_BASE + "/payment",
  LOCATION: ADMIN_BASE + "/location",
  SPECIFICATIONS: ADMIN_BASE + "/products/specifications",
  TRANSACTION: ADMIN_BASE + "/transaction",
  GOLD_WITHDRAWAL: ADMIN_BASE + "/transaction/gold-withdrawals",
} as const;

export const AppMenus: {
  url: string;
  urlType: "select" | "link";
  title: string;
}[] = [
  {
    url: APP_ROUTES.SERVICE,
    urlType: "link",
    title: "Dịch vụ / Tính năng",
  },
  {
    url: APP_ROUTES.REDIRECT_PRODUCT,
    urlType: "select",
    title: "Sản phẩm",
  },
  {
    url: APP_ROUTES.DANCING_STONE,
    urlType: "link",
    title: "Trang sức Happy Stone",
  },
  // {
  //   url: APP_ROUTES.GOLD_COINS,
  //   urlType: "link",
  //   title: "Đồng vàng Kim Khổng Tước",
  // },
  {
    url: APP_ROUTES.MARKETPLACE,
    urlType: "link",
    title: "Đại lý",
  },
  {
    url: APP_ROUTES.ABOUT_US,
    urlType: "link",
    title: "Về HanaGold",
  },
  {
    url: APP_ROUTES.NEWS,
    urlType: "link",
    title: "Tin tức",
  },
  // {
  //   url: APP_ROUTES.ADVISORY,
  //   urlType: "link",
  //   title: "Cố vấn",
  // },
];
