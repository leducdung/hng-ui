const NUMBER_ONLY = /^-?\d*(\.\d*)?$/;
const PHONE_NUMBER = /^\d+$/;
const EMAIL =
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const REGEX = {
  NUMBER_ONLY,
  PHONE_NUMBER,
  EMAIL,
};
