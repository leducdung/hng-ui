import Partner1 from "../public/images/partner/live-trade.png";
import Partner2 from "../public/images/partner/cystack.png";
import Partner3 from "../public/images/partner/Citipass.png";
import Partner4 from "../public/images/partner/Blockup.png";
import Partner5 from "../public/images/partner/trust-pay.png";
import Partner6 from "../public/images/partner/trust-card.png";
import Partner7 from "../public/images/partner/HVA.png";
import Partner8 from "../public/images/partner/Olacity.png";
import Partner9 from "../public/images/partner/DAH.png";
import Partner10 from "../public/images/partner/Real-estate.png";
import Partner11 from "../public/images/partner/kim-nam-tin.png";
import Partner12 from "../public/images/partner/kim-tin-phat.png";
import Partner1act from "../public/images/partner/live-trade-act.png";
import Partner2act from "../public/images/partner/cystack-act.png";
import Partner3act from "../public/images/partner/Citipass-act.png";
import Partner4act from "../public/images/partner/Blockup-act.png";
import Partner5act from "../public/images/partner/trust-pay-act.png";
import Partner6act from "../public/images/partner/trust-card-act.png";
import Partner7act from "../public/images/partner/HVA-act.png";
import Partner8act from "../public/images/partner/Olacity-act.png";
import Partner9act from "../public/images/partner/DAH-act.png";
import Partner10act from "../public/images/partner/Real-estate-act.png";
import Partner11act from "../public/images/partner/kim-nam-tin-act.png";
import Partner12act from "../public/images/partner/kim-tin-phat-act.png";

export const ImgPartner: { id: string; img: StaticImageData; img_act: StaticImageData }[] = [
  {
    id: "1",
    img: Partner1,
    img_act: Partner1act,
  },
  { id: "2", img: Partner2, img_act: Partner2act },
  { id: "3", img: Partner3, img_act: Partner3act },
  { id: "4", img: Partner4, img_act: Partner4act },
  { id: "5", img: Partner5, img_act: Partner5act },
  { id: "6", img: Partner6, img_act: Partner6act },
  { id: "7", img: Partner7, img_act: Partner7act },
  { id: "8", img: Partner8, img_act: Partner8act },
  { id: "9", img: Partner9, img_act: Partner9act },
  {
    id: "10",
    img: Partner10,
    img_act: Partner10act,
  },
  {
    id: "11",
    img: Partner11,
    img_act: Partner11act,
  },
  {
    id: "12",
    img: Partner12,
    img_act: Partner12act,
  },
];
