export const LOCATIONS = [
  {
    id: 79,
    name: "Thành phố Hồ Chí Minh",
    type: "Thành phố Trung ương",
    districts: [
      {
        id: 760,
        name: "Quận 1",
        type: "Quận",
        provinceId: 79,
        wards: [
          {
            id: 26734,
            name: "Phường Tân Định",
            type: "Phường",
            districtId: 760,
          },
          {
            id: 26737,
            name: "Phường Đa Kao",
            type: "Phường",
            districtId: 760,
          },
          {
            id: 26740,
            name: "Phường Bến Nghé",
            type: "Phường",
            districtId: 760,
          },
          {
            id: 26743,
            name: "Phường Bến Thành",
            type: "Phường",
            districtId: 760,
          },
          {
            id: 26746,
            name: "Phường Nguyễn Thái Bình",
            type: "Phường",
            districtId: 760,
          },
          {
            id: 26749,
            name: "Phường Phạm Ngũ Lão",
            type: "Phường",
            districtId: 760,
          },
          {
            id: 26752,
            name: "Phường Cầu Ông Lãnh",
            type: "Phường",
            districtId: 760,
          },
          {
            id: 26755,
            name: "Phường Cô Giang",
            type: "Phường",
            districtId: 760,
          },
          {
            id: 26758,
            name: "Phường Nguyễn Cư Trinh",
            type: "Phường",
            districtId: 760,
          },
          {
            id: 26761,
            name: "Phường Cầu Kho",
            type: "Phường",
            districtId: 760,
          },
        ],
      },
    ],
  },
];
