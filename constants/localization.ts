import { AppLang } from "@Models";

export const MAPPING_LANGUAGES = {
  [AppLang.VN]: "Vn",
  [AppLang.EN]: "En",
  [AppLang.AS]: "As",
};
