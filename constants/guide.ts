/* eslint-disable */
import Num1 from "../public/images/guide/1.png";
import Num2 from "../public/images/guide/2.png";
import Num3 from "../public/images/guide/3.png";
import Num4 from "../public/images/guide/4.png";
import Num5 from "../public/images/guide/5.png";
import Num6 from "../public/images/guide/6.png";
import Num7 from "../public/images/guide/7.png";
import Num8 from "../public/images/guide/8.png";
import Icon1 from "../public/images/guide/sp1.png";
// import Icon2 from "../public/images/guide/sp2.png";
import Icon3 from "../public/images/guide/sp3.png";

export const ShoppingGuide = [
  {
    img: Icon1,
    title: "Cách 1: Đối với các sản phẩm trang sức, mỹ nghệ",
    description:
      "Quý khách hàng có thể đặt hàng trực tiếp tại website www.hanagold.vn hoặc trên các trang thương mại điện tử tên shop “HanaGold Official” tại Tiki, Lazada, Shopee.",
  },
  // {
  //   img: Icon2,
  //   title: "Cách 2: Đối với sản phẩm Đồng vàng HanaGold tích lũy",
  //   description:
  //     "Quý khách hàng có thể tải App HanaGold trên CHPlay và App store để tiến hành mua vàng tích lũy. Ngoài ra, quý khách có thể chọn mua tích lũy tại VNDC wallet pro App (kênh đối tác HanaGold).",
  // },
  {
    img: Icon3,
    title: "Cách 3: Liên hệ trực tiếp",
    description:
      "Để nhanh chóng hơn, quý khách hàng có thể gọi điện thoại trực tiếp đến hotline 0889028009 để đặt hàng hoặc gửi email contact@hanagold.vn.",
  },
];

export const Warranty = [
  {
    cate: "Các sản phẩm trang sức Vàng",
    guarantee: [
      {
        gua: "• Bảo hành 6 tháng lỗi kỹ thuật, nước xi.",
      },
      {
        gua: "• Bảo hành đánh bóng, siêu âm miễn phí trọn đời.",
      },
      {
        gua: "• Miễn phí thay đá CZ và đá tổng hợp trong suốt thời gian bảo hành.",
      },
      {
        gua: "• Đối với Sản phẩm bị oxy hóa, xuống màu, sẽ được siêu âm làm sạch bằng máy chuyên dụng (siêu âm, không xi) miễn phí trọn đời tại cửa hàng.",
      },
    ],
    note: [
      {
        not: "* Không áp dụng bảo hành cho các trường hợp sau:",
      },
      { not: "• Dây chuyền, lắc chế tác bị đứt gãy; bị biến dạng hoặc hư hỏng nặng." },
      { not: "• Khách hàng cung cấp thông tin truy lục hóa đơn không chính xác." },
    ],
  },
  {
    cate: "Các sản phẩm trang sức Bạc",
    guarantee: [
      {
        gua: "• Các sản phẩm Bạc sẽ được bảo hành miễn phí 3 tháng đầu về mặt kỹ thuật, nước xi.",
      },
      {
        gua: "• Từ tháng 4 đến hết tháng thứ 12, sản phẩm sẽ được bảo hành có tính phí.",
      },
      {
        gua: "• Trên 12 tháng Công ty không nhận bảo hành.",
      },
      {
        gua: "• Đối với sản phẩm bị oxy hóa, xuống màu, sẽ được siêu âm làm sạch bằng máy chuyên dụng (siêu âm, không xi) miễn phí trọn đời tại cửa hàng.",
      },
      {
        gua: "• Thay miễn phí đá CZ, đá tổng hợp trong suốt thời gian sản phẩm được bảo hành.",
      },
      {
        gua: "• Riêng đối với dòng sản phẩm charm chỉ nhận bảo hành sản phẩm trong các trường hợp sau:",
      },
      {
        gua: "- Sản phẩm là mẫu bạc hoàn toàn (không gắn đá, nhựa, thủy tinh).",
      },
      {
        gua: "- Mẫu bạc có phủ colorit.",
      },
    ],
    note: [
      {
        not: "* Không áp dụng bảo hành cho các trường hợp sau:",
      },
      { not: "• Dây chuyền, lắc chế tác bị đứt gãy; sản phẩm bị biến dạng hoặc hư hỏng nặng." },
      { not: "• Khách hàng cung cấp thông tin truy lục hóa đơn không chính xác." },
      { not: "* Đối với dòng sản phẩm charm không nhận bảo hành trong trường hợp sau:" },
      { not: "- Sản phẩm có gắn đá, nhựa, thủy tinh." },
      { not: "- Sản phẩm bị biến dạng, đứt gãy, rớt đá." },
      { not: "- Các họa tiết nhỏ, họa tiết phụ bị mất." },
    ],
  },
  {
    cate: "Trang sức Happy Stone",
    guarantee: [
      {
        gua: "• Miễn phí trọn đời làm sạch, đánh bóng.",
      },
      {
        gua: "• Miễn phí lần đầu tiên xi lại sản phẩm.",
      },
      {
        gua: "• Kiểm tra ổ đá miễn phí dưới 3 tháng sử dụng.",
      },
    ],
    note: [
      {
        not: "* Không áp dụng bảo hành cho các trường hợp sau:",
      },
      { not: "• Ổ đá bị bể, hư hỏng nặng do lực tác động bên ngoài." },
      { not: "• Đá không nhảy do cố tình va đập mạnh." },
    ],
  },
  {
    cate: "Vật phẩm phong thủy mạ vàng",
    guarantee: [
      {
        gua: "Sửa chữa hoặc đổi lại miễn phí nếu do lỗi sản xuất.",
      },
    ],
    note: [
      {
        not: "* Không áp dụng bảo hành cho các trường hợp sau:",
      },
      { not: "Sản phẩm hư hỏng do các yếu tố bên ngoài (không phải lỗi sản xuất)." },
    ],
  },
];

export const Exchange48H = [
  { text: "• Sản phẩm phải còn nguyên vẹn." },
  {
    text: "• Đối với vỏ, sau khi đã vô hột thì Khách hàng sẽ chịu chi phí trồng chấu (theo quy định bảo hành HanaGold).",
  },
  {
    text: "• Khách Hàng mua món mới có giá trị cao hoặc bằng món cũ: HanaGold mua lại 100% giá trị ban đầu. Khách Hàng mua món mới có giá trị thấp hơn món cũ: HanaGold mua lại 90% giá trị ban đầu.",
  },
  {
    text: "• Đối với nữ trang tính công: Giá trị trả lại 70% trị giá tiền công (Khách Hàng mất 30% tiền công), giá trị vàng mua lại theo trọng lượng và giá vàng bán ra niêm yết tại thời điểm.",
  },
  {
    text: "• Không hỗ trợ trả hàng, khách hàng cần mua lại sản phẩm theo quy định của từng nhóm hàng.",
  },
  { text: "• Khách hàng chỉ được đổi sản phẩm 01 lần duy nhất." },
  {
    text: "• Trường hợp đổi hàng trong vòng 02 ngày (48h) áp dụng cho sản phẩm HanaGold tại Showroom HanaGold được ủy quyền.",
  },
  {
    text: "• Giá trị đổi: Sản phẩm mới có giá trị lớn hơn sản phẩm cũ (Khách Hàng phải trả thêm cho sản phẩm mới và cấn trừ giá trị của sản phẩm cũ) trong vòng 02 ngày cho khách hàng khi thực hiện.",
  },
  {
    text: "• Địa điểm: Giao dịch ngay tại cửa hàng mà khách đã mua. Không áp dụng và không chấp nhận cho đổi khi khách mua và đổi tại hai showroom khác nhau.",
  },
];

export const JewelryPreservation = [
  {
    img: Num1,
    name: "Trang sức có gắn ngọc trai",
    text: "Do viên ngọc trai được bọc một lớp xà cừ là chất hữu cơ nên khi sử dụng bạn nên tránh không để tiếp xúc với các loại nước hoa, sữa dưỡng da, nước lau sơn móng tay, các loại keo xịt tóc, chanh, giấm, … và các loại chất tẩy hoặc hóa chất dễ bay hơi khác. Vì nếu tiếp xúc với các chất này viên ngọc trai sẽ bị bào mòn dần. Tốt nhất trong thời gian sử dụng, bạn nên ngâm rửa bằng nước ấm (không nên dùng bàn chải) sau đó lau nhẹ bằng vải mềm cotton thì viên ngọc trai sẽ bóng đẹp tự nhiên.",
  },
  {
    img: Num2,
    name: "Trang sức có gắn đá quý / bán quý",
    des: [
      {
        des: "Trang sức có gắn các loại đá quý/ bán quý như: Ruby, Sapphire, Spinell, Tourmaline, Garnet, Peridot, Amethyst, Citrine, Topaz...",
      },
      {
        des: "Khi sử dụng không nên để trang sức đá quý, đá bán quý va chạm vào các vật cứng khác để tránh bị xước hay có tì vết sẽ khiến viên đá không còn đẹp và giảm đi giá trị.",
      },
      {
        des: "Nên làm sạch các sản phẩm đá quý, đá bán quý bằng bàn chải mềm và nước xà phòng ấm. Để tăng độ sáng bóng cho các loại đá quý và bán quý, bạn có thể lau bằng dầu không màu hoặc sáp ong.",
      },
    ],
  },
  {
    img: Num3,
    name: "Trang sức có gắn đá Emerald, Opal",
    text: "Bạn nên bảo quản sản phẩm như các loại đá quý, đá bán quý trên. Tuy nhiên, do đặc tính đặc biệt của các loại đá này là mềm và cần phải lấy ra khỏi ổ hột trong quá trình làm sạch, vì thế khi có nhu cầu làm mới trang sức, quý khách nên đến các cửa hàng của HanaGold để món trang sức của mình được chăm sóc hoàn hảo nhất.",
  },
  {
    img: Num4,
    name: "Trang sức kim cương",
    des: [
      {
        des: "Kim cương không thể bị mài mòn do các tác động bình thường vì nó có độ cứng rất cao nhưng cần phải giữ sạch để có được độ sáng vĩnh viễn. Bạn có thể ngâm trong nước xà phòng ấm khoảng 30 phút sau đó rửa lại bằng nước sạch rồi dùng vải mềm lau khô.",
      },
      {
        des: "Vì kim cương có độ cứng rất cao nên nữ trang kim cương nên được bảo quản riêng biệt khỏi các đồ trang sức khác để tránh trầy xước.",
      },
    ],
  },
  {
    img: Num5,
    name: "Trang sức vàng trắng",
    des: [
      {
        des: "Vàng trắng có màu trắng ngà nên khi được gia công thành trang sức thường phải phủ lên bề mặt một chất có tên là Rhodium để tạo nên màu trắng sáng giống như Platinum.",
      },
      {
        des: "Vàng trắng khi sử dụng dưới tác động của nhiều yếu tố như thời gian, mồ hôi và cả tính chất của vàng trắng nên có thể bị ngả màu, vì vậy để trang sức vàng trắng luôn sáng đẹp cứ khoảng từ 4 đến 6 tháng, bạn nên mang sản phẩm đến hệ thống HanaGold để làm mới.",
      },
    ],
  },
  {
    img: Num6,
    name: "Trang sức vàng",
    des: [
      {
        des: "Khi sử dụng nữ trang bằng vàng, bạn cần lưu ý:",
      },
      {
        des: "Không nên để vàng tiếp xúc với thủy ngân, thuốc uốn tóc, nhuộm tóc và một số loại mỹ phẩm vì các chất này sẽ làm cho vàng bị ngả màu.",
      },
      {
        des: "Để vàng luôn sáng đẹp, mỗi tuần nên dùng bàn chải đánh răng loại mềm nhúng vào xà phòng hoặc kem đánh răng chải rửa trong và ngoài món nữ trang để tẩy đi bụi bẩn và mồ hôi bám vào. Khi nữ trang bị ướt thì không nên để nữ trang tự khô mà phải dùng vải thun cotton hoặc giấy mềm lau khô.",
      },
      {
        des: "Qua thời gian dài sử dụng, các món nữ trang có thể giảm đi độ sáng bóng, màu vàng bị nhạt, mờ. Để trở về trạng thái sáng bóng ban đầu chỉ có cách duy nhất là làm sạch, xi mới, đánh bóng bằng những dụng cụ chuyên ngành nữ trang. Tất cả các cửa hàng HanaGold đều có dịch vụ đánh bóng, siêu âm làm sạch miễn phí vĩnh viễn cho các sản phẩm HanaGold, riêng với dịch vụ xi mới, sản phẩm nhẫn cưới được miễn phí, còn các dòng sản phẩm khác khách hàng sẽ trả 1 mức phí hợp lý tùy theo chủng loại sản phẩm.",
      },
    ],
  },
  {
    img: Num7,
    name: "Trang sức bạc",
    des: [
      {
        des: "Trang sức bạc đeo lâu ngày có thể bị mờ đi. Bạc bị oxy hóa khi tiếp xúc với không khí, ánh sáng mạnh  và các hóa chất như keo xịt tóc, nước hoa, thuốc tẩy, nước biển,…",
      },
      {
        des: "Để món trang sức bạc luôn sáng đẹp, bạn nên ngâm và rửa nhẹ nhàng trong nước tẩy rửa nhẹ (nước tẩy rửa chén đĩa, ly tách) pha với nước sạch. Sau đó, rửa lại bằng nước sạch và dùng khăn mềm lau khô thật kỹ. Tuyệt đối không sử dụng nước tẩy rửa có tính chất tẩy mạnh.",
      },
      {
        des: "Để đảm bảo và duy trì vẻ đẹp của trang sức bạc, nên cất giữ và khi đeo tránh va chạm hoặc sử dụng bất kì dụng cụ nào tác động trực tiếp đến món trang sức.",
      },
    ],
  },
];

export const UserInfo = [
  {
    img: Num1,
    name: "MỤC ĐÍCH THU THẬP THÔNG TIN CÁ NHÂN CỦA KHÁCH HÀNG",
    text: "Cung cấp dịch vụ cho Khách hàng và quản lý, sử dụng thông tin cá nhân của Người Tiêu Dùng nhằm mục đích quản lý cơ sở dữ liệu về Người Tiêu Dùng và kịp thời xử lý các tình huống phát sinh (nếu có).",
  },
  {
    img: Num2,
    name: "PHẠM VI SỬ DỤNG THÔNG TIN CÁ NHÂN",
    text: "Website https://hanagold.vn/  sử dụng thông tin của Người Tiêu Dùng cung cấp để:",
    para: [
      { des: "• Cung cấp các dịch vụ đến Người Tiêu Dùng;" },
      {
        des: "• Gửi các thông báo về các hoạt động trao đổi thông tin giữa Người Tiêu Dùng và HanaGold;",
      },
      {
        des: "• Ngăn ngừa các hoạt động phá hủy, chiếm đoạt tài khoản người dùng của Người Tiêu Dùng hoặc các hoạt động giả mạo Người Tiêu Dùng;",
      },
      { des: "• Liên lạc và giải quyết khiếu nại với Người Tiêu Dùng;" },
      {
        des: "• Xác nhận và trao đổi thông tin về giao dịch của Người Tiêu Dùng tại HanaGold;",
      },
      { des: "• Trong trường hợp có yêu cầu của cơ quan quản lý nhà nước có thẩm quyền." },
    ],
  },
  {
    img: Num3,
    name: "THỜI GIAN LƯU TRỮ THÔNG TIN CÁ NHÂN",
    text: "Không có thời hạn ngoại trừ trường hợp Người Tiêu Dùng gửi có yêu cầu hủy bỏ tới cho Ban quản trị hoặc Công ty giải thể hoặc bị phá sản.",
  },
  {
    img: Num4,
    name: "NHỮNG NGƯỜI HOẶC TỔ CHỨC CÓ THỂ ĐƯỢC TIẾP CẬN VỚI THÔNG TIN CÁ NHÂN CỦA NGƯỜI TIÊU DÙNG",
    text: "Người Tiêu Dùng đồng ý rằng, trong trường hợp cần thiết, các cơ quan/ tổ chức/cá nhân sau có quyền được tiếp cận và thu thập các thông tin cá nhân của mình, bao gồm:",
    para: [
      { des: "• Ban quản trị." },
      { des: "• Bên thứ ba có dịch vụ tích hợp với Website https://hanagold.vn/" },
      { des: "• Công ty tổ chức sự kiện và nhà tài trợ" },
      {
        des: "• Cơ quan nhà nước có thẩm quyền trong trường hợp có yêu cầu theo quy định tại quy chế hoạt động",
      },
      { des: "• Công ty nghiên cứu thị trường" },
      { des: "• Cố vấn tài chính, pháp lý và Công ty kiểm toán" },
      { des: "• Bên khiếu nại chứng minh được hành vi vi phạm của Người Tiêu Dùng" },
      { des: "• Theo yêu cầu của cơ quan nhà nước có thẩm quyền" },
    ],
  },
  {
    img: Num5,
    name: "ĐỊA CHỈ CỦA ĐƠN VỊ THU THẬP VÀ QUẢN LÝ THÔNG TIN",
    text: "Công ty cổ phần Vàng bạc đá quý HanaGold",
    para: [
      { des: "• Địa chỉ: 136 Lê Tuấn Mậu, Phường 13, Quận 6, TP. Hồ Chí Minh" },
      { des: "• Điện thoại: 0889 028 009" },
      { des: "• Email: contact@hanagold.vn" },
    ],
  },
  {
    img: Num6,
    name: "PHƯƠNG TIỆN VÀ CÔNG CỤ ĐỂ NGƯỜI TIÊU DÙNG TIẾP CẬN VÀ CHỈNH SỬA DỮ LIỆU THÔNG TIN CÁ NHÂN CỦA MÌNH",
    texts: [
      {
        des: "Người Tiêu Dùng có quyền tự kiểm tra, cập nhật, điều chỉnh hoặc hủy bỏ thông tin cá nhân của mình bằng cách đăng nhập vào Website https://hanagold.vn/  và chỉnh sửa thông tin cá nhân hoặc yêu cầu Ban quản trị thực hiện việc này.",
      },
      {
        des: "Người Tiêu Dùng có quyền gửi khiếu nại về việc lộ thông tin cá nhân của mình cho bên thứ 3 đến Ban quản trị. Khi tiếp nhận những phản hồi này, HanaGold sẽ xác nhận lại thông tin, phải có trách nhiệm trả lời lý do và hướng dẫn Người Tiêu Dùng khôi phục và bảo mật lại thông tin.",
      },
      {
        des: "Các hình thức tiếp nhận thông tin khiếu nại của Người Tiêu Dùng:",
      },
    ],

    para: [
      {
        des: "i) Qua email: contact@hanagold.vn",
      },
      {
        des: "ii) Qua điện thoại: 0889 028 009",
      },
    ],
  },
  {
    img: Num7,
    name: "CAM KẾT BẢO MẬT THÔNG TIN CÁ NHÂN CỦA NGƯỜI TIÊU DÙNG",
    texts: [
      {
        des: "Thông tin cá nhân của Người Tiêu Dùng trên Website https://hanagold.vn/  được Ban quản trị cam kết bảo mật tuyệt đối theo chính sách bảo mật thông tin cá nhân được đăng tải trên Website https://hanagold.vn/ . Việc thu thập và sử dụng thông tin của mỗi Người Tiêu Dùng chỉ được thực hiện khi có sự đồng ý của Người Tiêu Dùng trừ những trường hợp pháp luật có quy định khác và quy định này.",
      },
      {
        des: "Không sử dụng, không chuyển giao, cung cấp hoặc tiết lộ cho bên thứ 3 về thông tin cá nhân của Người Tiêu Dùng khi không có sự đồng ý của Người Tiêu Dùng ngoại trừ các trường hợp được quy định tại quy định này hoặc quy định của pháp luật.",
      },
      {
        des: "Trong trường hợp máy chủ lưu trữ thông tin bị hacker tấn công dẫn đến mất mát dữ liệu cá nhân của Người Tiêu Dùng, Ban quản trị có trách nhiệm thông báo và làm việc với cơ quan chức năng điều tra và xử lý kịp thời, đồng thời thông báo cho Người Tiêu Dùng được biết về vụ việc.",
      },
      {
        des: "Bảo mật tuyệt đối mọi thông tin giao dịch trực tuyến của Người Tiêu Dùng bao gồm thông tin hóa đơn kế toán chứng từ số hóa tại khu vực dữ liệu trung tâm an toàn cấp 1 của HanaGold.",
      },
    ],
  },
  {
    img: Num8,
    name: "CƠ CHẾ TIẾP NHẬN VÀ GIẢI QUYẾT KHIẾU NẠI LIÊN QUAN ĐẾN VIỆC THÔNG TIN CỦA NGƯỜI TIÊU DÙNG",
    text: "Khi phát hiện thông tin cá nhân của mình bị sử dụng sai mục đích hoặc phạm vi, Người Tiêu Dùng gửi email khiếu nại đến email contact@hanagold.vn  hoặc gọi điện thoại tới số 0889028009 để khiếu nại và cung cấp chứng cứ liên quan tới vụ việc cho Ban quản trị. Ban quản trị cam kết sẽ phản hồi ngay lập tức hoặc muộn nhất là trong vòng 24 (hai mươi tư) giờ làm việc kể từ thời điểm nhận được khiếu nại.",
  },
];

export const SizeRing = [
  {
    img: Num1,
    title: "ĐO SIZE NHẪN",
    des: "Với hướng dẫn này bạn có thể dễ dàng mua sắm nhẫn vàng, nhẫn bạc, nhẫn cưới, nhẫn kim cương hay nhẫn đôi ngay trên online mà không còn gì phải lo ngại.",
    step: [
      {
        name: "Bước 1 : ",
        des: "Quý khách chọn chiếc nhẫn đeo vừa với ngón tay mong muốn",
      },
      {
        name: "Bước 2 : ",
        des: "Quý khách dùng thước đo đường kính bên trong của nhẫn",
      },
      {
        name: "Bước 3 : ",
        des: "Quý khách đối chiếu với bảng đo size nhẫn chuẩn",
      },
      {
        name: "Bước 4 : ",
        des: "Quý khách điền size nhẫn theo chuẩn (Size số...) vào ô trống điền size nhẫn khi đặt hàng online",
      },
    ],
    step1: [
      {
        name: "Bước 1 : ",
        des: "Chuẩn bị một cây thước, 1 cây kéo, 1 cây bút & một tờ giấy",
      },
      {
        name: "Bước 2 : ",
        des: "Cắt một mảnh giấy dài khoảng 10 cm và rộng 1 cm.",
      },
      {
        name: "Bước 3 : ",
        des: "Sử dụng đoạn giấy vừa cắt để quấn sát quanh ngón tay muốn đo.",
      },
      {
        name: "Bước 4 : ",
        des: "Đánh dấu điểm giao nhau.",
      },
      {
        name: "Bước 5 : ",
        des: "Tháo ra dùng thước đo chiều dài của đoạn giấy từ điểm đầu cho đến phần đánh dấu. Lấy kết quả đo được chia cho 3,14. Sau đó đối chiếu với bảng SIZE NHẪN CHUẨN",
      },
    ],
    step2: [
      {
        name: "1/ Kích cỡ ngón tay phụ thuộc nhiệt độ",
        des: "Nhiệt độ là nguyên nhân gây ra việc đo kích thước nhẫn không chính xác. Khi thời tiết lạnh ngón tay của bạn có thể nhỏ hơn bình thường, bạn nên cộng thêm cho chu vi là 1 mm còn khi thời tiết nóng thì ngược lại, trừ đi 1 mm. Trường hợp xương khớp ngón tay của bạn to, thì bạn nên đo chu vi ở gần khớp (không phải trên khớp) Sao cho khi đeo nhẫn dễ vào nhưng không bị tuột mất.",
      },
      {
        name: "2/ Chú ý độ dày của nhẫn",
        des: "Độ dày của nhẫn cũng ảnh hưởng đến size ni của mỗi người. Đối với các loại nhẫn như nhẫn vàng, nhẫn bạc, độ dày sẽ khác nhau. Do đó, khi tiến hành đo, bạn cần phải chú ý và cho kích thước thêm hoặc bớt đi một khoản để khi đeo nhẫn có thể thoải mái mà không bị gò bó.",
      },
      {
        name: "3/ Để ý những khớp tay",
        des: "Các khớp tay có thể to hơn so với ở phía gốc của ngón. Do đó bạn cần phải đo kích thước ở khớp ngón và gốc ngón, sau đó lấy kích thước giữa 2 phần để chọn size nhẫn phù hợp. Lúc này tay đeo nhẫn của bạn sẽ có cảm giác thoải mái và không bị khó chịu khi co hoặc gập ngón tay.",
        des2: "Trong trường hợp khớp xương ngón tay của bạn to, bạn nên đo chu vi ở gần khớp (không phải trên khớp) sao cho khi bạn đeo nhẫn thì nhẫn dễ vào và không bị tuột mất.",
      },
      {
        name: "4/ Đo lại nhiều lần để có số liệu chính xác nhất",
        des: "Khi đo size tay chắc chắn sẽ có những sai sót, vì vậy hãy cẩn thận đo từ 3-4 lần để tìm ra số đo chính xác nhất. Làm theo cách này, kích cỡ bạn đo được sẽ có độ chính xác cao từ 85-95%.",
      },
    ],
  },
];

export const SizeBangle = [
  {
    img: Num2,
    title: "HƯỚNG DẪN ĐO SIZE LẮC",
    des: "Chiều dài của dây là 17 cm tương ứng với size 17",
    step: [
      {
        name: "Size lắc tay phổ biến cho nữ: Size 16-18",
        des: "16 cm - 17 cm - 18 cm",
      },
      {
        name: "Size lắc chân phổ biến cho nữ: Size 23-25",
        des: "23 cm - 24 cm - 25m",
      },
      {
        name: "Size lắc chân phổ biến cho trẻ em: Size 13 -15",
        des: "13 cm - 14 cm - 15 m",
      },
    ],
    step2: [
      {
        name: "Bước 1: ",
        des: "Bạn dùng thước để đo chiều dài của chiếc lắc tay.",
      },
      {
        name: "Bước 2: ",
        des: "Sau khi có kích thước, bạn đối chiếu số cm của thước với kích thước của bảng kích thước lắc như hình trên.",
      },
      {
        name: "Bước 3: ",
        des: "Kích thước lắc tay của bạn chính là size số ghi dưới đường thẳng. Sau khi chọn size lắc, nhân viên HanaGold sẽ gọi điện thoại xác nhận yêu cầu của bạn và tư vấn cụ thể để đảm bảo đáp ứng đúng yêu cầu của bạn.",
      },
    ],
    note: [
      {
        title: "Lưu ý:",
        note1:
          "- Size lắc phổ biến của HanaGold thường có đối với lắc tay nữ là size 16,17,18; đối với lắc chân nữ là size 23 - 25 và đối với lắc tay trẻ em là size 13 - 15.",
        note2:
          "- Trường hợp size lắc bạn đo được không có, HanaGold sẽ đặt làm trong thời gian tối đa là 2 tuần và chỉ áp dụng cho trang sức bằng vàng.",
      },
      {
        title: "Đo thủ công:",
        note1:
          "- Nếu bạn không có một chiếc lắc tay khác để đo, HanaGold giới thiệu bạn một cách nhưng lưu ý bạn áp dụng cách này sẽ không đưa ra được kết quả chính xác nhưng có thể sử dụng được.",
        note2:
          "- Bạn sử dụng một miếng giấy nhỏ như trong hình minh họa trên đây, quấn chặt vòng theo vòng tay/vòng chân bạn, đo độ dài bằng thước kẻ như thao tác đã hướng dẫn ở cách 2. Sau khi chọn size lắc, nhân viên HanaGold sẽ gọi điện thoại xác nhận yêu cầu của bạn và tư vấn cụ thể để đảm bảo đáp ứng đúng yêu cầu của bạn.",
      },
    ],
  },
];

export const SizeBracelet = [
  {
    img: Num3,
    title: "HƯỚNG DẪN ĐO SIZE VÒNG",
    des: "Đường kính lọt lòng của chiếc vòng (size vòng)",
    step: [
      {
        name: "Kích thước vòng dành cho nữ: Size 52-55",
        des: "52 mm - 53 mm - 54 mm - 55 mm",
      },
      {
        name: "Kích thước vòng dành cho nam: Size 56-60",
        des: "56 mm - 57 mm - 58 mm - 59 mm - 60 mm",
      },
      {
        name: "Kích thước vòng dành cho trẻ em: Size 36-42",
        des: "36 mm - 38 mm - Các size khác - 42 mm",
      },
    ],
    step1: [
      {
        name: "Bước 1: ",
        des: "Bạn dùng thước để đo đường kính bên trong của chiếc vòng tay.",
      },
      {
        name: "Bước 2: ",
        des: "Sau khi có kích thước, bạn đối chiếu số mm của thước với kích thước đường kính của bảng kích thước vòng như hình trên.",
      },
      {
        name: "Bước 3: ",
        des: "Kích thước vòng tay của bạn chính là size số ghi dưới vòng tròn. Sau khi chọn size vòng, nhân viên HanaGold sẽ gọi điện thoại xác nhận yêu cầu của bạn và tư vấn cụ thể để đảm bảo đáp ứng đúng yêu cầu của bạn.",
      },
    ],
    step2: [
      {
        name: "Bước 1: ",
        des: "Dùng một sợi dây (hoặc thước dây/ chỉ/giấy bản nhỏ) quấn quanh cổ tay đeo vòng, đánh dấu chỗ tiếp giáp.",
      },
      {
        name: "Bước 2: ",
        des: "Đo chiều dài sợi dây giữa 1 chỗ tiếp giáp.",
      },
      {
        name: "Bước 3: ",
        des: "Lấy chiều dài đó chia cho 3.14 là sẽ ra đường kính vòng.",
      },
      {
        name: "Bước 3: ",
        des: "Bạn đối chiếu đường kính bạn vừa đo (theo mm) với bảng kích thước vòng bên trên. Kích thước vòng của bạn tương ứng với size số ghi dưới vòng tròn.",
      },
    ],
  },
];
export const SizeNecklace = [
  {
    img: Num4,
    title: "HƯỚNG DẪN ĐO SIZE DÂY CHUYỀN",
    des: "Chiều dài của dây là 40 cm tương ứng với size 40",
    step: [
      {
        des: "Nên có một chiếc dây chuyền/dây cổ khác để đo, cách chính xác nhất là bạn dùng một chiếc dây chuyền/dây cổ có độ rộng và kiểu dáng tương tự chiếc bạn định mua và làm theo các bước sau:",
      },
      {
        des: "- Bạn dùng thước để đo chiều dài đoạn dây.",
      },
      {
        des: "- Sau khi có kích thước, bạn đối chiếu số cm của thước với kích thước minh họa như hình trên",
      },
      {
        des: "- Kích thước dây chuyền của bạn chính là size số ghi bên dưới đường thẳng.",
      },
    ],
    step1: [
      {
        des: "- Nếu bạn không có một dây chuyền/dây cổ khác để đo, HanaGold giới thiệu bạn một cách nhưng lưu ý bạn áp dụng cách này sẽ không đưa ra được kết quả chính xác nhưng có thể sử dụng được.",
      },
      {
        des: "- Bạn sử dụng một miếng giấy nhỏ, quấn chừng quanh cổ theo vòng dây mà bạn muốn đeo, sau đó đo độ dài bằng thước kẻ như thao tác đã hướng dẫn ở cách 2. Sau khi chọn size dây chuyền/dây cổ, nhân viên HanaGold sẽ gọi điện thoại xác nhận yêu cầu của bạn và tư vấn cụ thể để đảm bảo đáp ứng đúng yêu cầu của bạn.",
      },
    ],
  },
];
