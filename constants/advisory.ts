import Banner1 from "../public/images/consultant/tranquangchien.png";
import Banner2 from "../public/images/consultant/nguyendinhthang.png";
import Banner3 from "../public/images/consultant/trinhthanhhai.png";
import Banner4 from "../public/images/consultant/daotienphong.png";
import Banner5 from "../public/images/consultant/hoangductrung.png";
import Banner6 from "../public/images/consultant/luonghoanghung.png";
import Banner1act from "../public/images/consultant/tranquangchienact.png";
import Banner2act from "../public/images/consultant/nguyendinhthangact.png";
import Banner3act from "../public/images/consultant/trinhthanhhaiact.png";
import Banner4act from "../public/images/consultant/daotienphongact.png";
import Banner5act from "../public/images/consultant/hoangductrungact.png";
import Banner6act from "../public/images/consultant/luonghoanghungact.png";

export const ADVISORY_DATA = [
  {
    id: "0",
    img: Banner1,
    imgact: Banner1act,
    name: "Ông Trần Quang Chiến",
    title: "Cố vấn Công Nghệ & Bảo Mật",
    description_name: "Ông Trần Quang Chiến",
    des: ", CEO, Co - Founder VNDC, Chủ tịch HĐQT Cystack.",
    description: [
      {
        paragraph:
          "Hơn 05 năm kinh nghiệm trong lĩnh vực bảo mật thông tin, công nghệ phần mềm, công nghệ blockchain.Ông đang tham gia với nhiều vai trò cố vấn quan trọng trong nhiều dự án sáng tạo, phát triển các ứng dụng thương mại điện tử ứng dụng nền tảng công nghệ blockchain và tiền mã hóa. Các dự án tiêu biểu như VNDC Wallet Pro, 100man.",
      },
    ],
  },
  {
    id: "1",
    img: Banner2,
    imgact: Banner2act,
    name: "Ông Nguyễn Đình Thắng",
    title: "Cố vấn Công Nghệ Tài Chính",
    description_name: "Ông Nguyễn Đình Thắng",
    des: " - Cử nhân Kinh tế - Đại học Kinh tế Quốc dân, Phó Chủ tịch HĐQT - Ngân hàng TMCP Bưu điện Liên Việt. Đồng thời giữ chức vụ Phó Chủ tịch - Hiệp hội Doanh nghiệp phần mềm Việt Nam nhiệm kỳ I, II, III, IV năm 2002 đến nay. Hơn 10 năm kinh nghiệm trong các lĩnh vực quản lý công nghệ, phầm mềm, tài chính đầu tư và ngân hàng thương mại.",
  },
  {
    id: "2",
    img: Banner3,
    imgact: Banner3act,
    name: "Ông Trịnh Thanh Hải",
    title: "Cố vấn Đối Ngoại",
    description_name: "Ông Trịnh Thanh Hải",
    des: " - Thạc sỹ Quản trị Kinh doanh chuyên ngành Tài chính - Đại học George Washington - Hoa Kỳ, Cử nhân Kinh tế - Đại học Kinh tế Quốc dân.",
    description: [
      {
        paragraph:
          "Ông Hải là chuyên gia Tài chính Đầu tư với 30 năm kinh nghiệm, đảm nhiệm nhiều vị trí quản lý tại các Doanh nghiệp hàng đầu của Việt Nam và Hoa Kỳ như New York Life, Indochina Capital và Bảo Việt. Hiện nay ông Hải là Giám đốc Điều hành Công ty TransPacific Pathway Link LLC.",
      },
    ],
  },
  {
    id: "3",
    img: Banner4,
    imgact: Banner4act,
    name: "Ông Đào Tiến Phong",
    title: "Cố vấn Pháp Lý",
    description_name: "Ông Đào Tiến Phong",
    des: " - Cố vấn Pháp Lý, Luật sư điều hành Investpush Legal. Ông có hơn 16 năm kinh nghiệm trong lĩnh vực luật kinh doanh (đầu tư nước ngoài, thương mại, M&A, startup, thương mại điện tử, fintech, tài chính, ngân hàng etc...) và kinh nghiệm làm việc với nhiều loại dự án đầu tư như phân phối, xây dựng, sản xuất, giáo dục, logistics.",
    description: [
      {
        paragraph:
          "Một số dự án tham gia tiêu biểu như: Siêu thi: Parkson (Le Thanh Ton, Hung Vuong, Hai Phong, Ha Noi), Family Mart. Phân phối: Cochine, Servicom, Noble Coffee. Logistics: Addicon, Securiforces, Pacific Airlift, Eimskip. - Bảo hiểm, kế toán: AIG Vietnam (Chartis), Odyssey, Inta. Bất động sản: Endo-Viet Quoc, Gamuda - Nam Long; BCC Hai An; Blooming Park (Imperia An Phu); Fideco (Dong Binh Duong) …Thương mại điện tử : BACS, Brian Coles Partnership, Thue LLC, Getlinks …",
      },
    ],
  },
  {
    id: "4",
    img: Banner5,
    imgact: Banner5act,
    name: "Ông Hoàng Đức Trung",
    title: "Cố vấn Đầu Tư",
    description_name: "Ông Hoàng Đức Trung - Cố vấn Đầu Tư.",
    des: " Ông có hơn 28 năm kinh nghiệm trong lĩnh vực tư vấn kinh doanh, vận hành doanh nghiệp và đầu tư mạo hiểm trên toàn khu vực tại SEA và Thung lũng Silicon. Trong sự nghiệp của mình, ông ấy đã phục vụ cố vấn trong nhiều hội đồng quản trị cho các công ty khởi nghiệp công nghệ đã mở rộng thành công trên toàn khu vực và đã đạt được các đợt IPO thành công tại địa phương. Ông nhận bằng MBA của Trường Quản lý Maastricht ở Hà Lan.",
  },
  {
    id: "5",
    img: Banner6,
    imgact: Banner6act,
    name: "Nhà báo Lương Hoàng Hưng",
    title: "Cố vấn Truyền Thông",
    description_name: "Nhà báo Lương Hoàng Hưng",
    des: " - Phó Chủ tịch Liên Hiệp Khoa Học Doanh Nhân Việt Nam, Tổng Biên tập Tạp chí Sở Hữu Trí Tuệ và Sáng Tạo. Nhà báo Lương Hoàng Hưng từ lâu đã là cái tên quen thuộc với không chỉ trong giới báo chí, mà còn cả trong cộng đồng doanh nghiệp, đặc biệt là đối với các doanh nghiệp StartUp.",
    description: [
      {
        paragraph:
          "Ông đã có 30 năm kinh nghiệm trong lĩnh vực truyền thông, khi từng làm việc ở nhiều cơ quan báo chí lớn như Sài Gòn Giải Phóng, Giáo dục, Thương mại,... Hiện tại, ông đang là Tổng Biên tập của Tạp chí Sở hữu Trí tuệ và Sáng tạo - một trong những cơ quan báo chí hàng đầu về lĩnh vực sở hữu trí tuệ, công nghệ sáng tạo.",
      },
      {
        paragraph:
          "Ông cũng đã tham gia sáng lập phối hợp và hỗ trợ hoạt động cho nhiều tổ chức tạo được hiệu ứng xã hội tốt như: Liên hiệp Khoa học Doanh nhân Việt Nam, Viện Doanh nhân Việt Nam, Trung Tâm Quản Lý Tài Sản Số, Golf Việt điện tử, Câu lạc bộ Doanh nhân & Quản trị, Câu lạc bộ Doanh nhân Khánh Hòa Sài Gòn, Viện Khởi Nghiệp Thực Tế, Câu lạc bộ Golf Việt Nam, Câu lạc bộ Doanh nhân Tư vấn Nợ Ngân hàng, Group Quản trị và Khởi nghiệp,...",
      },
    ],
  },
];
