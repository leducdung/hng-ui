export enum ProductSugarOpt {
  NATURAL,
  SUGAR_5ML,
  SUGAR_10ML,
}

export const ProductSugarOptions: { id: ProductSugarOpt; name: string }[] = [
  { id: ProductSugarOpt.NATURAL, name: "Ngọt Tự Nhiên" },
  { id: ProductSugarOpt.SUGAR_5ML, name: "5ml Đường Phèn" },
  { id: ProductSugarOpt.SUGAR_10ML, name: "10ml Đường Phèn" },
];

export const Category: { id: string; name: string }[] = [
  { id: "1", name: "Dây" },
  { id: "2", name: "Kiềng" },
  { id: "3", name: "Lắc" },
  { id: "4", name: "Mặt" },
  { id: "5", name: "Nhẫn" },
  { id: "6", name: "Vòng" },
];

export const Price: { price: string }[] = [
  {
    price: "< 1.000.000",
  },
  {
    price: "1.000.000 - 4.000.000 đ",
  },
  {
    price: ">4.000.000 đ",
  },
];

// eslint-disable-next-line
export const ProductSizeRings: { name: string; description: any[] }[] = [
  {
    name: "Nhẫn",
    description: [
      {
        title1: "Kích cỡ US",
        title2: "Chu vi bên trong (mm)",
        sizeus: [
          {
            number: "2",
          },

          {
            number: "3",
          },
          {
            number: "3.5",
          },
          {
            number: "4",
          },
          {
            number: "4.5",
          },
          {
            number: "5",
          },
          {
            number: "5.5",
          },
          {
            number: "6",
          },
          {
            number: "6.5",
          },
        ],

        perimetermm: [
          {
            perimeter: "41.7",
          },

          {
            perimeter: "42.9",
          },
          {
            perimeter: "45.5",
          },
          {
            perimeter: "46.8",
          },
          {
            perimeter: "48.0",
          },
          {
            perimeter: "49.3",
          },
          {
            perimeter: "50.6",
          },
          {
            perimeter: "51.9",
          },
          {
            perimeter: "53.1",
          },
        ],
      },
    ],
  },
];
// eslint-disable-next-line
export const ProductSizeBracelet: { name: string; description: any[] }[] = [
  {
    name: "Vòng tay",
    description: [
      {
        title1: "Kích cỡ",
        title2: "Chu vi bên trong (mm)",
        size: [
          {
            number: "XS",
          },

          {
            number: "S",
          },
          {
            number: "M",
          },
          {
            number: "L",
          },
          {
            number: "XL",
          },
        ],
        perimetermm: [
          {
            perimeter: "4.76 - 5.25 in.  |  121 - 133mm",
          },

          {
            perimeter: "5.26 - 5.75 in.  |  134 - 146 mm",
          },
          {
            perimeter: "5.76 - 6.25 in.  |  146 - 159 mm",
          },
          {
            perimeter: "6.26 - 6.75 in.  |  146 - 159 mm",
          },
          {
            perimeter: "6.76 - 7.25 in  |  172 - 184 mm",
          },
        ],
      },
    ],
  },
];
// eslint-disable-next-line
export const ProductSizeNecklace: { name: string; description: any[] }[] = [
  {
    name: "Dây chuyền",
    description: [
      {
        title1: "Chiều dài (in)",
        title2: "Chiều dài (cm)",
        lengthin: [
          {
            number: "16",
          },

          {
            number: "18",
          },
          {
            number: "20",
          },
          {
            number: "24",
          },
          {
            number: "30",
          },
          {
            number: "36",
          },
        ],
        lengthcm: [
          {
            number: "40.50",
          },

          {
            number: "45.75",
          },
          {
            number: "50.75",
          },
          {
            number: "61.00",
          },
          {
            number: "76.25",
          },
          {
            number: "91.50",
          },
        ],
      },
    ],
  },
];

export const CategoryStone: { id: string; name: string }[] = [
  { id: "1", name: "Tất cả" },
  { id: "2", name: "Vàng 18k" },
  { id: "3", name: "Vàng hồng" },
  { id: "4", name: "Bạc nguyên chất" },
  { id: "5", name: "Bạc xi bạch kim" },
  { id: "6", name: "Bạc xi 2 màu" },
];
