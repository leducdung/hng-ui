import React from "react";
import { APP_ROUTES } from ".";
import {
  Person as PersonIcon,
  Description as DescriptionIcon,
  VpnKey as VpnKeyIcon,
  LocationOn as LocationOnIcon,
} from "@material-ui/icons";

export const CustomerMenus: {
  title: string;
  url: string;
  icon: { active: React.ReactNode; inactive: React.ReactNode };
}[] = [
  {
    title: "Hồ sơ cá nhân",
    url: APP_ROUTES.CUSTOMER_PROFILE,
    icon: {
      active: <PersonIcon style={{ fontSize: "2.2rem", color: "#2ea936" }} />,
      inactive: <PersonIcon style={{ fontSize: "2.2rem", color: "#3C3B3B" }} />,
    },
  },
  {
    title: "Danh sách địa chỉ",
    url: APP_ROUTES.CUSTOMER_SHIPPING_ADDRESS,
    icon: {
      active: <LocationOnIcon style={{ fontSize: "2.2rem", color: "#2ea936" }} />,
      inactive: <LocationOnIcon style={{ fontSize: "2.2rem", color: "#3C3B3B" }} />,
    },
  },
  {
    title: "Đổi mật khẩu",
    url: APP_ROUTES.CUSTOMER_PASSWORD,
    icon: {
      active: <VpnKeyIcon style={{ fontSize: "2.2rem", color: "#2ea936" }} />,
      inactive: <VpnKeyIcon style={{ fontSize: "2.2rem", color: "#3C3B3B" }} />,
    },
  },
  {
    title: "Lịch sử đơn hàng",
    url: APP_ROUTES.CUSTOMER_ORDER,
    icon: {
      active: <DescriptionIcon style={{ fontSize: "2.2rem", color: "#2ea936" }} />,
      inactive: <DescriptionIcon style={{ fontSize: "2.2rem", color: "#3C3B3B" }} />,
    },
  },
];
