import { APP_ROUTES, DEFAULT_FETCHING } from ".";
import { AppLang, FetchingPaginationDataModel, TBreadcrumb } from "@Models";

export enum FORM_MODE {
  VIEW,
  CREATE,
  UPDATE,
}

/* eslint-disable-next-line */
export const INITIAL_FETCHING_PAGINATION_DATA: FetchingPaginationDataModel<any> = {
  page: 1,
  totalPages: 1,
  limit: DEFAULT_FETCHING.LIMIT,
  totalRecords: 1,
  data: [],
  loading: false,
};

export enum Direction {
  UP = "UP",
  DOWN = "DOWN",
  LEFT = "LEFT",
  RIGHT = "RIGHT",
}

export const HomeBreadcrumb: TBreadcrumb = {
  path: APP_ROUTES.HOME,
  title: "Trang chủ",
};

export const APP_LANGUAGES: { id: AppLang; name: string }[] = [
  { id: AppLang.VN, name: "Tiếng Việt" },
  { id: AppLang.EN, name: "Tiếng Anh" },
  { id: AppLang.AS, name: "Tiếng Arab" },
];

export const PAGE_BANNER_BACKGROUND_SRC = {
  PRODUCT: "/images/banner/service-page-banner-bg.png",
  SERVICE: "/images/banner/service-page-banner-bg.png",
  NEWS: "/images/banner/news-page-banner-bg.png",
  PROJECT: "/images/banner/project-page-banner-bg.png",
  ABOUTUS: "/images/banner/aboutus-page-banner-bg.png",
};

export const PAGE_BANNER_ICON_SRC = {
  PRODUCT: "/images/banner/product-page-banner-icon.png",
  SERVICE: "/images/banner/service-page-banner-icon.png",
  NEWS: "/images/banner/news-page-banner-icon.png",
  PROJECT: "/images/banner/project-page-banner-icon.png",
};

export const GOLD_4_DATA: { img: string; title: string; description: string }[] = [
  {
    img: "/images/whyus/img1.png",
    title: "MUA BÁN",
    description:
      "Trải nghiệm việc mua trang sức đơn giản, nhanh chóng và dễ dàng hơn qua việc mua tích lũy theo zem, ly, phân vàng đến khi đạt trọn vẹn giá trị của món trang sức đó. Đồng thời có thể bán lại nhanh chóng cho HanaGold và nhận tiền về tài khoản ngân hàng",
  },
  {
    img: "/images/whyus/img2.jpg",
    title: "CHUYỂN ĐỔI",
    description:
      "Dễ dàng quy đổi vàng tích lũy theo đơn vị chỉ sang vàng vật chất tại hệ thống tiệm kim hoàn 4.0 HanaGold trên toàn quốc",
  },
  {
    img: "/images/whyus/img3.png",
    title: "TÍCH LŨY",
    description:
      "Sở hữu vàng dễ dàng hơn dù chỉ với 100 ngàn đồng bạn cũng có thể mua vàng với HanaGold để tích lũy làm tài sản cho tương lai với các định lượng siêu nhỏ như zem, ly, phân vàng",
  },
  {
    img: "/images/whyus/img4.png",
    title: "CẤT GIỮ",
    description:
      "Loại bỏ những băn khoăn và lo lắng khi cất giữ tài sản quý giá này tại nhà. HanaGold sẽ giúp bạn lưu giữ với bảo mật cao nhất và bạn cũng là người trực tiếp quản lý số tài sản này",
  },
  {
    img: "/images/whyus/img5.png",
    title: "GỬI TIẾT KIỆM",
    description:
      "Bạn có thể gửi tiết kiệm tài sản của bạn tại HanaGold và nhận tỷ lệ sinh lời 8%/năm từ 1 chỉ vàng trở lên",
  },
  {
    img: "/images/whyus/img6.png",
    title: "cầm đồ",
    description:
      "Dịch vụ cầm cố trang sức, tài sản vàng giúp bạn có nguồn vốn lưu động ngay lập tức mà không phải bán đi số tài sản vàng đã tích lũy      ",
  },
];
