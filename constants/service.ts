import Img1 from "../public/images/service/1.png";
import Img2 from "../public/images/service/2.png";
import Img3 from "../public/images/service/3.png";
import Img4 from "../public/images/service/4.png";
import Img5 from "../public/images/service/5.png";
import ImgLeft1 from "../public/images/service/1left.png";
import ImgLeft2 from "../public/images/service/2left.png";
import ImgLeft3 from "../public/images/service/3left.png";
import ImgLeft4 from "../public/images/service/4left.png";

import ImgRight1 from "../public/images/service/1right.png";
import ImgRight2 from "../public/images/service/2right.png";
import ImgRight3 from "../public/images/service/3right.png";
import ImgRight4 from "../public/images/service/4right.png";

export const ImgService = [
  {
    img: Img1,
    label: "MUA BÁN",
    labelmb: "MUA BÁN",
  },
  {
    img: Img2,
    label: "CHUYỂN ĐỔI",
    labelmb: "CHUYỂN ĐỔI",
  },
  {
    img: Img3,
    label: "HỢP TÁC KINH DOANH",
    labelmb: "GỬI VÀNG",
  },
  {
    img: Img4,
    label: "TÍCH LŨY",
    labelmb: "TÍCH LŨY",
  },
  {
    img: Img5,
    label: "CẦM ĐỒ",
    labelmb: "THẾ CHẤP",
  },
] as const;

type MockServiceImage = {
  img: StaticImageData;
  number: string;
  title: string;
};

export const Online: MockServiceImage[] = [
  {
    img: ImgLeft1,
    number: "01.",
    title: "Đăng kí thành viên HanaGold",
  },
  {
    img: ImgLeft2,
    number: "02.",
    title: "Nạp tiền",
  },
  {
    img: ImgLeft3,
    number: "03.",
    title: "Mua tích lũy mỗi ngày",
  },
  {
    img: ImgLeft4,
    number: "04.",
    title: "Nhận trực tiếp sản phẩm trang sức, mỹ nghệ tại tiệm vàng HanaGold",
  },
];

export const Offline: MockServiceImage[] = [
  {
    img: ImgRight1,
    number: "01.",
    title: "Tìm địa điểm showroom HanaGold gần nhất với bạn",
  },
  {
    img: ImgRight2,
    number: "02.",
    title: "Đến showroom trải nghiệm sản phẩm và dịch vụ",
  },
  {
    img: ImgRight3,
    number: "03.",
    title: "Chọn mẫu mã trang sức yêu thích",
  },
  {
    img: ImgRight4,
    number: "04.",
    title: "Thanh toán và nhận hàng",
  },
];
