export const REQUEST_URL = {
  // auth
  SIGN_IN: "/auth/login",
  SIGN_UP: "/auth/register",
  FORGET_PASSWORD: "/auth/forgot-password/{EMAIL}",
  LOAD_ME: "/auth/me",
  CHANGE_PASSWORD: "/customer/profile/change-password",

  // product
  FILTER_PRODUCT: "/product/filter/get-all-product",
  GET_PRODUCT_BY_SLUG: "/product/slug/{SLUG}",
  GET_ALL_ORDER_PRODUCT: "/product/filter/get-product",

  // category
  GET_ALL_CATEGORIES: "/category/get-all/categories",

  // blog
  FILTER_BLOG: "/blogs/filter/get-all-blog",
  GET_BLOG_BY_SLUG: "/blogs/slug/{SLUG}",
  GET_BLOG_CATEGORIES: "/blog-category",

  // service
  FILTER_SERVICE: "/accompany-service/filter/get-all-accompany-service",
  GET_SERVICE_BY_SLUG: "/accompany-service/slug/{SLUG}",

  // project
  FILTER_PROJECT: "/project/filter/get-all-project",
  GET_PROJECT_BY_SLUG: "/project/slug/{SLUG}",

  // order
  ORDER: "/orders",
  GET_ALL_ORDER: "/customer/profile/get-all-order",
  GET_ORDER_DETAIL_BY_CODE: "/orders/order-code/{ORDER_CODE}",

  // user
  GET_ALL_SHIPPING_ADDRESS: "/customer/profile/get-all-shipping",
  SHIPPING_ADDRESS: "/shipping",
  SHIPPING_ADDRESS_DETAIL: "/shipping/{ID}",
  UPDATE_PROFILE: "/customer/profile/update",
  UPDATE_AVATAR: "/customer/update-avatar",
};
