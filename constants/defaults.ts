import { BASE_IMG_DOMAIN } from ".";
import { AppLang } from "@Models";

export const DEFAULT_FALLBACK_LANGUAGE = AppLang.VN;

export const DEFAULT_FETCHING = {
  LIMIT: 10,
  PAGE_NUMBER: 1,
};

export const DEFAULT_HTML = {
  TITLE: "HanaGold - Đồng vàng HanaGold 24k",
  META_TITLE: "HanaGold - Đồng vàng HanaGold 24k",
  META_DESCRIPTION:
    "HanaGold là nền tảng kết nối tiệm kim hoàn và người đi mua vàng, tại đó người dùng có thể chọn lựa mẫu mã, mua vàng tích lũy online.",
};

export const DEFAULT_IMG_ALT = "hana-gold-img";
export const DEFAULT_LOGO_IMG_ALT = "Hana gold Logo";
export const DEFAULT_PRODUCT_IMG_PLACEHOLDER = "/images/product/product-placeholder.png";
export const DEFAULT_NEWS_IMG_PLACEHOLDER = "/images/news/news-placeholder.png";
export const DEFAULT_CUSTOMER_IMG_PLACEHOLDER = "/images/auth/empty-avatar.png";
export const DEFAULT_CUSTOMER_AVATAR = "/images/customer-avatar.png";

export const DEFAULT_STORE_INFOS = {
  FULL_NAME: "Công ty CP Vàng bạc đá quý HanaGold",
  NAME: "HanaGold",
  EMAIL: "contact@hanagold.vn",
  PHONE_NUMBER: "0889 028 009",
  PHONE_NUMBER_TO_CALL: "0889028009",
  ADDRESS: {
    HCM: "136 Lê Tuấn Mậu, phường 13, quận 6, TP.HCM",
    CONTHO: "81 Nguyễn Hiền, KDC 91B, Phường An Khánh, Ninh Kiều",
    BINHDUONG: "256 Hùng Vương, Thị trấn Dầu Tiếng, Huyện Dầu Tiếng",
  },
  URL: {
    FACEBOOK: "https://www.facebook.com/midomsamdua/",
    INSTAGRAM: "https://www.instagram.com/midomcoco/",
    YOUTUBE: "https://www.youtube.com/channel/UCEEh2_CofeIS6AZDsxmGTcw",
    GG_MAP:
      "https://www.google.com/maps/place/C%C3%B4ng+ty+C%E1%BB%95+ph%E1%BA%A7n+v%C3%A0ng+b%E1%BA%A1c+%C4%91%C3%A1+qu%C3%BD+HanaGold/@10.7499808,106.6240576,19.64z/data=!4m5!3m4!1s0x31752d495bcac5c1:0x8f0c5e5c9373740b!8m2!3d10.7500294!4d106.6243755",
  },

  FILE: "/file/cong-bo-TCCS-HanaGold-01QĐ.pdf",
};

export const DEFAULT_COMING_SOON = {
  TITLES: {
    INFO: "Tính năng này sẽ sớm được cập nhật",
    FEATURE: "Chúng tôi sẽ sớm cập nhật tính năng này",
  },
  SUB_TITLES: {
    INFO: `Cảm ơn bạn đã quan tâm ${DEFAULT_STORE_INFOS.NAME}`,
    FEATURE: "Vui lòng liên hệ (+84) 2877 766 339 để đặt hàng",
  },
};

// image
export const IMG_BASE_URL = {
  PRODUCT: `${BASE_IMG_DOMAIN}/uploads/products`,
  PRODUCT_CATEGORIES: `${BASE_IMG_DOMAIN}/uploads/categories`,
  TOPPING: `${BASE_IMG_DOMAIN}/uploads/toppings`,
  NEWS: `${BASE_IMG_DOMAIN}/uploads/blogs`,
  CUSTOMER: `${BASE_IMG_DOMAIN}/uploads/customers`,
  SERVICE: `${BASE_IMG_DOMAIN}/uploads/services`,
  PROJECT: `${BASE_IMG_DOMAIN}/uploads/projects`,
};

export const INFO_STORES = [
  {
    name: "Công ty cổ phần vàng bạc đá quý HanaGold",
    address: "136 Lê Tuấn Mậu, phường 13, quận 6, TP.HCM",
    city: "Hồ Chí Minh, quận 6: ",
    avatar: "/images/branch/branch-1.png",
    coordinate: {
      lat: 10.75003,
      lng: 106.62438,
    },
  },
  {
    name: "Showroom HanaGold, Tòa nhà BIG 96 Nguyễn Thị Thập,",
    address: "Khu Đô thị Himlam, Phường Tân Hưng, Quận 7",
    city: "Hồ Chí Minh, quận 7:",
    avatar: "/images/branch/branch-3.png",
    coordinate: {
      lat: 10.75003,
      lng: 106.62438,
    },
  },
  {
    name: "Chi nhánh CTCP Vàng bạc đá quý HanaGold",
    address: "81 Nguyễn Hiền, KDC 91B, Phường An Khanh, Ninh Khiều",
    city: "Cần Thơ:",
    avatar: "/images/branch/branch-2.png",
    coordinate: {
      lat: 10.75003,
      lng: 106.62438,
    },
  },
];
