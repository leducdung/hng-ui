import { NewsOutputModel } from "@Models";
import ImgNew1 from "../public/images/mock/news/n1.png";
import ImgNew2 from "../public/images/mock/news/n2.png";
import ImgNew3 from "../public/images/mock/news/n3.png";
import ImgNew4 from "../public/images/mock/news/n4.png";
import HotNew1 from "../public/images/mock/news/1a.png";
import ImgNew5 from "../public/images/mock/news/n5.png";
import Img1 from "../public/images/mock/news/1.png";
import Img2 from "../public/images/mock/news/2.png";
import Img3 from "../public/images/mock/news/3.png";
import Img4 from "../public/images/mock/news/4.png";
import Img5 from "../public/images/mock/news/5.png";
import Img6 from "../public/images/mock/news/6.png";
import ImgNd1 from "../public/images/mock/news/nd1.png";
import ImgNd2 from "../public/images/mock/news/nd2.png";
import ImgNd3 from "../public/images/mock/news/nd3.png";
import ImgNd4 from "../public/images/mock/news/nd4.png";
import ImgNd5 from "../public/images/mock/news/nd5.png";

const staff = { id: "staff-1", fullName: "Phuong Phan" };

export const MOCK_NEWS: NewsOutputModel[] = [
  {
    id: "news-1",
    title: "PHỤ NỮ VỚI NHỮNG NỖI LO VỀ TÀI CHÍNH",
    shortDescription:
      "Các chuyên gia nghiên cứu ở Anh kết hợp cùng tổ chức Weight Watcher đã khảo sát trên 2.000 phụ nữ về những nỗi lo lắng thường trực của phái đẹp. Kết quả cho thấy phụ nữ “chỉ trích” bản thân ít nhất 8 lần/ngày...",
    timePublication: null,
    tags: "",
    slugs: ["bai-viet"],
    imageFeatured: "/images/mock/news/news-1.png",
    author: staff,
  },
  {
    id: "news-2",
    title: "9 CÂU CHUYỆN Ý NGHĨA VỀ TIỀN BẠC, Ý CHÍ VÀ SỰ GIÀU NGHÈO",
    shortDescription:
      "Tiếp tục những câu chuyện ý nghĩa của Kỳ 1, Kỳ 2 sẽ mang đến cho bạn những góc nhìn mới mẻ và thông điệp cuộc sống ẩn sau những câu chuyện ngắn này.",
    timePublication: null,
    tags: "",
    slugs: ["bai-viet"],
    imageFeatured: "/images/mock/news/news-2.png",
    author: staff,
  },
  {
    id: "news-3",
    title: "BIRBAL VÀ 300 ĐỒNG VÀNG: CÂU CHUYỆN THỨC TỈNH CHÚNG TA ...",
    shortDescription:
      "Birbal vốn nổi tiếng thông minh, tài ba xuất chúng dưới thời đức vua Akbar. Ghen tị, đố kỵ với Birbal, người anh họ của đức vua luôn nói xấu Birbal và thuyết phục Akbar cho mình được thế chỗ “cánh tay phải”, vốn thuộc về Birbal.",
    timePublication: null,
    tags: "",
    slugs: ["bai-viet"],
    imageFeatured: "/images/mock/news/news-3.png",
    author: staff,
  },
  {
    id: "news-4",
    title: "PHỤ NỮ VỚI NHỮNG NỖI LO VỀ TÀI CHÍNH",
    shortDescription:
      "Các chuyên gia nghiên cứu ở Anh kết hợp cùng tổ chức Weight Watcher đã khảo sát trên 2.000 phụ nữ về những nỗi lo lắng thường trực của phái đẹp. Kết quả cho thấy phụ nữ “chỉ trích” bản thân ít nhất 8 lần/ngày...",
    timePublication: null,
    tags: "",
    slugs: ["bai-viet"],
    imageFeatured: "/images/mock/news/news-1.png",
    author: staff,
  },
  {
    id: "news-5",
    title: "9 CÂU CHUYỆN Ý NGHĨA VỀ TIỀN BẠC, Ý CHÍ VÀ SỰ GIÀU NGHÈO",
    shortDescription:
      "Tiếp tục những câu chuyện ý nghĩa của Kỳ 1, Kỳ 2 sẽ mang đến cho bạn những góc nhìn mới mẻ và thông điệp cuộc sống ẩn sau những câu chuyện ngắn này.",
    timePublication: null,
    tags: "",
    slugs: ["bai-viet"],
    imageFeatured: "/images/mock/news/news-2.png",
    author: staff,
  },
];
export const NewsOverview = [
  {
    id: "1",
    img: ImgNew1,
    category: "Danh mục 1",
    name: "Phân biệt kim cương tự nhiên và tổng hợp trên thị trường Việt Nam",
    view: "30",
    time: "2",
  },
  {
    id: "2",
    img: ImgNew2,
    category: "Danh mục 1",
    name: "Đại diện Tập đoàn DOJI tham dự Hội nghị Ngọc học và trang sức...",
    view: "30",
    time: "2",
  },
  {
    id: "3",
    img: ImgNew3,
    category: "Danh mục 1",
    name: "Phân biệt kim cương tự nhiên và tổng hợp trên thị trường Việt Nam",
    view: "30",
    time: "2",
  },
  {
    id: "4",
    img: ImgNew4,
    category: "Danh mục 1",
    name: "Vàng giảm do đồng USD vững chắc hậu COVID",
    view: "30",
    time: "2",
  },
];

export const NewsCategory = [
  {
    id: "1",
    name: "CÁC SỰ KIỆN NỔI BẬT",
  },
  {
    id: "2",
    name: "TIN TỨC HANAGOLD",
  },
  {
    id: "3",
    name: "TIN TỨC VỀ VÀNG",
  },
];
export const News = [
  {
    img: Img1,
    category: "Danh mục 1",
    name: "Phân biệt kim cương tự nhiên và tổng hợp trên thị trường Việt Nam",
    view: "30",
    time: "2",
  },

  {
    img: Img2,
    category: "Danh mục 1",
    name: "Phân biệt kim cương tự nhiên và tổng hợp trên thị trường Việt Nam",
    view: "30",
    time: "2",
  },
  {
    img: Img3,
    category: "Danh mục 1",
    name: "Phân biệt kim cương tự nhiên và tổng hợp trên thị trường Việt Nam",
    view: "30",
    time: "2",
  },
  {
    img: Img4,
    category: "Danh mục 1",
    name: "Phân biệt kim cương tự nhiên và tổng hợp trên thị trường Việt Nam",
    view: "30",
    time: "2",
  },
  {
    img: Img5,
    category: "Danh mục 1",
    name: "Phân biệt kim cương tự nhiên và tổng hợp trên thị trường Việt Nam",
    view: "30",
    time: "2",
  },
  {
    img: Img6,
    category: "Danh mục 1",
    name: "Phân biệt kim cương tự nhiên và tổng hợp trên thị trường Việt Nam",
    view: "30",
    time: "2",
  },
  {
    img: Img1,
    category: "Danh mục 1",
    name: "Phân biệt kim cương tự nhiên và tổng hợp trên thị trường Việt Nam",
    view: "30",
    time: "2",
  },
  {
    img: Img3,
    category: "Danh mục 1",
    name: "Phân biệt kim cương tự nhiên và tổng hợp trên thị trường Việt Nam",
    view: "30",
    time: "2",
  },
  {
    img: Img2,
    category: "Danh mục 1",
    name: "Phân biệt kim cương tự nhiên và tổng hợp trên thị trường Việt Nam",
    view: "30",
    time: "2",
  },
];

export const HotNews = [
  {
    id: "1",
    img: HotNew1,
    category: "Danh mục 1",
    name: "Phân biệt kim cương tự nhiên và tổng hợp trên thị trường Việt Nam",
    view: "30",
    time: "2",
  },
  {
    id: "2",
    img: ImgNew2,
    category: "Danh mục 1",
    name: "Năm 2021, giá vàng có thể lập kỷ lục mới?",
    view: "30",
    time: "2",
  },
  {
    id: "3",
    img: ImgNew3,
    category: "Danh mục 1",
    name: "Phân biệt kim cương tự nhiên và tổng hợp trên thị trường Việt Nam",
    view: "30",
    time: "2",
  },
  {
    id: "4",
    img: ImgNew5,
    category: "Danh mục 1",
    name: "Đại diện Tập đoàn DOJI tham dự Hội nghị Ngọc học và trang sức...",
    view: "30",
    time: "2",
  },
];

export const RELATED_NEWS = [
  {
    img: ImgNd1,
    name: "Philanthropist MacKenzie Scott, Ex-Wife of Jeff Bezos, Marries Seattle School...",
    view: "30",
    time: "2",
  },
  {
    img: ImgNd2,
    name: "He Got $300,000 From Credit-Card Rewards. The IRS Said It Was Taxable...",
    view: "30",
    time: "2",
  },
  {
    img: ImgNd3,
    name: "Philanthropist MacKenzie Scott, Ex-Wife of Jeff Bezos, Marries Seattle School...",
    view: "30",
    time: "2",
  },
  {
    img: ImgNd4,
    name: "Philanthropist MacKenzie Scott, Ex-Wife of Jeff Bezos, Marries Seattle School...",
    view: "30",
    time: "2",
  },
  {
    img: ImgNd5,
    name: "Philanthropist MacKenzie Scott, Ex-Wife of Jeff Bezos, Marries Seattle School...",
    view: "30",
    time: "2",
  },
];
export const Times = [
  {
    time: "24h",
  },
  {
    time: "3D",
  },
  {
    time: "15D",
  },
  {
    time: "1M",
  },
  {
    time: "3M",
  },
  {
    time: " 6M",
  },
  {
    time: "1Y",
  },
  {
    time: "3Y",
  },
];
