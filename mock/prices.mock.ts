export const GoldPrice = [
  {
    name: "SJC",
    buy: {
      price: "5.410",
      num: "5",
    },
    sell: {
      price: "5.410",
      num: "5",
    },
  },
  {
    name: "PNJ",
    buy: {
      price: "5.664",
      num: "3",
    },
    sell: {
      price: "5.664",
      num: "3",
    },
  },

  {
    name: "DOJI",
    buy: {
      price: "5.664",
      num: "3",
    },
    sell: {
      price: "5.664",
      num: "3",
    },
  },
  {
    name: "HNG",
    buy: {
      price: "5.664",
      num: "3",
    },
    sell: {
      price: "5.664",
      num: "3",
    },
  },
];

export const PriceData = [
  {
    name: "SJC",
    date: "31/03/2021",
    time: "04:01:00",
    buy: {
      price: "1.758",
      num: "4",
    },
    sell: {
      price: "1.758",
      num: "4",
    },
  },
  {
    name: "PNJ",
    date: "31/03/2021",
    time: "04:01:00",
    buy: {
      price: "2.032,45",
      num: "4",
    },
    sell: {
      price: "2.032,45",
      num: "4",
    },
  },
  {
    name: "DOJI",
    date: "31/03/2021",
    time: "04:01:00",
    buy: {
      price: "4.784.803",
      num: "4",
    },
    sell: {
      price: "4.784.803",
      num: "4",
    },
  },
  {
    name: "HNG",
    date: "31/03/2021",
    time: "04:01:00",
    buy: {
      price: "1.292.786",
      num: "4",
    },
    sell: {
      price: "1.292.786",
      num: "4",
    },
  },
  {
    name: "SJC",
    date: "31/03/2021",
    time: "04:01:00",
    buy: {
      price: "1.758",
      num: "4",
    },
    sell: {
      price: "1.758",
      num: "4",
    },
  },
  {
    name: "PNJ",
    date: "31/03/2021",
    time: "04:01:00",
    buy: {
      price: "2.032,45",
      num: "4",
    },
    sell: {
      price: "2.032,45",
      num: "4",
    },
  },
  {
    name: "DOJI",
    date: "31/03/2021",
    time: "04:01:00",
    buy: {
      price: "4.784.803",
      num: "4",
    },
    sell: {
      price: "4.784.803",
      num: "4",
    },
  },
  {
    name: "HNG",
    date: "31/03/2021",
    time: "04:01:00",
    buy: {
      price: "1.292.786",
      num: "4",
    },
    sell: {
      price: "1.292.786",
      num: "4",
    },
  },
];
