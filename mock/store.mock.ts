import ImgStore1 from "../public/images/mock/store/img1.png";
import ImgStore2 from "../public/images/mock/store/img2.png";
import Banner1 from "../public/images/hng-app/img1.png";
import Banner2 from "../public/images/hng-app/img2.png";
import Banner3 from "../public/images/hng-app/img3.png";
import Banner4 from "../public/images/hng-app/img4.png";
import Banner5 from "../public/images/hng-app/img5.png";
import Banner6 from "../public/images/hng-app/img6.png";

export const Store = [
  {
    id: "1",
    img: Banner1,
  },
  { id: "2", img: Banner2 },
  { id: "3", img: Banner3 },
  { id: "4", img: Banner4 },
  { id: "5", img: Banner5 },
  { id: "6", img: Banner6 },
];

export type StoreItem = {
  id: string;
  img: StaticImageData;
};

export const Branch = [
  {
    img: ImgStore1,
    name: "Hồ Chí Minh, chi nhánh quận 7",
    address: "81 Nguyễn Hiền, KDC 91B, Phường An Khanh, Ninh Kiều",
  },
  {
    name: "Chi nhánh Tân Phú",
    img: ImgStore2,
    address: "81 Nguyễn Hiền, KDC 91B, Phường An Khanh, Ninh Kiều",
  },
  {
    img: ImgStore1,
    name: "Hồ Chí Minh, chi nhánh quận 7",
    address: "81 Nguyễn Hiền, KDC 91B, Phường An Khanh, Ninh Kiều",
  },
];
