/* eslint-disable */
export const MOCK_DANCING_STONE_PRODUCTS = [
  {
    id: "1",
    img: `/images/mock/products-stone/1.gif`,
    title: "Dây chuyền Dancing Love Diamond Vàng trắng K18",
    price: "2.290.000đ",
    discount: "20",
  },
  {
    id: "2",
    img: `/images/mock/products-stone/2.gif`,
    title: "Dây chuyền Dancing Love Diamond Vàng trắng K18",
    price: "2.290.000đ",
    discount: "20",
  },
  {
    id: "3",
    img: `/images/mock/products-stone/3.gif`,
    title: "Dây chuyền Dancing Love Diamond Vàng trắng K18",
    price: "2.290.000đ",
    discount: "20",
  },
  {
    id: "4",
    img: `/images/mock/products-stone/4.gif`,
    title: "Dây chuyền Dancing Love Diamond Vàng trắng K18",
    price: "2.290.000đ",
    discount: "",
  },
  {
    id: "5",
    img: `/images/mock/products-stone/5.gif`,
    title: "Dây chuyền Dancing Love Diamond Vàng trắng K18",
    price: "2.290.000đ",
    discount: "",
  },
  {
    id: "6",
    img: `/images/mock/products-stone/6.gif`,
    title: "Dây chuyền Dancing Love Diamond Vàng trắng K18",
    price: "2.290.000đ",
    discount: "",
  },
  {
    id: "7",
    img: `/images/mock/products-stone/7.gif`,
    title: "Dây chuyền Dancing Love Diamond Vàng trắng K18",
    price: "2.290.000đ",
    discount: "",
  },
  {
    id: "8",
    img: `/images/mock/products-stone/8.gif`,
    title: "Dây chuyền Dancing Love Diamond Vàng trắng K18",
    price: "2.290.000đ",
    discount: "",
  },
  {
    id: "9",
    img: `/images/mock/products-stone/9.gif`,
    title: "Dây chuyền Dancing Love Diamond Vàng trắng K18",
    price: "2.290.000đ",
    discount: "",
  },
  {
    id: "10",
    img: `/images/mock/products-stone/10.gif`,
    title: "Dây chuyền Dancing Love Diamond Vàng trắng K18",
    price: "2.290.000đ",
    discount: "20",
  },
  {
    id: "11",
    img: `/images/mock/products-stone/4.gif`,
    title: "Dây chuyền Dancing Love Diamond Vàng trắng K18",
    price: "2.290.000đ",
    discount: "",
  },
  {
    id: "12",
    img: `/images/mock/products-stone/5.gif`,
    title: "Dây chuyền Dancing Love Diamond Vàng trắng K18",
    price: "2.290.000đ",
    discount: "",
  },
];

export const MOCK_PRODUCTS = [
  {
    id: "1",
    img: `images/mock/products/1.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    price: "2.290.000đ",
    discount: "20",
  },
  {
    id: "2",
    img: `images/mock/products/2.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    price: "2.290.000đ",
    discount: "20",
  },
  {
    id: "3",
    img: `images/mock/products/3.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    price: "2.290.000đ",
    discount: "",
  },
  {
    id: "4",
    img: `images/mock/products/4.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    price: "2.290.000đ",
    discount: "",
  },
  {
    id: "5",
    img: `images/mock/products/5.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    price: "2.290.000đ",
    discount: "20",
  },
  {
    id: "6",
    img: `images/mock/products/6.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    price: "2.290.000đ",
    discount: "",
  },
  {
    id: "7",
    img: `images/mock/products/1.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    price: "2.290.000đ",
    discount: "20",
  },
  {
    id: "8",
    img: `images/mock/products/2.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    price: "2.290.000đ",
    discount: "20",
  },
  {
    id: "9",
    img: `images/mock/products/3.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    price: "2.290.000đ",
    discount: "",
  },
  {
    id: "10",
    img: `images/mock/products/4.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    price: "2.290.000đ",
    discount: "",
  },
  {
    id: "11",
    img: `images/mock/products/5.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    price: "2.290.000đ",
    discount: "20",
  },
  {
    id: "12",
    img: `images/mock/products/6.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    price: "2.290.000đ",
    discount: "",
  },
];

export const SPECIFICATION_MOCK = [
  {
    specification: [
      {
        name: "Bộ Sưu Tập",
        description: "Young Generation",
      },
      {
        name: "Trọng lượng vàng",
        description: "20g",
      },
      {
        name: "Loại đá chính",
        description: "CZ",
      },
      {
        name: "Chất liệu",
        description: "Vàng",
      },
      {
        name: "Màu chất liệu",
        description: "Vàng trắng",
      },
      {
        name: "Loại đá phụ - Màu đá phụ",
        description: "Đá Crystal - Vàng hồng",
      },
      {
        name: "Tuổi vàng",
        description: "Vàng 14k",
      },
      {
        name: "Độ tinh khiết",
        description: "90%",
      },
      {
        name: "Giới tính",
        description: "Nữ",
      },
      {
        name: "Loại sản phẩm",
        description: "Dây chuyền",
      },
      {
        name: "Hình dạng đá chính",
        description: "Tròn",
      },
    ],
  },
] as const;

export const SIZE = [
  { size: "2" },
  { size: "3" },
  { size: "4" },
  { size: "5" },
  { size: "6" },
  { size: "2.5" },
  { size: "3.5" },
  { size: "4.5" },
  { size: "5.5" },
  { size: "6.5" },
] as const;

export const SPECS_BY_SLUG = [
  {
    //1
    slug: "day-chuyen-chu-love-vang",
    specification: [
      {
        name: "Thương hiệu",
        description: "HanaGold",
      },
      {
        name: "Bộ sưu tập",
        description: "Happy Stone - Love",
      },
      {
        name: "Loại đá chính",
        description: "CZ",
      },
      {
        name: "Màu đá chính",
        description: "Trắng",
      },
      {
        name: "Loại đá phụ (nếu có)",
        description: "CZ",
      },
      {
        name: "Giới tính",
        description: "Nữ",
      },
      {
        name: "Dịp tặng quà",
        description: "Sinh nhật, Tình yêu, Ngày kỷ niệm",
      },
      {
        name: "Quà tặng cho người thân",
        description: "Cho Nàng",
      },
      {
        name: "Chủng loại",
        description: "Dây chuyền",
      },
      {
        name: "Chất liệu",
        description: "Vàng",
      },
    ],
  },
  {
    //2
    slug: "day-chuyen-chu-love-bac",
    specification: [
      {
        name: "Thương hiệu",
        description: "HanaGold",
      },
      {
        name: "Bộ sưu tập",
        description: "Happy Stone - Love",
      },
      {
        name: "Loại đá chính",
        description: "CZ",
      },
      {
        name: "Màu đá chính",
        description: "Trắng",
      },
      {
        name: "Loại đá phụ (nếu có)",
        description: "CZ",
      },
      {
        name: "Giới tính",
        description: "Nữ",
      },
      {
        name: "Dịp tặng quà",
        description: "Sinh nhật, Tình yêu, Ngày kỷ niệm",
      },
      {
        name: "Quà tặng cho người thân",
        description: "Cho Nàng",
      },
      {
        name: "Chủng loại",
        description: "Dây chuyền",
      },
      {
        name: "Chất liệu",
        description: "Bạc 950",
      },
    ],
  },
  {
    //3
    slug: "day-chuyen-chu-love-xi-trang",
    specification: [
      {
        name: "Thương hiệu",
        description: "HanaGold",
      },
      {
        name: "Bộ sưu tập",
        description: "Happy Stone - Love",
      },
      {
        name: "Loại đá chính",
        description: "CZ",
      },
      {
        name: "Màu đá chính",
        description: "Trắng",
      },
      {
        name: "Loại đá phụ (nếu có)",
        description: "CZ",
      },
      {
        name: "Giới tính",
        description: "Nữ",
      },
      {
        name: "Dịp tặng quà",
        description: "Sinh nhật, Tình yêu, Ngày kỷ niệm",
      },
      {
        name: "Quà tặng cho người thân",
        description: "Cho Nàng",
      },
      {
        name: "Chủng loại",
        description: "Dây chuyền",
      },
      {
        name: "Chất liệu",
        description: "Xi Trắng",
      },
    ],
  },
  {
    //4
    slug: "day-chuyen-vuong-mien-trai-tim-vang",
    specification: [
      {
        name: "Thương hiệu",
        description: "HanaGold",
      },
      {
        name: "Bộ sưu tập",
        description: "Happy Stone - The Queen",
      },
      {
        name: "Loại đá chính",
        description: "CZ",
      },
      {
        name: "Màu đá chính",
        description: "Trắng",
      },
      {
        name: "Loại đá phụ (nếu có)",
        description: "CZ",
      },
      {
        name: "Giới tính",
        description: "Nữ",
      },
      {
        name: "Dịp tặng quà",
        description: "Sinh nhật, Tình yêu, Ngày kỷ niệm",
      },
      {
        name: "Quà tặng cho người thân",
        description: "Cho Nàng",
      },
      {
        name: "Chủng loại",
        description: "Dây chuyền",
      },
      {
        name: "Chất liệu",
        description: "Vàng",
      },
    ],
  },
  {
    //5
    slug: "day-chuyen-vuong-mien-trai-tim-bac",
    specification: [
      {
        name: "Thương hiệu",
        description: "HanaGold",
      },
      {
        name: "Bộ sưu tập",
        description: "Happy Stone - The Queen",
      },
      {
        name: "Loại đá chính",
        description: "CZ",
      },
      {
        name: "Màu đá chính",
        description: "Trắng",
      },
      {
        name: "Loại đá phụ (nếu có)",
        description: "CZ",
      },
      {
        name: "Giới tính",
        description: "Nữ",
      },
      {
        name: "Dịp tặng quà",
        description: "Sinh nhật, Tình yêu, Ngày kỷ niệm",
      },
      {
        name: "Quà tặng cho người thân",
        description: "Cho Nàng",
      },
      {
        name: "Chủng loại",
        description: "Dây chuyền",
      },
      {
        name: "Chất liệu",
        description: "Bạc 950",
      },
    ],
  },
  {
    //6
    slug: "day-chuyen-vuong-mien-trai-tim-xi-trang",
    specification: [
      {
        name: "Thương hiệu",
        description: "HanaGold",
      },
      {
        name: "Bộ sưu tập",
        description: "Happy Stone - The Queen",
      },
      {
        name: "Loại đá chính",
        description: "CZ",
      },
      {
        name: "Màu đá chính",
        description: "Trắng",
      },
      {
        name: "Loại đá phụ (nếu có)",
        description: "CZ",
      },
      {
        name: "Giới tính",
        description: "Nữ",
      },
      {
        name: "Dịp tặng quà",
        description: "Sinh nhật, Tình yêu, Ngày kỷ niệm",
      },
      {
        name: "Quà tặng cho người thân",
        description: "Cho Nàng",
      },
      {
        name: "Chủng loại",
        description: "Dây chuyền",
      },
      {
        name: "Chất liệu",
        description: "Xi Trắng",
      },
    ],
  },
  {
    //7
    slug: "day-chuyen-mat-tron-vang",
    specification: [
      {
        name: "Thương hiệu",
        description: "HanaGold",
      },
      {
        name: "Bộ sưu tập",
        description: "Happy Stone - LoveMoon",
      },
      {
        name: "Loại đá chính",
        description: "CZ",
      },
      {
        name: "Màu đá chính",
        description: "Trắng",
      },
      {
        name: "Loại đá phụ (nếu có)",
        description: "CZ",
      },
      {
        name: "Giới tính",
        description: "Nữ",
      },
      {
        name: "Dịp tặng quà",
        description: "Sinh nhật, Tình yêu, Ngày kỷ niệm",
      },
      {
        name: "Quà tặng cho người thân",
        description: "Cho Nàng",
      },
      {
        name: "Chủng loại",
        description: "Dây chuyền",
      },
      {
        name: "Chất liệu",
        description: "Vàng",
      },
    ],
  },
  {
    //8
    slug: "day-chuyen-mat-tron-bac",
    specification: [
      {
        name: "Thương hiệu",
        description: "HanaGold",
      },
      {
        name: "Bộ sưu tập",
        description: "Happy Stone - LoveMoon",
      },
      {
        name: "Loại đá chính",
        description: "CZ",
      },
      {
        name: "Màu đá chính",
        description: "Trắng",
      },
      {
        name: "Loại đá phụ (nếu có)",
        description: "CZ",
      },
      {
        name: "Giới tính",
        description: "Nữ",
      },
      {
        name: "Dịp tặng quà",
        description: "Sinh nhật, Tình yêu, Ngày kỷ niệm",
      },
      {
        name: "Quà tặng cho người thân",
        description: "Cho Nàng",
      },
      {
        name: "Chủng loại",
        description: "Dây chuyền",
      },
      {
        name: "Chất liệu",
        description: "Bạc 950",
      },
    ],
  },
  {
    //9
    slug: "day-chuyen-mat-tron-xi-trang",
    specification: [
      {
        name: "Thương hiệu",
        description: "HanaGold",
      },
      {
        name: "Bộ sưu tập",
        description: "Happy Stone - Love",
      },
      {
        name: "Loại đá chính",
        description: "CZ",
      },
      {
        name: "Màu đá chính",
        description: "Trắng",
      },
      {
        name: "Loại đá phụ (nếu có)",
        description: "CZ",
      },
      {
        name: "Giới tính",
        description: "Nữ",
      },
      {
        name: "Dịp tặng quà",
        description: "Sinh nhật, Tình yêu, Ngày kỷ niệm",
      },
      {
        name: "Quà tặng cho người thân",
        description: "Cho Nàng",
      },
      {
        name: "Chủng loại",
        description: "Dây chuyền",
      },
      {
        name: "Chất liệu",
        description: "Xi Trắng",
      },
    ],
  },
  {
    //10
    slug: "day-chuyen-mat-buom-vang",
    specification: [
      {
        name: "Thương hiệu",
        description: "HanaGold",
      },
      {
        name: "Bộ sưu tập",
        description: "Happy Stone - Hồ Điệp",
      },
      {
        name: "Loại đá chính",
        description: "CZ",
      },
      {
        name: "Màu đá chính",
        description: "Trắng",
      },
      {
        name: "Loại đá phụ (nếu có)",
        description: "CZ",
      },
      {
        name: "Giới tính",
        description: "Nữ",
      },
      {
        name: "Dịp tặng quà",
        description: "Sinh nhật, Tình yêu, Ngày kỷ niệm",
      },
      {
        name: "Quà tặng cho người thân",
        description: "Cho Nàng",
      },
      {
        name: "Chủng loại",
        description: "Dây chuyền",
      },
      {
        name: "Chất liệu",
        description: "Vàng",
      },
    ],
  },
  {
    //11
    slug: "day-chuyen-mat-buom-bac",
    specification: [
      {
        name: "Thương hiệu",
        description: "HanaGold",
      },
      {
        name: "Bộ sưu tập",
        description: "Happy Stone - Hồ Điệp",
      },
      {
        name: "Loại đá chính",
        description: "CZ",
      },
      {
        name: "Màu đá chính",
        description: "Trắng",
      },
      {
        name: "Loại đá phụ (nếu có)",
        description: "CZ",
      },
      {
        name: "Giới tính",
        description: "Nữ",
      },
      {
        name: "Dịp tặng quà",
        description: "Sinh nhật, Tình yêu, Ngày kỷ niệm",
      },
      {
        name: "Quà tặng cho người thân",
        description: "Cho Nàng",
      },
      {
        name: "Chủng loại",
        description: "Dây chuyền",
      },
      {
        name: "Chất liệu",
        description: "Bạc 950",
      },
    ],
  },
  {
    //12
    slug: "day-chuyen-mat-buom-xi-trang",
    specification: [
      {
        name: "Thương hiệu",
        description: "HanaGold",
      },
      {
        name: "Bộ sưu tập",
        description: "Happy Stone - Hồ Điệp",
      },
      {
        name: "Loại đá chính",
        description: "CZ",
      },
      {
        name: "Màu đá chính",
        description: "Trắng",
      },
      {
        name: "Loại đá phụ (nếu có)",
        description: "CZ",
      },
      {
        name: "Giới tính",
        description: "Nữ",
      },
      {
        name: "Dịp tặng quà",
        description: "Sinh nhật, Tình yêu, Ngày kỷ niệm",
      },
      {
        name: "Quà tặng cho người thân",
        description: "Cho Nàng",
      },
      {
        name: "Chủng loại",
        description: "Dây chuyền",
      },
      {
        name: "Chất liệu",
        description: "Xi Trắng",
      },
    ],
  },
] as const;
