import React from "react";
import { DEFAULT_IMG_ALT } from "@Constants";
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Grid,
  BottomNavigationAction,
  BottomNavigation,
} from "@material-ui/core";
import { ArrowDropDown as ArrowDropDownIcon } from "@material-ui/icons";
import { useBreakpoints } from "../../hooks";
import Image from "next/image";
import Icon from "../../public/images/banner/Vector.png";

const PACKAGES = [
  {
    id: "0",
    package: (
      <span>
        500 TRIỆU
        <br />
        <br />
        ĐỒNG
      </span>
    ),
  },
  {
    id: "1",
    package: "1 TỶ ĐỒNG",
  },
  {
    id: "2",
    package: "2 TỶ ĐỒNG",
  },
];

const SHOWROOM_DATA: { title: string; content: { [key: string]: React.ReactNode }[] }[] = [
  {
    title: "1. Trang trí nội thất",
    content: [
      {
        name: "bảng hiệu hanagold",
        num0: "có",
        num1: "có",
        num2: "có",
      },
      {
        name: "tủ trưng bày trang sức ",
        num0: "có",
        num1: "có",
        num2: "có",
      },
      {
        name: "tủ trưng bày quà tặng",
        num0: "có",
        num1: "có",
        num2: "có",
      },
      {
        name: "quầy dịch vụ",
        num0: "có",
        num1: "có",
        num2: "có",
      },
      {
        name: "banner, decal trang trí",
        num0: "có",
        num1: "có",
        num2: "có",
      },
    ],
  },
  {
    title: "2. Trang thiết bị",
    content: [
      {
        name: "điện thoại hoặc máy tính bảng",
        num0: "tự trang bị",
        num1: "tự trang bị",
        num2: "tự trang bị",
      },
      {
        name: "camera/wifi",
        num0: "tự trang bị",
        num1: "tự trang bị",
        num2: "tự trang bị",
      },
      {
        name: "máy bán hàng",
        num0: "có",
        num1: "có",
        num2: "có",
      },
      {
        name: "catalogue sản phẩm trang sức, quà tặng",
        num0: "có",
        num1: "có",
        num2: "có",
      },
      {
        name: "brochure, tờ tơi, danh thiếp",
        num0: "có",
        num1: "có",
        num2: "có",
      },
    ],
  },
  {
    title: "3. Hàng hóa",
    content: [
      {
        name: "Đồng vàng HanaGold ép vỉ",
        num0: "10 chỉ",
        num1: "30 chỉ",
        num2: "60 chỉ",
      },
      {
        name: "CHI",
        num0: "15 CHI",
        num1: "35 CHI",
        num2: "80 CHI",
      },
      {
        name: "Sản phẩm quà tặng",
        num0: "50 triệu",
        num1: "80 triệu",
        num2: "150 triệu",
      },
      {
        name: "Trang sức bạc Happy Stone",
        num0: "10 mẫu",
        num1: "30 mẫu",
        num2: "50 mẫu",
      },
    ],
  },
  {
    title: "4. Nền tảng kinh doanh",
    content: [
      {
        name: "Nền tảng công nghệ HanaGold (web, mobile app)",
        num0: "có",
        num1: "có",
        num2: "có",
      },
      {
        name: "Phần mềm quản lý bán hàng tại showroom",
        num0: "có",
        num1: "có",
        num2: "có",
      },
    ],
  },
  {
    title: "5. Truyền thông",
    content: [
      {
        name: "Truyền thông và marketing trong 3 tháng đầu kinh doanh",
        num0: "có",
        num1: "có",
        num2: "có",
      },
      {
        name: "Tập khách hàng tại khu vực kinh doanh",
        num0: "có",
        num1: "có",
        num2: "có",
      },
      {
        name: "Đào tạo nhân sự",
        num0: "có",
        num1: "có",
        num2: "có",
      },
      {
        name: "Hỗ trợ đổi mẫu mã sản phẩm quà tặng, trang sức nếu không bán được trong 3 tháng ",
        num0: (
          <span>
            Hỗ trợ đổi
            <br />
            mẫu 5 lần
          </span>
        ),
        num1: (
          <span>
            Hỗ trợ đổi
            <br />
            mẫu 15 lần
          </span>
        ),
        num2: (
          <span>
            đổi bất cứ
            <br />
            lúc nào
          </span>
        ),
      },
    ],
  },
];
const Showroom: React.FC = () => {
  const breakpoints = useBreakpoints();
  const [value, setValue] = React.useState<string>("0");
  return (
    <>
      <div className="showroom">
        <div className="showroom__title">CÁC GÓI SHOWROOM</div>
        {breakpoints.lg && (
          <>
            <Grid
              item
              container
              direction="row"
              justify="space-between"
              className="showroom__item showroom__item__list">
              <Grid item lg={3} className="showroom__item__title">
                Chọn gói&nbsp;&nbsp;&nbsp;&nbsp;
                <Image src={Icon} alt={DEFAULT_IMG_ALT} objectFit="contain" />
              </Grid>
              {PACKAGES.map((p) => {
                return (
                  <Grid
                    key={p.id}
                    item
                    container
                    justify="center"
                    alignItems="center"
                    lg={3}
                    className="showroom__item__title">
                    {p.package}
                  </Grid>
                );
              })}
            </Grid>

            {SHOWROOM_DATA.map((data, index: number) => {
              return (
                <Accordion key={index} defaultExpanded={true}>
                  <AccordionSummary
                    expandIcon={<ArrowDropDownIcon className="showroom__icon" />}
                    className="showroom__name">
                    {data.title}
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid
                      container
                      direction="column"
                      justify="space-between"
                      alignItems="flex-start">
                      {data.content.map((o, index: number) => {
                        return (
                          <Grid
                            key={index}
                            item
                            container
                            direction="row"
                            justify="space-between"
                            className="showroom__item">
                            <Grid item lg={3} className="showroom__item__name">
                              {o.name}
                            </Grid>
                            <Grid item container justify="center" alignItems="center" lg={3}>
                              {o.num0}
                            </Grid>
                            <Grid item container justify="center" alignItems="center" lg={3}>
                              {o.num1}
                            </Grid>
                            <Grid item container justify="center" alignItems="center" lg={3}>
                              {o.num2}
                            </Grid>
                          </Grid>
                        );
                      })}
                    </Grid>
                  </AccordionDetails>
                </Accordion>
              );
            })}
          </>
        )}
      </div>

      {!breakpoints.lg && (
        <div className="showroom-mobile">
          <BottomNavigation
            value={parseInt(value)}
            onChange={(_, newValue) => {
              setValue(newValue.toString());
            }}
            showLabels>
            {PACKAGES.map((p) => {
              return <BottomNavigationAction key={p.id} label={p.package} />;
            })}
          </BottomNavigation>
          {SHOWROOM_DATA.map((data, i) => {
            return (
              <Grid
                key={data.title}
                container
                direction="column"
                justify="space-between"
                alignItems="flex-start"
                className="showroom-mobile__list">
                <Grid item container justify="space-between">
                  <Grid item xs={8} container className="showroom__name">
                    {data.title}
                  </Grid>
                  {PACKAGES.map((p) => {
                    if (i === 0 && p.id === value)
                      return (
                        <Grid
                          key={p.id}
                          xs={4}
                          item
                          container
                          justify="center"
                          className="showroom__name">
                          {p.package}
                        </Grid>
                      );
                  })}
                </Grid>
                {data.content.map((o, index: number) => {
                  return (
                    <Grid
                      item
                      container
                      direction="row"
                      justify="space-between"
                      className="showroom__item"
                      key={index}>
                      <Grid item xs={8} className="showroom__item__name">
                        {o.name}
                      </Grid>
                      <Grid item container justify="center" alignItems="center" xs={4}>
                        {o[`num${value}`]}
                      </Grid>
                    </Grid>
                  );
                })}
              </Grid>
            );
          })}
        </div>
      )}
    </>
  );
};

export default Showroom;
