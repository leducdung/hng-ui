import React from "react";
import { Dialog } from "@material-ui/core";
import { Check as CheckIcon } from "@material-ui/icons";

type Props = {
  show: boolean;
  title: string;
  content: string;
  children: React.ReactNode;
};
const NotificationModal: React.FC<Props> = ({ show, title, content, children }) => {
  return (
    <Dialog open={show}>
      <div className="notification-modal">
        <div className="icon">
          <div className={`pulse yellow`}>
            <div className="notification-icon-box gold">
              <CheckIcon style={{ fontSize: "5rem", color: "#fff" }} />
            </div>
          </div>
        </div>
        <div className="title">{title}</div>
        <div className="content">{content}</div>
        <div className="actions">{children}</div>
      </div>
    </Dialog>
  );
};

export default React.memo(NotificationModal);
