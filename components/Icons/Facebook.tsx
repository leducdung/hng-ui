import React from "react";
import { IconModel } from "@Models";
import { SvgCommon } from "./SvgCommon";

export const FacebookIcon: React.FC<IconModel> = (props) => {
  return (
    <SvgCommon {...props}>
      <path
        d="M6.08 2.952H7.328V0.823999C6.72 0.76 6.112 0.728 5.504 0.728C4.576 0.728 3.84 0.994666 3.296 1.528C2.73067 2.09333 2.448 2.872 2.448 3.864V5.608H0.4V7.992H2.448V14.072H4.912V7.992H6.944L7.248 5.608H4.912V4.104C4.912 3.70933 4.98133 3.42667 5.12 3.256C5.30133 3.05333 5.62133 2.952 6.08 2.952Z"
        fill={props.color}
      />
    </SvgCommon>
  );
};
