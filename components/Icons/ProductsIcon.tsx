import React from "react";
import { SvgCommon } from "./SvgCommon";
import { IconModel } from "@Models";

export const ProductsIcon: React.FC<IconModel> = (props) => {
  return (
    <SvgCommon {...props}>
      <svg
        width="14"
        height="15"
        viewBox="0 0 14 15"
        fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path
          d="M1 4.40625L7 6.8125M1 4.40625V10.5938L7 13M1 4.40625L4 3.20312L7 2L13 4.40625M7 6.8125L10 5.60938L13 4.40625M7 6.8125V13M13 4.40625V10.5938L7 13"
          stroke={props.color}
          strokeWidth="2"
        />
      </svg>
    </SvgCommon>
  );
};
