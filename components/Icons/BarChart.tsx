import React from "react";
import { IconModel } from "@Models";
import { SvgCommon } from "./SvgCommon";

export const BarChartIcon: React.FC<IconModel> = (props) => {
  return (
    <SvgCommon {...props}>
      <path
        d="M27.9851 16.3246V23.3209C27.9851 24.6089 26.941 25.653 25.653 25.653H20.1567V16.3246C20.1567 15.0367 21.2008 13.9925 22.4888 13.9925H25.653C26.941 13.9925 27.9851 15.0367 27.9851 16.3246Z"
        fill={props.color}
      />
      <path
        d="M16.3246 0H11.6605C10.3725 0 9.32837 1.04411 9.32837 2.33209V25.653H18.6567V2.33209C18.6567 1.04411 17.6126 0 16.3246 0Z"
        fill={props.color}
      />
      <path
        d="M0 23.3209V11.6605C0 10.3725 1.04411 9.32837 2.33209 9.32837H5.49627C6.78425 9.32837 7.82837 10.3725 7.82837 11.6605V25.653H2.33209C1.04411 25.653 0 24.6089 0 23.3209Z"
        fill={props.color}
      />
    </SvgCommon>
  );
};
