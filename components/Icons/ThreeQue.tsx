import React from "react";
import { IconModel } from "@Models";
import { SvgCommon } from "./SvgCommon";

export const ThreeQueIcon: React.FC<IconModel> = (props) => {
  return (
    <SvgCommon {...props}>
      <path
        d="M24 2H8.6M24 10H2M24 18H8.6"
        stroke={props.color}
        strokeWidth="4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </SvgCommon>
  );
};
