import React from "react";
import { SvgCommon } from "./SvgCommon";
import { IconModel } from "@Models";

export const InvoiceIcon: React.FC<IconModel> = (props) => {
  return (
    <SvgCommon {...props}>
      <svg
        width="19"
        height="23"
        viewBox="0 0 19 23"
        fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path
          d="M14.1924 17L4.19238 17L4.19238 19L14.1924 19L14.1924 17ZM14.1924 14L4.19238 14L4.19238 16L14.1924 16L14.1924 14Z"
          fill={props.color}
        />
        <rect
          x="1.19238"
          y="1.5"
          width="16"
          height="20"
          rx="1"
          stroke={props.color}
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M9.3252 4.5V11.7"
          stroke={props.color}
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M10.859 5.5H8.35905C8.04963 5.5 7.75288 5.6317 7.53409 5.86612C7.3153 6.10054 7.19238 6.41848 7.19238 6.75C7.19238 7.08152 7.3153 7.39946 7.53409 7.63388C7.72019 7.82211 7.91238 8.00004 8.27238 8M7.19238 10.5H10.0257C10.3351 10.5 10.6319 10.3683 10.8507 10.1339C11.0695 9.89946 11.1924 9.58152 11.1924 9.25C11.1924 8.91848 11.0695 8.60054 10.8507 8.36612C10.6319 8.1317 10.3351 8 10.0257 8H9.35238"
          stroke={props.color}
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    </SvgCommon>
  );
};
