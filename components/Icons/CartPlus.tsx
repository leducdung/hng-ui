import React from "react";
import { IconModel } from "@Models";
import { SvgCommon } from "./SvgCommon";

export const CartPlusIcon: React.FC<IconModel> = (props) => {
  return (
    <SvgCommon {...props}>
      <mask
        id="mask0"
        mask-type="alpha"
        maskUnits="userSpaceOnUse"
        x="0"
        y="0"
        width="24"
        height="24">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M18 0H0V24H24V6V0H18ZM18 0C21.3137 0 24 2.68629 24 6C24 9.31371 21.3137 12 18 12C14.6863 12 12 9.31371 12 6C12 2.68629 14.6863 0 18 0Z"
          fill="#BDC6D7"
        />
      </mask>
      <g mask="url(#mask0)">
        <path
          d="M7.79169 22.1665C8.55108 22.1665 9.16669 21.5509 9.16669 20.7915C9.16669 20.0321 8.55108 19.4165 7.79169 19.4165C7.0323 19.4165 6.41669 20.0321 6.41669 20.7915C6.41669 21.5509 7.0323 22.1665 7.79169 22.1665Z"
          stroke={props.color}
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M15.125 22.1665C15.8844 22.1665 16.5 21.5509 16.5 20.7915C16.5 20.0321 15.8844 19.4165 15.125 19.4165C14.3656 19.4165 13.75 20.0321 13.75 20.7915C13.75 21.5509 14.3656 22.1665 15.125 22.1665Z"
          stroke={props.color}
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M2.74942 4.75C4.17779 4.75 4.17779 5.45118 4.89228 7.55416L4.93686 7.72917M16.9994 16.6667H7.61247C7.27469 16.6667 7.1058 16.2887 7.03513 15.9655L6.49884 13.9109M4.93686 7.72917H19.2494L16.8537 13.9109H6.49884M4.93686 7.72917L6.49884 13.9109"
          stroke={props.color}
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </g>
      <path
        d="M17.9999 3V9M21 5.99994H15"
        stroke={props.color}
        strokeWidth="1.8"
        strokeLinecap="round"
      />
    </SvgCommon>
  );
};
