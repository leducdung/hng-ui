import React from "react";
import { IconModel } from "@Models";
import { SvgCommon } from "./SvgCommon";

export const PlusIcon: React.FC<IconModel> = (props) => {
  return (
    <SvgCommon {...props}>
      <path
        d="M14.853 8.81385H8.90156V14.7653H6.05684V8.81385H0.105379V5.9317H6.05684V0.0176705H8.90156V5.9317H14.853V8.81385Z"
        fill={props.color}
      />
    </SvgCommon>
  );
};
