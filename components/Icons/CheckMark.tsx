import React from "react";
import { IconModel } from "@Models";
import { SvgCommon } from "./SvgCommon";

export const CheckMarkIcon: React.FC<IconModel> = (props) => {
  return (
    <SvgCommon {...props}>
      <svg
        width="15"
        height="12"
        viewBox="0 0 15 12"
        fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path
          d="M1.66675 5.26873L6.01675 9.72917L13.7501 1.875"
          stroke={props.color}
          strokeWidth="2"
          strokeLinecap="round"
        />
      </svg>
    </SvgCommon>
  );
};
