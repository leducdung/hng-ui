import React from "react";
import { SvgCommon } from "./SvgCommon";
import { IconModel } from "@Models";

export const GoodsIcon: React.FC<IconModel> = (props) => {
  return (
    <SvgCommon {...props}>
      <path
        d="M25.04 3H2.96C2.42981 3 2 3.42869 2 3.95751V24.0425C2 24.5713 2.42981 25 2.96 25H25.04C25.5702 25 26 24.5713 26 24.0425V3.95751C26 3.42869 25.5702 3 25.04 3Z"
        stroke={props.color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M18.3644 14.4369L14.0003 11.3058L9.63708 14.4369V3H18.3644V14.4369Z"
        stroke={props.color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </SvgCommon>
  );
};
