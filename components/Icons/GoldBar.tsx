import React from "react";
import { IconModel } from "@Models";
import { SvgCommon } from "./SvgCommon";

export const GoldBarIcon: React.FC<IconModel> = (props) => {
  return (
    <SvgCommon {...props}>
      <path
        d="M0.5 32.0001L2.75 24.5001H13.25L15.5 32.0001H0.5ZM18.5 32.0001L20.75 24.5001H31.25L33.5 32.0001H18.5ZM8 21.5001L10.25 14.0001H20.75L23 21.5001H8ZM33.5 8.07515L27.71 9.71015L26.075 15.5001L24.44 9.71015L18.65 8.07515L24.44 6.44015L26.075 0.650146L27.71 6.44015L33.5 8.07515Z"
        fill={props.color}
      />
    </SvgCommon>
  );
};
