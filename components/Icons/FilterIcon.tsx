import React from "react";
import { IconModel } from "@Models";
import { SvgCommon } from "./SvgCommon";

export const FilterIcon: React.FC<IconModel> = (props) => {
  return (
    <SvgCommon {...props}>
      <path d="M6.5 4H19.5" stroke={props.color} strokeWidth="2" />
      <path d="M1.5 16H3.5M19.5 16H11.5" stroke={props.color} strokeWidth="2" />
      <path d="M1.5 10H13.5" stroke={props.color} strokeWidth="2" />
      <circle cx="4.5" cy="4" r="2" stroke={props.color} strokeWidth="2" />
      <circle cx="15.5" cy="10" r="2" stroke={props.color} strokeWidth="2" />
      <circle cx="7.5" cy="16" r="2" stroke={props.color} strokeWidth="2" />
    </SvgCommon>
  );
};
