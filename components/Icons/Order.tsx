import React from "react";
import { IconModel } from "@Models";
import { SvgCommon } from "./SvgCommon";

export const OrderIcon: React.FC<IconModel> = (props) => {
  return (
    <SvgCommon {...props}>
      <path
        d="M4.66666 1.16675V0.166748H3.66666V1.16675H4.66666ZM17.5 7.00008H16.5V8.00008H17.5V7.00008ZM23.3333 26.8334V27.8334H24.3333V26.8334H23.3333ZM4.66666 26.8334H3.66666V27.8334H4.66666V26.8334ZM17.5 1.16675L18.2071 0.459641C18.0196 0.272105 17.7652 0.166748 17.5 0.166748V1.16675ZM23.3333 7.00008H24.3333C24.3333 6.73486 24.228 6.48051 24.0404 6.29297L23.3333 7.00008ZM23.3333 25.8334H4.66666V27.8334H23.3333V25.8334ZM5.66666 26.8334V1.16675H3.66666V26.8334H5.66666ZM4.66666 2.16675H17.5V0.166748H4.66666V2.16675ZM16.5 1.16675V7.00008H18.5V1.16675H16.5ZM17.5 8.00008H23.3333V6.00008H17.5V8.00008ZM22.3333 7.00008V26.8334H24.3333V7.00008H22.3333ZM16.7929 1.87385L22.6262 7.70719L24.0404 6.29297L18.2071 0.459641L16.7929 1.87385Z"
        fill={props.color}
      />
      <path d="M14 22.75L14 12.25" stroke={props.color} strokeWidth="2" strokeLinecap="round" />
      <path
        d="M9.91666 22.75L9.91666 18.0833"
        stroke={props.color}
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        d="M18.0833 22.75L18.0833 14.5833"
        stroke={props.color}
        strokeWidth="2"
        strokeLinecap="round"
      />
    </SvgCommon>
  );
};
