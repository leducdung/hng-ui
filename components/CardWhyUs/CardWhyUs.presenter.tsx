import React, { useState } from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { DEFAULT_IMG_ALT } from "@Constants";
import { P_Props } from "./CardWhyUs.type";
import { useBreakpoints } from "../../hooks";

const CardWhyUsPresenter: React.FC<P_Props> = (props) => {
  const breakpoints = useBreakpoints();
  const [visibleViewerModal, setVisibleViewerModal] = useState<boolean>(false);

  const toggleViewerModal = () => {
    setVisibleViewerModal(!visibleViewerModal);
  };
  return (
    <article
      className="card-why-us__item"
      onClick={!breakpoints.md ? toggleViewerModal : undefined}>
      <div className="card-why-us__img">
        <LazyLoadImage alt={DEFAULT_IMG_ALT} src={props.img} />
      </div>
      <div className="card-why-us__info">
        <div className="card-why-us__title">{props.title}</div>
        <div
          className={
            !visibleViewerModal ? "card-why-us__description" : "card-why-us__description-active"
          }>
          {props.description}
        </div>
      </div>
    </article>
  );
};

export default CardWhyUsPresenter;
