export type P_Props = {
  img: string;
  title: string;
  description: string;
};
