import React from "react";
import clsx from "clsx";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Link from "next/link";
import Image from "next/image";
import styles from "./index.module.scss";
import HNGIcon from "../../public/app-icon.png";
import navigatorList, { ACTIVE_NAVBAR_ICON_COLOR, DEFAULT_NAVBAR_ICON_COLOR } from "./admin-routes";
import { useRouter } from "next/router";
import {
  AppBar,
  Toolbar,
  List,
  IconButton,
  Drawer,
  ListItem,
  ListItemIcon,
  Popover,
} from "@material-ui/core";
import { Menu as MenuIcon, MenuOpen as MenuOpenIcon } from "@material-ui/icons";
import SubNavbar from "./SubNavbar";
import AdminQuery from "@Lib/queries/Admin.graphql";
import { useLazyQuery } from "@apollo/client";
import { ADMIN_ROUTES } from "@Constants";

export type Props = {
  settingsPage?;
};

const drawerWidth = 250;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    appBar: {
      color: "#070d14",
      backgroundColor: "#fff",
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: "none",
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: "nowrap",
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: "hidden",
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9) + 1,
      },
    },
    toolbar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  })
);

// @TODO handle validate loading, error
const AdminLayout: React.FC<Props> = (props) => {
  const router = useRouter();
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [logout] = useLazyQuery(AdminQuery.Logout, {
    onCompleted: () => {
      router.push(ADMIN_ROUTES.LOGIN);
    },
  });
  const [anchorUserEl, setAnchorUserEl] = React.useState<HTMLElement | null>(null);
  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorUserEl(event.currentTarget);
  };
  const handleLogout = () => {
    try {
      logout();
    } catch (error) {}
  };
  const handleCloseUserMenu = () => {
    setAnchorUserEl(null);
  };
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  // const { data } = useQuery(CustomerQuery.GetData);
  const currentRoute = React.useMemo(() => {
    // reverse the routes to get longest first
    const routes = navigatorList.map(({ baseRoute }) => baseRoute).reverse();
    const selected = routes.find((route) => router.pathname.startsWith(route));
    return selected;
  }, [router.pathname]);

  const selectedRoute = React.useMemo(() => {
    const selected = navigatorList.find((route) => currentRoute === route.baseRoute);
    return selected;
  }, [currentRoute]);

  return (
    <section className={styles.appLayout}>
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, styles.appbar, {
          [classes.appBarShift]: open,
        })}>
        <Toolbar className={styles.center}>
          <div className={styles.left}>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, {
                [classes.hide]: open,
              })}>
              <MenuIcon className={styles.icon} />
            </IconButton>

            <div className={styles.title}> {selectedRoute?.headingLabel}</div>
          </div>
          <div onClick={(e) => handleOpenUserMenu(e)} className={styles["user-avatar"]}>
            <Image
              loader={({ src }) => src}
              src={"https://cdn.statically.io/avatar/admin"}
              alt="avatar"
              width={80}
              height={80}
            />
          </div>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, styles.sidebar, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}>
        <div className={classes.toolbar}>
          <Image src={HNGIcon} alt="HanaGold icon" width={50} height={50} />
          <IconButton onClick={handleDrawerClose}>
            <MenuOpenIcon className={styles.icon} />
          </IconButton>
        </div>
        <List className={styles["nav-menu"]}>
          {navigatorList.map(({ label, icon: IconComponent, iconConfigs, baseRoute }) => {
            const selected = currentRoute === baseRoute;
            const color = selected ? ACTIVE_NAVBAR_ICON_COLOR : DEFAULT_NAVBAR_ICON_COLOR;
            return (
              <Link key={baseRoute} passHref href={baseRoute}>
                <a>
                  <ListItem
                    onClick={() => {
                      router.push(baseRoute);
                    }}
                    className={`${styles["nav-item"]} ${
                      baseRoute === ADMIN_ROUTES.LOCATION ? styles["away"] : ""
                    }`}
                    button>
                    <ListItemIcon className={clsx(styles["nav-icon"], selected && styles.active)}>
                      {<IconComponent {...iconConfigs} color={color} />}
                    </ListItemIcon>
                    {open && <div className={styles["nav-icon-text"]}>{label}</div>}
                  </ListItem>
                </a>
              </Link>
            );
          })}
        </List>
      </Drawer>
      <main
        className={props.settingsPage ? styles["main-wrapper-settings"] : styles["main-wrapper"]}>
        {!props.settingsPage && <div className={classes.toolbar} />}
        {selectedRoute &&
          (props.settingsPage ? (
            <SubNavbar routes={selectedRoute?.subRoutes} settingsPage />
          ) : (
            <SubNavbar routes={selectedRoute?.subRoutes} />
          ))}
        <section className={styles["main-section"]}>{props.children}</section>
      </main>
      <Popover
        open={Boolean(anchorUserEl)}
        anchorEl={anchorUserEl}
        onClose={handleCloseUserMenu}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}>
        <div className="main-layout__user-menu">
          <button className="main-layout__user-menu__logout" onClick={handleLogout}>
            Đăng xuất
          </button>
        </div>
      </Popover>
    </section>
  );
};

export default AdminLayout;
