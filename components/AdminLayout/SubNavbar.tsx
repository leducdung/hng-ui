import React from "react";
import clsx from "clsx";
import Link from "next/link";
import { useRouter } from "next/router";
import { ACTIVE_NAVBAR_ICON_COLOR, DEFAULT_NAVBAR_ICON_COLOR, NavigatorItem } from "./admin-routes";
import styles from "./SubNavbar.module.scss";

export type Props = {
  routes: NavigatorItem[];
  settingsPage?;
};

const SubNavbar: React.FC<Props> = (props) => {
  const router = useRouter();
  const isActiveRoute = (pathname: string): boolean => {
    return router.pathname === pathname;
  };
  if (props.routes.length === 0) return <div></div>;
  return (
    <div className={props.settingsPage ? styles.wrapperCol : styles.wrapper}>
      <div className={styles["sub-route-menu"]}>
        {props.routes.map(({ label, icon: IconComponent, iconConfigs, baseRoute }) => {
          const isActive = isActiveRoute(baseRoute);
          const color = isActive ? ACTIVE_NAVBAR_ICON_COLOR : DEFAULT_NAVBAR_ICON_COLOR;
          const classes = clsx(
            styles["nav-item"],
            baseRoute === router.pathname && styles["selected"]
          );
          return (
            <Link key={baseRoute} passHref href={baseRoute}>
              <a className={classes}>
                <div className={styles.icon}>
                  {<IconComponent {...iconConfigs} color={color} />}
                </div>
                <span className="title">{label}</span>
              </a>
            </Link>
          );
        })}
      </div>
    </div>
  );
};

export default SubNavbar;
