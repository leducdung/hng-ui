import { CustomerIcon } from "./../Icons/Customer";
import { IconModel } from "@Models";
import { ADMIN_ROUTES } from "@Constants";
import { OrderIcon } from "@Components/Icons/Order";
import { InvoiceIcon } from "@Components/Icons/Invoice";
import { GoodsIcon } from "@Components/Icons/Goods";
import { StoreIcon } from "@Components/Icons/Store";
import { PriceIcon } from "@Components/Icons/Price";
import { SettingsIcon } from "@Components/Icons/Settings";
import { ProfileIcon } from "@Components/Icons/Profile";
import { SpecificationIcon } from "@Components/Icons/Specification";
import { TransactionIcon } from "@Components/Icons/Transaction";
import { MoneyTransactionIcon } from "@Components/Icons/MoneyTransaction";
import { GoldWithdrawalIcon } from "@Components/Icons/GoldWithdrawal";
import { ProductsIcon } from "@Components/Icons/ProductsIcon";

export const DEFAULT_NAVBAR_ICON_COLOR = "#6C778D";
export const ACTIVE_NAVBAR_ICON_COLOR = "#FFF";

export type NavigatorItem = {
  label: string;
  headingLabel: string;
  icon: React.ElementType;
  baseRoute: string;
  iconConfigs: IconModel;
  subRoutes: Array<NavigatorItem>;
};

const navigatorList: NavigatorItem[] = [
  {
    label: "Đơn hàng",
    headingLabel: "Đơn hàng",
    icon: OrderIcon,
    baseRoute: ADMIN_ROUTES.ORDER,
    iconConfigs: {
      color: DEFAULT_NAVBAR_ICON_COLOR,
      size: [33, 31.35],
      viewBox: [33, 31.35],
    },
    subRoutes: [
      {
        label: "Đơn hàng",
        headingLabel: "Đơn hàng",
        icon: OrderIcon,
        baseRoute: ADMIN_ROUTES.ORDER,
        iconConfigs: {
          color: DEFAULT_NAVBAR_ICON_COLOR,
          size: [24, 24],
          viewBox: [24, 24],
        },
        subRoutes: [],
      },
      {
        label: "Hóa đơn",
        headingLabel: "Hóa đơn",
        icon: InvoiceIcon,
        baseRoute: ADMIN_ROUTES.BILLING,
        iconConfigs: {
          color: DEFAULT_NAVBAR_ICON_COLOR,
          size: [24, 24],
          viewBox: [24, 24],
        },
        subRoutes: [],
      },
    ],
  },
  {
    label: "Sản phẩm",
    headingLabel: "Sản phẩm",
    icon: GoodsIcon,
    baseRoute: ADMIN_ROUTES.PRODUCT,
    iconConfigs: {
      color: DEFAULT_NAVBAR_ICON_COLOR,
      size: [28, 28],
      viewBox: [29, 29],
    },
    subRoutes: [
      {
        label: "Sản phẩm",
        headingLabel: "Sản phẩm",
        icon: ProductsIcon,
        baseRoute: ADMIN_ROUTES.PRODUCT,
        iconConfigs: {
          color: DEFAULT_NAVBAR_ICON_COLOR,
          size: [20, 20],
          viewBox: [15, 15],
        },
        subRoutes: [],
      },
      {
        label: "Thông số kĩ thuật",
        headingLabel: "Thông số kĩ thuật",
        icon: SpecificationIcon,
        baseRoute: ADMIN_ROUTES.SPECIFICATIONS,
        iconConfigs: {
          color: DEFAULT_NAVBAR_ICON_COLOR,
          size: [24, 24],
          viewBox: [24, 24],
        },
        subRoutes: [],
      },
    ],
  },
  {
    label: "Quản lý Khách hàng",
    headingLabel: "Quản lý Khách hàng",
    icon: CustomerIcon,
    baseRoute: ADMIN_ROUTES.CUSTOMER,
    iconConfigs: {
      color: DEFAULT_NAVBAR_ICON_COLOR,
      size: [24, 24],
      viewBox: [24, 24],
    },
    subRoutes: [
      {
        label: "Khách hàng",
        headingLabel: "Khách hàng",
        icon: ProfileIcon,
        baseRoute: ADMIN_ROUTES.CUSTOMER,
        iconConfigs: {
          color: DEFAULT_NAVBAR_ICON_COLOR,
          size: [24, 24],
          viewBox: [24, 24],
        },
        subRoutes: [],
      },
    ],
  },
  {
    label: "Giao dịch",
    headingLabel: "Giao dịch",
    icon: TransactionIcon,
    baseRoute: ADMIN_ROUTES.TRANSACTION,
    iconConfigs: {
      color: DEFAULT_NAVBAR_ICON_COLOR,
      size: [28, 28],
      viewBox: [28, 28],
    },
    subRoutes: [
      {
        label: "Nạp tiền",
        headingLabel: "Nạp tiền",
        icon: MoneyTransactionIcon,
        baseRoute: ADMIN_ROUTES.TRANSACTION,
        iconConfigs: {
          color: DEFAULT_NAVBAR_ICON_COLOR,
          size: [24, 24],
          viewBox: [24, 24],
        },
        subRoutes: [],
      },
      {
        label: "Rút vàng",
        headingLabel: "Rút vàng",
        icon: GoldWithdrawalIcon,
        baseRoute: ADMIN_ROUTES.GOLD_WITHDRAWAL,
        iconConfigs: {
          color: DEFAULT_NAVBAR_ICON_COLOR,
          size: [24, 24],
          viewBox: [24, 24],
        },
        subRoutes: [],
      },
    ],
  },
  {
    label: "Giá vàng HNG",
    headingLabel: "Giá vàng HNG",
    icon: PriceIcon,
    baseRoute: ADMIN_ROUTES.GOLD_PRICE,
    iconConfigs: {
      color: DEFAULT_NAVBAR_ICON_COLOR,
      size: [33, 31.35],
      viewBox: [33, 31.35],
    },
    subRoutes: [],
  },
  {
    label: "Cài đặt",
    headingLabel: "Cài đặt",
    icon: SettingsIcon,
    baseRoute: ADMIN_ROUTES.LOCATION,
    iconConfigs: {
      color: DEFAULT_NAVBAR_ICON_COLOR,
      size: [35, 35],
      viewBox: [29, 29],
    },
    subRoutes: [
      {
        label: "Chi nhánh",
        headingLabel: "Chi nhánh",
        icon: StoreIcon,
        baseRoute: ADMIN_ROUTES.LOCATION,
        iconConfigs: {
          color: DEFAULT_NAVBAR_ICON_COLOR,
          size: [24, 24],
          viewBox: [24, 24],
        },
        subRoutes: [],
      },
    ],
  },
];

export default navigatorList;
