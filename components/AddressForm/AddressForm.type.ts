import { FORM_MODE } from "@Constants";
import { ShippingAddressOutputModel } from "@Models";

export type C_Props = {
  onComplete_Create?: (newAddress: ShippingAddressOutputModel) => void;
  onComplete_Update?: (updatedAddress: ShippingAddressOutputModel) => void;
  initData?: ShippingAddressOutputModel;
  onCancel: () => void;
};

export type C_States = {
  mode: FORM_MODE;
};

export type P_Props = C_Props &
  C_States & {
    onSubmit: (values) => void;
  };
