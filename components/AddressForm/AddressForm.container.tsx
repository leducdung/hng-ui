import React, { Component } from "react";
import { FORM_MODE } from "@Constants";
import { UserService } from "../../services";
import { loadCallback } from "@Utils";

import AddressFormPresenter from "./AddressForm.presenter";
import { C_Props, C_States } from "./AddressForm.type";

class AddressFormContainer extends Component<C_Props, C_States> {
  constructor(props: C_Props) {
    super(props);

    const mode = this.props.initData?.id ? FORM_MODE.UPDATE : FORM_MODE.CREATE;

    this.state = {
      mode,
    };
  }

  // eslint-disable-next-line
  onSubmit = (values) => {
    const data = { ...values };

    if (this.state.mode === FORM_MODE.CREATE) {
      this.onCreateAddress(data);
    } else {
      this.onUpdateAddress(data);
    }
  };

  // eslint-disable-next-line
  onCreateAddress = (data) => {
    UserService.createShippingAddress(data)
      .then((res) => {
        if (res && (res.status === 200 || res.status === 201)) {
          loadCallback(this.props.onComplete_Create, res.data.data);
        } else {
        }
      })
      .catch((err) => {
        console.error(err);
      });
  };

  // eslint-disable-next-line
  onUpdateAddress = (data) => {
    UserService.updateShippingAddress(this.props.initData!.id, data)
      .then((res) => {
        if (res && (res.status === 200 || res.status === 201)) {
          loadCallback(this.props.onComplete_Update, res.data.data);
        } else {
        }
      })
      .catch((err) => {
        console.error(err);
      });
  };

  render(): JSX.Element {
    return <AddressFormPresenter {...this.props} {...this.state} onSubmit={this.onSubmit} />;
  }
}

export default AddressFormContainer;
