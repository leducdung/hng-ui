import {
  Checkbox,
  FormControlLabel,
  MenuItem,
  OutlinedInput,
  Radio,
  RadioGroup,
  Select,
  TextField,
} from "@material-ui/core";
import React from "react";
import { Field, Form } from "react-final-form";
import { DEFAULT_STORE_INFOS, FORM_MODE, REGEX } from "@Constants";
import { FormUIUtil, FormUtil, loadCallback } from "@Utils";
import {
  CheckBoxOutlineBlank as CheckBoxOutlineBlankIcon,
  CheckBox as CheckBoxIcon,
} from "@material-ui/icons";
import { P_Props } from "./AddressForm.type";
import { ShippingAddressLocation, ShippingAddressOutputModel } from "@Models";
import { LOCATIONS } from "@Constants";

const AddressFormPresenter: React.FC<P_Props> = (props) => {
  let submit;

  const bindingAddressDataToForm = (data: ShippingAddressOutputModel) => {
    return {
      name: data.name,
      phoneNumber: data.phoneNumber,
      address: data.address,
      isDefault: data.shippingDefault || false,
      provinceId: data.provinceId,
      districtId: data.districtId,
      wardId: data.wardId,
      place: data.place,
    };
  };

  return (
    <Form
      initialValues={
        props.initData && props.mode === FORM_MODE.UPDATE
          ? bindingAddressDataToForm(props.initData)
          : {
              isDefault: false,
              place: ShippingAddressLocation.HOME,
            }
      }
      initialValuesEqual={() => true}
      onSubmit={async (values) => {
        props.onSubmit(values);
      }}
      render={({ handleSubmit, form }) => {
        submit = handleSubmit;
        return (
          <form onSubmit={handleSubmit} className="address-form">
            <Field name="name" validate={FormUtil.Rule.required("Xin nhập tên người nhận")}>
              {({ input, meta, ...rest }) => {
                return FormUIUtil.renderFormItem(
                  "Tên người nhận",
                  <TextField
                    {...input}
                    {...rest}
                    fullWidth
                    placeholder={`Vd. ${DEFAULT_STORE_INFOS.NAME}`}
                    className="form-text-field"
                    inputProps={{ className: "input" }}
                    variant="outlined"
                    onChange={(e) => input.onChange(e.target.value)}
                    helperText={meta.touched ? meta.error : ""}
                    error={meta.error && meta.touched}
                  />
                );
              }}
            </Field>

            <Field
              name="phoneNumber"
              validate={FormUtil.composeValidators([
                FormUtil.Rule.required("Xin nhập số điện thoại"),
                FormUtil.Rule.pattern(REGEX.PHONE_NUMBER, {
                  errorMessage: "Số điện thoại không hợp lệ",
                }),
              ])}>
              {({ input, meta, ...rest }) => {
                return FormUIUtil.renderFormItem(
                  "Số điện thoại",
                  <TextField
                    {...input}
                    {...rest}
                    fullWidth
                    placeholder={`Vd. ${DEFAULT_STORE_INFOS.PHONE_NUMBER}`}
                    className="form-text-field"
                    inputProps={{ className: "input" }}
                    variant="outlined"
                    onChange={(e) => input.onChange(e.target.value)}
                    helperText={meta.touched ? meta.error : ""}
                    error={meta.error && meta.touched}
                  />
                );
              }}
            </Field>

            <Field name="provinceId" validate={FormUtil.Rule.required("Xin chọn thành phố")}>
              {({ input, meta, ...rest }) => {
                return FormUIUtil.renderFormItem(
                  "Tỉnh/ Thành phố",
                  <Select
                    {...input}
                    {...rest}
                    // native
                    value={input.value}
                    onChange={(e) => {
                      input.onChange(e.target.value);
                      form.change("districtId", undefined);
                      form.change("wardId", undefined);
                    }}
                    error={meta.error && meta.touched}
                    fullWidth
                    input={<OutlinedInput className="form-text-field" />}
                    variant="outlined"
                    renderValue={(selected) => {
                      const selectedCity = LOCATIONS.find((city) => city.id === selected);

                      return selectedCity?.name;
                    }}
                    className="form-select-field">
                    {LOCATIONS.map((city) => (
                      <MenuItem key={city.id} value={city.id} className="form-select-field-item">
                        {city.name}
                      </MenuItem>
                    ))}
                  </Select>
                );
              }}
            </Field>

            <Field name="districtId" validate={FormUtil.Rule.required("Xin chọn quận/ huyện")}>
              {({ input, meta, ...rest }) => {
                const selectedCityId = form.getFieldState("provinceId")?.value;
                const districts =
                  LOCATIONS.find((city) => city.id === selectedCityId)?.districts || [];

                return FormUIUtil.renderFormItem(
                  "Quận/ Huyện",
                  <Select
                    {...input}
                    {...rest}
                    // native
                    value={input.value}
                    onChange={(e) => {
                      input.onChange(e.target.value);
                      form.change("wardId", undefined);
                    }}
                    error={meta.error && meta.touched}
                    fullWidth
                    input={<OutlinedInput className="form-text-field" />}
                    variant="outlined"
                    renderValue={(selected) => {
                      const selectedDistrict = districts.find(
                        (district) => district.id === selected
                      );

                      return selectedDistrict?.name;
                    }}
                    className="form-select-field">
                    {districts.map((district) => (
                      <MenuItem
                        key={district.id}
                        value={district.id}
                        className="form-select-field-item">
                        {district.name}
                      </MenuItem>
                    ))}
                  </Select>
                );
              }}
            </Field>

            <Field name="wardId" validate={FormUtil.Rule.required("Xin chọn phường/ xã")}>
              {({ input, meta, ...rest }) => {
                const selectedCityId = form.getFieldState("provinceId")?.value;
                const selectedDistrictId = form.getFieldState("districtId")?.value;
                const districts =
                  LOCATIONS.find((city) => city.id === selectedCityId)?.districts || [];
                const wards =
                  districts.find((district) => district.id === selectedDistrictId)?.wards || [];

                return FormUIUtil.renderFormItem(
                  "Phường/ Xã",
                  <Select
                    {...input}
                    {...rest}
                    // native
                    value={input.value}
                    error={meta.error && meta.touched}
                    fullWidth
                    input={<OutlinedInput className="form-text-field" />}
                    variant="outlined"
                    renderValue={(selected) => {
                      const selectedWard = wards.find((ward) => ward.id === selected);

                      return selectedWard?.name;
                    }}
                    className="form-select-field">
                    {wards.map((ward) => (
                      <MenuItem key={ward.id} value={ward.id} className="form-select-field-item">
                        {ward.name}
                      </MenuItem>
                    ))}
                  </Select>
                );
              }}
            </Field>

            <Field name="address" validate={FormUtil.Rule.required("Xin nhập địa chỉ")}>
              {({ input, meta, ...rest }) => {
                return FormUIUtil.renderFormItem(
                  "Địa chỉ",
                  <TextField
                    {...input}
                    {...rest}
                    fullWidth
                    placeholder={`Vd. 25`}
                    className="form-text-field"
                    inputProps={{ className: "input" }}
                    variant="outlined"
                    onChange={(e) => input.onChange(e.target.value)}
                    helperText={meta.touched ? meta.error : ""}
                    error={meta.error && meta.touched}
                  />
                );
              }}
            </Field>

            <Field name="place">
              {({ input, ...rest }) => {
                return (
                  <RadioGroup {...input} {...rest} style={{ flexDirection: "row" }}>
                    <FormControlLabel
                      value={ShippingAddressLocation.HOME}
                      control={<Radio className="app-radio-box" />}
                      label={<div className="address-form__radio-label">Nhà Riêng</div>}
                    />
                    <FormControlLabel
                      value={ShippingAddressLocation.OFFICE}
                      control={<Radio className="app-radio-box" />}
                      label={<div className="address-form__radio-label">Nơi Làm Việc</div>}
                    />
                  </RadioGroup>
                );
              }}
            </Field>

            <Field name="isDefault">
              {({ input, ...rest }) => {
                return (
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <Checkbox
                      className="app-check-box"
                      {...input}
                      {...rest}
                      checked={input.value}
                      onChange={(e) => input.onChange(e.target.checked)}
                      icon={<CheckBoxOutlineBlankIcon style={{ fontSize: "2.2rem" }} />}
                      checkedIcon={<CheckBoxIcon style={{ fontSize: "2.2rem" }} />}
                    />
                    <span className="address-form__check-box-label">
                      Sử dụng làm địa chỉ mặc định
                    </span>
                  </div>
                );
              }}
            </Field>

            <div className="address-form__actions">
              <button
                className="address-form__actions__cancel"
                onClick={(e) => {
                  e.preventDefault();
                  props.onCancel();
                }}>
                Hủy
              </button>
              <button
                className="address-form__actions__submit"
                onClick={(e) => {
                  loadCallback(submit, e);
                }}>
                {props.mode === FORM_MODE.CREATE ? "Tạo mới" : "Cập nhật"}
              </button>
            </div>
          </form>
        );
      }}
    />
  );
};

export default AddressFormPresenter;
