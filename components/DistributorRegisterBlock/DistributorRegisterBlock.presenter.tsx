import React, { useState } from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { DEFAULT_IMG_ALT } from "@Constants";
import Zoom from "react-reveal/Zoom";
import Fade from "react-reveal/Fade";
import Pulse from "react-reveal/Pulse";
import { Dialog } from "@material-ui/core";
import { Registration } from "..";
import { useBreakpoints } from "../../hooks";

const DistributorRegisterBlockPresenter: React.FC = () => {
  const [visibleViewerModal, setVisibleViewerModal] = useState<boolean>(false);
  const breakpoints = useBreakpoints();

  const toggleViewerModal = () => {
    setVisibleViewerModal(!visibleViewerModal);
  };
  return (
    <>
      <section className="distributor-register-block">
        <LazyLoadImage alt={DEFAULT_IMG_ALT} src={"/images/aboutus/banner2.png"} />
        <article className="distributor-register-block__present">
          <Zoom>
            <h2 style={{ margin: "0" }} className="distributor-register-block__title">
              đại lý NHƯỢNG QUYỀN hanagold
            </h2>
          </Zoom>
          <Fade top>
            <h3 style={{ margin: "0" }} className="distributor-register-block__text">
              Đăng ký tham gia mạng lưới nhượng quyền của HanaGold
            </h3>
          </Fade>
          <Fade bottom>
            <Pulse delay={1500} forever>
              <button className="distributor-register-block__btn" onClick={toggleViewerModal}>
                ĐĂNG KÝ NGAY
              </button>
            </Pulse>
          </Fade>
        </article>
      </section>
      <Dialog
        open={visibleViewerModal}
        onClose={() => {
          toggleViewerModal();
        }}
        fullScreen={!breakpoints.lg ? true : false}
        className="registration__dialog">
        <Registration
          onClose={() => {
            toggleViewerModal();
          }}
        />
      </Dialog>
    </>
  );
};

export default DistributorRegisterBlockPresenter;
