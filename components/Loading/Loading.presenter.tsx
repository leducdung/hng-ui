import React from "react";
import clsx from "clsx";
import { CircularProgress } from "@material-ui/core";

import { P_Props } from "./Loading.type";

const LoadingPresenter: React.FC<P_Props> = (props) => {
  const classes = clsx({ "loading-comp": true, [props.className || ""]: Boolean(props.className) });

  return (
    <div className={classes}>
      <CircularProgress />
    </div>
  );
};

export default LoadingPresenter;
