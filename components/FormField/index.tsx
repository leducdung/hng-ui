import React from "react";
import { Field } from "react-final-form";
import { FormUtil } from "../../utils";
import { InfoOutlined as InfoOutlinedIcon } from "@material-ui/icons";
import { REGEX } from "@Constants";
import { TextField } from "@material-ui/core";

type Props = {
  label: string;
  name: string;
  placeholder: string;
  requiredMessage: string;
  numberRequiredMessage?: string;
  emailRequiredMessage?: string;
  fieldConfig?: React.ComponentProps<typeof TextField>;
};

const FormField: React.FC<Props> = ({
  label,
  name,
  placeholder,
  requiredMessage,
  numberRequiredMessage,
  emailRequiredMessage,
  fieldConfig = {},
}) => {
  const validateOpts: ((value: string) => string | undefined)[] = [];
  requiredMessage && validateOpts.push(FormUtil.Rule.required(requiredMessage));
  numberRequiredMessage &&
    validateOpts.push(
      FormUtil.Rule.pattern(REGEX.NUMBER_ONLY, {
        errorMessage: numberRequiredMessage,
      })
    );
  emailRequiredMessage &&
    validateOpts.push(FormUtil.Rule.pattern(REGEX.EMAIL, { errorMessage: emailRequiredMessage }));
  const validateFn =
    validateOpts.length > 0 ? FormUtil.composeValidators(validateOpts) : () => undefined;

  return (
    <Field
      name={name}
      validate={validateFn}
      subscription={{
        touched: true,
        error: true,
        value: true,
      }}>
      {({ input, meta, ...rest }) => (
        <div className="form-item">
          <label htmlFor={name} className="form-label">
            {label} {!!requiredMessage && <InfoOutlinedIcon style={{ marginLeft: "0.5rem" }} />}
          </label>
          <TextField
            {...input}
            {...rest}
            id={name}
            fullWidth
            placeholder={placeholder}
            className="form-text-field"
            inputProps={{ className: "input" }}
            variant="outlined"
            onChange={(e) => input.onChange(e.target.value)}
            helperText={meta.touched ? meta.error : ""}
            error={meta.error && meta.touched}
            {...fieldConfig}
          />
        </div>
      )}
    </Field>
  );
};

export default React.memo(FormField);
