import { withStyles } from "@material-ui/core/styles";
import { Switch, WithStyles } from "@material-ui/core";

const EotySwitch = withStyles((theme) => ({
  root: {
    width: 42,
    height: 26,
    padding: 0,
    margin: theme.spacing(1),
  },
  switchBase: {
    padding: 1,
    "&$checked": {
      transform: "translateX(16px)",
      color: theme.palette.background.paper,
      "& + $track": {
        backgroundColor: "#6658F3",
        opacity: 1,
        border: "none",
      },
    },
    "&$focusVisible $thumb": {
      color: "#52d869",
      border: "6px solid #fff",
    },
  },
  thumb: {
    width: 24,
    height: 24,
  },
  track: {
    borderRadius: 26 / 2,
    // border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: "#F0F3F8",
    opacity: 1,
    transition: theme.transitions.create(["background-color", "border"]),
  },
  checked: {},
  focusVisible: {},
}))((props: React.ComponentProps<typeof Switch> & WithStyles) => {
  const { classes } = props;
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      {...props}
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
    />
  );
});

export default EotySwitch;
