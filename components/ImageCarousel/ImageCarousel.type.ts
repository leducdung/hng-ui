import { ImageOutputModel } from "@Models";
export type P_Props = {
  className?: string;
  images: ImageOutputModel[];
  onClickImg?: (selectedImg: ImageOutputModel) => void;
  /**
   * số hình sẽ hiện lên view trước
   */
  nbOfDisplayImages: number;
  buildImgSrc: (img: ImageOutputModel | undefined | null) => string | undefined;
  viewerTitle?: string;
};
