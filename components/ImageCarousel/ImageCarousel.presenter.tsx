import React, { useState } from "react";
import clsx from "clsx";
import { P_Props } from "./ImageCarousel.type";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { DEFAULT_IMG_ALT } from "@Constants";
import { Dialog, Grid, IconButton } from "@material-ui/core";
import { useBreakpoints } from "../../hooks";
import { ImageOutputModel } from "@Models";
import { Close as CloseIcon } from "@material-ui/icons";
import { loadCallback } from "@Utils";
import Image from "next/image";

const ImageCarouselPresenter: React.FC<P_Props> = (props) => {
  const breakpoints = useBreakpoints();
  const classes = clsx({
    "image-carousel": true,
    [props.className || ""]: Boolean(props.className),
  });
  const displayImages = props.images.slice(0, props.nbOfDisplayImages);
  const restImages = props.images.slice(props.nbOfDisplayImages, props.images.length);

  const [visibleImgViewerModal, setVisibleImgViewerModal] = useState<boolean>(false);
  const [currentViewImg, setCurrentViewImg] = useState<ImageOutputModel | undefined>(
    props.images.length ? props.images[0] : undefined
  );

  const toggleImgViewerModal = () => {
    setVisibleImgViewerModal(!visibleImgViewerModal);
  };

  return (
    <>
      {Boolean(props.images.length) && (
        <div className={classes}>
          {displayImages.map((img, idx) => {
            const isLast = props.nbOfDisplayImages - 1 === idx;

            return (
              <div
                key={img.alt + img.id}
                className="image-carousel__item"
                onClick={() => {
                  loadCallback(props.onClickImg, img);
                }}>
                <div className="image-carousel__item__img">
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={props.buildImgSrc(img)} />
                  {isLast && Boolean(restImages.length) && (
                    <div
                      className="rest-imgs"
                      onClick={() => {
                        toggleImgViewerModal();
                      }}>
                      + {restImages.length}
                    </div>
                  )}
                </div>
              </div>
            );
          })}
        </div>
      )}
      <Dialog
        open={visibleImgViewerModal}
        onClose={() => {
          toggleImgViewerModal();
        }}
        maxWidth={breakpoints.lg ? "lg" : undefined}>
        <div className="image-carousel__viewer">
          <div className="image-carousel__viewer__header">
            <div className="image-carousel__viewer__header__title">
              {props.viewerTitle || "Hình ảnh"}
            </div>
            <IconButton
              onClick={() => {
                toggleImgViewerModal();
              }}>
              <CloseIcon style={{ fontSize: "2.2rem", color: "#000" }} />
            </IconButton>
          </div>
          <Grid container spacing={4} style={{ overflow: "hidden", height: "50vh" }}>
            <Grid item xs={12} lg={8} className="image-carousel__viewer__selected">
              <Image
                src={props.buildImgSrc(currentViewImg) || "/app-icon.png"}
                alt={currentViewImg?.alt || DEFAULT_IMG_ALT}
                layout="fill"
              />
            </Grid>
            <Grid item xs={12} lg={4} className="image-carousel__viewer__list">
              {props.images.map((img, idx) => {
                return (
                  <div
                    key={img.id + idx}
                    className="image-carousel__viewer__item"
                    onClick={() => {
                      setCurrentViewImg(img);
                    }}>
                    <LazyLoadImage alt={img?.alt || DEFAULT_IMG_ALT} src={props.buildImgSrc(img)} />
                  </div>
                );
              })}
            </Grid>
          </Grid>
        </div>
      </Dialog>
    </>
  );
};

export default ImageCarouselPresenter;
