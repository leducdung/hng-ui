export type P_Props = {
  className?: string;

  onClose?: () => void;
  title?: string;
  subTitle?: string;
  imgSrc?: string;
  onBackToHome?: () => void;
  modifyBackButton?: {
    onClick: () => void;
    text: string;
  };
};
