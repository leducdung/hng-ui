import React from "react";
import { P_Props } from "./SizeInstructions.type";
import {
  DEFAULT_IMG_ALT,
  ProductSizeBracelet,
  ProductSizeNecklace,
  ProductSizeRings,
} from "@Constants";
import { Divider, Grid } from "@material-ui/core";
import Image from "next/image";
import Banner1 from "../../public/images/banner/bannersize1.png";
import Banner2 from "../../public/images/banner/bannersize2.png";
import Banner3 from "../../public/images/banner/bannersize3.png";

const SizeInstructionsPresenter: React.FC<P_Props> = () => {
  return (
    <div className="size-instructions">
      <div className="size-instructions__text-title">HƯỚNG DẪN CHỌN KÍCH CỠ</div>
      <Divider style={{ backgroundColor: "#6C6C6C", margin: "1rem 3rem 2.6rem" }} />
      <Grid container className="size-instructions__table">
        <Grid item lg={4}>
          {ProductSizeRings.map((r, index: number) => {
            return (
              <div key={index} className="size-instructions__table-left">
                <div className="size-instructions__name">{r.name}</div>
                {r.description.map((d, idx: number) => {
                  return (
                    <Grid key={idx} container>
                      <Grid item lg={4}>
                        <div className="size-instructions__title">{d.title1}</div>
                        {d.sizeus.map((s, id: number) => {
                          return (
                            <div key={id} className="size-instructions__text">
                              {s.number}
                            </div>
                          );
                        })}
                      </Grid>
                      <Grid item lg={8}>
                        <div className="size-instructions__title">{d.title2}</div>
                        {d.perimetermm.map((p, id: number) => {
                          return (
                            <div key={id} className="size-instructions__text">
                              {p.perimeter}
                            </div>
                          );
                        })}
                      </Grid>
                    </Grid>
                  );
                })}
              </div>
            );
          })}
        </Grid>
        <Grid item lg={4}>
          {ProductSizeBracelet.map((r) => {
            return (
              <div key={r.name} className="size-instructions__table-center">
                <div className="size-instructions__name">{r.name}</div>
                {r.description.map((d, index: number) => {
                  return (
                    <Grid key={index} container>
                      <Grid item lg={3}>
                        <div className="size-instructions__title">{d.title1}</div>
                        {d.size.map((s, idx: number) => {
                          return (
                            <div key={idx} className="size-instructions__text">
                              {s.number}
                            </div>
                          );
                        })}
                      </Grid>
                      <Grid item lg={9}>
                        <div className="size-instructions__title">{d.title2}</div>
                        {d.perimetermm.map((p, idx: number) => {
                          return (
                            <div key={idx} className="size-instructions__text">
                              {p.perimeter}
                            </div>
                          );
                        })}
                      </Grid>
                    </Grid>
                  );
                })}
              </div>
            );
          })}
        </Grid>
        <Grid item lg={4}>
          {ProductSizeNecklace.map((r) => {
            return (
              <div key={r.name} className="size-instructions__table-right">
                <div className="size-instructions__name">{r.name}</div>
                {r.description.map((d, index: number) => {
                  return (
                    <Grid key={index} container>
                      <Grid item lg={6}>
                        <div className="size-instructions__title">{d.title1}</div>
                        {d.lengthin.map((i, idx: number) => {
                          return (
                            <div key={idx} className="size-instructions__text">
                              {i.number}
                            </div>
                          );
                        })}
                      </Grid>
                      <Grid item lg={6}>
                        <div className="size-instructions__title">{d.title2}</div>
                        {d.lengthcm.map((c, idx: number) => {
                          return (
                            <div key={idx} className="size-instructions__text">
                              {c.number}
                            </div>
                          );
                        })}
                      </Grid>
                    </Grid>
                  );
                })}
              </div>
            );
          })}
        </Grid>
      </Grid>
      <Grid container>
        <Grid item lg={4} className="size-instructions__img">
          <Image src={Banner1} alt={DEFAULT_IMG_ALT} objectFit="contain" />
        </Grid>
        <Grid item lg={4} className="size-instructions__img">
          <Image src={Banner2} alt={DEFAULT_IMG_ALT} objectFit="contain" />
        </Grid>
        <Grid item lg={4} className="size-instructions__img">
          <Image src={Banner3} alt={DEFAULT_IMG_ALT} objectFit="contain" />
        </Grid>
      </Grid>
    </div>
  );
};

export default SizeInstructionsPresenter;
