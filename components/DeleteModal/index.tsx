import React from "react";
import { PriorityHigh as PriorityHighIcon } from "@material-ui/icons";
import Pulse from "./Pulse";
import { Dialog } from "@material-ui/core";
import styles from "./DeleteModal.module.scss";

type Props = {
  show: boolean;
  title: string;
  content: string;
  cancelEvent: () => void;
  confirmEvent: () => void;
};
const DeleteModal: React.FC<Props> = ({ show, title, content, cancelEvent, confirmEvent }) => {
  return (
    <Dialog className={styles.delete_modal_wrapper} open={show}>
      <div className={styles.deleteModal}>
        <div className={styles.icon}>
          <Pulse color="red">
            <div className={`${styles.red} ${styles.notification_icon_box}`}>
              <PriorityHighIcon style={{ fontSize: "5rem", color: "#fff" }} />
            </div>
          </Pulse>
        </div>
        <div className={styles.title}>{title}</div>
        <div className={styles.content}>{content}</div>
        <div className={styles.actions}>
          <button
            type="button"
            className={styles.cancel}
            onClick={() => {
              cancelEvent();
            }}>
            Hủy
          </button>
          <button
            className={styles.confirm}
            onClick={() => {
              confirmEvent();
            }}>
            Xác nhận
          </button>
        </div>
      </div>
    </Dialog>
  );
};

export default React.memo(DeleteModal);
