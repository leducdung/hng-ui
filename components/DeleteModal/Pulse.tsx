import { NextPage } from "next";
import React from "react";
import styles from "./Pulse.module.scss";

export type P_Props = {
  color: "green" | "purple" | "yellow" | "red" | "blue";
};

const PulsePresenter: NextPage<P_Props> = (props) => {
  return <div className={`${styles.pulse} ${`styles.${props.color}`}`}>{props.children}</div>;
};

export default PulsePresenter;
