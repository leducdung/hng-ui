import React from "react";
import { DEFAULT_IMG_ALT } from "@Constants";
import { LazyLoadImage } from "react-lazy-load-image-component";
import QuantityBtnGroup from "../QuantityBtnGroup";
import { useBreakpoints } from "../../hooks";
import { CartProduct } from "@Lib/cart-module";
import { currencyFormat } from "@Utils";
type Props = {
  cartItem: CartProduct;
  changeItemQuantity: (id: number, quantity: number) => void;
  freeze?: boolean;
};
const OrderProductCart1Presenter: React.FC<Props> = ({ cartItem, changeItemQuantity }) => {
  const breakpoints = useBreakpoints();
  return (
    <div className="order-product-cart">
      {!breakpoints.md && (
        <div className="order-product-cart__item">
          <div className="order-product-cart__img">
            <LazyLoadImage alt={DEFAULT_IMG_ALT} src={cartItem.product.images[0].path} />
          </div>
          <div className="order-product-cart__group-quantity">
            <div className="order-product-cart__name">{cartItem.product.name}</div>
            <QuantityBtnGroup
              quantity={cartItem.quantity}
              onChangeQuantity={(value) => {
                changeItemQuantity(cartItem.id, value);
              }}
            />
          </div>
          <div className="order-product-cart__group-price">
            <div className="order-product-cart__price">
              {currencyFormat(cartItem.product.price)}
              <sup>đ</sup>
            </div>
          </div>
        </div>
      )}
      {breakpoints.md && (
        <div className="order-product-cart__item">
          <div className="order-product-cart__group-img">
            <div className="order-product-cart__img">
              <LazyLoadImage alt={DEFAULT_IMG_ALT} src={cartItem.product.images[0].path} />
            </div>
          </div>
          <div className="order-product-cart__name">{cartItem.product.name}</div>

          <div className="order-product-cart__group-quantity">
            <QuantityBtnGroup
              quantity={cartItem.quantity}
              onChangeQuantity={(value) => {
                changeItemQuantity(cartItem.id, value);
              }}
            />
            <div className="order-product-cart__price">
              {currencyFormat(cartItem.product.price)}
              <sup>đ</sup>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default OrderProductCart1Presenter;
