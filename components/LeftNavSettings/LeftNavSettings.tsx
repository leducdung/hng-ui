import IconA from "../../public/images/account/ic_a.png";
import IconB from "../../public/images/account/ic_b.png";
import IconC from "../../public/images/account/ic_c.png";
import IconD from "../../public/images/account/ic_d.png";
import IconE from "../../public/images/account/ic_e.png";
import IconAact from "../../public/images/account/ic_a1.png";
// import IconBact from "../../public/images/account/ic_b.png";
import IconCact from "../../public/images/account/ic_c1.png";
import IconDact from "../../public/images/account/ic_d1.png";
// import IconEact from "../../public/images/account/ic_e.png";

export const Menu = [
  {
    icon: IconA,
    icon_act: IconAact,
    text: "Hồ sơ cá nhân",
    url: "/app/settings",
  },
  {
    icon: IconB,
    icon_act: IconB,
    text: "Hạng VIP",
    url: "/app/settings/vip",
  },
  {
    icon: IconC,
    icon_act: IconCact,
    text: "Đổi mật khẩu",
    url: "/app/settings/password",
  },
  {
    icon: IconD,
    icon_act: IconDact,
    text: "Thanh toán",
    url: "/app/settings/payment",
  },
  {
    icon: IconE,
    icon_act: IconE,
    text: "Người môi giới",
    url: "/app/settings/broker",
  },
];

export type MenuType = {
  icon: StaticImageData;
  icon_act: StaticImageData;
  text: string;
};
