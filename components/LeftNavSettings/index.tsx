import React from "react";
import { DEFAULT_IMG_ALT, DEFAULT_CUSTOMER_AVATAR } from "@Constants";
import { Divider } from "@material-ui/core";
import { Menu } from "./LeftNavSettings";
import { useRouter } from "next/router";
import clsx from "clsx";
import Image from "next/image";
import Icon from "../../public/images/account/ic_img.png";

const LeftNavSettings: React.FC = () => {
  const router = useRouter();
  return (
    <div className="left-nav-settings">
      <div className="top">
        <div className="image">
          <Image
            src={DEFAULT_CUSTOMER_AVATAR}
            alt="your profile picture"
            width={79}
            height={79}
            objectFit="contain"
          />
          <div className="icon">
            <Image src={Icon} alt={DEFAULT_IMG_ALT} width={25} height={25} objectFit="contain" />
          </div>
        </div>
        <div className="name">Jennifer Lawrence</div>
        <div className="state">&#8226; Đã xác thực</div>
        <Divider
          style={{
            backgroundColor: "#424242",
            width: "100%",
            marginBottom: "5.6rem",
            marginTop: "3.3rem",
          }}
        />
      </div>
      <div className="bottom">
        {Menu.map((m) => {
          const isActive = m.url === router.asPath;
          const itemClasses = clsx({ bottom__item: true, active: isActive });
          return (
            <div key={m.url} className={itemClasses} onClick={() => router.push(m.url)}>
              <div className="image">
                <Image
                  src={isActive ? m.icon_act : m.icon}
                  alt={DEFAULT_IMG_ALT}
                  width={40}
                  height={40}
                  objectFit="scale-down"
                />
              </div>
              <div className="text">{m.text}</div>
            </div>
          );
        })}
        <button className="btn-logout">đăng xuất</button>
      </div>
    </div>
  );
};
export default LeftNavSettings;
