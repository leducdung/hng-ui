import React from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { DEFAULT_IMG_ALT } from "@Constants";
import { ImageUtil } from "@Utils";

const ProjectCard1Presenter: React.FC<{
  projectData: {
    imageFeatured: string;
    titleVn: string;
  };
}> = (props) => {
  return (
    <div className="project-card-1">
      <div className="project-card-1__info">
        {/* <Link href={NewsUtil.buildProjectDetailPath(props.projectData)}>
          <a> */}
        <div className="project-card-1__info__img">
          <LazyLoadImage
            alt={DEFAULT_IMG_ALT}
            src={ImageUtil.buildProjectImgSrc(props.projectData.imageFeatured)}
          />
        </div>
        {/* </a>
        </Link> */}
        <div className="project-card-1__info__note">
          <div className="project-card-1__info__name">{props.projectData.titleVn}</div>
        </div>
      </div>
    </div>
  );
};

export default ProjectCard1Presenter;
