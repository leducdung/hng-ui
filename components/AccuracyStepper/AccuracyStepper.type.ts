import React from "react";

export type P_Props = {
  className?: string;
  style?: React.CSSProperties;
  activeStep: number;
};
