import React from "react";
import clsx from "clsx";
import { P_Props } from "./AccuracyStepper.type";
import { Divider } from "@material-ui/core";

const AccuracyStepper = [
  {
    name: "Nhập thông tin cá nhân",
  },
  {
    name: "Nhập thông tin cá nhân",
  },
  {
    name: "Chụp ảnh nhận diện mặt",
  },
  {
    name: "Chờ xác thực từ hệ thống",
  },
];

const AccuracyStepperPresenter: React.FC<P_Props> = (props) => {
  const classes = clsx({
    "accuracy-stepper": true,
    [props.className || ""]: Boolean(props.className),
  });

  return (
    <div className="accuracy-stepper__form">
      <div className={classes} style={props.style}>
        {AccuracyStepper.map((step, idx) => {
          const isActive = idx === props.activeStep;
          const stepClasses = clsx({ "accuracy-stepper__step": true, active: isActive });
          return (
            <div key={step.name} className={stepClasses}>
              <div className="name">
                <span className="number">{idx + 1}</span>
                <span>{step.name}</span>
              </div>

              <Divider
                orientation="vertical"
                style={{ backgroundColor: "#8B8B8B", height: "2.3rem", marginLeft: "1.1rem" }}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default AccuracyStepperPresenter;
