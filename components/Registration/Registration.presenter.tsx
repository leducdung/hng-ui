import React from "react";
import { DEFAULT_IMG_ALT } from "@Constants";
import { IconButton, TextField } from "@material-ui/core";
import { Close as CloseIcon, ArrowBack as ArrowBackIcon } from "@material-ui/icons";
import { useBreakpoints } from "../../hooks";
import { Field, Form } from "react-final-form";
import { P_Props } from "./Registration.type";
import { loadCallback } from "@Utils";
import Image from "next/image";
import Banner from "../../public/images/banner/registration.png";

const RegistrationPresenter: React.FC<P_Props> = (props) => {
  const breakpoints = useBreakpoints();

  return (
    <div className="registration">
      {breakpoints.lg && (
        <div className="registration__img">
          <Image src={Banner} alt={DEFAULT_IMG_ALT} objectFit="cover" />
        </div>
      )}
      <div className="registration__right">
        {breakpoints.lg && (
          <IconButton
            onClick={() => {
              loadCallback(props.onClose);
            }}>
            <CloseIcon style={{ fontSize: "2.2rem", color: "#515151" }} />
          </IconButton>
        )}
        {!breakpoints.lg && (
          <IconButton
            onClick={() => {
              loadCallback(props.onClose);
            }}>
            <ArrowBackIcon style={{ fontSize: "2.2rem", color: "#ffffff" }} />
          </IconButton>
        )}

        <Form
          onSubmit={async () => {
            // signup(values);
          }}
          render={({ handleSubmit }) => {
            return (
              <form onSubmit={handleSubmit} className="registration__form">
                <div className="registration__title">
                  đăng ký
                  <br />
                  tư vấn
                </div>
                <Field name="fullName">
                  {({ input, meta, ...rest }) => {
                    return (
                      <TextField
                        {...input}
                        {...rest}
                        fullWidth
                        placeholder={"Họ Tên"}
                        className="form-text-field"
                        inputProps={{ className: "input" }}
                        variant="outlined"
                        onChange={(e) => input.onChange(e.target.value)}
                        helperText={meta.touched ? meta.error : ""}
                        error={meta.error && meta.touched}
                      />
                    );
                  }}
                </Field>
                <Field name="email" type="email">
                  {({ input, meta, ...rest }) => {
                    return (
                      <TextField
                        {...input}
                        {...rest}
                        fullWidth
                        placeholder={"Địa chỉ email"}
                        className="form-text-field"
                        inputProps={{ className: "input" }}
                        variant="outlined"
                        onChange={(e) => input.onChange(e.target.value)}
                        helperText={meta.touched ? meta.error : ""}
                        error={meta.error && meta.touched}
                      />
                    );
                  }}
                </Field>
                <Field name="phoneNumber">
                  {({ input, meta, ...rest }) => {
                    return (
                      <TextField
                        {...input}
                        {...rest}
                        fullWidth
                        placeholder={"Số điện thoại"}
                        className="form-text-field"
                        inputProps={{ className: "input" }}
                        variant="outlined"
                        onChange={(e) => input.onChange(e.target.value)}
                        helperText={meta.touched ? meta.error : ""}
                        error={meta.error && meta.touched}
                      />
                    );
                  }}
                </Field>
                <Field name="description">
                  {({ input, ...rest }) => {
                    return (
                      <TextField
                        {...input}
                        {...rest}
                        className="form-textarea-field"
                        variant="outlined"
                        placeholder="Nội dung"
                        fullWidth
                        rows={3}
                        multiline
                        onChange={(e) => input.onChange(e.target.value)}
                      />
                    );
                  }}
                </Field>
                <button type="submit" className="registration__form__submit-btn">
                  Gửi thông tin
                </button>
              </form>
            );
          }}
        />
      </div>
    </div>
  );
};

export default RegistrationPresenter;
