import { Breadcrumbs } from "@material-ui/core";
import clsx from "clsx";
import Link from "next/link";
import React from "react";
import { HomeBreadcrumb } from "@Constants";
import { TBreadcrumb } from "@Models";

import { P_Props } from "./CustomBreadcrumbs.type";

const CustomBreadcrumbsPresenter: React.FC<P_Props> = (props) => {
  const classes = clsx({
    "custom-breadcrumbs": true,
    [props.className || ""]: Boolean(props.className),
  });
  const breadcrumbs: TBreadcrumb[] = props.withHome
    ? [HomeBreadcrumb, ...props.list]
    : [...props.list];

  const renderBreadcrumbItem = (item: TBreadcrumb, idx: number, breadcrumbsLength: number) => {
    const isHavePath = Boolean(item.path);
    const isLast = idx === breadcrumbsLength - 1;
    const itemClasses = clsx({ "custom-breadcrumbs__text": true, active: isLast });

    return isHavePath ? (
      <Link href={item.path!}>
        <a className={itemClasses}>{item.title}</a>
      </Link>
    ) : (
      <div className={itemClasses}>{item.title}</div>
    );
  };

  return (
    <div className={classes} style={props.style}>
      <Breadcrumbs
        aria-label="breadcrumb"
        separator={<div className="custom-breadcrumbs__separator">/</div>}>
        {breadcrumbs.map((br, idx) => {
          return (
            <div key={br.title + idx}>{renderBreadcrumbItem(br, idx, breadcrumbs.length)}</div>
          );
        })}
      </Breadcrumbs>
    </div>
  );
};

export default CustomBreadcrumbsPresenter;
