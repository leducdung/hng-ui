import React from "react";
import { TBreadcrumb } from "@Models";

export type P_Props = {
  className?: string;
  style?: React.CSSProperties;
  withHome?: boolean;
  list: TBreadcrumb[];
};
