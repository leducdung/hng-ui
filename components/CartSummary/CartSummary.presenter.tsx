import React from "react";
import { ProductSugarOptions } from "@Constants";
import { useBreakpoints } from "../../hooks";
import { ProductCartOutputModel } from "@Models";
import { CartUtil, currencyFormat } from "@Utils";

import { P_Props } from "./CartSummary.type";

const CartSummaryPresenter: React.FC<P_Props> = (props) => {
  const breakpoints = useBreakpoints();
  const renderProductCartInfo = (data: ProductCartOutputModel) => {
    const selectedSugar = ProductSugarOptions.find((o) => o.id === data.sugar);

    let infoStr = `${data.productVariant.flavor ? data.productVariant.flavor : ""}${
      data.productVariant.volume ? " • " + data.productVariant.volume : ""
    }${selectedSugar ? " • " + selectedSugar.name : ""}`;

    data.toppings.forEach((top) => {
      if (top.quantity <= 1) {
        infoStr += ` • ${top.topping.name}`;
      } else {
        infoStr += ` • ${top.topping.name} x ${top.quantity}`;
      }
    });

    return infoStr;
  };

  const renderPriceLine = (label: string, price: number | string) => {
    return (
      <div className="cart-summary__calc__price">
        <div className="label">{label}:</div>
        <div className="value">{price} đ</div>
      </div>
    );
  };

  return (
    <div className="cart-summary">
      {!breakpoints.lg && <div className="cart-summary__title">thông tin đặt hàng</div>}
      <div className="cart-summary__products">
        {!props.Cart.loading && props.Cart.data.length === 0 && (
          <div className="cart-summary__products__empty">Không có sản phẩm!</div>
        )}

        {!props.Cart.loading &&
          props.Cart.data.map((productCart) => {
            const selectedProductPrice = CartUtil.calcOrderProduct(productCart);

            return (
              <div key={productCart.id} className="cart-summary__products__item">
                <div className="cart-summary__products__item__header">
                  <div className="product-name">{productCart.productVariant.name}</div>
                  {props.viewMode && (
                    <div className="cart-summary__products__item__price">
                      {productCart.quantity} x {currencyFormat(selectedProductPrice)}đ
                    </div>
                  )}
                </div>
                <div className="cart-summary__products__item__body">
                  {renderProductCartInfo(productCart)}
                </div>
                {!props.viewMode && (
                  <div className="cart-summary__products__item__actions">
                    <div className="cart-summary__products__item__price">
                      {productCart.quantity} x {currencyFormat(selectedProductPrice)}đ
                    </div>
                    <div
                      className="delete"
                      onClick={() => {
                        props.onRemoveItemInCart(productCart.id);
                      }}>
                      Xóa
                    </div>
                  </div>
                )}
              </div>
            );
          })}
      </div>
      <div className="cart-summary__calc">
        {(() => {
          const cartPrice = CartUtil.calcCart(props.Cart.data, props.Cart.isVAT);

          return (
            <>
              {renderPriceLine("Tổng tạm tính", currencyFormat(cartPrice.provisionalTotal))}
              {renderPriceLine("Giảm giá", currencyFormat(cartPrice.totalDiscount))}
              {renderPriceLine("Phiếu mua hàng", 0)}
              {renderPriceLine("Phí vận chuyển", 0)}
              {renderPriceLine("Thành tiền", currencyFormat(cartPrice.total))}
            </>
          );
        })()}
      </div>
    </div>
  );
};

export default CartSummaryPresenter;
