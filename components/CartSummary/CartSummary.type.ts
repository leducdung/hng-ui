import { AppState } from "@Models";
import { CommonActions } from "../../redux/actions";

export type C_Props = {
  viewMode?: boolean;
};
export type C_States = Record<string, unknown>;

export type MapStateToProps = {
  Cart: AppState.ProductCartState;
};
export type MapDispatchToProps = {
  actions: typeof CommonActions;
};
export type MergedProps = C_Props & MapStateToProps & MapDispatchToProps;

export type P_Props = MergedProps &
  C_States & {
    onRemoveItemInCart: (orderProductId: string) => void;
  };
