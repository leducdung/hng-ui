import React, { Component } from "react";
import CartSummaryPresenter from "./CartSummary.presenter";
import { bindActionCreators, Dispatch } from "redux";
import { connect } from "react-redux";
import {
  MapStateToProps,
  MapDispatchToProps,
  C_Props,
  MergedProps,
  C_States,
} from "./CartSummary.type";
import { RootState } from "@Models";
import { CartActionTypes, CommonActions } from "../../redux/actions";

class CartSummaryContainer extends Component<MergedProps, C_States> {
  onRemoveItemInCart = (orderProductId: string) => {
    this.props.actions.createCommonType(CartActionTypes.REMOVE_ITEM_IN_CART_START, {
      data: { id: orderProductId },
    });
  };

  render() {
    return (
      <CartSummaryPresenter
        {...this.props}
        {...this.state}
        onRemoveItemInCart={this.onRemoveItemInCart}
      />
    );
  }
}

export default connect<MapStateToProps, MapDispatchToProps, C_Props, RootState>(
  (state: RootState) => ({
    Cart: state.Cart,
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(CommonActions, dispatch),
  })
)(CartSummaryContainer);
