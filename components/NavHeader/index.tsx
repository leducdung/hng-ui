import React from "react";
import clsx from "clsx";
import { PersonOutlined as PersonOutlinedIcon, Clear as ClearIcon } from "@material-ui/icons";
import Image from "next/image";
import Link from "next/link";

type P_Props = {
  className?: string;
  style?: React.CSSProperties;
  activeStep: number;
  name: string;
  code: string;
  id: number;
};

const NavHeaderCustomer = [
  {
    label: "Thông tin",
    baseRoute: "/admin/customer/",
    icon: (
      <PersonOutlinedIcon
        style={{ fontSize: "2.2rem", fontWeight: "bold", marginRight: "0.8rem" }}
      />
    ),
  },
];

const NavHeaderPresenter: React.FC<P_Props> = ({
  className,
  style,
  activeStep,
  name,
  code,
  id,
}) => {
  const classes = clsx({
    "nav-header-list": true,
    [className || ""]: Boolean(className),
  });

  return (
    <div className="nav-header">
      <div className="app-form-header">
        <div className="title">Chi tiết khách hàng</div>
        <button type="button" className="close">
          <Link href={"/admin/customer"} passHref>
            <a aria-hidden className="closeIcon">
              <ClearIcon style={{ fontSize: "2.2rem", cursor: "pointer" }} />
            </a>
          </Link>
        </button>
      </div>
      <div className="group">
        <div>
          <div className="info">
            <Image
              loader={({ src }) => src}
              loading="lazy"
              src={"/images/customer-avatar.png"}
              alt={"avata"}
              width={40}
              height={40}
              objectFit="contain"
            />
            <div className="name">
              {name}
              <br />
              <span>{code}</span>
            </div>
          </div>
          <div className={classes} style={style}>
            {NavHeaderCustomer.map((step, idx) => {
              const isActive = idx === activeStep;
              const stepClasses = clsx({ "nav-header-list__step": true, active: isActive });
              return (
                <div className={stepClasses} key={step.label}>
                  <Link href={step.baseRoute + id} passHref key={step.label}>
                    <a className="item">
                      {step.icon}
                      {step.label}
                    </a>
                  </Link>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default NavHeaderPresenter;
