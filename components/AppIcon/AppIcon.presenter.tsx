import Image from "next/image";
import { DEFAULT_IMG_ALT } from "@Constants";
import { P_Props } from "./AppIcon.type";

const AppIcon: React.FC<P_Props> = (props) => {
  return (
    <Image
      className={props.className}
      src="/images/text-logo.png"
      alt={DEFAULT_IMG_ALT}
      width={60}
      height={42}
    />
  );
};

export default AppIcon;
