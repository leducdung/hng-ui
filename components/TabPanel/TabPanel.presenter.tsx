import React from "react";
import clsx from "clsx";

import { P_Props } from "./TabPanel.type";

const TabPanelPresenter: React.FC<P_Props> = (props) => {
  const classes = clsx({
    "custom-tabpanel": true,
    [props.className || ""]: Boolean(props.className),
  });

  return (
    <div
      role="tabpanel"
      hidden={props.value !== props.index}
      className={classes}
      style={props.style}>
      {props.value === props.index && props.children}
    </div>
  );
};

export default TabPanelPresenter;
