import React from "react";

export type P_Props = {
  className?: string;
  style?: React.CSSProperties;
  value: number;
  index: number;
};
