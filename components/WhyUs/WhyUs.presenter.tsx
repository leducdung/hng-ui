import React from "react";
import CardWhyUs from "../CardWhyUs";
import { P_Props } from "./WhyUs.type";
import Zoom from "react-reveal/Zoom";
import { GOLD_4_DATA } from "@Constants";

const WhyUsPresenter: React.FC<P_Props> = () => {
  return (
    <section id="gold-4" className="why-us">
      <div className="why-us__body">
        <Zoom>
          <div className="why-us__body-left">
            <h2 className="why-us__title">Ưu điểm nổi bật của HanaGold</h2>
            {GOLD_4_DATA.slice(0, Math.floor(GOLD_4_DATA.length / 2)).map((item) => {
              return <CardWhyUs key={item.title} {...item} />;
            })}
          </div>
        </Zoom>
        <Zoom>
          <div className="why-us__body-right">
            {GOLD_4_DATA.slice(Math.floor(GOLD_4_DATA.length / 2), GOLD_4_DATA.length).map(
              (item) => {
                return <CardWhyUs key={item.title} {...item} />;
              }
            )}
          </div>
        </Zoom>
      </div>
    </section>
  );
};

export default WhyUsPresenter;
