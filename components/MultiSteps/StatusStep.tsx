import { CheckMarkIcon } from "@Components/Icons/CheckMark";
import React from "react";
import styles from "./StatusStep.module.scss";

type Props = {
  info: {
    label: string[];
    index: number;
    timestamp: string;
  };
  currentStep: string;
  passed: boolean;
  configs: Partial<{
    showSeparator: boolean;
    delay: number;
    showLabel: boolean;
    showTimestamp: boolean;
  }>;
};

const StatusStep: React.FC<Props> = ({
  info: { timestamp, label, index },
  passed,
  currentStep,
  configs: { showSeparator = true, showLabel = true, showTimestamp = true, delay = 0 },
}) => {
  const stateIndex = label.findIndex((l) => l === currentStep);
  const completed = stateIndex <= 0;
  const initial = stateIndex === label.length - 1 && label.length > 2;
  return (
    <div className={`${styles[`step-wrapper`]} ${passed && completed ? styles.complete : ``}`}>
      <div
        style={
          {
            "--grid-template": showSeparator ? "1fr 2fr" : "1fr",
          } as React.CSSProperties
        }
        className={styles.badge}>
        {passed ? (
          <div
            style={
              {
                "--complete-percent": completed ? 100 : 50,
                "--label-complete-color": initial ? "#BDC6D7" : "#08c4e3",
              } as React.CSSProperties
            }
            className={styles[`step-state`]}>
            <CheckMarkIcon color="white" size={[15, 12]} viewBox={[15, 12]} />
          </div>
        ) : (
          <div className={styles[`step-number`]}>{index}</div>
        )}
        {showSeparator && (
          <div
            style={{ "--anim-delay": delay } as React.CSSProperties}
            className={styles.separator}></div>
        )}
      </div>
      {showLabel && <div className={styles.label}>{label[0]}</div>}
      {passed && showTimestamp && <div className={styles.timestamp}>{timestamp}</div>}
    </div>
  );
};

export default React.memo(StatusStep);
