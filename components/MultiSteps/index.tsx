import React from "react";
import StatusStep from "./StatusStep";
import styles from "./index.module.scss";

const normalizeState = (states: string[]) => {
  const aggregated = states.reduce((result, current) => {
    const [key] = current.split("__");
    if (result[key]) return { ...result, [key]: [...result[key], current] };
    return { ...result, [key]: [current] };
  }, {});
  return Object.values(aggregated) as Array<string[]>;
};

type Props = {
  initialState: string[];
  currentState: string;
};

const MultiSteps: React.FC<Props> = ({ initialState, currentState }) => {
  const states = normalizeState(initialState);
  const stateIndex = states.findIndex((s) => s.includes(currentState));
  return (
    <div className={styles[`multi-step-wrapper`]}>
      {states.map((state, index, arr) => {
        return (
          <StatusStep
            key={index}
            currentStep={currentState}
            passed={index <= stateIndex}
            info={{
              label: state,
              index: index + 1,
              timestamp: Intl.DateTimeFormat("en-US", {
                dateStyle: "short",
                timeStyle: "short",
                hour12: false,
              }).format(new Date()),
            }}
            configs={{
              delay: index,
              showSeparator: !(index === arr.length - 1),
            }}
          />
        );
      })}
    </div>
  );
};

export default React.memo(MultiSteps);
