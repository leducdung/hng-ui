import React from "react";
import { DEFAULT_IMG_ALT } from "@Constants";
import { useBreakpoints } from "../../hooks";
import Fade from "react-reveal/Fade";
import Zoom from "react-reveal/Zoom";
import { Grid } from "@material-ui/core";
import Image from "next/image";
import Banner from "../../public/images/aboutus/banner.png";
import Img from "../../public/images/aboutus/img1.png";

const AboutUsPresenter: React.FC = () => {
  const breakpoints = useBreakpoints();
  return (
    <section id="about-us" className="about-us">
      <div className="about-us__banner">
        <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="fill" objectFit="cover" />
      </div>
      <div className="about-us__content">
        {breakpoints.lg && (
          <Fade top>
            <h2 style={{ margin: "0" }} className="about-us__title">
              VỀ HANAGOLD
            </h2>
          </Fade>
        )}
        <Grid container className="about-us__top">
          <Grid item xs={12} lg={4} className="about-us__top__left">
            <Zoom ssrReveal left>
              <div className="about-us__top__img">
                <Image alt={DEFAULT_IMG_ALT} src={Img} placeholder="blur" />
              </div>
            </Zoom>
          </Grid>
          {!breakpoints.lg && (
            <Fade top>
              <div className="about-us__title">VỀ HANAGOLD</div>
            </Fade>
          )}
          <Grid item xs={12} lg={7} className="about-us__description about-us__top__description">
            <Fade right>
              Công ty cổ phần Vàng Bạc Đá Quý HanaGold thành lập vào năm 2020, nhận kế thừa nền tảng
              từ công ty TNHH Vàng Bạc Đá Quý Bảo Tín Phát (2018), là doanh nghiệp khởi nghiệp đổi
              mới sáng tạo trong lĩnh vực vàng bạc đá quý.
              {/* Chúng tôi ứng dụng công nghệ 4.0 trong
              hoạt động kinh doanh vàng (tiệm vàng công nghệ 4.0).
              <br />
              Tiềm năng trong thời đại công nghệ 4.0 cùng với nhu cầu sử dụng và đầu tư thị trường
              vàng, HanaGold nhanh chóng chớp lấy thời cơ phát triển ngành vàng tại Việt Nam theo mô
              hình cải tiến hơn so với truyền thống, tạo bước đột phá cho nền kinh doanh vàng tại
              Việt Nam.
              <br />
              Nhiệm vụ của chúng tôi là đảm bảo giá trị tài sản cho bạn và đem lại sự an tâm khi gắn
              kết. */}
            </Fade>
          </Grid>
        </Grid>
        <Grid container justify="space-between" className="about-us__bottom">
          <Grid item xs={12} lg={4}>
            <Fade bottom>
              <div className="about-us__bottom__title">CHIẾN LƯỢC</div>
            </Fade>
            <Fade top>
              <div className="about-us__description">
                Ứng dụng công nghệ 4.0 trong phát triển kinh doanh ngành vàng bạc đá quý tại Việt
                Nam, biến vàng trở nên linh hoạt hơn
              </div>
            </Fade>
          </Grid>
          <Grid item xs={12} lg={3}>
            <Fade bottom>
              <div className="about-us__bottom__title">TẦM NHÌN</div>
            </Fade>
            <Fade top>
              <div className="about-us__description">
                Trở thành thương hiệu vàng Quốc dân vào năm 2025
              </div>
            </Fade>
          </Grid>
          <Grid item xs={12} lg={3}>
            <Fade bottom>
              <div className="about-us__bottom__title">GIÁ TRỊ CỐT LÕI</div>
            </Fade>
            <Fade top>
              <div className="about-us__description">An toàn - Tiện lợi - Nhanh chóng</div>
            </Fade>
          </Grid>
        </Grid>
      </div>
    </section>
  );
};

export default AboutUsPresenter;
