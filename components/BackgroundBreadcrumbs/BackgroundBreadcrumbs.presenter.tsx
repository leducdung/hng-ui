import React from "react";
import { CustomBreadcrumbs } from "..";
import { DEFAULT_IMG_ALT } from "@Constants";
import clsx from "clsx";

import { P_Props } from "./BackgroundBreadcrumbs.type";
import { LazyLoadImage } from "react-lazy-load-image-component";

const BackgroundBreadcrumbsPresenter: React.FC<P_Props> = (props) => {
  const classes = clsx({
    "background-breadcrumbs": true,
    [props.className || ""]: Boolean(props.className),
  });

  return (
    <div
      className={classes}
      style={{ ...props.style, backgroundImage: `url("${props.banner.bgSrc}")` }}>
      <CustomBreadcrumbs {...props.breadcrumbsCompProps} />
      <div className="background-breadcrumbs__banner">
        <div className="background-breadcrumbs__banner__icon">
          <LazyLoadImage alt={DEFAULT_IMG_ALT} src={props.banner.iconSrc} />
        </div>
        <div className="background-breadcrumbs__banner__title">{props.banner.title}</div>
      </div>
    </div>
  );
};

export default BackgroundBreadcrumbsPresenter;
