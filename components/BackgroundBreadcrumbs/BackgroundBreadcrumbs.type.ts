import React from "react";
import { P_Props as BreadcrumbsCompProps } from "../CustomBreadcrumbs/CustomBreadcrumbs.type";

export type P_Props = {
  className?: string;
  style?: React.CSSProperties;
  breadcrumbsCompProps: BreadcrumbsCompProps;
  banner: {
    bgSrc: string;
    iconSrc: string;
    title: string;
  };
};
