import React from "react";
import moment from "moment";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { DEFAULT_IMG_ALT } from "@Constants";
import { ImageUtil, LocalizationUtil, NewsUtil } from "@Utils";
import { P_Props } from "./NewsCard1.type";
import Link from "next/link";

const NewsCard1Presenter: React.FC<P_Props> = (props) => {
  return (
    <div className="news-card-1">
      <Link href={NewsUtil.buildNewsDetailPath(props.data)}>
        <a>
          <div className="news-card-1__img">
            <LazyLoadImage
              alt={props.data.imageAltText || DEFAULT_IMG_ALT}
              src={ImageUtil.buildNewsImgSrc(props.data.imageFeatured)}
            />
          </div>
        </a>
      </Link>
      <div className="news-card-1__info">
        <div className="news-card-1__info__title">
          {LocalizationUtil.translateKey({
            data: props.data,
            mainKey: "title",
            currentLang: props.currentLang,
          })}
        </div>
        <div className="news-card-1__info__content">
          {LocalizationUtil.translateKey({
            data: props.data,
            mainKey: "shortDescription",
            currentLang: props.currentLang,
          })}
        </div>
        <div className="news-card-1__info__created">
          {moment(props.data.timePublication).format("DD-MM-YYYY")}
        </div>
      </div>
    </div>
  );
};

export default NewsCard1Presenter;
