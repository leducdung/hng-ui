import { NewsOutputModel } from "@Models";

export type P_Props = {
  data: NewsOutputModel;
  currentLang?: string;
};
