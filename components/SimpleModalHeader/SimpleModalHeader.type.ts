export type P_Props = {
  title: React.ReactNode;
  onClose?: () => void;
  className?: string;
  style?: React.CSSProperties;
};
