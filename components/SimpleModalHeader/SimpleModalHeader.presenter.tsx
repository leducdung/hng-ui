import React from "react";
import { P_Props } from "./SimpleModalHeader.type";
import { Close as CloseIcon } from "@material-ui/icons";
// import "./SimpleModalHeader.style.scss";
import { loadCallback } from "@Utils";

const SimpleModalHeaderPresenter: React.FC<P_Props> = (props) => {
  return (
    <div className="app-form-header">
      <div className="title">{props.title}</div>
      {props.onClose && (
        <button
          type="button"
          className="close"
          onClick={() => {
            loadCallback(props.onClose);
          }}>
          <span className="sr-only"></span>
          <span aria-hidden className="close-icon">
            <CloseIcon style={{ fontSize: "2.2rem" }} />
          </span>
        </button>
      )}
    </div>
  );
};

export default React.memo(SimpleModalHeaderPresenter);
