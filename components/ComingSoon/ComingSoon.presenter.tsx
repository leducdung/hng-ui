import React from "react";
import clsx from "clsx";
import { P_Props } from "./ComingSoon.type";
import { APP_ROUTES, DEFAULT_COMING_SOON } from "@Constants";
import Link from "next/link";
import Image from "next/image";
import { loadCallback } from "@Utils";
import { IconButton } from "@material-ui/core";
import { Close as CloseIcon } from "@material-ui/icons";
import Img from "../../public/images/comingsoon.png";

const ComingSoonPresenter: React.FC<P_Props> = (props) => {
  const classes = clsx({
    "comingsoon-comp": true,
    [props.className || ""]: Boolean(props.className),
  });

  return (
    <div className={classes} style={props.style}>
      <Image alt="coming soon" src={Img} width={600} height={301} />
      <div className="comingsoon-comp__info">
        <div className="comingsoon-comp__info__title">
          {props.title || DEFAULT_COMING_SOON.TITLES.INFO}
        </div>
        {!props.modifyBackButton ? (
          <Link href={APP_ROUTES.HOME}>
            <a
              className="comingsoon-comp__info__back"
              onClick={() => {
                loadCallback(props.onBackToHome);
              }}>
              Quay lại trang chủ
            </a>
          </Link>
        ) : (
          <button
            className="comingsoon-comp__info__back"
            onClick={() => {
              loadCallback(props.modifyBackButton?.onClick);
            }}>
            {props.modifyBackButton.text}
          </button>
        )}
      </div>
      {props.onClose && (
        <IconButton
          className="comingsoon-comp__close"
          onClick={() => {
            loadCallback(props.onClose);
          }}>
          <CloseIcon style={{ fontSize: "2.2rem", color: "#fff" }} />
        </IconButton>
      )}
    </div>
  );
};

export default ComingSoonPresenter;
