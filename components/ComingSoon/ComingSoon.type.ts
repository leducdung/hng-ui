import React from "react";

export type P_Props = {
  className?: string;
  style?: React.CSSProperties;
  onClose?: () => void;
  title?: string;
  subTitle?: string;
  imgSrc?: string;
  onBackToHome?: () => void;
  modifyBackButton?: {
    onClick: () => void;
    text: string;
  };
};
