export type P_Props = {
  id: string;
  img: string;
  title: string;
  price: string;
  discount: string | null;
  slug?: string;
};
