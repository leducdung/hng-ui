import React from "react";
import { P_Props } from "./ProductCard1.type";
import { useBreakpoints } from "../../hooks";
import { Add as AddIcon } from "@material-ui/icons";
import Link from "next/link";
import Image from "next/image";

const ProductCard1Presenter: React.FC<P_Props> = (props) => {
  const breakpoints = useBreakpoints();

  return (
    <div className="product-card-1">
      <Link href={props.slug ?? "/product/slug"}>
        <a className="product-card-1__item">
          <div className="product-card-1__item__img">
            <Image
              loader={({ src }) => src}
              loading="lazy"
              src={props.img || "/images/app-icon.png"}
              alt={props.title}
              layout="fill"
            />
          </div>
          <div className="product-card-1__item__title">{props.title}</div>
          <div className="product-card-1__item__price">{props.price}</div>
        </a>
      </Link>
      {props.discount != "0" && (
        <div className="product-card-1__item__discount">
          <div className="product-card-1__item__discount__percent">
            <span>{props.discount}</span>
            <span>%</span>
          </div>
          <div className="product-card-1__item__discount__off">OFF</div>
        </div>
      )}
      {!breakpoints.lg && (
        <div className="product-card-1__item__plus">
          <AddIcon style={{ fontSize: "2.2rem", color: "#9E9E9E" }} />
        </div>
      )}
    </div>
  );
};

export default ProductCard1Presenter;
