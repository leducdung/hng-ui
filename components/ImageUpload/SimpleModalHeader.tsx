import React from "react";
import { loadCallback } from "@Utils";
import { Close as CloseIcon } from "@material-ui/icons";
import styles from "./SimpleModalHeader.module.scss";

export type P_Props = {
  title: React.ReactNode;
  onClose?: () => void;
  className?: string;
  style?: React.CSSProperties;
};

const SimpleModalHeaderPresenter: React.FC<P_Props> = (props) => {
  return (
    <div className={styles.app_form_header}>
      <div className={styles.title}>{props.title}</div>
      {props.onClose && (
        <button
          type="button"
          className={styles.close}
          onClick={() => {
            loadCallback(props.onClose);
          }}>
          <span className={styles.sr_only}></span>
          <span aria-hidden className={styles.close_icon}>
            <CloseIcon style={{ fontSize: "2.2rem" }} />
          </span>
        </button>
      )}
    </div>
  );
};

export default React.memo(SimpleModalHeaderPresenter);
