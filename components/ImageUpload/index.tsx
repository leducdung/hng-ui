import React, { useState } from "react";
import clsx from "clsx";
import { ImageUploadIcon, SelectedStarIcon, UnselectedStarIcon } from "@Components/Icons";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { DEFAULT_IMG_ALT } from "@Constants";
import {
  DeleteOutline as DeleteOutlineIcon,
  VisibilityOutlined as VisibilityOutlinedIcon,
} from "@material-ui/icons";
import { Dialog, TextField } from "@material-ui/core";
import SimpleModalHeader from "./SimpleModalHeader";
import { loadCallback } from "@Utils";
import styles from "./ImageUpload.module.scss";

export type UploadedImageModel = {
  id?: string;
  file: File | undefined;
  fileStr?: string;
  alt?: string;
};

export type OutputImageModel = {
  path: string;
  __typename: string;
};

export type P_Props = {
  className?: string;
  max: number;
  style?: React.CSSProperties;
  fileList: OutputImageModel[];
  onChange: (file: File | undefined) => void;
  onDelete: (path: string) => void;
  config?: {
    uploadModal?: boolean;
  };
  selectConfig?: {
    selectedIds: string[];
    onSelect: (path: string, fileInList: OutputImageModel) => void;
  };
};

type ImagePreview = {
  visible: boolean;
  // initData: { id?: string; src?: string; alt: string; file: any };
  initData: { file: File | undefined; path?: string; alt: string };
};

const ImageUploadPresenter: React.FC<P_Props> = (props) => {
  const classes = clsx({
    // {styles.image_upload}: true,
    [props.className || ""]: Boolean(props.className),
  });
  const [imagePreview, setImagePreview] = useState<ImagePreview>({
    visible: false,
    initData: {
      file: undefined,
      alt: "",
    },
  });

  const changeImagePreviewState = <T extends keyof ImagePreview>(
    key: T,
    value: ImagePreview[T]
  ) => {
    setImagePreview({
      ...imagePreview,
      [key]: value,
    });
  };

  const closeImagePreviewModal = () => {
    setImagePreview({
      visible: false,
      initData: {
        alt: "",
        file: undefined,
      },
    });
  };

  const handleUpload = (event) => {
    const file: File = event.target.files[0];
    if (!file) return;
    props.onChange(file);
    // const reader = new FileReader();
    // reader.onloadend = function (e) {
    //   props.onChange({ file, fileStr: reader.result });
    // };

    // if (file) {
    //   reader.readAsDataURL(file);
    // }
  };

  const handleModalUpload = ({ target: { validity, files } }) => {
    const file = files[0];
    const reader = new FileReader();
    reader.onloadend = function () {
      validity.valid &&
        changeImagePreviewState("initData", {
          ...imagePreview.initData,
          file: file,
          path: reader.result as string,
        });
    };

    if (file) {
      reader.readAsDataURL(file);
    }
  };

  const handelModalSave = () => {
    // props.onChange({
    //   id: imagePreview.initData.id,
    //   file: imagePreview.initData.file,
    //   fileStr: imagePreview.initData.src,
    //   alt: imagePreview.initData.alt,
    // });
    props.onChange(imagePreview.initData.file);
    setImagePreview({
      visible: false,
      initData: {
        alt: "",
        file: undefined,
      },
    });
  };

  return (
    <>
      <div className={`${classes} ${styles.image_upload}`} style={props.style}>
        {props.fileList.length < props.max && !Boolean(props.config?.uploadModal) && (
          <div className={styles.image_upload__input}>
            <label>
              <ImageUploadIcon viewBox={[32, 32]} size={[32, 32]} color="#6658F3" />
            </label>
            <input type="file" onChange={handleUpload} />
          </div>
        )}

        {props.fileList.length < props.max && Boolean(props.config?.uploadModal) && (
          <button
            className={styles.image_upload__upload_btn}
            onClick={(e) => {
              e.preventDefault();
              changeImagePreviewState("visible", true);
            }}>
            <ImageUploadIcon viewBox={[32, 32]} size={[32, 32]} color="#6658F3" />
          </button>
        )}

        {props.fileList.map((file, idx) => {
          let isSelected = false;
          if (props.selectConfig?.selectedIds.length) {
            isSelected = props.selectConfig.selectedIds.includes(file.path);
          }

          return (
            <div key={idx} className={styles.image_upload__img}>
              <LazyLoadImage alt={DEFAULT_IMG_ALT} src={file.path} />
              <div className={styles.image_upload__img__actions}>
                {Boolean(props.config?.uploadModal) && (
                  <button
                    onClick={(e) => {
                      e.preventDefault();
                      setImagePreview({
                        visible: true,
                        initData: {
                          ...file,
                          alt: file.path || "",
                          file: undefined,
                        },
                      });
                    }}>
                    <VisibilityOutlinedIcon style={{ fontSize: "2rem", color: "#fff" }} />
                  </button>
                )}
                <button
                  onClick={(e) => {
                    e.preventDefault();
                    props.onDelete(file.path);
                  }}>
                  <DeleteOutlineIcon style={{ fontSize: "2rem", color: "#fff" }} />
                </button>
              </div>

              {props.selectConfig &&
                (isSelected ? (
                  <button
                    className={styles.image_upload__img__selected}
                    onClick={(e) => {
                      e.preventDefault();
                      loadCallback(props.selectConfig?.onSelect, file.path, file);
                    }}>
                    <SelectedStarIcon viewBox={[19, 19]} size={[19, 19]} color="#ffbc1f" />
                  </button>
                ) : (
                  <button
                    className={styles.image_upload__img__unselected}
                    onClick={(e) => {
                      e.preventDefault();
                      loadCallback(props.selectConfig?.onSelect, file.path, file);
                    }}>
                    <UnselectedStarIcon viewBox={[19, 19]} size={[19, 19]} color="#bdc6d7" />
                  </button>
                ))}
            </div>
          );
        })}
      </div>
      <Dialog open={imagePreview.visible} maxWidth="sm" fullWidth>
        <div className={styles.image_upload__preview}>
          <SimpleModalHeader
            title="Tải ảnh"
            onClose={() => {
              closeImagePreviewModal();
            }}
          />
          <div className={styles.image_upload__preview__body}>
            <div className={styles.image_upload__preview__body__upload}>
              {imagePreview.initData.path ? (
                <div className={styles.image_upload__preview__body__upload__img}>
                  <LazyLoadImage alt={DEFAULT_IMG_ALT} src={imagePreview.initData.path} />
                  <div className={styles.image_upload__preview__body__upload__img__actions}>
                    <button
                      onClick={() => {
                        changeImagePreviewState("initData", {
                          ...imagePreview.initData,
                          path: undefined,
                          file: undefined,
                        });
                      }}>
                      <DeleteOutlineIcon style={{ fontSize: "2rem", color: "#fff" }} />
                    </button>
                  </div>
                </div>
              ) : (
                <div className={styles.image_upload__preview__body__input}>
                  <label>
                    <ImageUploadIcon viewBox={[32, 32]} size={[32, 32]} color="#6658F3" />
                  </label>
                  <input accept="image/*" type="file" onChange={handleModalUpload} />
                </div>
              )}
            </div>
            <div className={styles.image_upload__preview__body__alt}>Mô tả hình ảnh (ALT)</div>
          </div>
          <div className={styles.image_upload__preview__actions}>
            <TextField
              fullWidth
              placeholder="Alt"
              variant="outlined"
              className="form-text-field"
              inputProps={{ className: "input" }}
              // value={imagePreview.initData.alt}
              onChange={() => {
                // const value = e.target.value;

                changeImagePreviewState("initData", {
                  ...imagePreview.initData,
                  // alt: value,
                });
              }}
            />
            <button
              disabled={Boolean(
                !imagePreview.initData.file
                // (!imagePreview.initData.file && !imagePreview.initData.id) ||
                //   (imagePreview.initData.id && !imagePreview.initData.src)
              )}
              className={styles.image_upload__preview__actions__save}
              onClick={() => {
                handelModalSave();
              }}>
              Lưu
            </button>
          </div>
        </div>
      </Dialog>
    </>
  );
};

export default ImageUploadPresenter;
