import React, { useState } from "react";
import { DEFAULT_IMG_ALT } from "@Constants";
import Pulse from "react-reveal/Pulse";
import { useBreakpoints } from "../../hooks";
import { Dialog } from "@material-ui/core";
import { Registration } from "@Components";
import Image from "next/image";
import Banner from "../../public/images/aboutus/banner.png";
import Img1 from "../../public/images/mock/new/v1.png";
import Img2 from "../../public/images/mock/new/v2.png";
import Img3 from "../../public/images/mock/new/v3.png";
import Img4 from "../../public/images/mock/new/v4.png";
import Img5 from "../../public/images/mock/new/v5.png";
import Img6 from "../../public/images/mock/new/v6.png";

const COLLECTION_ITEM_STYLES = [
  { top: "9%", left: "19%" },
  { top: "9%", right: "19%" },
  { top: "40%", left: "0%" },
  { top: "40%", right: "0%" },
  { top: "67%", left: "19%" },
  { top: "67%", right: "19%" },
];

const COLLECTION_ITEM_STYLES_MOBILE = [
  { top: "40%", left: "24%" },
  { top: "40%", right: "24%" },
  { top: "70%", left: "5%" },
  { top: "70%", right: "5%" },
  { top: "100%", left: "24%" },
  { top: "100%", right: "24%" },
];

const SPECIAL_PRODUCTS = [
  {
    id: "1",
    img: Img1,
  },
  {
    id: "2",
    img: Img2,
  },
  {
    id: "3",
    img: Img3,
  },
  {
    id: "4",
    img: Img4,
  },
  {
    id: "5",
    img: Img5,
  },
  {
    id: "6",
    img: Img6,
  },
];

export type ProductItem = {
  id: string;
  img: StaticImageData;
};

const SpecialProducts: React.FC = () => {
  const breakpoints = useBreakpoints();
  const [selectCollection, setSelectCollection] = useState<ProductItem | null>(
    SPECIAL_PRODUCTS[0] ?? null
  );
  const [visibleViewerModal, setVisibleViewerModal] = useState<boolean>(false);

  const toggleViewerModal = () => {
    setVisibleViewerModal(!visibleViewerModal);
  };

  return (
    <>
      <div className="special-products">
        <div className="special-products__banner">
          <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="fill" objectFit="cover" />
        </div>
        <div className="special-products__body">
          <div className="special-products-left">
            {breakpoints.md && (
              <div className="special-products__title">
                sản phẩm ưu đãi và
                <br />
                độc quyền
              </div>
            )}
            <div className="special-products__list">
              <div className="special-products__list__item">
                <div className="special-products__list__item__circle"></div>
                <div className="special-products__list__item__text">
                  Bộ sản phẩm Trang sức Happy Stone, công nghệ chế tác trang sức độc đáo từ HanaGold
                </div>
              </div>
              <div className="special-products__list__item">
                <div className="special-products__list__item__circle"></div>
                <div className="special-products__list__item__text">
                  Mẫu mã đa dạng, bao gồm các mẫu trang sức theo món, theo bộ sưu tập với phiên bản
                  giới hạn và độc quyền sang trọng, tinh tế.
                </div>
              </div>
              <div className="special-products__list__item">
                <div className="special-products__list__item__circle"></div>
                <div className="special-products__list__item__text">
                  Tối ưu nguồn thu với 10 hạng mục sinh lời trên các phương diện phát triển kinh
                  doanh như mở rộng đối tượng khách hàng, mua bán sản phẩm và các chính sách hoa
                  hồng hấp dẫn.
                </div>
              </div>
            </div>
            <Pulse delay={1500} forever>
              <button className="btn-submit" onClick={toggleViewerModal}>
                Đăng kí tư vấn
              </button>
            </Pulse>
          </div>
          <div className="special-products-right">
            {!breakpoints.md && (
              <div className="special-products__title">sản phẩm ưu đãi và độc quyền</div>
            )}
            {selectCollection && (
              <div className="special-products-right__show">
                <Image src={selectCollection.img} alt={DEFAULT_IMG_ALT} />
              </div>
            )}

            <div className="special-products-right__list">
              {SPECIAL_PRODUCTS.map((item, idx) => {
                return (
                  <>
                    {breakpoints.md && (
                      <div
                        key={idx}
                        className="special-products-right__images"
                        onClick={() => setSelectCollection(item)}
                        style={COLLECTION_ITEM_STYLES[idx]}>
                        <div className="image">
                          <Image
                            src={item.img}
                            alt={DEFAULT_IMG_ALT}
                            placeholder="blur"
                            layout="fill"
                            objectFit="cover"
                          />
                        </div>
                      </div>
                    )}
                    {!breakpoints.md && (
                      <div
                        key={idx}
                        className="special-products-right__images"
                        onClick={() => setSelectCollection(item)}
                        style={COLLECTION_ITEM_STYLES_MOBILE[idx]}>
                        <div className="image">
                          <Image
                            src={item.img}
                            alt={DEFAULT_IMG_ALT}
                            placeholder="blur"
                            layout="fill"
                            objectFit="cover"
                          />
                        </div>
                      </div>
                    )}
                  </>
                );
              })}
            </div>
          </div>
        </div>
      </div>
      <Dialog
        open={visibleViewerModal}
        onClose={() => {
          toggleViewerModal();
        }}
        fullScreen={!breakpoints.lg ? true : false}
        className="registration__dialog">
        <Registration
          onClose={() => {
            toggleViewerModal();
          }}
        />
      </Dialog>
    </>
  );
};

export default SpecialProducts;
