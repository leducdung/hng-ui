import React from "react";
import clsx from "clsx";
import style from "./Dot.module.scss";

export type P_Props = {
  className?: string;
  style?: React.CSSProperties;
};

const Dot: React.FC<P_Props> = (props) => {
  const classes = clsx({
    [props.className || ""]: Boolean(props.className),
  });

  return <div className={`${classes} ${style.dotComp}`} style={props.style}></div>;
};

export default Dot;
