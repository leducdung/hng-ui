import React, { useState } from "react";
import { P_Props } from "./CategoryCard1.type";
import { Avatar } from "@material-ui/core";
import { getFirstChar, ImageUtil, loadCallback } from "@Utils";
import { MoreHoriz as MoreHorizIcon } from "@material-ui/icons";
import styles from "./CategoryCard1.module.scss";

const CategoryCard1Presenter: React.FC<P_Props> = (props) => {
  const buildPictureSrc = props.buildPictureSrc || ImageUtil.buildProductCategoryImgSrc;
  const [moreIconVisible, showMoreIcon] = useState<boolean>(false);

  return (
    <div
      className={`${props.selected && styles.active} ${styles["category-card-1"]}`}
      onClick={() => {
        props.onClick();
      }}
      onMouseEnter={() => showMoreIcon(true)}
      onMouseLeave={() => showMoreIcon(false)}>
      <Avatar
        alt={props.name}
        src={props.isAvatar ? buildPictureSrc(props.picture) : props.picture}>
        {props.isAvatar ? (props.name !== undefined ? getFirstChar(props.name) : "") : ""}
      </Avatar>
      <div className={styles["category-card-1__name"]}>{props.name}</div>

      {moreIconVisible && (
        <div
          className={styles["category-card-1__action"]}
          onClick={(e) => {
            e.stopPropagation();
            loadCallback(props.onActionClick, e);
          }}>
          {props.moreIcon ? <MoreHorizIcon style={{ fontSize: "2.2rem" }} /> : ""}
        </div>
      )}
    </div>
  );
};

export default React.memo(CategoryCard1Presenter);
