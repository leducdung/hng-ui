export type P_Props = {
  selected?: boolean;
  onClick: () => void;
  name: string;
  picture?: string;
  buildPictureSrc?: (src: string | null | undefined) => string | undefined;
  onActionClick: (e) => void;
  moreIcon: boolean;
  isAvatar: boolean;
};
