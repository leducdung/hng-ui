import React, { useState } from "react";
import { DEFAULT_IMG_ALT } from "@Constants";
import Pulse from "react-reveal/Pulse";
import { useBreakpoints } from "../../hooks";
import { Dialog } from "@material-ui/core";
import { Registration } from "@Components";
import Image from "next/image";
import Banner from "../../public/images/banner/bg_why.png";
import Img1 from "../../public/images/mock/new/img1.png";
import Img2 from "../../public/images/mock/new/img2.png";

const Opportunity: React.FC = () => {
  const breakpoints = useBreakpoints();

  const [visibleViewerModal, setVisibleViewerModal] = useState<boolean>(false);

  const toggleViewerModal = () => {
    setVisibleViewerModal(!visibleViewerModal);
  };

  return (
    <>
      <div className="opportunity">
        <div className="opportunity__banner">
          <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="fill" objectFit="cover" />
        </div>
        <div className="opportunity__content">
          <div className="opportunity__title">
            CƠ HỘI TRỞ THÀNH
            <br />
            CHỦ TIỆM KIM HOÀN 4.0
          </div>
          <Pulse delay={1500} forever>
            <button className="opportunity__btn-submit" onClick={toggleViewerModal}>
              Đăng kí tư vấn
            </button>
          </Pulse>
          <div className="opportunity__body">
            <div className="opportunity__body__left">
              <div className="opportunity__body__img">
                <Image
                  src={Img1}
                  alt={DEFAULT_IMG_ALT}
                  layout="fill"
                  objectFit="cover"
                  placeholder="blur"
                />
              </div>
              <div className="opportunity__body__left__text">
                {breakpoints.lg ? (
                  <>
                    <div className="opportunity__body__title">CAM KẾT</div>
                    <div className="opportunity__body__description">
                      Hỗ trợ xuyên suốt thời gian hoạt động showroom
                      <br />
                      Cung cấp 100% sản phẩm bán và trưng bày
                      <br />
                      Trang bị phần mềm bản quyền - decor độc quyền
                    </div>
                  </>
                ) : (
                  <>
                    <div className="opportunity__body__title">
                      NGÂN SÁCH CHỈ
                      <br />
                      TỪ 500 TRIỆU
                    </div>
                    <div className="opportunity__body__description">
                      Hỗ trợ decor, set up toàn bộ showroom vàng <br />
                      Tư vấn, đào tạo nhân sự chuyên nghiệp <br />
                      Triển khai kinh doanh, truyền thông trong 3 tháng đầu
                    </div>
                  </>
                )}
              </div>
            </div>
            <div className="opportunity__body__right">
              <div className="opportunity__body__right__text">
                {breakpoints.lg ? (
                  <>
                    <div className="opportunity__body__title">NGÂN SÁCH CHỈ TỪ 500 TRIỆU</div>
                    <div className="opportunity__body__description">
                      Hỗ trợ decor, set up toàn bộ showroom vàng <br />
                      Tư vấn, đào tạo nhân sự chuyên nghiệp <br />
                      Triển khai kinh doanh, truyền thông trong 3 tháng đầu
                    </div>
                  </>
                ) : (
                  <>
                    <div className="opportunity__body__title">CAM KẾT</div>
                    <div className="opportunity__body__description">
                      Hỗ trợ xuyên suốt thời gian hoạt động showroom
                      <br />
                      Cung cấp 100% sản phẩm bán và trưng bày
                      <br />
                      Trang bị phần mềm bản quyền - decor độc quyền
                    </div>
                  </>
                )}
              </div>
              <div className="opportunity__body__img opportunity__body__img__right">
                <Image
                  src={Img2}
                  alt={DEFAULT_IMG_ALT}
                  layout="fill"
                  objectFit="cover"
                  placeholder="blur"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <Dialog
        open={visibleViewerModal}
        onClose={() => {
          toggleViewerModal();
        }}
        fullScreen={!breakpoints.lg ? true : false}
        className="registration__dialog">
        <Registration
          onClose={() => {
            toggleViewerModal();
          }}
        />
      </Dialog>
    </>
  );
};

export default Opportunity;
