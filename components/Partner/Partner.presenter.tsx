import React from "react";
import { DEFAULT_IMG_ALT } from "../../constants";
import { ImgPartner } from "../../constants/partner";
import Image from "next/image";

const PartnerPresenter: React.FC = () => {
  return (
    <div className="partner">
      <div className="partner__title">đỐI TÁC HANAGOLD</div>
      <div className="partner__list">
        {ImgPartner.map((p) => {
          return (
            <div key={p.id} className="partner__img">
              <div className="img-white">
                <Image src={p.img} alt={DEFAULT_IMG_ALT} placeholder="blur" />
              </div>
              <div className="img-color">
                <Image src={p.img_act} alt={DEFAULT_IMG_ALT} placeholder="blur" />
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default PartnerPresenter;
