import Image from "next/image";
import Link from "next/link";
import React, { useState } from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { P_Props } from "./Collection.type";
import Fade from "react-reveal/Fade";
import { useBreakpoints } from "../../hooks";
import { BtnGroup } from "..";
import { APP_ROUTES } from "@Constants";
const IMG_PATH = `/images/collection/`;

const COLLECTION_ITEM_STYLES = [
  { top: "24%", left: "17%" },
  { top: "24%", right: "17%" },
  { top: "47%", left: "5%" },
  { top: "47%", right: "5%" },
  { top: "70%", left: "17%" },
  { top: "70%", right: "17%" },
];

const COLLECTION_4_DATA = [
  {
    id: "1",
    img: `${IMG_PATH}img1.gif`,
    title: "Dây chuyền Dancing Love Diamond",
    description: "16,000,000 đ",
  },
  {
    id: "2",
    img: `${IMG_PATH}img2.gif`,
    title: "Dây chuyền Dancing Love Diamond",
    description: "16,000,000 đ",
  },
  {
    id: "3",
    img: `${IMG_PATH}img3.gif`,
    title: "Dây chuyền Dancing Love Diamond",
    description: "16,000,000 đ",
  },
  {
    id: "4",
    img: `${IMG_PATH}img4.gif`,
    title: "Dây chuyền Dancing Love Diamond",
    description: "16,000,000 đ",
  },
  {
    id: "5",
    img: `${IMG_PATH}img5.gif`,
    title: "Dây chuyền Dancing Love Diamond",
    description: "16,000,000 đ",
  },
  {
    id: "6",
    img: `${IMG_PATH}img6.gif`,
    title: "Dây chuyền Dancing Love Diamond",
    description: "16,000,000 đ",
  },
];

export type CollectionItem = {
  id: string;
  img: string;
  title: string;
  description: string;
};

const CollectionPresenter: React.FC<P_Props> = () => {
  const breakpoints = useBreakpoints();
  const [selectCollection, setSelectCollection] = useState<CollectionItem | null>(
    COLLECTION_4_DATA[0] ?? null
  );

  return (
    <section>
      {breakpoints.lg && (
        <div className="collection">
          <Image
            src={"/images/collection/bg.png"}
            objectFit={"cover"}
            layout={"fill"}
            alt={"collection background"}
          />

          <div className="collection__body">
            <div className="collection__container">
              <Fade ssrReveal top>
                <div className="collection__title">bộ sưu tập đá khiêu vũ</div>
                <div className="collection__description">
                  Lấy cảm hứng từ quy luật điểm cân bằng và năng lượng chuyển động liên tục
                  <br />
                  của cơ thể người, khiến cho đá chủ có thể lắc qua lại, lấp lánh không ngừng.
                </div>
              </Fade>
              <div className="collection__select">
                {selectCollection && (
                  <Image
                    src={selectCollection.img}
                    objectFit={"cover"}
                    layout={"fill"}
                    alt={selectCollection.title}
                  />
                )}
              </div>
              <div className="collection__info">
                <div className="collection__info__title">{selectCollection?.title}</div>
                <div className="collection__info__desc">{selectCollection?.description}</div>
                <Link href={APP_ROUTES.DANCING_STONE}>
                  <a className="home-page__news__list-link">Xem tất cả sản phẩm</a>
                </Link>
              </div>
            </div>

            <div className="collection__list">
              {COLLECTION_4_DATA.map((item, idx) => (
                <Fade key={idx} bottom ssrReveal>
                  <div
                    className="collection__list__item"
                    onClick={() => setSelectCollection(item)}
                    style={COLLECTION_ITEM_STYLES[idx]}>
                    <Image src={item.img} layout={"fill"} alt={item.title} />
                  </div>
                </Fade>
              ))}
            </div>
          </div>
        </div>
      )}
      {!breakpoints.lg && (
        <div className="collection">
          <LazyLoadImage
            src={"/images/collection/bg.png"}
            alt={"collection background"}
            className="collect-bg"
          />

          <div className="collect">
            <div className="collect__body">
              <Fade ssrReveal top>
                <div className="collection__title">
                  bộ sưu tập đá
                  <br />
                  khiêu vũ
                </div>
              </Fade>
              <div className="collect__list">
                <BtnGroup<CollectionItem>
                  list={COLLECTION_4_DATA}
                  onClick={(item) => {
                    setSelectCollection(item);
                  }}
                  renderBtnLabel={(item) => {
                    return (
                      <Fade bottom>
                        <div className="collect__list__item">
                          <LazyLoadImage src={item.img} alt={item.title} />
                        </div>
                      </Fade>
                    );
                  }}
                />
              </div>
              <div className="collect__container">
                <div className="collect__select">
                  {selectCollection && (
                    <LazyLoadImage src={selectCollection.img} alt={selectCollection.title} />
                  )}
                </div>
                <div className="collect__info">
                  <div className="collection__info__title">{selectCollection?.title}</div>
                  <div className="collection__info__desc">{selectCollection?.description}</div>
                  <div className="home-page__product__btn">
                    <Link href={APP_ROUTES.DANCING_STONE}>
                      <a className="btn">Xem tất cả sản phẩm</a>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </section>
  );
};

export default CollectionPresenter;
