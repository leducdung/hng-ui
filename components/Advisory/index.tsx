import React from "react";
import { ADVISORY_DATA, DEFAULT_IMG_ALT } from "../../constants";
import Image from "next/image";
import styles from "./index.module.scss";
import Bg from "../../public/images/consultant/bg.png";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation } from "swiper";
import { useBreakpoints } from "hooks";

SwiperCore.use([Navigation]);

const Advisory: React.FC = () => {
  const breakpoints = useBreakpoints();
  return (
    <div className={styles.advisory}>
      <Image src={Bg} alt={DEFAULT_IMG_ALT} layout="fill" objectFit="cover" />
      <div className={styles.content}>
        <div className={styles.title}>ban cố vấn</div>
        <div className={styles.list}>
          {breakpoints.lg ? (
            <Swiper
              centeredSlides
              slidesPerView={3}
              loop
              navigation={{
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
              }}
              className={styles.swiper}>
              {ADVISORY_DATA.map((s, idx) => {
                return (
                  <SwiperSlide key={s.id}>
                    {({ isNext }) => {
                      return (
                        <div className={styles.item}>
                          <div className={isNext ? styles.imageact : styles.image}>
                            <Image
                              src={isNext ? s.imgact : s.img}
                              alt={DEFAULT_IMG_ALT}
                              objectFit="cover"
                            />
                          </div>
                          {isNext && (
                            <>
                              {(() => {
                                const activeInfo = ADVISORY_DATA.find(
                                  (o) => idx.toString() === o.id
                                );
                                console.log(idx);
                                if (!activeInfo) {
                                  return <div></div>;
                                }
                                return (
                                  <div className={styles.des}>
                                    <div className={styles.name}>
                                      {activeInfo.name}
                                      <br />
                                      {activeInfo.title}
                                    </div>
                                    <br />
                                    <div className={styles.paragraph}>
                                      <div>
                                        <span className={styles.des_name}>
                                          {activeInfo.description_name}
                                        </span>
                                        {activeInfo.des}
                                      </div>
                                      <br />
                                      {activeInfo.description?.map((d) => {
                                        return (
                                          <>
                                            <div className={styles.desc_description}>
                                              {d.paragraph}
                                            </div>
                                            <br />
                                          </>
                                        );
                                      })}
                                    </div>
                                  </div>
                                );
                              })()}
                            </>
                          )}
                        </div>
                      );
                    }}
                  </SwiperSlide>
                );
              })}
            </Swiper>
          ) : (
            <Swiper
              slidesPerView={1}
              pagination={{
                clickable: true,
              }}
              loop
              navigation={{
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
              }}>
              {ADVISORY_DATA.map((s) => {
                return (
                  <SwiperSlide key={s.id}>
                    <div className={styles.item}>
                      <div className={styles.image}>
                        <Image src={s.imgact} alt={DEFAULT_IMG_ALT} objectFit="cover" />
                      </div>
                      <div className={styles.des}>
                        <div className={styles.name}>
                          {s.name}
                          <br />
                          {s.title}
                        </div>
                        <br />
                        <div className={styles.paragraph}>
                          <div>
                            <span className={styles.des_name}>{s.description_name}</span>
                            {s.des}
                          </div>
                          <br />
                          {s.description?.map((d) => {
                            return (
                              <>
                                <div className={styles.desc_description}>{d.paragraph}</div>
                                <br />
                              </>
                            );
                          })}
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                );
              })}
            </Swiper>
          )}
          <button className={`swiper-button-prev ${styles.button}`}></button>
          <button className={`swiper-button-next ${styles["button-next"]}`}></button>
        </div>
      </div>
    </div>
  );
};

export default Advisory;
