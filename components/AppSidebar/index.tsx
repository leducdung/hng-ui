import React from "react";
import Image from "next/image";
import { DEFAULT_LOGO_IMG_ALT } from "@Constants";
import { useRouter } from "next/router";
import navigatorList, { DEFAULT_NAVBAR_ICON_COLOR } from "./NavigatorItems";
import Link from "next/link";

const AppSidebar: React.FC = () => {
  const router = useRouter();

  const currentRoute = React.useMemo(() => {
    // reverse the routes to get longest first
    const routes = navigatorList.map(({ baseRoute }) => baseRoute).reverse();
    const selected = routes.find((route) => router.pathname.startsWith(route));
    return selected;
  }, [router.pathname]);
  return (
    <nav className="app-sidebar">
      <ul>
        <li className="nav-item">
          <Image width="100%" height="100%" alt={DEFAULT_LOGO_IMG_ALT} src="/images/app-icon.png" />
        </li>
        {navigatorList.map(({ label, icon: IconComponent, iconConfigs, baseRoute }) => {
          const selected = currentRoute === baseRoute;
          const color = selected ? "#FFF" : DEFAULT_NAVBAR_ICON_COLOR;
          return (
            <li key={baseRoute}>
              <Link href={baseRoute} passHref>
                <a>
                  <div className={`nav-item ${selected ? "selected" : ""}`}>
                    <div>
                      <IconComponent {...iconConfigs} color={color} />
                    </div>
                    <div className="label">{label}</div>
                  </div>
                </a>
              </Link>
            </li>
          );
        })}
      </ul>
    </nav>
  );
};
export default AppSidebar;
