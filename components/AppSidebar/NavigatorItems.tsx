import { IconModel } from "@Models";
import { GoldBarIcon } from "@Components/Icons/GoldBar";
import { XOIcon } from "@Components/Icons/XO";
import { BarChartIcon } from "@Components/Icons/BarChart";
import { StoreIcon } from "@Components/Icons/Store";
import { ThreeQueIcon } from "@Components/Icons/ThreeQue";

export const DEFAULT_NAVBAR_ICON_COLOR = "#808283";

type NavigatorItem = {
  label: string;
  headingLabel: string;
  icon: React.ElementType;
  baseRoute: string;
  iconConfigs: IconModel;
  subRoutes: Array<Pick<NavigatorItem, "label" | "baseRoute">>;
};

const navigatorList: NavigatorItem[] = [
  {
    label: "vàng 4.0",
    headingLabel: "",
    icon: GoldBarIcon,
    baseRoute: "/app",
    iconConfigs: {
      color: DEFAULT_NAVBAR_ICON_COLOR,
      size: [33, 31.35],
      viewBox: [33, 31.35],
    },
    subRoutes: [],
  },
  {
    label: "sản phẩm",
    headingLabel: "sản phẩm",
    icon: XOIcon,
    baseRoute: "/app/product",
    iconConfigs: {
      color: DEFAULT_NAVBAR_ICON_COLOR,
      size: [27, 27],
      viewBox: [27, 27],
    },
    subRoutes: [
      {
        baseRoute: "/app/product",
        label: "",
      },
    ],
  },
  {
    label: "Biểu đồ",
    headingLabel: "Biểu đồ",
    icon: BarChartIcon,
    baseRoute: "/app/statistics",
    iconConfigs: {
      color: DEFAULT_NAVBAR_ICON_COLOR,
      size: [28, 25.65],
      viewBox: [28, 25.65],
    },
    subRoutes: [
      {
        baseRoute: "/app/statistics",
        label: "TỔNG QUAN",
      },
      {
        baseRoute: "/app/statistics/prices",
        label: "GIÁ VÀNG",
      },
      {
        baseRoute: "/app/statistics/charts",
        label: "BIỂU ĐỒ",
      },
      {
        baseRoute: "/app/statistics/news",
        label: "TIN TỨC",
      },
    ],
  },
  {
    label: "Cửa hàng",
    headingLabel: "Cửa hàng",
    icon: StoreIcon,
    baseRoute: "/app/stores",
    iconConfigs: {
      color: DEFAULT_NAVBAR_ICON_COLOR,
      size: [33, 27],
      viewBox: [33, 27],
    },
    subRoutes: [
      {
        baseRoute: "/app/stores",
        label: "",
      },
    ],
  },
  {
    label: "khác",
    headingLabel: "cài đặt",
    icon: ThreeQueIcon,
    baseRoute: "/app/settings",
    iconConfigs: {
      color: DEFAULT_NAVBAR_ICON_COLOR,
      size: [22, 20],
      viewBox: [22, 20],
    },
    subRoutes: [
      {
        baseRoute: "/app/settings",
        label: "CÀI ĐẶT CHUNG",
      },
      {
        baseRoute: "/app/settings/private",
        label: "BẢO MẬT",
      },
      {
        baseRoute: "/app/settings/support",
        label: "HỖ TRỢ",
      },
    ],
  },
];

export default navigatorList;
