import React, { useState } from "react";
import { APP_ROUTES, DEFAULT_CUSTOMER_AVATAR, DEFAULT_IMG_ALT } from "@Constants";
import { useRouter } from "next/router";
import { Badge, Popover } from "@material-ui/core";
import { ShoppingCartOutlined as ShoppingCartOutlinedIcon } from "@material-ui/icons";
import navigatorList from "@Components/AppSidebar/NavigatorItems";
import Link from "next/link";
import CustomerQuery from "../../lib/queries/Customer.graphql";
import { useLazyQuery } from "@apollo/client";
import styles from "./index.module.scss";
import Image from "next/image";
import Icon from "../../public/images/icon/default.png";
import { useCart } from "hooks/useCart";

type Props = {
  userName: string;
  title?: string;
};
const AppHeader: React.FC<Props> = ({ userName, title }) => {
  const router = useRouter();
  const [cart] = useCart();
  const [anchorUserEl, setAnchorUserEl] = useState<HTMLElement | null>(null);
  const [logout] = useLazyQuery(CustomerQuery.Logout);
  const handleCloseUserMenu = () => {
    setAnchorUserEl(null);
  };
  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorUserEl(event.currentTarget);
  };
  const found = navigatorList.find((item) => {
    const paths = item.subRoutes.map((route) => route.baseRoute);
    return paths.includes(router.pathname);
  });
  const handleLogout = () => {
    try {
      logout();
      router.push("/");
    } catch (error) {}
  };
  const cartCount = cart.reduce((total, p) => {
    return total + p.quantity;
  }, 0);
  // @TODO change title to h1
  return (
    <React.Fragment>
      <header className={styles.appHeader}>
        <div className={styles.welcome}>
          {title ? (
            <div className={styles.title}>
              <Link href="/app" passHref>
                <a>
                  <Image
                    src={Icon}
                    alt={DEFAULT_IMG_ALT}
                    width={24}
                    height={24}
                    objectFit="contain"
                  />
                  &ensp;{title}
                </a>
              </Link>
            </div>
          ) : (
            <>
              {!found?.subRoutes.length && <div className={styles.greeting}>Welcome</div>}

              <div className={styles.title}>{(!!found && found.headingLabel) || userName}</div>
            </>
          )}
        </div>

        {!!found && (
          <nav>
            <ul className="">
              {found.subRoutes.map(({ baseRoute, label }) => {
                return (
                  <li key={baseRoute}>
                    <Link href={baseRoute} passHref>
                      <div
                        className={`${styles.navItem} ${
                          baseRoute === router.pathname ? styles.selected : ""
                        }`}>
                        <a>{label}</a>
                      </div>
                    </Link>
                  </li>
                );
              })}
            </ul>
          </nav>
        )}
        <div className={styles.userActions}>
          <div
            onClick={() => {
              router.push(APP_ROUTES.ORDER);
            }}
            className={styles.checkoutCart}>
            <Badge
              variant="dot"
              badgeContent={cartCount}
              overlap="circle"
              color="secondary"
              className="checkout-badge">
              <ShoppingCartOutlinedIcon style={{ fontSize: "2.1rem", color: "#fff" }} />
            </Badge>
          </div>
          <div onClick={handleOpenUserMenu} className={styles.profile}>
            <Image
              src={DEFAULT_CUSTOMER_AVATAR}
              alt="your profile picture"
              width={46}
              height={46}
              objectFit="contain"
            />
          </div>
        </div>
      </header>
      <Popover
        open={Boolean(anchorUserEl)}
        anchorEl={anchorUserEl}
        onClose={handleCloseUserMenu}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}>
        <div className={styles.userActionPopover}>
          <button className={styles.logout} onClick={() => handleLogout()}>
            Đăng xuất
          </button>
        </div>
      </Popover>
    </React.Fragment>
  );
};

export default AppHeader;
