import React from "react";
import clsx from "clsx";
import { REGEX } from "@Constants";
import { Remove as RemoveIcon, Add as AddIcon } from "@material-ui/icons";

import { P_Props } from "./QuantityBtnGroup.type";

const QuantityBtnGroupPresenter: React.FC<P_Props> = (props) => {
  const classes = clsx({
    "quantity-btn-group": true,
    [props?.className || ""]: Boolean(props?.className),
  });

  return (
    <div className={classes}>
      <button
        disabled={props.disableMinusBtn}
        className={`quantity-btn-group__btn minus ${props.disableMinusBtn ? "disabled" : ""}`}
        onClick={(e) => {
          e.preventDefault();
          e.stopPropagation();
          props.onChangeQuantity(props.quantity - 1);
        }}>
        <RemoveIcon
          className="icon"
          style={{ fontSize: "2.2rem", color: props.iconColor ?? "unset" }}
        />
      </button>
      <input
        className="quantity-btn-group__value"
        value={props.quantity}
        onChange={(e) => {
          const { value } = e.target;
          const reg = REGEX.NUMBER_ONLY;
          if ((!Number.isNaN(value) && reg.test(value)) || value === "" || value === "-") {
            props.onChangeQuantity(Number(value));
          }
        }}
      />
      <button
        disabled={props.disablePlusBtn}
        className={`quantity-btn-group__btn plus ${props.disablePlusBtn ? "disabled" : ""}`}
        onClick={(e) => {
          e.preventDefault();
          e.stopPropagation();
          props.onChangeQuantity(props.quantity + 1);
        }}>
        <AddIcon
          className="icon"
          style={{ fontSize: "2.2rem", color: props.iconColor ?? "unset" }}
        />
      </button>
    </div>
  );
};

export default QuantityBtnGroupPresenter;
