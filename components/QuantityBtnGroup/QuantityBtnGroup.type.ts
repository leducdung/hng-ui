export type P_Props = {
  className?: string;
  disableMinusBtn?: boolean;
  disablePlusBtn?: boolean;
  quantity: number;
  iconColor?: string;
  onChangeQuantity: (value: number) => void;
};
