import clsx from "clsx";
import React from "react";

import { P_Props } from "./HtmlPreviewer.type";

const HtmlPreviewerPresenter: React.FC<P_Props> = (props) => {
  const classes = clsx({
    "html-previewer": true,
    [props.className || ""]: Boolean(props.className),
  });
  if (!props.data) return <div></div>;
  return (
    <div className={classes}>
      <div className="ql-snow">
        <div className="ql-editor" dangerouslySetInnerHTML={{ __html: props.data }} />
      </div>
    </div>
  );
};

export default HtmlPreviewerPresenter;
