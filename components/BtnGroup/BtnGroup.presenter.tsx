/* eslint-disable */
import React, { useRef } from "react";
import clsx from "clsx";
import { useDragHorizontal } from "../../hooks";
import { loadCallback, MouseUtil } from "@Utils";
import { P_Props } from "./BtnGroup.type";

function BtnGroupPresenter<
  T extends {
    id: string;
    [key: string]: any;
  }
>(props: P_Props<T>) {
  const classes = clsx({ "btn-group": true, [props.className || ""]: Boolean(props.className) });
  const btnGroupRef: React.MutableRefObject<any | null> = useRef(null);
  useDragHorizontal(btnGroupRef);

  return (
    <article ref={btnGroupRef} className={classes}>
      {props.list.map((item, idx) => {
        const isActive = item.id === props.activeId;
        const itemClasses = clsx({ "btn-group__item": true, active: isActive });

        return (
          <button
            key={item.id + idx.toString()}
            {...MouseUtil.validateClick({
              onClick: () => {
                loadCallback(props.onClick, item);
              },
            })}
            {...MouseUtil.validateTouch({
              onTouchEnd: () => {
                loadCallback(props.onClick, item);
              },
            })}
            className={itemClasses}>
            {props.renderBtnLabel(item)}
          </button>
        );
      })}
    </article>
  );
}

export default BtnGroupPresenter;
