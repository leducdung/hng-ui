/* eslint-disable */
export type P_Props<
  T extends {
    id: string;
    [key: string]: any;
  }
> = {
  className?: string;
  list: T[];
  activeId?: string;
  onClick?: (item: T) => void;
  renderBtnLabel: (item: T) => any;
};
