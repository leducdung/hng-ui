import React from "react";
import { P_Props } from "./BranchMap.type";
import { GoogleMap, LoadScript, InfoWindow, Circle } from "@react-google-maps/api";
import { DEFAULT_IMG_ALT, INFO_STORES } from "@Constants";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Fade from "react-reveal/Fade";
import Slide from "react-reveal/Slide";
import Image from "next/image";
import Logo from "../../public/images/branch/branch-logo.png";

const branchMarkerStyle = {
  strokeColor: "#FFF",
  strokeWeight: 2,
  fillColor: "rgba(46, 169, 54, 0.4)",
  fillOpacity: 1,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  zIndex: 2,
};

const BranchMapPresenter: React.FC<P_Props> = () => {
  return (
    <section id="map" className="branch-map">
      <div className="branch-map__info">
        <Fade top>
          <h2 className="branch-map__info__title">HỆ THỐNG CỬA HÀNG</h2>
        </Fade>
        <Slide left ssrReveal cascade>
          <div className="branch-map__info__container">
            {INFO_STORES.map((store, idx) => {
              return (
                <article key={idx} className="branch-map__info__branch">
                  <div className="branch-map__info__branch__img">
                    <LazyLoadImage alt={DEFAULT_IMG_ALT} src={store.avatar} />
                  </div>
                  <div className="branch-map__info__branch__info">
                    <div className="branch-map__info__branch__info__name">{store.city}</div>
                    <div className="branch-map__info__branch__info__text">{store.name}</div>
                    <div className="branch-map__info__branch__info__text">{store.address}</div>
                  </div>
                </article>
              );
            })}
          </div>
        </Slide>
      </div>
      <div className="branch-map__gg-map">
        <LoadScript googleMapsApiKey="AIzaSyC3j2Sh7HPU3b6vtM2Otqt75nJ6DrROu7s">
          <GoogleMap
            id={`branch-map`}
            mapContainerClassName="branch-map"
            mapContainerStyle={{ width: "100%", height: "100%" }}
            center={INFO_STORES[0].coordinate}
            zoom={10}>
            {INFO_STORES.map((store, idx) => {
              return (
                <React.Fragment key={idx}>
                  <Circle center={store.coordinate} options={branchMarkerStyle} radius={10} />
                  {/* <Marker position={store.coordinate} options={branchMarkerStyle} /> */}
                  <InfoWindow position={store.coordinate}>
                    <div>
                      <Image src={Logo} alt={DEFAULT_IMG_ALT} height={43} width={17} />
                    </div>
                  </InfoWindow>
                </React.Fragment>
              );
            })}
          </GoogleMap>
        </LoadScript>
      </div>
    </section>
  );
};

export default BranchMapPresenter;
