import React from "react";
import clsx from "clsx";
import Image from "next/image";
import { P_Props } from "./AddressCard1.type";
import { DEFAULT_LOGO_IMG_ALT } from "@Constants";
import { Grid } from "@material-ui/core";

const AddressCard1Presenter: React.FC<P_Props> = (props) => {
  const classes = clsx({
    "address-card-1": true,
    selected: props.selected,
    [props.className || ""]: Boolean(props.className),
  });

  return (
    <div className={classes} style={props.style}>
      <Grid container spacing={2}>
        <Grid item xs={3}>
          <div className="address-card-1__avatar">
            <Image width="100%" height="100%" alt={DEFAULT_LOGO_IMG_ALT} src={props.data.image!} />
          </div>
        </Grid>
        <Grid item xs={9}>
          <div className="address-card-1__info">
            <div className="address-card-1__info__name">{props.data.name}</div>
            <div className="address-card-1__info__address">Địa chỉ: {props.data.address}</div>
            <div className="address-card-1__info__phone">
              Số điện thoại: {props.data.phoneNumber}
            </div>
          </div>
        </Grid>
      </Grid>

      {/* {!props.chooseToOrder && (
        <div className="address-card-1__default">{props.data.shippingDefault && "Mặc định"}</div>
      )} */}

      {/* <div className="address-card-1__actions">
        {!props.notAllowDelete && props.onClickDelete && (
          <>
            {!props.data.shippingDefault && (
              <div
                className="address-card-1__actions__delete"
                onClick={() => {
                  loadCallback(props.onClickDelete, props.data);
                }}>
                Xóa
              </div>
            )}
          </>
        )}
        <div
          className="address-card-1__actions__update"
          onClick={() => {
            props.onClickUpdate(props.data);
          }}>
          Chỉnh sửa
        </div>

        {props.chooseToOrder && (
          <div
            className="address-card-1__actions__choose"
            onClick={() => {
              loadCallback(props.chooseToOrder);
            }}>
            Giao tới địa chỉ này
          </div>
        )}
      </div> */}
    </div>
  );
};

export default AddressCard1Presenter;
