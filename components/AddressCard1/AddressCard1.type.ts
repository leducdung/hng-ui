import React from "react";
import { ShippingAddressOutputModel } from "@Models";

export type P_Props = {
  data: ShippingAddressOutputModel;
  onClickUpdate: (data: ShippingAddressOutputModel) => void;
  onClickDelete?: (data: ShippingAddressOutputModel) => void;
  notAllowDelete?: boolean;
  chooseToOrder?: () => void;
  className?: string;
  style?: React.CSSProperties;
  selected: boolean;
};
