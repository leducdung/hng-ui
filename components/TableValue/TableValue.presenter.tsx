import React from "react";
import { useQuery } from "@apollo/client";
import GoldPriceQuery from "@Lib/queries/GoldPrice.graphql";
import {
  // InputAdornment, TextField, Select, MenuItem,
  Grid,
} from "@material-ui/core";

const TableValuePresenter: React.FC = () => {
  const { data } = useQuery(GoldPriceQuery.GetGoldPrice);
  // const [currency, setCurrency] = React.useState("");

  // const handleChange = (event) => {
  //   setCurrency(event.target.value);
  // };

  return (
    <section className="table-value">
      <div className="table-value__group-input-curren">
        <h2 className="table-value__title">QUY ĐỔI giá trị vàng hôm nay</h2>
        {/* <div className="table-value__textfield">
          <TextField
            variant="outlined"
            placeholder="0"
            fullWidth
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <Select value={currency} onChange={handleChange} variant="standard" displayEmpty>
                    <MenuItem value={""}>VND</MenuItem>
                    <MenuItem value={"zem"}>zem</MenuItem>
                  </Select>
                </InputAdornment>
              ),
            }}
          />
          <div className="equal">=</div>
          <TextField
            variant="outlined"
            placeholder="0"
            fullWidth
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <Select value={currency} onChange={handleChange} variant="standard" displayEmpty>
                    <MenuItem value={""}>VND</MenuItem>
                    <MenuItem value={"zem"}>zem</MenuItem>
                  </Select>
                </InputAdornment>
              ),
            }}
          />
        </div> */}
      </div>
      {!!data && (
        <Grid container direction="row" alignItems="center" className="table-value__table">
          {data.prices.map((v) => {
            return (
              <Grid key={v.name} item lg={3} xs={6} className="table-value__table__item">
                <div className="table-value__table__name">{v.company}</div>
                <div className="table-value__table__group">
                  <div className="table-value__table__group__text">
                    <div className="table-value__table__text">Bán</div>
                    <div className="table-value__table__text">Mua</div>
                  </div>
                  <div className="table-value__table__group__price">
                    <div className="table-value__table__number">{v.buy}</div>
                    <div className="table-value__table__number number-color">{v.sell}</div>
                  </div>
                </div>
              </Grid>
            );
          })}
        </Grid>
      )}
    </section>
  );
};

export default TableValuePresenter;
