import React from "react";
import { DEFAULT_IMG_ALT } from "@Constants";
import Image from "next/image";
import Img1 from "../../public/images/mock/new/1.png";
import Img2 from "../../public/images/mock/new/4.png";
import Img3 from "../../public/images/mock/new/2.png";
import Img4 from "../../public/images/mock/new/5.png";
import Img5 from "../../public/images/mock/new/3.png";

const SPECIAL_OFFER = [
  {
    img: Img1,
    title: "THƯƠNG HIỆU",
    description:
      "Toàn quyền sử dụng thương hiệu HanaGold trong hoạt động kinh doanh và các kênh truyền thông chính thống.",
  },
  {
    img: Img2,
    title: "BẢO MẬT & an toàn",
    description:
      "Hệ thống bảo mật phân tầng công nghệ cao, đảm bảo an toàn về tài chính với sự tham gia từ nhiều công ty tài chính và quỹ đầu tư lớn.",
  },
  {
    img: Img3,
    title: "HỆ THỐNG VẬN HÀNH",
    description:
      "Đảm bảo hệ thống hóa quy trình vận hành, cũng như quá trình kết nối được bảo hộ, chứng nhận.",
  },
  {
    img: Img4,
    title: "KINH NGHIỆM",
    description:
      "Kế thừa phương thức vận hành và kinh doanh thành công từ HanaGold, cùng với đội ngũ nhân sự chuyên nghiệp sẵn sàng hỗ trợ và đào tạo.",
  },
  {
    img: Img5,
    title: "GIẢM CHI PHÍ",
    description:
      "Lựa chọn tối ưu cho bài toán khởi nghiệp: tối giảm chi phí - rủi ro tài chính thấp - tỉ lệ thành công cao hơn.",
  },
];

const SpecialOffer: React.FC = () => {
  return (
    <div className="special-offer">
      <div className="special-offer__title">ĐẶC QUYỀN ƯU ĐÃI DÀNH CHO ĐỐI TÁC</div>
      <div className="special-offer__body__top">
        {SPECIAL_OFFER.filter((_, index) => index % 2 === 0).map((p) => {
          return (
            <div key={p.title} className="special-offer__item">
              <div className="special-offer__item__img">
                <Image src={p.img} alt={DEFAULT_IMG_ALT} objectFit="contain" />
              </div>
              <div className="special-offer__item__content">
                <div className="special-offer__item__title">{p.title}</div>
                <div className="special-offer__item__description">{p.description}</div>
              </div>
            </div>
          );
        })}
      </div>
      <div className="special-offer__body__bottom">
        {SPECIAL_OFFER.filter((_, index) => index % 2 !== 0).map((p) => {
          return (
            <div key={p.title} className="special-offer__item">
              <div className="special-offer__item__img">
                <Image src={p.img} alt={DEFAULT_IMG_ALT} objectFit="contain" />
              </div>
              <div className="special-offer__item__content">
                <div className="special-offer__item__title">{p.title}</div>
                <div className="special-offer__item__description">{p.description}</div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default SpecialOffer;
