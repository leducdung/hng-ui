import Link from "next/link";
import React from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Zoom from "react-reveal/Zoom";
import { BtnGroup } from "..";
import { P_Props } from "./HomeProduct.type";
import { useBreakpoints } from "../../hooks";
import { APP_ROUTES } from "@Constants";

const IMG_PATH = `/images/mock/products/`;

const PRODUCT_4_DATA = [
  {
    id: "1",
    img: `${IMG_PATH}img1.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    description: "2.290.000đ",
    discount: "20",
  },
  {
    id: "2",
    img: `${IMG_PATH}img2.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    description: "2.290.000đ",
    discount: "20",
  },
  {
    id: "3",
    img: `${IMG_PATH}img3.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    description: "2.290.000đ",
    discount: "",
  },
  {
    id: "4",
    img: `${IMG_PATH}img1.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    description: "2.290.000đ",
    discount: "",
  },
  {
    id: "5",
    img: `${IMG_PATH}img2.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    description: "2.290.000đ",
    discount: "20",
  },
  {
    id: "6",
    img: `${IMG_PATH}img3.png`,
    title: "Nhẫn cưới vàng 18K Sánh Duyên 0007845",
    description: "2.290.000đ",
    discount: "",
  },
];

export type ProductItem = {
  id: string;
  img: string;
  title: string;
  description: string;
  discount: string;
};

export type ProductCategoryType = {
  img_def: string;
  img_act: string;
  title: string;
};

const HomeProductPresenter: React.FC<P_Props> = () => {
  const breakpoints = useBreakpoints();

  return (
    <section id="product" className="home-page__product">
      <article>
        <Zoom ssrReveal down>
          <div className="home-page__product__header">
            <div className="home-page__product__title">SẢN PHẨM NỔI BẬT</div>
            {breakpoints.lg && (
              <Link passHref href={APP_ROUTES.DANCING_STONE}>
                <a className="home-page__news__list-link">Xem tất cả sản phẩm</a>
              </Link>
            )}
          </div>
        </Zoom>
        {/* <Zoom up ssrReveal>
          <div className="home-page__product__categories">
            {PRODUCT_CATEGORIES_4_DATA.map((item, idx) => {
              const isActive = _isEqual(activeCategory, item);
              return (
                <div
                  className={
                    `home-page__product__categories__item` +
                    (isActive ? " home-page__product__categories__item--active" : "")
                  }
                  key={idx}
                  onClick={() => setActiveCategory(item)}>
                  <LazyLoadImage src={isActive ? item.img_act : item.img_def} alt={item.title} />
                  <div className="home-page__product__categories__item__title">{item.title}</div>
                </div>
              );
            })}
          </div>
        </Zoom> */}
      </article>
      <article className="home-page__product__body">
        <BtnGroup<ProductItem>
          list={PRODUCT_4_DATA}
          renderBtnLabel={(item) => {
            return (
              <div className="home-page__product__list__item">
                <a href="#product" className="home-page__product__list__item-info">
                  <div className="home-page__product__list__item__img">
                    <LazyLoadImage src={item.img} alt={item.title} />
                  </div>
                  <div className="home-page__product__list__item__title">{item.title}</div>
                  <div className="home-page__product__list__item__description">
                    {item.description}
                  </div>
                </a>
                {item.discount && (
                  <div className="home-page__product__list__item-discount">
                    <div className="home-page__product__list__item-discount__percent">
                      <span>{item.discount}</span>
                      <span>%</span>
                    </div>
                    <div className="home-page__product__list__item-discount__off">OFF</div>
                  </div>
                )}
              </div>
            );
          }}
        />
        {!breakpoints.lg && (
          <div className="home-page__product__btn">
            <Link passHref href={APP_ROUTES.DANCING_STONE}>
              <a className="btn">Xem tất cả sản phẩm</a>
            </Link>
          </div>
        )}
      </article>
    </section>
  );
};

export default HomeProductPresenter;
