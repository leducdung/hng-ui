import React from "react";
import { DEFAULT_IMG_ALT } from "@Constants";
import Fade from "react-reveal/Fade";
import Zoom from "react-reveal/Zoom";
import Flip from "react-reveal/Flip";
import { Grid } from "@material-ui/core";
import Image from "next/image";
import Banner from "../../public/images/aboutus/bg.png";
import IconGGP from "../../public/images/aboutus/iconggplay.png";
import IconAS from "../../public/images/aboutus/iconapple.png";
import Iphone from "../../public/images/aboutus/img2.png";
import Link from "next/link";
import { useBreakpoints } from "hooks";

const IntroDownAppPresenter: React.FC = () => {
  const breakpoints = useBreakpoints();
  return (
    <section className="intro-down-app">
      <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="fill" objectFit="cover" />
      <Grid container className="intro-down-app__content">
        <Grid item xs={12} lg={6} className="intro-down-app__left">
          <Fade duration={1000} top>
            <h2 className="intro-down-app__title">
              tất cả trong một
              <br />
              với app hanagold
            </h2>
          </Fade>
          <Zoom left>
            <div className="intro-down-app__description">
              Ứng dụng nổi bật giúp quản lý và kinh doanh hiệu quả cho tiệm vàng. Mang đến đầy đủ
              các tính năng tối ưu cho người dùng như mua bán, lưu trữ, chuyển đổi, gửi tặng, tích
              lũy,...
            </div>
          </Zoom>
          <Flip top>
            <div className="intro-down-app__btn-group">
              <Link href={"https://play.google.com/store/apps/details?id=com.unibiz.hanagold"}>
                <a className="intro-down-app__btn">
                  <Image src={IconGGP} alt={DEFAULT_IMG_ALT} objectFit="cover" />
                  {/* <div> */}
                  {/* <div className="intro-down-app__btn__get">GET IT ON</div> */}
                  <div className="intro-down-app__btn__name">
                    {breakpoints.md && "Get it on"} Google Play
                  </div>
                  {/* </div> */}
                </a>
              </Link>
              <Link href={"https://testflight.apple.com/join/RoqAkqPl"}>
                <a className="intro-down-app__btn btn-right">
                  <Image src={IconAS} alt={DEFAULT_IMG_ALT} objectFit="cover" />
                  {/* <div>
                    <div className="intro-down-app__btn__get">Download on the</div> */}
                  <div className="intro-down-app__btn__name">
                    {breakpoints.md && "Download on the"} App Store
                  </div>
                  {/* </div> */}
                </a>
              </Link>
            </div>
          </Flip>
        </Grid>

        <Grid item xs={12} lg={6} className="intro-down-app__right">
          <Fade ssrReveal bottom>
            <div className="intro-down-app__right__img">
              <Image src={Iphone} alt={DEFAULT_IMG_ALT} objectFit="cover" />
            </div>
          </Fade>
        </Grid>
      </Grid>
    </section>
  );
};

export default IntroDownAppPresenter;
