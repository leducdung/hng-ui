import React from "react";
import { DEFAULT_IMG_ALT, APP_ROUTES } from "@Constants";
import { ArrowBack as ArrowBackIcon } from "@material-ui/icons";
import Link from "next/link";
import { P_Props } from "./DownloadApp.type";
import { loadCallback } from "@Utils";
import Image from "next/image";
import Banner from "../../public/images/aboutus/bg-da.png";
import Img from "../../public/images/aboutus/img-da.png";
import IconGGP from "../../public/images/aboutus/iconggplay.png";
import IconAS from "../../public/images/aboutus/iconapple.png";

const DownLoadAppPresenter: React.FC<P_Props> = (props) => {
  return (
    <div className="download-app">
      <Image src={Banner} alt={DEFAULT_IMG_ALT} layout="fill" objectFit="cover" />
      <div className="body">
        {!props.modifyBackButton ? (
          <Link href={APP_ROUTES.HOME}>
            <a
              className="icon-back"
              onClick={() => {
                loadCallback(props.onClose);
                loadCallback(props.onBackToHome);
              }}>
              <ArrowBackIcon style={{ fontSize: "2.2rem", marginRight: "1.2rem" }} />
              Trang chủ
            </a>
          </Link>
        ) : (
          <button
            onClick={() => {
              loadCallback(props.modifyBackButton?.onClick);
            }}>
            {props.modifyBackButton.text}
          </button>
        )}

        <div className="image">
          <Image src={Img} alt={DEFAULT_IMG_ALT} objectFit="contain" />
        </div>
        <div className="title">
          tải ứng dụng hanagold và
          <br />
          Tiến hành đặt hàng
        </div>
        <div className="text">
          Tiếp tục mua sắm với với HanaGold.
          <br />
          Ứng dụng sẵn sàng trên hệ điều
          <br />
          hành Android và iOS
        </div>
        <div className="btn-group">
          <Link href={"https://play.google.com/store/apps/details?id=com.unibiz.hanagold"}>
            <a className="btn">
              <Image src={IconGGP} alt={DEFAULT_IMG_ALT} objectFit="cover" />
              <div>
                <div className="btn__get">GET IT ON</div>
                <div className="btn__name">Google Play</div>
              </div>
            </a>
          </Link>
          <Link href={"https://testflight.apple.com/join/RoqAkqPl"}>
            <a className="btn">
              <Image src={IconAS} alt={DEFAULT_IMG_ALT} objectFit="cover" />
              <div>
                <div className="btn__get">Download on the</div>
                <div className="btn__name">App Store</div>
              </div>
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default DownLoadAppPresenter;
