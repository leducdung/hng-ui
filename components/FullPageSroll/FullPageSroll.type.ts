/* eslint-disable */
export type FullPageApiRef = {
  currentPage: number;
  handelMoveToNextPage: () => void;
  handelMoveToPrevPage: () => void;
  handleMoveToPage: (page: number) => void;
};

export type FullPageApi = {
  isActive: boolean;
  currentPage: number;
  handelMoveToNextPage: () => void;
  handelMoveToPrevPage: () => void;
  handleMoveToPage: (page: number) => void;
};

export type P_Props = {
  containerHeight?: number | string;
  renderAllPagesOnFirstRender?: boolean;
  pages: {
    render: (api: FullPageApi) => any;
  }[];
  /**
   * nếu page đó được active sẽ truyền class active vào chung với itemClass
   */
  navigation?: {
    className: string;
    itemClassName: string;
  };
  renderBackground?: (api: FullPageApiRef) => any;
};

export type P_FullPage_Props = {
  id?: string;
  className?: string;
};
