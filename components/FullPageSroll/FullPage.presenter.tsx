import React from "react";
import clsx from "clsx";
import { P_FullPage_Props } from "./FullPageSroll.type";

const FullPageScroll: React.FC<P_FullPage_Props> = (props) => {
  const classes = clsx({ section: true, [props.className || ""]: Boolean(props.className) });

  return (
    <div id={props.id} className={classes}>
      {props.children}
    </div>
  );
};

export default FullPageScroll;
