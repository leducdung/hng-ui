import React, { useImperativeHandle, useState } from "react";
import clsx from "clsx";
import ReactPageScroller from "react-page-scroller";
import { FullPageApiRef, P_Props } from "./FullPageSroll.type";

const FullPageScroll: React.ForwardRefRenderFunction<FullPageApiRef, P_Props> = (props, ref) => {
  const [currentPage, setCurrentPage] = useState<number>(0);

  // use as ref
  // const fullPageScrollRef = useRef<React.ElementRef<typeof FullPageSroll>>();

  const handleMoveToPage = (page: number) => {
    if (0 <= page && page < props.pages.length) {
      setCurrentPage(page);
    }
  };

  const handelMoveToNextPage = () => {
    const nextPage = currentPage + 1;
    if (nextPage < props.pages.length) {
      setCurrentPage(nextPage);
    }
  };

  const handelMoveToPrevPage = () => {
    const prevPage = currentPage - 1;
    if (prevPage >= 0) {
      setCurrentPage(prevPage);
    }
  };

  useImperativeHandle(ref, () => ({
    currentPage,
    handelMoveToNextPage,
    handelMoveToPrevPage,
    handleMoveToPage,
  }));

  return (
    <React.Fragment>
      {props.renderBackground &&
        props.renderBackground({
          currentPage,
          handelMoveToNextPage,
          handelMoveToPrevPage,
          handleMoveToPage,
        })}

      <ReactPageScroller
        pageOnChange={handleMoveToPage}
        customPageNumber={currentPage}
        renderAllPagesOnFirstRender={props.renderAllPagesOnFirstRender}
        containerHeight={props.containerHeight || undefined}>
        {props.pages.map((page, idx) => {
          const isActive = idx === currentPage;
          return (
            <React.Fragment key={"full-page" + idx}>
              {page.render({
                handelMoveToNextPage,
                handelMoveToPrevPage,
                handleMoveToPage,
                currentPage,
                isActive,
              })}
            </React.Fragment>
          );
        })}
      </ReactPageScroller>
      {props.navigation && (
        <div className={props.navigation.className}>
          {props.pages.map((nav, idx) => {
            const isActive = idx === currentPage;
            const classes = clsx({
              [props.navigation?.itemClassName || ""]: true,
              active: isActive,
            });
            return (
              <div
                key={"full-page-navigation" + idx}
                className={classes}
                onClick={() => {
                  handleMoveToPage(idx);
                }}></div>
            );
          })}
        </div>
      )}
    </React.Fragment>
  );
};

export default React.forwardRef(FullPageScroll);
