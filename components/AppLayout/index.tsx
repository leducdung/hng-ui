import React from "react";
import AppSidebar from "@Components/AppSidebar";
import AppHeader from "@Components/AppHeader";
import { useQuery } from "@apollo/client";
import CustomerQuery from "../../lib/queries/Customer.graphql";
import styles from "./index.module.scss";

type Props = {
  hasBackground?: boolean;
  title?: string;
};

// @TODO handle validate loading, error
const AppLayout: React.FC<Props> = ({ children, hasBackground = false, title }) => {
  const { data } = useQuery(CustomerQuery.GetData);
  return (
    <section className={styles.appLayout}>
      <AppSidebar />
      <div className={`${styles.mainLayout} ${hasBackground ? styles.hasBackground : ""}`}>
        <AppHeader userName={data ? data.user.fullName : "--/--"} title={title} />
        <div className={styles.content}>{children}</div>
      </div>
    </section>
  );
};

export default AppLayout;
